# INF112 Project – *Buncharted*

* Team: *Microissant* (Gruppe 1): *Thomas Alme Matre, Ståle Karlstad, Martin Gabriel Westerheim Gundersen, Shahnaz Shaheen al Mhethawy, Ole Fossbakk Birketvedt*
* [Development board](https://git.app.uib.no/microissant/project-gluten/-/boards/3866)

## Om spillet

<div style="text-align: center;">
    <img src="assets/buncharted_screenshot_01.png" alt="Screenshot of the game. Player is shooting at enemies." width="400">
</div>


Kokkene har blitt late og gidder ikke lage mat mer. Menneskeheten sender nå roboter inn i matdimensjonene for å sanke det vi trenger. Vi følger en robots reise inn i bakedimensjonen hvor motstanden har blitt stor! Vår cyberhelt må bekjempe deigete monstre og andre glutenbaserte fiender for å returnere til sine humanoide mestre.

For å fullføre et nivå, må du ta knerten sjefen over fiendene, Mr. Ice Scream, som vokter utgangsportalen. På veien kan du finne kister med kjøpbare power-ups som kan gi deg fordeler i kampen.

Aldersgrense: 12 år (inneholder skytevåpen og skremmende fiender)

## Kontroller
* Bevegelse: W, A, S, D
* Skyting: Venstre museklikk eller piltastene
* Dash: Mellomrom
* Interaksjon: E
* Pause: Esc
* Bytte våpen: 1 / 2

## Kjøring
* Kompileres med `mvn package`.
* Kjøres med `java -jar target/buncharted-1.0-SNAPSHOT-fat.jar`
* Krever Java 17 eller senere

## Kjente feil
Se [bug issue board](https://git.app.uib.no/microissant/project-gluten/-/boards/4155)

## Credits
* Grafikk - Martin Gabriel Westerheim Gundersen

* Musikk - Ståle Karlstad:
    *   Copyright 2023 Ståle Karlstad

        All rights reserved.

        This music is proprietary and may not be used, copied, or distributed without the express written permission of Ståle Karlstad.


* Cherry bomb yell samples: 
    *   title: Kung Fu Yell
        * creator: oldedgar
        * source: https://freesound.org/people/oldedgar/sounds/97977/
        * license: https://creativecommons.org/publicdomain/zero/1.0/
        * modified
    *   title: Zombie monster scream
        * creator: Awaludian 
        * source:  https://freesound.org/people/redafs/sounds/348310/ 
        * license: https://creativecommons.org/licenses/by/4.0/
        * modified
* explosion:
    * title: explosion2.wav
        * creator: steveygos93
        * source: https://freesound.org/people/steveygos93/sounds/80401/
        * license: https://creativecommons.org/licenses/by/3.0/
        * modified
* Footsteps:
    * title: Footsteps Leather, Cloth, Armor
        * creator: HaelDB
        * source: https://opengameart.org/content/footsteps-leather-cloth-armor
        * license: OGA-BY 3.0 & CC0 1.0
* death sound:
    * title clap.wav
        * creator: xUMR
        * source: https://freesound.org/people/xUMR/sounds/478657/
        * license: https://creativecommons.org/publicdomain/zero/1.0/
        * modified
* exhale:
    * title Exhale.aif
        * creator: carroll27
        * source: https://freesound.org/people/carroll27/sounds/151920/
        * license: https://creativecommons.org/publicdomain/zero/1.0/
        * modified

* French laugh:
    * title FrenchLaugh.mp3
        * creator: Ståle Karlstad
        * license: https://creativecommons.org/publicdomain/zero/1.0/

