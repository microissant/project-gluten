#ifdef GL_ES
precision mediump float;
#endif

varying vec2 v_texCoords;
uniform sampler2D u_texture;
uniform float u_flashIntensity;

void main() {
    vec4 texColor = texture2D(u_texture, v_texCoords);
    vec3 flashColor = vec3(u_flashIntensity, u_flashIntensity, u_flashIntensity);
    gl_FragColor = vec4(texColor.rgb + flashColor, texColor.a);
}