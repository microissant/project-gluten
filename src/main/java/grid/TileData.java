package grid;

/**
 * The information stored in each cell of the game grid
 */
public record TileData(grid.TileData.TileType tileType) {

    public enum TileProperty {
        EMPTY(1),
        WALL(2),
        HOLE(4),
        DESTRUCTIBLE(8),
        CHEST(16), //property for being interactible
        CRATE(32);
        //BUTTON(16);
        public final int val;

        TileProperty(int val) {
            this.val = val;
        }
    }

    public enum TileType {
        EMPTY(TileProperty.EMPTY.val),
        DESTRUCTIBLEWALL(TileProperty.WALL.val | TileProperty.DESTRUCTIBLE.val),
        INDESTRUCTIBLEWALL(TileProperty.WALL.val),
        CHEST(TileProperty.WALL.val | TileProperty.CHEST.val),
        CRATE(TileProperty.CRATE.val | TileProperty.DESTRUCTIBLE.val),
        HOLE(TileProperty.HOLE.val);

        public final int prop;

        TileType(int prop) {
            this.prop = prop;
        }
    }

    public enum TileRuleset {
        FLYING(TileProperty.WALL.val | TileProperty.CRATE.val),
        WALKING(TileProperty.WALL.val | TileProperty.HOLE.val | TileProperty.CRATE.val),
        SLIDING(TileProperty.WALL.val | TileProperty.CRATE.val),
        BULLET(TileProperty.WALL.val);

        public final int val;

        TileRuleset(int val) {
            this.val = val;
        }
    }

    /**
     * If we decide that some tiles can be destructible
     */
    public TileData {
    }

}
