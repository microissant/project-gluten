package grid;

public record CoordinateItem<E>(GridCoordinate coordinate, E item) {
}