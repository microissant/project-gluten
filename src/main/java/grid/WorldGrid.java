package grid;

import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;
import entities.enemy.IEnemy;
import grid.TileData.TileProperty;
import grid.TileData.TileRuleset;
import grid.TileData.TileType;
import grid.autotiling.AutoTiler;
import media.TileSetManager.TileSetID;
import model.event.Subscribe;
import model.event.events.AllMainObjectivesCompleteEvent;
import model.event.events.collision.CloseDoorsEvent;
import model.event.events.entities.CreateCrateEvent;
import model.event.events.entities.DestroyCrateEvent;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**The specific grid that keeps track of tile information in the game world. 
*/
public class WorldGrid implements IGrid<TileData>{
    public final int cellSize;
    private TiledMap tileMap;
    private TileSetID tileSet;
    private final IGrid<TileData> grid;
    private boolean TILEMAPCREATED =false;
    private AutoTiler autoTiler = null;
    private GridCoordinate[][] preAllocatedGridCells;
    private final HashMap<Integer,List<GridCoordinate>> explosionRadius = new HashMap<>();
    private GridCoordinate doorPosition = null;
    private final HashMap<GridCoordinate, IEnemy> crates;
    public WorldGrid(int rows, int columns, int cellSize, TileData initElement) {
        grid = new Grid<>(rows,columns,initElement);  
        this.cellSize = cellSize;
        crates = new HashMap<>();
        
    }

    /**creates a list of coordinate offsets that will be used for explosions*/
    private List<GridCoordinate> createExplosionRadius(int radius){
        List<GridCoordinate> resultList  = new LinkedList<>();
        for(int row =  -radius; row<= radius; row++){
            for(int col =-radius; col<=radius; col++){
                if(row * row + col * col <= radius*radius) {
                    resultList.add(new GridCoordinate(row,col));
                }
            }
        }
        return resultList;
    }

    /**blows up the tiles close to the position argument that are within the radius */
    public void explodeTiles(Vector2 position, int radius){
        GridCoordinate coordPosition = getGridCoordinate(position);
        List<GridCoordinate> explosion = explosionRadius.get(radius);
        if(explosion == null){
            explosion = createExplosionRadius(radius);
            explosionRadius.put(radius,explosion);
        }
        for(GridCoordinate coord : explosion){
            GridCoordinate tobeExploded = new GridCoordinate(coordPosition.row+coord.row,coordPosition.col+coord.col);
            explodeCell(tobeExploded);
        }
    }

    /**checks if the current cell is destructible and if it is, sets it to empty */
    private void explodeCell(GridCoordinate cell){
        if(!coordinateIsOnGrid(cell)) return;
        if(checkForTileProperty(cell.row,cell.col, TileProperty.DESTRUCTIBLE.val)){
            IEnemy crate = crates.get(cell);
            if(crate!=null){
                crate.destroy();
            }
            set(cell,new TileData(TileType.EMPTY));
            if(TILEMAPCREATED) updateSurroundingTextures(cell);
        }

    }

    /**gets the tiledata at the given coordinate
     * 
     * @param row the row of the grid location
     * @param col the column of the grid location
     * @return the tiledata at the given location
     */
    public TileData get(int row, int col){
        if(!coordinateIsOnGrid(row, col))return null;
        return grid.get(row,col);

    }
    /**gets the tiledata at the given location
     * 
     * @param coord the coordinate to get the value from
     * @return the tiledata at the given coordinate
     */
    public TileData get(GridCoordinate coord){
        return get(coord.row,coord.col);
    }

    @Override
    public void set(GridCoordinate coord,TileData value){
        grid.set(coord,value);
        if(TILEMAPCREATED)updateSurroundingTextures(coord);

    }

    public void set(int row, int col, TileData value){
        GridCoordinate coord = new GridCoordinate(row, col);
        set(coord, value);
    }

    /**
     * @return number of columns in the world grid
     */
    public int numCols(){
        return grid.numCols();
    }

    /**
     * Initializes the tilemap for the world grid.
     *
     * @param tileSet the TileSetID of the tileset to be used in the grid
     */
    public void initTileMap(TileSetID tileSet){
        autoTiler = new AutoTiler(this);

        if(TILEMAPCREATED) throw new IllegalCallerException("tilemap cannot be initialized twice");
        TILEMAPCREATED = true;
        tileMap= new TiledMap();
        this.tileSet = tileSet;
        MapLayers layers= tileMap.getLayers();
        
        TiledMapTileLayer backgroundLayer = new TiledMapTileLayer(numCols() * cellSize, numRows() * cellSize, cellSize, cellSize);
        //the layer displaying the front of the walls
        TiledMapTileLayer wallLayer = new TiledMapTileLayer(numCols() * cellSize, numRows() * cellSize, cellSize, cellSize);
        //the layer displaying the top of the walls. This has to be separate in order to let things go behind walls.
        TiledMapTileLayer wallTopLayer = new TiledMapTileLayer(numCols() * cellSize, numRows() * cellSize, cellSize, cellSize);
        
        layers.add(backgroundLayer);
        layers.add(wallLayer);
        layers.add(wallTopLayer);

        updateAllTextures();
    }

    private void preAllocateCells() {
        preAllocatedGridCells = new GridCoordinate[numRows()][numCols()];
        for (int row = 0; row < numRows(); row++) {
            for (int col = 0; col < numCols(); col++) {
                preAllocatedGridCells[row][col] = new GridCoordinate(row, col);
            }
        }
    }

    /**
     * Returns the pre-allocated grid cells.
     *
     * @return a 2D array of GridCoordinate objects representing pre-allocated grid cells
     */
    public GridCoordinate[][] getPreAllocatedGridCells() {
        return preAllocatedGridCells;
    }

    /** checks the surrounding tiles and sets the tile texture to the correct texture from the tilemap
     * call updateSurroundingTexture instead of this unless you specifically only want to update this cell's texture
     */
    public void updateTileTexture(GridCoordinate coordinate){
        //don't draw null tiles
        if(get(coordinate) == null || get(coordinate).tileType() == null) return;
        TileType tileType = get(coordinate).tileType();
        //check surrounding tiles and set the bitmask value to one if there is an idential tile there
        int bitmask = 0;
        int i = -1;
        for(int r = coordinate.row+1; r>=coordinate.row-1; r-- ){
            for(int c = coordinate.col-1; c<=coordinate.col+1; c++ ){
                if(r==coordinate.row && c == coordinate.col) continue;

                i++;
                if(!coordinateIsOnGrid(r,c) || get(r,c)==null) continue;
                if(get(r,c).tileType() == tileType) bitmask|= 1<<i;
            }

        }
        
        autoTiler.setAutoTile(bitmask, tileSet, tileType, tileMap, coordinate);
    }

    /**updates the textures of the 8 adjacent tiles */
    void updateSurroundingTextures(GridCoordinate coordinate){
        for(int r =coordinate.row-1; r <= coordinate.row+1; r++){
            for(int c =coordinate.col-1; c <= coordinate.col+1; c++){
                GridCoordinate coord = new GridCoordinate(r,c);
                
                if(coordinateIsOnGrid(coord))updateTileTexture(coord);
            }
        }
    }
    /**updates all of the tile textures of the grid
     * call updateSurroundingTextures if you've only changed a single tile since last change to the grid.
     */
    public void updateAllTextures(){
        for(CoordinateItem<TileData> cItem : this){
            updateTileTexture(cItem.coordinate());
        }

    }

    /** gets the tiledata object at the given pixel coordinate
     * 
     * @param x the x pixel coordinate
     * @param y the y pixel coordinate
     * @return the tiledata object at the given pixel position
     */
    public TileData getAtPixel(float x, float y){
        return get((int) (y / cellSize), (int) (x / cellSize));
    }

    /** Checks if the tile property at the given coordinate is one of the options given by the rules argument*/
    public boolean checkForTileProperty(int row, int col, int rules){
        TileData tileData = get(row,col);
        if(tileData == null) return false;
        return (tileData.tileType().prop & (rules)) !=0;
    }
    /** Checks if the tile property at the given coordinate is one of the options given by the rules argument*/
    public boolean checkForTile(int row, int col, TileRuleset rules){
        return checkForTileProperty(row,col,rules.val);
    }
    /** Checks if the tile property at the given pixel position is one of the options given by the rules argument*/
    public boolean checkForTilePropertyAtPixel(float x, float y, int rules){
        return (getAtPixel(x,y).tileType().prop & (rules)) !=0;
    }
    /** Checks if the tile property at the given pixel position is one of the options given by the rules argument*/
    public boolean checkForTilePropertyAtPixel(float x, float y, TileRuleset rules){
        return checkForTilePropertyAtPixel(x,y,rules.val);
    }

    /** Overwrites a section of the grid with a room layout.
     * 
     * @param room The room layout
     * @param bottomLeft the bottom left coordinate of where the room will be placed
     * @throws IllegalArgumentException if the placement of the room is outside of the bounds of the grid
     */
    public void overwrite(IGrid<TileData> room, GridCoordinate bottomLeft){
        if(room.numCols()+bottomLeft.col>numCols() || room.numRows()+bottomLeft.row>numRows() || bottomLeft.row<0 || bottomLeft.col<0){
            throw new IllegalArgumentException("room must be within the bounds of the grid");
        }
        
        for(CoordinateItem<TileData> cItem : room){
            GridCoordinate newCoord = cItem.coordinate().add(bottomLeft);
            set(newCoord,cItem.item());
        }

        preAllocateCells();
    }




    @Override
    public Iterator<CoordinateItem<TileData>> iterator() {
        return grid.iterator();
    }

    @Override
    public int numRows() {
        return grid.numRows();
    }



    @Override
    public boolean coordinateIsOnGrid(GridCoordinate coordinate) {
        return grid.coordinateIsOnGrid(coordinate);
    }
    @Override
    public boolean coordinateIsOnGrid(int row, int col) {
        return grid.coordinateIsOnGrid(row, col);
    }
    @Subscribe
    public void closeDoors(CloseDoorsEvent event){
        if(event == null) return;
        if(event.position() == null) return;
        doorPosition = getGridCoordinate(event.position().x-cellSize,event.position().y-3*cellSize);
        set(doorPosition,new TileData(TileType.INDESTRUCTIBLEWALL));
        set(doorPosition.row,doorPosition.col+1,new TileData(TileType.INDESTRUCTIBLEWALL));
    }

    @Subscribe 
    public void openDoors(AllMainObjectivesCompleteEvent event){
        if(event == null) return;
        if(doorPosition == null) return;
        set(doorPosition,new TileData(TileType.EMPTY));
        set(doorPosition.row,doorPosition.col+1,new TileData(TileType.EMPTY));
    }
    @Subscribe
    public void addCrate(CreateCrateEvent event){
        if(event == null) return;
        if(event.crate()==null) return;
        
        crates.put(getGridCoordinate(event.crate().getPosition()),event.crate());

    }

    @Subscribe
    public void removeCrate(DestroyCrateEvent event){
        if(event ==null) return;
        if(event.position()==null) return;
        GridCoordinate position = getGridCoordinate(event.position());
        crates.remove(position);
        set(position,new TileData(TileType.EMPTY));

    }

    /**
     * Returns the TiledMap object representing the grid.
     *
     * @return the TiledMap object
     */
    public TiledMap getTiledMap(){
        return tileMap;
    }

    /**
     * Converts pixel coordinates to grid coordinates.
     *
     * @param x the x-coordinate in pixels
     * @param y the y-coordinate in pixels
     * @return a GridCoordinate object representing the grid coordinates
     */
    public GridCoordinate getGridCoordinate(float x, float y){
        return new GridCoordinate((int) (y / cellSize), (int) (x / cellSize));
    }

    /**
     * Converts pixel coordinates to grid coordinates.
     *
     * @param coord a Vector2 object representing the pixel coordinates
     * @return a GridCoordinate object representing the grid coordinates
     */
    public GridCoordinate getGridCoordinate(Vector2 coord){
        int row = (int) (coord.y / cellSize);
        int col = (int) (coord.x / cellSize);

        if (!coordinateIsOnGrid(row, col)) return null;
        if (preAllocatedGridCells == null) return new GridCoordinate(row,col);
        if (preAllocatedGridCells.length <= row || preAllocatedGridCells[0].length <= col) return null;

        return preAllocatedGridCells[row][col];
    }

    /**
     * Converts grid coordinates to world position in the center of the cell.
     *
     * @param row the row index of the grid cell
     * @param col the column index of the grid cell
     * @return a Vector2 object representing the world position in the center of the cell
     */
    public Vector2 getCellCenteredWorldPosition(int row, int col) {
        return new Vector2(col * cellSize + cellSize / 2.0f, row * cellSize + cellSize / 2.0f);
    }

    /**
     * Converts grid coordinates to world position in the center of the cell.
     *
     * @param coord a GridCoordinate object representing the grid coordinates
     * @return a Vector2 object representing the world position in the center of the cell
     */
    public Vector2 getCellCenteredWorldPosition(GridCoordinate coord) {
        return getCellCenteredWorldPosition(coord.row, coord.col);
    }

}