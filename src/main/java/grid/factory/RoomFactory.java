package grid.factory;


import entities.enemy.IEnemy;
import entities.enemy.enemies.*;
import entities.inanimate.CloseDoorsTrigger;
import grid.*;
import grid.TileData.TileType;
import item.chest.Chest;
import item.chest.IChest;
import item.chest.LootTables;
import model.event.events.collision.CreateDoorEvent;
import model.event.events.entities.SpawnChestEvent;
import model.event.events.entities.SpawnEntityEvent;
import model.event.events.game.SetEndGoalPositionEvent;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;


public class RoomFactory {

    private final HashMap<String, FactoryCellData> stringDefinitions;

    /**record responsible for telling the factory what tiletype and entity to place */
    private record FactoryCellData(TileType tileType, FactoryEntityConstructor entity){}   

    public RoomFactory(){
        stringDefinitions = new HashMap<>();
        stringDefinitions.put("c",new FactoryCellData(TileType.CHEST, (bus, position) -> {
            IChest chest = new Chest(position, LootTables.standardLootTable(bus), 2, 4, bus);
            bus.post(new SpawnChestEvent(chest));
        }));
        stringDefinitions.put("r",new FactoryCellData(TileType.CRATE, (bus, position) -> {
            Crate crate = new Crate(position,bus);
            bus.post(new SpawnEntityEvent<IEnemy>(crate));
        }));
        stringDefinitions.put("w",new FactoryCellData(TileType.DESTRUCTIBLEWALL, null));
        stringDefinitions.put("I",new FactoryCellData(TileType.INDESTRUCTIBLEWALL, null));
        stringDefinitions.put(" ",new FactoryCellData(TileType.EMPTY, null));
        stringDefinitions.put("h",new FactoryCellData(TileType.HOLE, null));
        stringDefinitions.put("EvilCookie",new FactoryCellData(TileType.EMPTY, (bus, position) -> {
            EvilCookie evilCookie  = new EvilCookie(position,false,bus);
            bus.post(new SpawnEntityEvent<IEnemy>(evilCookie));
        }));
        stringDefinitions.put("CherryBomb",new FactoryCellData(TileType.EMPTY, (bus, position) -> {
            EnemyCherryBomb cherryBomb  = new EnemyCherryBomb(position,false,bus);
            bus.post(new SpawnEntityEvent<IEnemy>(cherryBomb));
        }));
        stringDefinitions.put("GhostPepper",new FactoryCellData(TileType.EMPTY, (bus, position) -> {
            GhostPepperEnemy ghostPepper  = new GhostPepperEnemy(position,false,bus);
            bus.post(new SpawnEntityEvent<IEnemy>(ghostPepper));
        }));
        stringDefinitions.put("IceCube",new FactoryCellData(TileType.EMPTY, (bus, position) -> {
            IceCube iceCube  = new IceCube(position,false,bus);
            bus.post(new SpawnEntityEvent<IEnemy>(iceCube));
        }));
        stringDefinitions.put("IceCreamBoss",new FactoryCellData(TileType.EMPTY, (bus, position) -> {
            IceCreamBoss iceCreamBoss  = new IceCreamBoss(position,true,bus);
            bus.post(new SpawnEntityEvent<IEnemy>(iceCreamBoss));
            bus.post(new SetEndGoalPositionEvent(position));
        }));
        stringDefinitions.put("DoorTrigger",new FactoryCellData(TileType.EMPTY, (bus, position) -> {
            CloseDoorsTrigger doorTrigger  = new CloseDoorsTrigger(bus,position);
            bus.post(new CreateDoorEvent(doorTrigger));
        }));
    }
    
    /*
    " example string:
    wwwwwwwwwww  www\n
    w              w\n
    w   1          w\n
    wwwwww      0  w\n
                    \n
                    \n
    whhhhhh        w\n
    whhhhhh    ww  w\n
    w    hh    ww  w\n
    w    hh        w\n
    wwwwwwwwwwwwwwww\n
    Cookie Waffle
    "
    */

    /** creates a Grid object from a string 
     *  use WorldGrid's overwrite method to place the room in the world grid.
     * @param input The string
     * @return the grid object
     */
    public RoomLayout createLayout(String input){
        String[] arguments = input.split(",");
        String[] rows = new String[arguments.length-1];
        System.arraycopy(arguments, 0, rows, 0, arguments.length-1);
        String[] enemyReferences = arguments[arguments.length-1].split(" ");

        IGrid<TileData> grid = new Grid<>(rows.length, rows[0].length());
        List<CoordinateItem<FactoryEntityConstructor>> entities = new LinkedList<>();
        
        for(int row = 0; row < rows.length; row++){
            for(int col = 0; col < rows[row].length(); col++){
                char chr = rows[row].charAt(col);  
                
                //the first row of the string is the top row,
                //the first row of the grid is the bottom row
                GridCoordinate coord = new GridCoordinate(rows.length - 1 - row, col);
                String lookUpString;

                if(Character.isDigit(chr)){
                    int index = Character.getNumericValue(chr);
                    lookUpString = enemyReferences[index];
                } else {
                    lookUpString = String.valueOf(chr);
                }
                FactoryCellData cellData = stringDefinitions.get(lookUpString);
                if(cellData.entity() != null){
                    entities.add(new CoordinateItem<>(coord, cellData.entity()));
                }
                grid.set(coord,new TileData(cellData.tileType()));
            }
            
        }
        return new RoomLayout(grid, entities);
    }

}
