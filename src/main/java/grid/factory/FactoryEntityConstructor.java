package grid.factory;

import com.badlogic.gdx.math.Vector2;
import model.event.EventBus;

/**
 * Interface for constructing game entities within the context of a factory.
 * Implementations of this interface define how specific game entities are created
 * and added to the game world using the EventBus.
 */
public interface FactoryEntityConstructor {

    /**
     * Constructs a game entity and adds it to the game world.
     * Implementations should define the entity creation process and post an appropriate
     * event to the EventBus to add the entity to the game world.
     *
     * @param bus the EventBus to post the entity creation event
     * @param position the position of the entity in the game world
     */
    void constructEntity(EventBus bus, Vector2 position);
}
