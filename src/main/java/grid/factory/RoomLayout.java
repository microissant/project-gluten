package grid.factory;

import com.badlogic.gdx.math.Vector2;
import grid.*;
import model.event.EventBus;

import java.util.List;


public class RoomLayout {
    private final IGrid<TileData> grid;
    private final List<CoordinateItem<FactoryEntityConstructor>> entities;


    public RoomLayout(IGrid<TileData> grid, List<CoordinateItem<FactoryEntityConstructor>> entities){
        this.entities = entities;
        this.grid = grid;
    }

    /** pastes the tiledata and the entities from this layout onto the worldgrid.
     * @param worldGrid the worldGrid that this layout will be placed in
     * @param coord the place where the bottom left corner of this layout will be pasted.
     * @param eventBus the eventBus responsible for broadcasting the creation of the entities in this layout.
     */
    public void paste(WorldGrid worldGrid, GridCoordinate coord, EventBus eventBus ){
        worldGrid.overwrite(grid,coord);
        for(CoordinateItem<FactoryEntityConstructor> cItem : entities){

            //calculate the position in the worldGrid
            Vector2 position  = new Vector2(coord.col + cItem.coordinate().col,coord.row + cItem.coordinate().row);
            position.scl(worldGrid.cellSize);
            position.add(worldGrid.cellSize * 0.5f,worldGrid.cellSize * 0.5f);
            cItem.item().constructEntity(eventBus, position);
        }   
    }

    /**
     * Gets the grid containing the tile data for this room layout.
     *
     * @return the grid with the tile data
     */
    public IGrid<TileData> getGrid(){
        return grid;
    }
}
