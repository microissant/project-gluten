package grid;
import java.util.Objects;

public class GridCoordinate {
    public final int row;
    public final int col;

    private final int hashCode;

    public GridCoordinate(int row, int col){
        this.row = row;
        this.col=col;

        hashCode = Objects.hash(row, col);
    }

    /** creates a new grid coordinate by adding the input coordinates to this coordinate
     * 
     * @param row the input row coordinate
     * @param col the input column coordinate
     * @return the sum of the coordinates
     */
    public GridCoordinate add(int row, int col){
        return new GridCoordinate(this.row + row,this.col + col);
    }
    /**creates a new grid coordinate by adding the input coordinates to this coordinate
     * 
     * @param coord the input coordinate
     * @return the sum of the coordinates
     */
    public GridCoordinate add(GridCoordinate coord){
        return add(coord.row,coord.col);
    }


    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof GridCoordinate Gridcoordinate)) {
            return false;
        }
        return row == Gridcoordinate.row && col == Gridcoordinate.col;
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public String toString(){
        return String.format("{ row='%d', col='%d' }",row,col);
    }
}