package grid.autotiling;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.utils.Json;
import grid.GridCoordinate;
import grid.TileData.TileType;
import grid.WorldGrid;
import media.TileSetManager;
import media.TileSetManager.TileSetID;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;



/**
 * Class containing methods for determining which tile in a tileset to use based on the surrounding context.
 * This class is responsible for automating the tiling process for a WorldGrid.
 */
public class AutoTiler {
    
    /**the map that converts a bitmasked value to an index in the tileset */
    static private final HashMap<Integer,Integer> eightBitHash = new HashMap<>();
    /**helper list used to determine which alternative version of a tile goes where */
    private final List<Integer> alternativeTileList;

    /**contains details about every tiletype in every tileset */
    static private final TileSetDataMap tileSetDataMap;

    private final WorldGrid parent;
    
    static {
        int[] list = new int[] {0,2, 8, 10, 11, 16, 18, 22, 24, 26, 27, 30, 31, 64, 66, 72, 74, 75, 80, 82, 86, 88, 90, 91, 94, 95, 104, 106, 107, 120, 122, 123, 126, 127, 208, 210, 214, 216, 218, 219, 222, 223, 248, 250, 251, 254, 255};
        for (int i =0; i < 47; i++) {
            eightBitHash.put(list[i], i);
        }
        
        //load auto tile instructions
        Json json = new Json();
        FileHandle file = Gdx.files.local("src/main/resources/autoTiling.json");
        tileSetDataMap = json.fromJson(TileSetDataMap.class,file.reader());
        
    }

    public AutoTiler(WorldGrid parent){
        alternativeTileList = new ArrayList<>(parent.numCols() * parent.numRows());
        this.parent = parent;
        for(int i =0; i < parent.numCols() * parent.numRows(); i++){
            alternativeTileList.add(i);
        }
        Collections.shuffle(alternativeTileList);
    }


 
    private static final int TOPLEFTBIT = 0;
    private static final int TOPBIT = 1;
    private static final int TOPRIGHTBIT = 2;
    private static final int LEFTBIT = 3;
    private static final int RIGHTBIT = 4;
    private static final int BOTTOMLEFTBIT = 5;
    private static final int BOTTOMBIT = 6;
    private static final int BOTTOMRIGHTBIT = 7;


    /**
     * Sets the auto-tile for the given GridCoordinate based on the surrounding context.
     * It uses the specified bitmask, tileSet, and type to automatically select the appropriate tile.
     *
     * @param bitmask the bitmask representing the surrounding context
     * @param tileSet the TileSetID of the tileset to use
     * @param type the TileType to use
     * @param tileMap the TiledMap containing the tile layers
     * @param coord the GridCoordinate of the tile to set
     */
    public void setAutoTile(int bitmask, TileSetID tileSet, TileType type, TiledMap tileMap, GridCoordinate coord){
        List<TileSetInstruction> instructions = tileSetDataMap.getInstructions(tileSet, type);
        //List<TileSetInstruction> ls = tileSetMetaData.get(tileSet.ordinal()).get(type.ordinal());
        MapLayers layers = tileMap.getLayers();

        for(TileSetInstruction f : instructions){
            int value = 0;

            //convert the bitmask into a number
            switch(f.complexity){
                case ONEBIT:
                    //move the bit in position 6 (directly below) to position 0
                    value = (bitmask & (1<<BOTTOMBIT))>>BOTTOMBIT;
                break;
                case TWOBIT:
                    //move the values in position 3 (Left) and 4 (right) to position 0 and 1
                    value = ((bitmask & (3<<LEFTBIT))>>LEFTBIT);
                break;
                case FOURBIT:
                    //compress bitmask to only include the four cardinal directions
                    value =  ((bitmask & (1<<TOPBIT))   >>TOPBIT);
                    value |= ((bitmask & (1<<LEFTBIT))  >>(LEFTBIT-1));
                    value |= ((bitmask & (1<<RIGHTBIT)) >>(RIGHTBIT-2));
                    value |= ((bitmask & (1<<BOTTOMBIT))>>(BOTTOMBIT-3));
                break;
                case EIGHTBIT:
                    //in order to get rid of redundancy, corners should only be 1 if both of the adjacent tiles are 1

                    value = Bitmask.removeCornerBits(bitmask);

                    value = eightBitHash.get(value);
                break;
                case ZEROBIT:
                    //the value is not changed by surrounding tiles so it should remain at 0
                break;
                default:
                    break;
            }

            value = getAlternative(tileSet,value + f.offset, coord);

            Cell cell = new Cell();
            cell.setTile(TileSetManager.getTileSet(tileSet).get(value));
            TiledMapTileLayer layer = (TiledMapTileLayer) layers.get(f.layer);
            layer.setCell(coord.col,coord.row,cell);
        }
    }

    


    private int getAlternative(TileSetID tileset,int value,GridCoordinate coord){
        int[] alternatives =  tileSetDataMap.getAlternates(tileset, value);
        if(alternatives != null){
            int index = coord.col + coord.row * parent.numCols();
            return alternatives[alternativeTileList.get(index) % alternatives.length]; 
        }
        return value;
    }

}
