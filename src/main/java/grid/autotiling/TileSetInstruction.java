package grid.autotiling;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

/**the instructions needed to automatically find the correct tile in a tileset given the information around the tile. Each tileSetInstruction object only accounts for one layer, so a list is needed to document all layers for a tile. */
public class TileSetInstruction implements Json.Serializable{
    protected enum TileComplexity{
        //the tile around this tile do not matter
        ZEROBIT,
        //tile below
        ONEBIT,
        //tile to the left and tile to the right
        TWOBIT,
        //up down left and right
        FOURBIT,
        //all 8 adjacent tiles
        EIGHTBIT
    }
    public TileComplexity complexity;
    public int layer;
    public int offset;

    public TileSetInstruction(){}

    public TileSetInstruction(TileComplexity complexity, int layer, int offset){
        this.complexity = complexity;
        this.layer = layer;
        this.offset = offset;
    }

    @Override
    public void write(Json json) {
        json.writeValue("complexity",complexity);
        json.writeValue("layer",layer);
        json.writeValue("offset",offset);
    }

    @Override
    public void read(Json json, JsonValue jsonData) {
        complexity = TileComplexity.valueOf(jsonData.getString("complexity"));
        layer = jsonData.getInt("layer");
        offset = jsonData.getInt("offset");
    }

}