package grid.autotiling;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import grid.TileData.TileType;
import media.TileSetManager.TileSetID;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**A class that stores information about how tiles in every tileset should be auto tiled and a list of alternative tiles for tiles with alternative versions. 
 * This object is primarily used to store information from a json file and does therefore not contain any setter methods.
 */
public class TileSetDataMap implements Json.Serializable {
    
    private final Map<TileSetID,TileSetData> tileSetDataMap;

    public TileSetDataMap(){
        tileSetDataMap = new EnumMap<>(TileSetID.class);
    }

     /** returns a list of tileset instructions for this specific tiletype in this tileset. the elements in the list represent a layer.
      * 
      * @param tileset the tileset containing the tiles that will be auto tiled
      * @param tileType the tiletype that will be represented by a tile in the tileset
      * @return the list of instructions. If this method returns null, then instructions are missing for this tiletype and default set of instructions should be used instead.
      */
    public List<TileSetInstruction> getInstructions(TileSetID tileset, TileType tileType){
        if(tileset == null) return null;
        TileSetData tileSetData = tileSetDataMap.get(tileset);
        if(tileSetData == null) return null;
        
        return tileSetData.getTileInstructions(tileType);
    }

    /** returns a list of alternative tile indexes for this specific index and tileset. Each index has a weight equivalent to the number of times it appears in the list.
     * @param tileset the tileset containing the tiles that may or may not have alternative tiles
     * @param key the tile index with equivalent tiles.
     * @return null if there is not a list of equivalent tiles. In this case you should just use the original tile. If there are alternatives, then this method returns an array of the alternatives.
     */
    public int[] getAlternates(TileSetID tileset, Integer key){
        return tileSetDataMap.get(tileset).getAlternatives(key);
    }


    @Override
    public void write(Json json) {
        json.writeObjectStart("tileSetDataMap");
        for(Map.Entry<TileSetID,TileSetData> set : tileSetDataMap.entrySet()){
            
            json.writeObjectStart(set.getKey().toString());
            set.getValue().write(json);
            json.writeObjectEnd();
            
        }
        
        json.writeObjectEnd();
    }

    @Override
    public void read(Json json, JsonValue jsonData) {
        jsonData = jsonData.get("tileSetDataMap");
        for(int i = 0; i < jsonData.size; i++){
            TileSetID key = TileSetID.valueOf(jsonData.get(i).name());
            TileSetData tileSetData = new TileSetData();
            tileSetData.read(json,jsonData.get(i));
            tileSetDataMap.put(key,tileSetData);
        } 
        
    }

}
