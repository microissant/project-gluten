package grid.autotiling;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import grid.TileData.TileType;

import java.util.*;


/**contains information about how a tileset should be auto tiled. 
 * The alternatives list is  a list of alternative indexes that are equivalent to the original, but are given different weights by repeated indexes in the list.
 * The tile instructions contain information about how a tile type should be auto tiled on a certain layer.
 * This object is primarily used to contain information from a json file and does therefore not contain any setter methods.
*/
public class TileSetData implements Json.Serializable{
   
    private final Map<TileType, List<TileSetInstruction>> tileTypeData;
    private final Map<Integer, int[]> alternatives;

    protected TileSetData(){
        tileTypeData = new EnumMap<>(TileType.class);
        alternatives = new HashMap<>();
    }

    /** gets a list of tile instructions for the input tiletype. Each element in the list represents a layer.
     * 
     * @param tileType the tiletype to get the instructions for
     * @return the list with the instructions
     */
    public List<TileSetInstruction> getTileInstructions(TileType tileType){
        return tileTypeData.get(tileType);
    }

    /** returns a list of alternative tile indexes for this specific index. Each index has a weight equivalent to the number of times it appears in the list.
     * 
     * @param integer the tile index with equivalent tiles.
     * @return null if there is not a list of equivalent tiles. In this case you should just use the original tile. If there are alternatives, then this method returns an array of the alternatives.
     */
    public int[] getAlternatives(Integer integer){
        return alternatives.get(integer);
    }

    @Override
    public void write(Json json) {
        json.writeObjectStart("tileTypeData");
        for(Map.Entry<TileType,List<TileSetInstruction>> set : tileTypeData.entrySet()){
            json.writeArrayStart(set.getKey().toString());
            for(TileSetInstruction instruction : set.getValue()){
                json.writeValue(instruction);
            }
            json.writeArrayEnd();
            
        }
        json.writeObjectEnd();

        json.writeObjectStart("alternatives");
        for(Map.Entry<Integer, int[]> set : alternatives.entrySet()){
            
            json.writeArrayStart(set.getKey().toString());
            for(int i =0;i < set.getValue().length; i++){
                json.writeValue(set.getValue()[i]);
                
            }
            json.writeArrayEnd();
            
        }
        json.writeObjectEnd();

    }

    @Override
    public void read(Json json, JsonValue jsonData) {
        JsonValue tileTypeJson = jsonData.get("tileTypeData");
        JsonValue alternativeJson = jsonData.get("alternatives");

        for(int i =0;i < tileTypeJson.size;i++){
            TileType key = TileType.valueOf(tileTypeJson.get(i).name());
            TileSetInstruction[] instructions = new TileSetInstruction[tileTypeJson.get(i).size];
            for(int j =0; j < tileTypeJson.get(i).size; j++){
                instructions[j] = new TileSetInstruction();
                instructions[j].read(json, tileTypeJson.get(i).get(j));
            }
            tileTypeData.put(key,Arrays.asList(instructions));
        }
        for(int i =0;i <alternativeJson.size;i++ ){
            int key =Integer.parseInt(alternativeJson.get(i).name());
            int[] value = new int[alternativeJson.get(i).size];
            for(int j = 0; j<alternativeJson.get(i).size;j++){
                value[j] = alternativeJson.get(i).getInt(j);
            }
            alternatives.put(key, value);
        }

    }


}
