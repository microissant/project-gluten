package grid.autotiling;


/**contains static methods for bitmasks and non static methods for path bitmasks */
public class Bitmask {
    
    static public final int TOPLEFT = 0;
    static public final int TOP = 1;
    static public final int TOPRIGHT = 2;
    static public final int LEFT = 3;
    static public final int RIGHT = 4;
    static public final int BOTTOMLEFT = 5;
    static public final int BOTTOM = 6;
    static public final int BOTTOMRIGHT = 7;
    /**removes an individual corner bit
     * 
     * @param bitmask the set of boolean flags representing whether or not surrounding tiles count as connecting
     * @param cornerpos the position of the corner bit to be removed
     * @param adjacent1 the position of the first adjacent bit
     * @param adjacent2 the position of the second adjacent bit
     * @return the bitmask without the redundant bit
     */
    public static int removeCornerBit(int bitmask, int cornerpos, int adjacent1, int adjacent2) {
        return makeDependent(makeDependent(bitmask,cornerpos,adjacent1),cornerpos, adjacent2);
    }

    /** sets a dependent bit to 0 if the independent bit is 0, does nothing otherwise
     * 
     * @param number the number with the independent and dependent bit
     * @param dependentPos the position of the dependent bit
     * @param independentPos the position of the independent bit
     * @return the number with the dependent bit set to 0 if the independent bit is 0
     */
    public static int makeDependent(int number, int dependentPos, int independentPos){
        //gets the bitmask in the independent position and moves it to the first position
        int mask = getBitAt(number, independentPos);
        //moves the independent bit to the position of the corner bit
        mask = mask << dependentPos;
        //turns every other value in the mask into 1 in order to not change any other bitmasks
        mask |= ~(1<<dependentPos);
        //apply mask
        return number & mask;
    }

    /** sets the bit in the dependent position to 1 if the independent bit is 1 
     * 
     * @param number the number containing the independent and dependent bits
     * @param dependentPos the position of the dependent bit
     * @param independentPos the position of the independent bit
     * @return the number but with the dependent bit set to 1 if the independent bit is 1
     */
    public static int orBit(int number,int dependentPos,int independentPos){
        //gets the value at the independent position
        int mask = getBitAt(number, independentPos);

        //moves the mask to the dependent position
        mask = mask<<dependentPos; 

        //apply mask
        return number | mask;
    }

    /**removes the redundant corner bits from the bitmask
     * a corner bit is only not redundant if both of the "adjacent" bits are 1.
     * "adjacent" does not mean next to in the integer representation of the boolean flags, but rather adjacent in the thing that the bitmask represents. In this case that would be the 8 tiles that surround the target tile.
     * @param bitmask the set of ordered boolean flags representing whether or not surrounding tiles count as connecting.
     * @return the bitmask value without redundant corner bits.
     */
    public static int removeCornerBits(int bitmask){
        int result = bitmask;
        result = removeCornerBit(result, TOPLEFT, TOP, LEFT);   
        result = removeCornerBit(result, TOPRIGHT, TOP, RIGHT);
        result = removeCornerBit(result, BOTTOMLEFT, BOTTOM, LEFT);
        result = removeCornerBit(result, BOTTOMRIGHT, BOTTOM, RIGHT); 
        return result;
    }

    /** returns 1 if the bit in the position given by the pos argument is 1 and 0 otherwise
     * @param value the value containing the bits
     * @param pos the position of the bit to check
     */
    public static int getBitAt(int value, int pos){
        return (value & (1<<pos)) >> pos;
    }
}
