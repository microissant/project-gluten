package grid;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**generic grid class that can be used for multiple things.
 * Use WallGrid for the grid that represents the game environment. 
 */
public class Grid<E> implements IGrid<E>{
    protected final int rows;
    protected final int columns;
    private final List<E> cells;
    

    public Grid(int rows, int columns, E initElement){
        if (rows <= 0 || columns <= 0) {
			throw new IllegalArgumentException();
		}

		this.columns = columns;
		this.rows = rows;
        cells = new ArrayList<>(columns * rows);
		for (int i = 0; i < columns * rows; ++i) {
			cells.add(initElement);
		}
    }
    public Grid(int rows, int columns){
        this(rows,columns,null);
    }


    @Override
    public Iterator<CoordinateItem<E>> iterator() {
        ArrayList<CoordinateItem<E>> result = new ArrayList<>(rows*columns);
        for(int row = 0; row < rows; row++){
            for(int col = 0; col < columns; col++){
                GridCoordinate coord = new GridCoordinate(row,col);
                CoordinateItem<E> coorditem = new CoordinateItem<>(coord, get(coord));
                result.add(coorditem);

            }
        }

        return result.iterator();
    }

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numCols() {
        return columns;
    }

    @Override
    public void set(GridCoordinate coordinate, E value) {
        set(coordinate.row,coordinate.col,value);
    }
    public void set(int row, int col, E value) {
        if(!coordinateIsOnGrid(row,col)) throw new IndexOutOfBoundsException();
        int index = calculateIndex(row,col);
        cells.set(index,value);
    }

    @Override
    public E get(GridCoordinate coordinate) {
        return get(coordinate.row,coordinate.col);
    }
    public E get(int row, int col) {
        if(!coordinateIsOnGrid(row,col)) throw new IndexOutOfBoundsException();
        int index = calculateIndex(row,col);
        return cells.get(index);
    }

    private int calculateIndex(int row, int col){
        return row+col*rows;
    }

    @Override
    public boolean coordinateIsOnGrid(GridCoordinate coordinate) {
        return coordinateIsOnGrid(coordinate.row,coordinate.col);
    }

    @Override
    public boolean coordinateIsOnGrid(int row, int col) {
        if(col<0 || row<0) return false;
        return (col<columns && row<rows);
    }
    
}
