package grid.collision;

import com.badlogic.gdx.math.Vector2;
import components.physics.Physics;
import grid.WorldGrid;

/**uses the dimensions set by the setdimensions method to collide with the grid using a box hitbox */
public class BoxCollider implements Collider{


    private float topToPos=0;
    private float bottomToPos=0;
    private float leftToPos=0;
    private float rightToPos=0;

    protected BoxCollider(){}

    /** sets the dimensions of the box
     * 
     * @param topToPos the distance from the top edge of the box to the y coordinate of the position of the physics component
     * @param bottomToPos the distance from the bottom edge of the box to the y coordinate of the position of the physics component
     * @param leftToPos the distance from the left edge of the box to the x coordinate of the position of the physics component
     * @param rightToPos the distance from the right edge of the box to the x coordinate of the position of the physics component
     */

    public void setDimensions(float topToPos, float bottomToPos, float leftToPos, float rightToPos){
        this.topToPos=Math.abs(topToPos);
        this.bottomToPos=Math.abs(bottomToPos);
        this.leftToPos=Math.abs(leftToPos);
        this.rightToPos=Math.abs(rightToPos);
    }


    @Override
    public boolean collide(Physics physics,Vector2 velocityDelta , WorldGrid grid, int ruleset, Vector2 resultVector) {
        boolean hitWall = false;
        resultVector.set(physics.getPosition());
        float vx = velocityDelta.x;
        float vy = velocityDelta.y;

        //if x is changing, check for a horizontal collision
        if(vx != 0){
            float xBoundary = Math.signum(vx) == 1 ? resultVector.x + rightToPos : (resultVector.x-leftToPos);
            int nextXCell = (int) ((xBoundary + vx) /grid.cellSize);
            int topYCell = (int) ((resultVector.y + topToPos) / grid.cellSize);
            int bottomYCell = (int) ((resultVector.y-bottomToPos)/ grid.cellSize);
    
            //if the box is already in an illegal position, 
            //don't collide with the cell in order to allow the box to escape the illegal position
            boolean diffCell =  nextXCell != (int) (xBoundary / grid.cellSize);

            if(diffCell){
                //check all of the cells that the side in the moving direction touches
                for(int yCell = bottomYCell; yCell <= topYCell; yCell++){
                    if(grid.checkForTileProperty(yCell,nextXCell, ruleset)){
                        hitWall = true;
                        //horizontal collision
                        if(Math.signum(vx) == 1){
                            //change the position so that the right boundary is very close to the left side of the tile
                            resultVector.x = nextXCell * grid.cellSize - 0.01f - (rightToPos);
                        } else if (Math.signum(vx) == -1) {
                            //change the position so that the left boundary is very close to the right side of the tile
                            resultVector.x = (nextXCell+1) * grid.cellSize + 0.01f + (leftToPos);
                        }
                        vx = 0;
                    }
                }
            }
        }
        resultVector.x += vx;

        //if y is changing, check for a vertical collision
        if(vy != 0){

            float yBoundary = Math.signum(vy) == 1 ? (resultVector.y + topToPos) : (resultVector.y-bottomToPos);
            int nextYCell = (int) ((yBoundary + vy) /grid.cellSize);
            int leftXCell = (int) ((resultVector.x-leftToPos) / grid.cellSize);
            int rightXCell = (int) ((resultVector.x + rightToPos) / grid.cellSize);

            /*
            if the box is already in an illegal position, 
            don't collide with the cell in order to allow the box to escape the illegal position
            */
            boolean diffCell =  nextYCell != (int) (yBoundary / grid.cellSize);

            if(diffCell){
                //check all of the cells that the side in the moving direction touches
                for(int xCell = leftXCell; xCell <= rightXCell; xCell++){
                    if(grid.checkForTileProperty(nextYCell,xCell, ruleset)){
                        hitWall = true;
                        //vertical collision
                        if(Math.signum(vy) == 1){
                            //change the position so that the bottom boundary is very close to the top of the tile
                            resultVector.y = nextYCell * grid.cellSize - 0.01f - (topToPos);
                        } else  if (Math.signum(vy) == -1) {
                            //change the position so that the top boundary is very close to the bottom of the tile
                            resultVector.y = (nextYCell+1) * grid.cellSize + 0.01f + (bottomToPos);
                        }
                        vy = 0;
                    }
                }
            }
        }

        resultVector.y+=vy;
        return hitWall;
    }


}
