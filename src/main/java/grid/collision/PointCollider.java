package grid.collision;

import com.badlogic.gdx.math.Vector2;
import components.physics.Physics;
import grid.WorldGrid;

public class PointCollider implements Collider{

    protected PointCollider(){}

    @Override
    public boolean collide(Physics physics,Vector2 velocityDelta, WorldGrid grid, int ruleset, Vector2 resultVector) {
                boolean hitWall = false;
                float vx = velocityDelta.x;
                float vy = velocityDelta.y;
                resultVector.set(physics.getPosition());
        
                //the x position after moving
                float nextX = resultVector.x + vx;
                //if the next cell is a wall
                if(grid.checkForTilePropertyAtPixel( nextX, resultVector.y,ruleset)){
                    hitWall = true;
                    //move to the left side of the next cell
                    resultVector.x = (int)(nextX / grid.cellSize) * grid.cellSize;
        
                    //if moving right, move slightly left so that the x coordinate is as far right as it can be in the correct cell
                    //if moving left, move one cell to the right to end up on the left side of the correct cell.
                    int dir = (int) Math.signum(vx);
                    if(dir==1) resultVector.x-=0.01; else if(dir==-1) resultVector.x+=grid.cellSize;
                    vx = 0;
                }
                //update x position
                resultVector.x += vx;
        
                //the y position after moving
                float nextY = resultVector.y + vy;
                
                //if the next cell is a wall
                if(grid.checkForTilePropertyAtPixel( resultVector.x, nextY,ruleset)){
                    hitWall =true;
                    //move to the bottom of the next cell
                    resultVector.y = (int)(nextY / grid.cellSize) * grid.cellSize;
        
                    //if moving up, move slightly down to end up at the top of the correct cell.
                    //if moving down, move up one cell to be at the bottom of the correct cell.
                    int dir = (int) Math.signum(vy);
                    if(dir==1) resultVector.y-=0.01; else if(dir==-1) resultVector.y+=grid.cellSize;
                    vy = 0;
                }
                //update y position
                resultVector.y += vy;
        
                return hitWall;
    }
    
}
