package grid.collision;

import com.badlogic.gdx.math.Vector2;
import components.physics.Physics;
import grid.WorldGrid;

public class LineCollider implements Collider{

    protected LineCollider(){}

    @Override
    public boolean collide(Physics physics,Vector2 velocityDelta, WorldGrid grid, int ruleset, Vector2 resultVector) {
        Vector2 start = physics.getPosition();
        resultVector.set(velocityDelta);
        resultVector.add(start);
        float dx = resultVector.x-start.x;
        float dy = resultVector.y-start.y;

        int currentRow = (int) (start.y / grid.cellSize);
        int currentCol = (int) (start.x / grid.cellSize);

        int targetRow = (int) (resultVector.y / grid.cellSize);
        int targetCol = (int) (resultVector.x / grid.cellSize);

        //if the line starts in an illegal position
        if(grid.checkForTileProperty(currentRow, currentCol, ruleset)){
            resultVector.set(start);
            return true;
        }
        int dirx =(int) Math.signum(dx);
        int diry =(int) Math.signum(targetRow - currentRow);

        //edge case
        if(dx == 0){
            do {
                if(grid.checkForTileProperty(currentRow, currentCol, ruleset)){
                    resultVector.set(start.x, currentRow * grid.cellSize + (diry==-1 ? grid.cellSize +0.01f: -0.01f));
                    return true;
                }
                currentRow+=diry;
            } while(currentRow!=targetRow);
            return false;
        }

        //create a line from start to end using y = mx + b
        float m = dy/dx;
        float b = start.y- start.x * m;

        while( !(currentRow == targetRow && currentCol == targetCol)){
            float nextX = (currentCol + dirx) * grid.cellSize;
            if(dirx==-1) nextX +=grid.cellSize;

            float nextY = (nextX * m + b);
            int nextYRow = (int) (nextY / grid.cellSize);

            if(currentRow != nextYRow){
                currentRow+=diry;
                //if the collision happens at the top/bottom of a cell
                if(grid.checkForTileProperty(currentRow, currentCol, ruleset)){
                    //find the x coordinate of the collision
                    m = dx / dy;
                    b = start.x - start.y * m;
                    nextY = (currentRow) * grid.cellSize + (diry ==-1 ? grid.cellSize : 0);
                    resultVector.set(nextY * m + b, nextY - diry * 0.01f);
                    return true;
                }
            } else {
                currentCol+=dirx;
                //if the collision happens on the left/right side of the cell
                if(grid.checkForTileProperty(currentRow, currentCol, ruleset)){
                    resultVector.set(nextX - dirx * 0.01f,nextY);
                    return true;
                }
            }

        }
        //no collision
        return false;
    }
    
}
