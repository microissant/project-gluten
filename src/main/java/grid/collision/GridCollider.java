package grid.collision;

/**class used for getting and making collider objects */
public class GridCollider {

    /**List of colliders that don't need to be instantiated more than once*/
    public enum StaticColliders{
        PointCollider(new PointCollider()),
        LineCollider(new LineCollider());

        public final Collider object;
        StaticColliders(Collider object){
            this.object = object;
        }
    }

    /** The box collider class needs information about the dimensions of the box, whic means that each object is unique
     * 
     * @param topToPos the distance from the top of the box to the y coordinate of the position
     * @param bottomToPos the distance from the bottom of the box to the y coordinate of the position
     * @param leftToPos the distance from the left edge of the box to the x coordinate of the position
     * @param rightToPos the distance from the right edge of the box to the x coordinate of the position
     * @return a new boxcollider object
     */
    public static BoxCollider makeBoxCollider(float topToPos, float bottomToPos, float leftToPos, float rightToPos ){
        BoxCollider result = new BoxCollider();
        result.setDimensions(topToPos,bottomToPos,leftToPos,rightToPos);
        return result;
    }


}

