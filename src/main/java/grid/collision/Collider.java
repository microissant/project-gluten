package grid.collision;

import com.badlogic.gdx.math.Vector2;
import components.physics.Physics;
import grid.WorldGrid;

/**interface for colliding with the worldgrid */
public interface Collider {
    
    
    /** Uses the velocity and position of the physics component to collide with the grid and return a legal position after a grid collision.
     * Use this code to update the position: velocity = result - position; position = result; 
     * @param physics the physics component containing the position and velocity
     * @param velocityDelta the velocity vector multiplied by the time delta
     * @param worldGrid the worldgrid to collide with
     * @param ruleset the rules determining which tile properties to stop at
     * @param resultVector vector that will contain the result. Needed for preallocation optimization.
     * @return true if the object hit a wall
     * 
     */
    boolean collide(Physics physics,Vector2 velocityDelta, WorldGrid worldGrid, int ruleset, Vector2 resultVector);
}
