package view.screens;

import app.Globals;
import app.global.RenderOptions;
import app.global.RuntimeOptions;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.ScreenUtils;
import controller.GameController;
import entities.Entity;
import entities.enemy.IEnemy;
import entities.particles.Particle;
import entities.player.PlayerCharacter;
import grid.WorldGrid;
import media.Renderable;
import model.Model;
import model.event.EventBus;
import model.event.events.game.GameViewActiveEvent;
import view.DebugShape;
import view.ui.hud.HUD;

import java.util.List;
import java.util.PriorityQueue;

/**
 * The GameView class represents the main game screen where the player interacts with the world.
 * It handles rendering of the world, entities, and HUD elements, as well as updating the camera
 * based on player and mouse positions.
 */
public class GameView implements Screen {

    private final Model model;
    private final OrthographicCamera camera;
    private final Gluten game;
    private final EventBus eventBus;
    private final OrthogonalTiledMapRenderer mapRenderer;
    private final Vector3 mouseScreenPos;
    private final HUD hud;
    private final SpriteBatch batch;
    private final InputMultiplexer inputMultiplexer;

    private final ShapeRenderer debugRenderer;
    private final GameController gameController;

    private final Stage stage;

    /**
     * Constructs a new GameView instance.
     *
     * @param game        The main game instance.
     * @param model       The game model containing the game state and objects.
     * @param gameController  The input controller for handling player input.
     * @param skin        The UI skin for HUD elements.
     * @param stage       The stage for rendering HUD elements.
     * @param eventBus    The event bus for posting and receiving game events.
     */
    public GameView(Gluten game,
                    Model model,
                    GameController gameController,
                    Skin skin,
                    Stage stage,
                    SpriteBatch spriteBatch,
                    ShapeRenderer shapeRenderer,
                    EventBus eventBus){
        this.gameController = gameController;
        this.stage = stage;
        this.batch = spriteBatch;
        inputMultiplexer = new InputMultiplexer();
        inputMultiplexer.addProcessor(stage);
        inputMultiplexer.addProcessor(gameController);


        hud = new HUD(eventBus, skin, stage,this, spriteBatch);
        this.game = game;
        this.camera = new OrthographicCamera();
        this.eventBus = eventBus;
        this.model = model;

        mouseScreenPos = new Vector3();
        
        camera.setToOrtho(false, Globals.VIEWPORT_WIDTH, Globals.VIEWPORT_HEIGHT);
        mapRenderer = new OrthogonalTiledMapRenderer(null,batch);
        mapRenderer.setView(camera);
        debugRenderer = shapeRenderer;
    }

    /**
     * Called when this screen becomes the current screen. This method:
     * <ol>
     *     <li>Posts a GameViewActiveEvent to the event bus.</li>
     *     <li>Sets the input processor to the input multiplexer.</li>
     * </ol>
     */
    @Override
    public void show() {
        eventBus.post(new GameViewActiveEvent());
        Gdx.input.setInputProcessor(inputMultiplexer);
    }

    /**
     * Renders the game world, entities, and HUD elements, and updates the camera
     * based on player and mouse positions. This method is responsible for the following tasks:
     * <ol>
     *   <li>Clearing the screen and updating the game model.</li>
     *   <li>Updating the camera position based on the player's position and mouse input.</li>
     *   <li>Rendering the background layers of the world grid.</li>
     *   <li>Rendering entities (enemies, various entities, items, and the player) in the order of their y-coordinate.</li>
     *   <li>Rendering projectiles.</li>
     *   <li>Rendering particles.</li>
     *   <li>Rendering the wall-top layer of the world grid.</li>
     *   <li>Rendering the cursor.</li>
     *   <li>Rendering the HUD elements using the HUD information from the game model.</li>
     * </ol>
     *
     * @param delta The time in seconds since the last render call.
     */
    @Override
    public void render(float delta) {
        model.update(delta);
        ScreenUtils.clear(0,0,0,1);
       
        //update camera position
        Vector2 playerPos = model.getPlayer().getPlayerCharacter().getPosition();
        WorldGrid grid = model.getWorldGrid();
        mouseScreenPos.x = Gdx.input.getX();
        mouseScreenPos.y = Gdx.input.getY();

        float scale = (float) Globals.VIEWPORT_WIDTH / (float) Globals.WINDOW_WIDTH;
        //move the camera closer to where the mouse is pointing
        float mouseXOffset = (0.3f*Globals.VIEWPORT_WIDTH)*((scale * mouseScreenPos.x)/(Globals.VIEWPORT_WIDTH)-0.5f);
        float mouseYOffset = (-0.3f*Globals.VIEWPORT_HEIGHT)*((scale * mouseScreenPos.y)/(Globals.VIEWPORT_HEIGHT)-0.5f);

        Vector2 newCamerapos = new Vector2(
            Math.max(Globals.VIEWPORT_WIDTH / 2.0f,Math.min(grid.numCols() * grid.cellSize-Globals.VIEWPORT_WIDTH / 2.0f,(float) Math.floor((playerPos.x+mouseXOffset) / scale)*scale)),
            Math.max(Globals.VIEWPORT_HEIGHT / 2.0f,Math.min(grid.numRows() * grid.cellSize-Globals.VIEWPORT_HEIGHT / 2.0f,(float) Math.floor((playerPos.y+mouseYOffset) / scale)*scale))
            );
        camera.position.set(newCamerapos,0);
        Globals.mouseWorldPosition = camera.unproject(mouseScreenPos);
        

        camera.update();
        batch.setProjectionMatrix(camera.combined);
        
        //game.batch.begin();
        if (RuntimeOptions.renderingEnabled()) {
            WorldGrid worldGrid  = model.getWorldGrid();
            mapRenderer.setMap(worldGrid.getTiledMap());
            mapRenderer.setView(camera);

            //render background layers
            mapRenderer.render(new int[] {0,1});
        }

        //render entities
        batch.begin();

        //render entities and the player based on their y position
        PriorityQueue<Renderable> renderOrder = new PriorityQueue<>((o1, o2) -> Float.compare(o2.getPosition().y, o1.getPosition().y));
        //add every enemy that can be drawn to the queue

        renderOrder.addAll(model.getEnemies());

        renderOrder.addAll(model.getVariousEntities());

        renderOrder.addAll(model.getItems());

        //add player to the queue
        PlayerCharacter player = model.getPlayer().getPlayerCharacter();
        renderOrder.add(player);
        
        //render everything in the queue
        while(!renderOrder.isEmpty()){
            Renderable toBeDrawn = renderOrder.poll();
            toBeDrawn.draw(batch, delta);
        }

        //render every projectile that can be drawn after the entities
        for (Entity projectile: model.getProjectiles()) {
            projectile.draw(batch, delta);
        }

        

        batch.end();

        //render walltop layer one tile above
        camera.translate(0,-16);
        camera.update();

        if (RuntimeOptions.renderingEnabled()) {
            mapRenderer.setView(camera);
            mapRenderer.render(new int[] {2});
        }
        
        //render foreground projectiles
        batch.begin();

        camera.translate(0,16);
        camera.update();
        batch.setProjectionMatrix(camera.combined);

        //render particles
        for(Particle particle : model.getParticles()){
            particle.draw(batch, delta);
        }
        batch.end();

        drawDebug();

        hud.render(model.getHUDInformation());
    }


    private void drawDebug() {
        if (!RenderOptions.enableDrawDebug.getCurrentOption()) return;
        Gdx.gl.glLineWidth(2.5f);

        debugRenderer.setProjectionMatrix(camera.combined);
        debugRenderer.begin(ShapeRenderer.ShapeType.Line);
        debugRenderer.setColor(Color.RED);

        if (RenderOptions.enableDebugCollision.getCurrentOption()) {
            for (DebugShape shape : model.getDebugShapes()) {
                shape.drawDebug(debugRenderer);
            }
        }

        if (RenderOptions.enableDebugPath.getCurrentOption()) {
            for (IEnemy enemy : model.getEnemies()) {
                List<Vector2> path = enemy.getCurrentPath();
                if (path == null || path.isEmpty()) continue;

                debugRenderer.setColor(Color.GREEN);

                for (int i = 0; i < path.size() - 1; i++) {
                    Vector2 current = path.get(i);
                    Vector2 next = path.get(i + 1);

                    debugRenderer.line(current.x, current.y, next.x, next.y);
                }
            }
        }

        debugRenderer.end();
    }

    /**
     * Called when the game window is resized. This method:
     * <ol>
     *     <li>Calculates the new width of the viewport while maintaining the aspect ratio.</li>
     *     <li>Sets the camera's projection to the new dimensions.</li>
     * </ol>
     *
     * @param width  The new width of the game window.
     * @param height The new height of the game window.
     */
    @Override
    public void resize(int width, int height) {
        int newWidth = (int) ((float) Globals.VIEWPORT_HEIGHT * ((float) width / (float) height));
        camera.setToOrtho(false, newWidth, Globals.VIEWPORT_HEIGHT);
        stage.getViewport().update(width, height,true);
        hud.resize();
    }

    @Override
    public void pause() {
        
    }

    @Override
    public void resume() {
        
    }

    @Override
    public void hide() {
        
    }

    @Override
    public void dispose() {
        mapRenderer.dispose();
        hud.dispose();
        debugRenderer.dispose();
    }

    /**
     * disable all input from the game
     */
    public void disableControllerInput() {
        gameController.disable();
        inputMultiplexer.removeProcessor(gameController);
    }

    /**
     * Enable registering of input in the game
     */
    public void enableControllerInput() {
        gameController.enable();
        inputMultiplexer.addProcessor(gameController);
    }
}

