package view.screens;

import app.global.RenderOptions;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import controller.GameController;
import controller.MenuController;
import media.AudioPlayer;
import media.MediaManager;
import model.Model;
import model.event.EventBus;
import model.event.Subscribe;
import model.event.events.game.EndLevelEvent;
import model.event.events.game.GameCompleteEvent;
import model.event.events.game.MainMenuActiveEvent;
import view.ui.*;
import view.ui.hud.widgets.DebugHUDView;


/**
 * The main game class that handles screen management, input processing,
 * and asset disposal. It contains both the game view and main menu screens.
 */
public class Gluten extends Game{

    public Screen gameView;
    private Screen mainMenu;
    private Screen levelComplete;
    private Screen gameCompleteView;
    private Screen creditsScreen;
    private GameController gameController;
    private AudioPlayer audioPlayer;

    public static ShaderProgram flashingShader;

    private DebugHUDView debugHUDView;
    private OptionsView optionsView;

    private Stage debugHUDStage;


    /**
     * Initializes the game by creating the event bus, model, controller,
     * audio player, and screens. It also sets the initial screen to the main menu.
     */
    @Override
    public void create() {

        MediaManager.initResources();

        EventBus eventBus = new EventBus();
        eventBus.register(this);
        Model model = new Model(eventBus);
        gameController = new GameController(eventBus);
        InputProcessor menuController = new MenuController(eventBus);
        audioPlayer = new AudioPlayer(eventBus);
        Skin skin = new Skin(Gdx.files.internal("src/main/resources/packed/ui/uiskin.json"));

        Gdx.graphics.setVSync(RenderOptions.vSync.getDefaultOption());
        Gdx.graphics.setForegroundFPS(RenderOptions.uncappedFPS.getDefaultOption() ? 0 : RenderOptions.maxFPS.getDefaultOption());


        if (RenderOptions.displayMode.getDefaultOption() == RenderOptions.DisplayOption.FULLSCREEN) {
            Gdx.graphics.setFullscreenMode(Gdx.graphics.getDisplayMode());
        } else {
            String[] resolution = RenderOptions.resolution.getDefaultOption().split("x");
            int width = Integer.parseInt(resolution[0]);
            int height = Integer.parseInt(resolution[1]);
            Gdx.graphics.setWindowedMode(width, height);
        }


        debugHUDStage = new Stage(new ScreenViewport());
        debugHUDView = new DebugHUDView(skin);
        debugHUDStage.addActor(debugHUDView);

        flashingShader = new ShaderProgram(Gdx.files.internal("src/main/resources/shaders/default_vertex_shader.vert"), Gdx.files.internal("src/main/resources/shaders/flashing_shader.frag"));
        if (!flashingShader.isCompiled()) {
            Gdx.app.error("FlashingShader", "Error compiling shader: " + flashingShader.getLog());
        }

        gameView = new GameView(this, model, gameController, skin, new Stage(new ScreenViewport()), new SpriteBatch(), new ShapeRenderer(), eventBus);
        mainMenu = new MainMenu(this, menuController, new Stage(new ScreenViewport()), skin, eventBus);
        levelComplete = new LevelComplete(this, new Stage(new ScreenViewport()), skin, eventBus);
        gameCompleteView = new GameComplete(this, new Stage(new ScreenViewport()), skin, eventBus);
        optionsView = new OptionsView(this, menuController, skin, new Stage(new ScreenViewport()), eventBus);
        creditsScreen = new CreditsScreen(this, skin, new Stage(new ScreenViewport()));

        this.setScreen(mainMenu);
    }

    /**
     * Disposes of resources such as the batch, audio player, main menu, and game view.
     */
    @Override
    public void dispose() {
        audioPlayer.dispose();
        mainMenu.dispose();
        gameView.dispose();
        debugHUDStage.dispose();
        optionsView.dispose();
    }

    /**
     * Calls the parent render method and processes controller input.
     */
    @Override
    public void render() {
        super.render();
        gameController.handleInput();
        if (RenderOptions.displayDebugStats.getCurrentOption()) {
            debugHUDView.update(Gdx.graphics.getDeltaTime());
            debugHUDStage.draw();
        }
    }


    /**
     * Sets the current screen to the game view.
     */
    public void setGameView() {
        this.setScreen(gameView);
    }

    /**
     * Sets the current screen to the main menu.
     */
    public void setMainMenu() {
        this.setScreen(mainMenu);
    }

    private void setLevelCompleteScreen() {
        setScreen(levelComplete);
    }

    private void setGameCompleteView() {
        setScreen(gameCompleteView);
    }

    /**
     * Handles the MainMenuActiveEvent by setting the current screen to the main menu.
     *
     * @param event The MainMenuActiveEvent triggered.
     */
    @Subscribe
    public void mainMenuOpened(MainMenuActiveEvent event) {
        setMainMenu();
    }

    /**
     * Register a level complete event, which set the game view to be level complete screen
     * @param event
     */
    @Subscribe
    public void levelComplete(EndLevelEvent event) {
        setLevelCompleteScreen();
    }

    /**
     * Register an game completed event, which sets the game view to be Game Complete screen
     *
     * @param event event from the eventbus
     */
    @Subscribe
    public void gameCompleted(GameCompleteEvent event) {
        setGameCompleteView();
    }

    /**
     * Set the screen to be the options view
     */
    public void setOptionsView() {
        this.setScreen(optionsView);
    }


    /**
     * Set the screen to be the credits view
     */
    public void setCredits() {
        setScreen(creditsScreen);
    }
}
