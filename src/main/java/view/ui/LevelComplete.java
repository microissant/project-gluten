package view.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import model.event.EventBus;
import model.event.Subscribe;
import model.event.events.game.MainMenuActiveEvent;
import model.event.events.ui.LevelStatsEvent;
import view.screens.Gluten;
import view.ui.widgets.TextButtonPadded;

public class LevelComplete implements Screen {

    private final Stage stage;
    private final Table table;

    private final Label missionSummary;

    public LevelComplete(Gluten mainScreen,
                    Stage stage,
                    Skin skin,
                    EventBus eventBus){

        this.stage = stage;


        TextButton continueButton = new TextButtonPadded("Continue", skin);
        TextButton quitButton = new TextButtonPadded("Quit to main menu", skin);

        table = new Table();
        table.setFillParent(true);
        table.defaults().pad(10);
        stage.addActor(table);

        Label title = new Label("Mission 1 complete", skin);
        missionSummary = new Label(
                "Elapsed time: 00:00:00", skin);

        table.add(title).align(Align.center).colspan(2).row();
        table.add(missionSummary).align(Align.center).colspan(2).row();
        table.add(continueButton).align(Align.center);
        table.add(quitButton).align(Align.center).row();

        continueButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mainScreen.setGameView();
            }
        });

        quitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                eventBus.post(new MainMenuActiveEvent());
            }
        });

        eventBus.register(this);
    }

    @Subscribe
    public void updateStats(LevelStatsEvent event) {
        missionSummary.setText("Elapsed time: " + event.time());
    }

    /**
     * Called when this screen becomes the current screen.
     * Sets the input processor to the stage and makes the table visible.
     */
    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
        table.setVisible(true);
    }

    @Override
    public void render(float v) {
        // Clear the screen
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Draw the stage
        stage.act(v);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height,true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        table.setVisible(false);
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

}


