package view.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import view.screens.Gluten;
import view.ui.widgets.TextButtonPadded;

public class CreditsScreen implements Screen {

    private final CreditsTable creditsTable;

    private final Stage stage;

    private final Gluten mainGame;


    public CreditsScreen(Gluten mainGame, Skin skin, Stage stage) {
        this.mainGame = mainGame;
        this.stage = stage;

        creditsTable = new CreditsTable(skin);

        TextButton exitButton = new TextButtonPadded("Back", skin, 10);

        Table buttonTable = new Table();
        buttonTable.setFillParent(true);

        buttonTable.add().expandX().expandY().row();
        buttonTable.add();
        buttonTable.add(exitButton).pad(20);

        stage.addActor(creditsTable);
        stage.addActor(buttonTable);

        show();

        exitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mainGame.setMainMenu();
            }
        });
    }

    @Override
    public void show() {
        resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        creditsTable.pack();
        creditsTable.align(Align.top);
        creditsTable.setPosition(0, -stage.getHeight());

        float scrollSpeed = stage.getHeight() * 0.1f; // (pixels/second)
        float distance = stage.getHeight() + creditsTable.getPrefHeight();
        float duration = distance / scrollSpeed; // Calculate duration based on scrollSpeed and distance
        creditsTable.addAction(Actions.sequence(
                Actions.moveTo(0, distance - stage.getHeight(), duration, Interpolation.linear),
                Actions.run(mainGame::setMainMenu)
        ));
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float v) {
        // Clear the screen
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Draw the stage
        stage.act(v);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height,true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        creditsTable.clearActions();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
