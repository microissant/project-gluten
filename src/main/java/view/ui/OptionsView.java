package view.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import model.event.EventBus;
import view.screens.Gluten;
import view.ui.widgets.TextButtonPadded;
import view.ui.widgets.options.AudioOptions;
import view.ui.widgets.options.ControlsOptions;
import view.ui.widgets.options.DisplayOptions;
import view.ui.widgets.options.OptionsTemplate;

import java.util.HashMap;

public class OptionsView implements Screen {


    private final Skin skin;
    private final Stage stage;

    private final HashMap<OptionsMode, OptionsTemplate> optionsMenu;

    private final Table rightTable;

    private OptionsMode currentOptionsMode;

    private final InputProcessor controller;

    private final InputMultiplexer inputMultiplexer;

    public OptionsView(Gluten mainGame, InputProcessor controller, Skin skin, Stage stage, EventBus eventBus) {
        this.skin = skin;
        this.stage = stage;
        this.controller = controller;
        inputMultiplexer = new InputMultiplexer();

        Table mainTable = new Table();
        mainTable.setFillParent(true);

        Table leftTable = new Table();
        TextButton displayButton = new TextButtonPadded("Display", skin);
        TextButton audioBuddon = new TextButtonPadded("Audio", skin);
        TextButton controlsButton = new TextButtonPadded("Controls", skin);
        TextButton backButton = new TextButtonPadded("Back", skin);

        displayButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                currentOptionsMode = OptionsMode.DISPLAY;
                updateOptionsMenu();
            }
        });

        audioBuddon.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                currentOptionsMode = OptionsMode.AUDIO;
                updateOptionsMenu();
            }
        });

        controlsButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                currentOptionsMode = OptionsMode.CONTROLS;
                updateOptionsMenu();
            }
        });

        optionsMenu = new HashMap<>();

        optionsMenu.put(OptionsMode.DISPLAY, new DisplayOptions(skin));
        optionsMenu.put(OptionsMode.CONTROLS, new ControlsOptions(skin, eventBus));
        optionsMenu.put(OptionsMode.AUDIO, new AudioOptions(skin, eventBus));

        currentOptionsMode = OptionsMode.DISPLAY;

        float paddingBottom = 20;
        int align = Align.left;

        leftTable.add(displayButton).align(align).padBottom(paddingBottom).row();
        leftTable.add(audioBuddon).align(align).padBottom(paddingBottom).row();
        leftTable.add(controlsButton).align(align).padBottom(paddingBottom).row();
        leftTable.add(backButton).align(align);

        backButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mainGame.setMainMenu();
            }
        });

        TextButton applyButton = new TextButtonPadded("Apply", skin);
        applyButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                apply();
            }
        });

        TextButton resetToDefault = new TextButtonPadded("Reset to default", skin);
        resetToDefault.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                resetToDefault();
            }
        });

        rightTable = new Table();

        Table buttonsTable = new Table();
        buttonsTable.add(resetToDefault).padRight(20);
        buttonsTable.add(applyButton);

        mainTable.add(leftTable).padRight(100);
        mainTable.add(rightTable).padBottom(20).row();
        mainTable.add();
        mainTable.add(buttonsTable);
        this.stage.addActor(mainTable);
    }

    private void resetToDefault() {
        if (!optionsMenu.containsKey(currentOptionsMode)) return;
        optionsMenu.get(currentOptionsMode).reset();
    }

    private void apply() {
        if (!optionsMenu.containsKey(currentOptionsMode)) return;
        optionsMenu.get(currentOptionsMode).apply();
    }


    @Override
    public void show() {
        inputMultiplexer.clear();
        inputMultiplexer.addProcessor(stage);
        inputMultiplexer.addProcessor(controller);
        Gdx.input.setInputProcessor(inputMultiplexer);

        updateOptionsMenu();
    }

    private void updateOptionsMenu() {
        if (!optionsMenu.containsKey(currentOptionsMode)) return;
        OptionsTemplate selectedTable = optionsMenu.get(currentOptionsMode);

        rightTable.clearChildren();
        rightTable.add(selectedTable);
        rightTable.layout();
        selectedTable.show();
    }

    @Override
    public void render(float delta) {
        // Clear the screen
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Draw the stage
        stage.act(delta);
        stage.draw();
    }


    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height,true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
        skin.dispose();
    }

    OptionsMode getCurrentOptionsMode() {
        return currentOptionsMode;
    }


    public enum OptionsMode {
        DISPLAY,
        AUDIO,
        CONTROLS

    }
}
