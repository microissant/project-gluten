package view.ui.widgets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import media.MediaManager;
import media.NinePatchID;

public class TextButtonStyles {

    private static final TextButton.TextButtonStyle standardStyle;

    static {
        Skin skin = new Skin(Gdx.files.internal("src/main/resources/packed/ui/uiskin.json"));

        standardStyle = new TextButton.TextButtonStyle();
        standardStyle.up = new NinePatchDrawable(MediaManager.getNinePatch(NinePatchID.BUTTON_STANDARD_UP));
        standardStyle.over = new NinePatchDrawable(MediaManager.getNinePatch(NinePatchID.BUTTON_STANDARD_OVER));
        standardStyle.down = new NinePatchDrawable(MediaManager.getNinePatch(NinePatchID.BUTTON_STANDARD_DOWN));
        standardStyle.checked = new NinePatchDrawable(MediaManager.getNinePatch(NinePatchID.BUTTON_STANDARD_CHECKED));
        standardStyle.font = skin.getFont("h1");
    }


    public static TextButton.TextButtonStyle standardStyle() {
        return standardStyle;
    }

}
