package view.ui.widgets.options;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;

public class OptionsEntry<E extends Actor> extends Table {

    private final String toolTip;
    private final Label label;

    public OptionsEntry(String label, E interactLeft, String tooltip, Skin skin) {
        this(label, interactLeft, null, tooltip, skin);
    }

    public OptionsEntry(String label, E interactLeft, E interactRight, String tooltip, Skin skin) {
        this.label = new Label(label, skin);
        this.label.setWrap(true);
        this.toolTip = tooltip;

        Table interactTable = new Table();
        interactTable.add(interactLeft);
        if (interactRight != null) {
            interactTable.add(interactRight).padLeft(20).minWidth(50);
        }

        setSkin(skin);
        float labelMaxWidth = 350; // Adjust this value according to your needs
        add(this.label).width(labelMaxWidth).padRight(50).align(Align.left);
        add(interactTable).width(150).align(Align.right);
    }



    public void updateText(String text) {
        this.label.setText(text);
    }

    public String getToolTip() {
        return toolTip;
    }

}
