package view.ui.widgets.options;

import app.global.ControllerSettings;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import model.event.EventBus;
import model.event.Subscribe;
import model.event.events.ui.MenuInputEvent;
import view.ui.widgets.TextButtonStyles;

import java.util.HashMap;

public class ControlsOptions extends OptionsTemplate {

    private final TextButton moveUpButton;
    private final TextButton moveDownButton;
    private final TextButton moveLeftButton;
    private final TextButton moveRightButton;
    private final TextButton pauseButton;
    private final TextButton dashButton;
    private final TextButton attackButton;
    private final TextButton interactButton;

//    private final List<TextButton> buttons;

    private final HashMap<Mode, TextButton> buttons;


    private Mode selectedMode;


    public ControlsOptions(Skin skin, EventBus eventBus) {
        super(skin);

        if (eventBus != null) {
            eventBus.register(this);
        }

        buttons = new HashMap<>();

        selectedMode = Mode.NONE;

        moveUpButton = new TextButton("W", TextButtonStyles.standardStyle());
        moveUpButton.pad(10);

        moveDownButton = new TextButton("S", TextButtonStyles.standardStyle());
        moveDownButton.pad(10);

        moveLeftButton = new TextButton("A", TextButtonStyles.standardStyle());
        moveLeftButton.pad(10);

        moveRightButton = new TextButton("D", TextButtonStyles.standardStyle());
        moveRightButton.pad(10);

        dashButton = new TextButton("Spacebar", TextButtonStyles.standardStyle());
        dashButton.pad(10);

        pauseButton = new TextButton("ESC", TextButtonStyles.standardStyle());
        pauseButton.pad(10);

        attackButton = new TextButton("Left mouse", TextButtonStyles.standardStyle());
        attackButton.pad(10);

        interactButton = new TextButton("E", TextButtonStyles.standardStyle());
        interactButton.pad(10);

        addListenerToButton(moveUpButton, Mode.UP);
        addListenerToButton(moveDownButton, Mode.DOWN);
        addListenerToButton(moveLeftButton, Mode.LEFT);
        addListenerToButton(moveRightButton, Mode.RIGHT);
        addListenerToButton(dashButton, Mode.DASH);
        addListenerToButton(interactButton, Mode.INTERACT);
        addListenerToButton(pauseButton, Mode.PAUSE);
        addListenerToButton(attackButton, Mode.ATTACK);

        addEntry(new OptionsEntry<>("Move up", moveUpButton, "", skin));
        addEntry(new OptionsEntry<>("Move down", moveDownButton, "", skin));
        addEntry(new OptionsEntry<>("Move right", moveLeftButton, "", skin));
        addEntry(new OptionsEntry<>("Move left", moveRightButton, "", skin));
        addEntry(new OptionsEntry<>("Dash", dashButton, "", skin));
        addEntry(new OptionsEntry<>("Interact", interactButton, "", skin));
        addEntry(new OptionsEntry<>("Attack", attackButton, "", skin));
        addEntry(new OptionsEntry<>("Pause", pauseButton, "", skin));

        init();
    }

    private void addListenerToButton(TextButton button, Mode mode) {
        buttons.put(mode, button);
        button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (selectedMode == Mode.NONE) {
                    selectedMode = mode;
                } else {
                    selectedMode = Mode.NONE;
                }


                uncheckAll(button);
            }
        });

    }

    @Subscribe
    public void keyEvent(MenuInputEvent event) {
        if (selectedMode == Mode.NONE) return;
        if (!buttons.containsKey(selectedMode)) return;

        int keycode = event.keycode();

        String buttonText = ControllerSettings.convertInputToString(keycode);
        if (buttonText == null) return;

        TextButton button = buttons.get(selectedMode);
        button.setText(buttonText);

        uncheckAll(null);
        selectedMode = Mode.NONE;
    }

    @Override
    public void apply() {
        ControllerSettings.MOVE_UP.setCurrentOption(ControllerSettings.convertStringToInput(moveUpButton.getText().toString()));
        ControllerSettings.MOVE_DOWN.setCurrentOption(ControllerSettings.convertStringToInput(moveDownButton.getText().toString()));
        ControllerSettings.MOVE_LEFT.setCurrentOption(ControllerSettings.convertStringToInput(moveLeftButton.getText().toString()));
        ControllerSettings.MOVE_RIGHT.setCurrentOption(ControllerSettings.convertStringToInput(moveRightButton.getText().toString()));
        ControllerSettings.DASH.setCurrentOption(ControllerSettings.convertStringToInput(dashButton.getText().toString()));
        ControllerSettings.INTERACT.setCurrentOption(ControllerSettings.convertStringToInput(interactButton.getText().toString()));
        ControllerSettings.PAUSE.setCurrentOption(ControllerSettings.convertStringToInput(pauseButton.getText().toString()));
        ControllerSettings.ATTACK.setCurrentOption(ControllerSettings.convertStringToInput(attackButton.getText().toString()));

        selectedMode = Mode.NONE;
    }

    @Override
    public void reset() {
        ControllerSettings.resetToDefault();
        show();
    }

    @Override
    public void show() {
        uncheckAll(null);
        selectedMode = Mode.NONE;

        moveUpButton.setText(ControllerSettings.convertInputToString(ControllerSettings.MOVE_UP.getCurrentOption()));
        moveDownButton.setText(ControllerSettings.convertInputToString(ControllerSettings.MOVE_DOWN.getCurrentOption()));
        moveLeftButton.setText(ControllerSettings.convertInputToString(ControllerSettings.MOVE_LEFT.getCurrentOption()));
        moveRightButton.setText(ControllerSettings.convertInputToString(ControllerSettings.MOVE_RIGHT.getCurrentOption()));
        dashButton.setText(ControllerSettings.convertInputToString(ControllerSettings.DASH.getCurrentOption()));
        interactButton.setText(ControllerSettings.convertInputToString(ControllerSettings.INTERACT.getCurrentOption()));
        attackButton.setText(ControllerSettings.convertInputToString(ControllerSettings.ATTACK.getCurrentOption()));
        pauseButton.setText(ControllerSettings.convertInputToString(ControllerSettings.PAUSE.getCurrentOption()));
    }

    private void uncheckAll(TextButton exception) {
        for (TextButton button: buttons.values()) {
            if (button == exception) continue;

            button.setChecked(false);
        }
    }

    private enum Mode {
        ATTACK,
        UP,
        DOWN,
        LEFT,
        RIGHT,
        PAUSE,
        DASH,
        INTERACT,
        NONE
    }
}
