package view.ui.widgets.options;

import app.global.AudioSettings;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import model.event.EventBus;
import model.event.events.audio.AdjustVolumeEvent;

public class AudioOptions extends OptionsTemplate {

    private final CheckBox masterMute;
    private final Slider masterSlider;
    private final Label masterLevel;

    private final CheckBox musicMute;
    private final Slider musicSlider;
    private final Label musicLevel;
    private final CheckBox SFXMute;
    private final Slider SFXSlider;
    private final Label SFXLevel;

    private final EventBus eventBus;

    public AudioOptions(Skin skin, EventBus eventBus) {
        super(skin);
        this.eventBus = eventBus;

        masterMute = new CheckBox("", skin);
        masterSlider = new Slider(0, 1, 0.05f, false, skin);
        masterLevel = new Label("", skin);

        musicMute = new CheckBox("", skin);
        musicSlider = new Slider(0, 1, 0.05f, false, skin);
        musicLevel = new Label("", skin);

        SFXMute = new CheckBox("", skin);
        SFXSlider = new Slider(0, 1, 0.05f, false, skin);
        SFXLevel = new Label("", skin);

        addEntry(new OptionsEntry<>("Mute all", masterMute, "Mute all music", skin));
        addEntry(new OptionsEntry<>("Master volume", masterSlider, masterLevel, "Adjust volume of the game music", skin));

        addEntry(new OptionsEntry<>("Mute music", musicMute, "Mute all music", skin));
        addEntry(new OptionsEntry<>("Music volume", musicSlider, musicLevel, "Adjust volume of the game music", skin));

        addEntry(new OptionsEntry<>("Mute SFX", SFXMute, "Mute all music", skin));
        addEntry(new OptionsEntry<>("SFX volume", SFXSlider, SFXLevel, "Adjust volume of the game music", skin));

        masterSlider.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                masterLevel.setText((int) (masterSlider.getValue() * 100));
            }
        });

        musicSlider.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                musicLevel.setText((int) (musicSlider.getValue() * 100));
            }
        });

        SFXSlider.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                SFXLevel.setText((int) (SFXSlider.getValue() * 100));
            }
        });

        init();
    }

    @Override
    public void apply() {
        AudioSettings.setMasterMute(masterMute.isChecked());
        AudioSettings.setMasterVolume(masterSlider.getValue());

        AudioSettings.setMusicMute(musicMute.isChecked());
        AudioSettings.setMusicVolume(musicSlider.getValue());

        AudioSettings.setSFXMute(SFXMute.isChecked());
        AudioSettings.setSFXVolume(SFXSlider.getValue());

        System.out.println(AudioSettings.getMasterMute());

        eventBus.post(new AdjustVolumeEvent());
    }

    @Override
    public void reset() {
        AudioSettings.resetToDefault();
        show();
        apply();
    }

    @Override
    public void show() {
        masterMute.setChecked(AudioSettings.getMusicMute());
        masterSlider.setValue(AudioSettings.getMasterVolume());
        masterLevel.setText(String.valueOf((int) (AudioSettings.getMasterVolume() * 100)));

        musicMute.setChecked(AudioSettings.getMusicMute());
        musicSlider.setValue(AudioSettings.getMusicVolume());
        musicLevel.setText(String.valueOf((int) (AudioSettings.getMusicVolume() * 100)));

        SFXMute.setChecked(AudioSettings.getSFXMute());
        SFXSlider.setValue(AudioSettings.getSFXVolume());
        SFXLevel.setText(String.valueOf((int) (AudioSettings.getSFXVolume() * 100)));
    }
}
