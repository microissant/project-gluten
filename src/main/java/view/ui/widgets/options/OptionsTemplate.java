package view.ui.widgets.options;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import java.util.ArrayList;
import java.util.List;


public abstract class OptionsTemplate extends Table {

    private final List<OptionsEntry<?>> entries;
    private final float entryPadding;

    public OptionsTemplate(Skin skin) {
        entries = new ArrayList<>();
        entryPadding = 10;
    }

    protected void init() {
        clearChildren();

        for (OptionsEntry<?> entry: entries) {
            add(entry).padBottom(entryPadding).row();
        }
    }

    protected void addEntry(OptionsEntry<?> entry) {
        entries.add(entry);
    }

    public abstract void apply();

    public abstract void reset();

    public abstract void show();
}
