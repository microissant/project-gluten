package view.ui.widgets.options;

import app.global.RenderOptions;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;

public class DisplayOptions extends OptionsTemplate {


    private final SelectBox<String> displayModes;
    private final SelectBox<String> resolutions;
    private final CheckBox vSync;
    private final Slider fpsSlider;
    private final Label fpsCount;
    private final CheckBox checkUncappedFPS;
    private final CheckBox enableDebugDrawing;
    private final CheckBox checkDrawCollisions;
    private final CheckBox checkDrawPath;
    private final CheckBox enableDebugStats;

    public DisplayOptions(Skin skin) {
        super(skin);

        displayModes = new SelectBox<>(skin);
        displayModes.setItems("Fullscreen", "Windowed");
        resolutions = new SelectBox<>(skin);
        resolutions.setItems(getAvailableResolutions());
        vSync = new CheckBox("", skin);
        checkUncappedFPS = new CheckBox("", skin);
        fpsSlider = new Slider(30, 144, 1, false, skin);
        fpsCount = new Label("60", skin);
        enableDebugStats = new CheckBox("", skin);
        checkDrawPath = new CheckBox("", skin);
        enableDebugDrawing = new CheckBox("", skin);
        checkDrawCollisions = new CheckBox("", skin);

        addEntry(new OptionsEntry<>("Display mode", displayModes, "Change from fullscreen/windowed. Enabling fullscreen may better performance.", skin));
        addEntry(new OptionsEntry<>("Resolution", resolutions, "Set display resolution. Lowering the resolution may better the performance", skin));
        addEntry(new OptionsEntry<>("VSync", vSync, "Synchronized the FPS to the screen refresh-rate", skin));
        addEntry(new OptionsEntry<>("Uncapped FPS", checkUncappedFPS, "Render as many frames per second as possible", skin));
        addEntry(new OptionsEntry<>("Max FPS", fpsSlider, fpsCount, "Set maximum FPS when capped FPS and vsync enabled. Lowering the FPS may provide more consistent performance for lower-end hardware", skin));
        addEntry(new OptionsEntry<>("Performance statistics", enableDebugStats, "Performance statistics include FPS, frametime, collision checks, pathfinding, and more", skin));
        addEntry(new OptionsEntry<>("Enable debug drawing", enableDebugDrawing, "Enable/disable all debug rendering", skin));
        addEntry(new OptionsEntry<>("Draw collision", checkDrawCollisions, "Set display resolution. Lowering the resolution may better the performance", skin));
        addEntry(new OptionsEntry<>("Draw pathfinding", checkDrawPath, "Draw a debug line showing the entities current paths", skin));

        fpsSlider.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                fpsCount.setText((int) fpsSlider.getValue());
            }
        });

        init();
    }

    @Override
    public void apply() {
        // Apply the selected resolution
        String[] resolution = resolutions.getSelected().split("x");
        int width = Integer.parseInt(resolution[0]);
        int height = Integer.parseInt(resolution[1]);
        if (displayModes.getSelected().equals("Fullscreen")) {
            Graphics.DisplayMode targetDisplayMode = null;
            for (Graphics.DisplayMode displayMode : Gdx.graphics.getDisplayModes()) {
                if (displayMode.width == width && displayMode.height == height) {
                    targetDisplayMode = displayMode;
                    break;
                }
            }
            if (targetDisplayMode != null) {
                Gdx.graphics.setFullscreenMode(targetDisplayMode);
            }
        } else {
            Gdx.graphics.setWindowedMode(width, height);
        }


        Gdx.graphics.setVSync(vSync.isChecked());
        RenderOptions.vSync.setCurrentOption(vSync.isChecked());

        int fps = checkUncappedFPS.isChecked() ? 0 : (int) fpsSlider.getValue();

        Gdx.graphics.setForegroundFPS(fps);
        RenderOptions.maxFPS.setCurrentOption(fps);
        RenderOptions.uncappedFPS.setCurrentOption(checkUncappedFPS.isChecked());

        RenderOptions.enableDrawDebug.setCurrentOption(enableDebugDrawing.isChecked());
        RenderOptions.enableDebugPath.setCurrentOption(checkDrawPath.isChecked());
        RenderOptions.enableDebugCollision.setCurrentOption(checkDrawCollisions.isChecked());

        RenderOptions.displayDebugStats.setCurrentOption(enableDebugStats.isChecked());
    }

    @Override
    public void reset() {
        RenderOptions.resetToDefault();
        show();
        apply();
    }

    @Override
    public void show() {
        vSync.setChecked(RenderOptions.vSync.getCurrentOption());
        fpsSlider.setValue(RenderOptions.maxFPS.getCurrentOption());
        fpsCount.setText(RenderOptions.maxFPS.getCurrentOption());
        checkDrawCollisions.setChecked(RenderOptions.enableDebugCollision.getCurrentOption());
        enableDebugStats.setChecked(RenderOptions.displayDebugStats.getCurrentOption());
        enableDebugDrawing.setChecked(RenderOptions.enableDrawDebug.getCurrentOption());
        checkUncappedFPS.setChecked(RenderOptions.uncappedFPS.getCurrentOption());
        checkDrawPath.setChecked(RenderOptions.enableDebugPath.getCurrentOption());
    }

    private Array<String> getAvailableResolutions() {
        Graphics.DisplayMode[] displayModes = Gdx.graphics.getDisplayModes();
        Array<String> availableResolutions = new Array<>();
        for (Graphics.DisplayMode displayMode : displayModes) {
            String formattedResolution = displayMode.width + "x" + displayMode.height;
            if (!availableResolutions.contains(formattedResolution, false)) {
                availableResolutions.add(formattedResolution);
            }
        }

        availableResolutions.reverse();
        return availableResolutions;
    }
}
