package view.ui.widgets;

import app.global.ControllerSettings;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import media.NinePatchID;

public class ControllerLayout extends Table {

    public ControllerLayout(Skin skin) {

        Label dash = new Label("Dash:", skin);
        Label move = new Label("Move:", skin);
        Label interact = new Label("Interact:", skin);
        Label pause = new Label("Pause:", skin);
        Label attack = new Label("Attack:", skin);

        float thickness = 1.1f;
        String font = "h1";
        NinePatchID patch = NinePatchID.BUTTON_FRAME_01;

        float left = 20;
        float right = 20;
        float top = 10;
        float bottom = 10;

        ButtonWithBorder buttonDash = new ButtonWithBorder(ControllerSettings.convertInputToString(ControllerSettings.DASH.getCurrentOption()), skin.getFont(font), patch, patch, thickness, left, right, top, bottom);
        ButtonWithBorder buttonMoveUp = new ButtonWithBorder(ControllerSettings.convertInputToString(ControllerSettings.MOVE_UP.getCurrentOption()), skin.getFont(font), patch, patch, thickness, left, right, top, bottom);
        ButtonWithBorder buttonMoveLeft = new ButtonWithBorder(ControllerSettings.convertInputToString(ControllerSettings.MOVE_LEFT.getCurrentOption()), skin.getFont(font), patch, patch, thickness, left, right, top, bottom);
        ButtonWithBorder buttonMoveDown = new ButtonWithBorder(ControllerSettings.convertInputToString(ControllerSettings.MOVE_DOWN.getCurrentOption()), skin.getFont(font), patch, patch, thickness, left, right, top, bottom);
        ButtonWithBorder buttonMoveRight = new ButtonWithBorder(ControllerSettings.convertInputToString(ControllerSettings.MOVE_RIGHT.getCurrentOption()), skin.getFont(font), patch, patch, thickness, left, right, top, bottom);
        ButtonWithBorder buttonAttack = new ButtonWithBorder(ControllerSettings.convertInputToString(ControllerSettings.ATTACK.getCurrentOption()), skin.getFont(font), patch, patch, thickness, left, right, top, bottom);
        ButtonWithBorder buttonInteract = new ButtonWithBorder(ControllerSettings.convertInputToString(ControllerSettings.INTERACT.getCurrentOption()), skin.getFont(font), patch, patch, thickness, left, right, top, bottom);
        ButtonWithBorder buttonPause = new ButtonWithBorder(ControllerSettings.convertInputToString(ControllerSettings.PAUSE.getCurrentOption()), skin.getFont(font), patch, patch, thickness, left, right, top, bottom);

        float padding = 30;

        Table layout = new Table();

        Table leftTable = new Table();
        leftTable.add(new LabelWithButton(dash, buttonDash, padding)).padBottom(10).align(Align.left).row();
        leftTable.add(new LabelWithButton(interact, buttonInteract, padding)).align(Align.left);

        Table moveTable = new Table();
        moveTable.add(move).padRight(padding);

        Table moveButtonsLayout = new Table();
        moveButtonsLayout.add();
        moveButtonsLayout.add(buttonMoveUp);
        moveButtonsLayout.add().padBottom(10).row();
        moveButtonsLayout.add(buttonMoveLeft);
        moveButtonsLayout.add(buttonMoveDown);
        moveButtonsLayout.add(buttonMoveRight);

        moveTable.add(moveButtonsLayout);

        layout.add(leftTable).padRight(padding);
        layout.add(moveTable).padRight(padding);

        Table rightTable = new Table();
        rightTable.add(new LabelWithButton(attack, buttonAttack, padding)).padBottom(10).align(Align.left).row();
        rightTable.add(new LabelWithButton(pause, buttonPause, padding)).align(Align.left);

        layout.add(rightTable);

        add(new Label("CONTROLS", skin, "h1_bold")).padBottom(padding).row();
        add(layout);
    }



}
