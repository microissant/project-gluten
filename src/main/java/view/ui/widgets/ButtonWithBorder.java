package view.ui.widgets;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import media.MediaManager;
import media.NinePatchID;

public class ButtonWithBorder extends Table {

    public ButtonWithBorder(String text, BitmapFont font, NinePatchID regular, NinePatchID hover, float borderThickness, float padLeft, float padRight, float padTop, float padBottom) {
        NinePatch regularNinePatch = MediaManager.getNinePatch(regular);
        NinePatch hoverNinePatch = MediaManager.getNinePatch(hover);

        regularNinePatch.scale(borderThickness, borderThickness);
        hoverNinePatch.scale(borderThickness, borderThickness);

        TextButton.TextButtonStyle buttonStyle = new TextButton.TextButtonStyle();
        buttonStyle.font = font;
        buttonStyle.up = new NinePatchDrawable(regularNinePatch);
        buttonStyle.down = new NinePatchDrawable(hoverNinePatch);
        buttonStyle.checked = new NinePatchDrawable();

        TextButton button = new TextButton(text, buttonStyle);
        button.padLeft(padLeft);
        button.padRight(padRight);
        button.padTop(padTop);
        button.padBottom(padBottom);

        add(button);
    }

    public ButtonWithBorder(String text, BitmapFont font, NinePatchID regular, int borderThickness, float padding) {
        this(text, font, regular, regular, borderThickness, padding, padding, padding, padding);
    }

}
