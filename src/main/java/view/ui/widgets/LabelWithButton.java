package view.ui.widgets;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;

public class LabelWithButton extends Table {

    public LabelWithButton(Label label, ButtonWithBorder buttonWithBorder, float padding) {

        add(label).align(Align.left).padRight(padding);
        add(buttonWithBorder).align(Align.center);


    }

}
