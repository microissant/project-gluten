package view.ui.widgets;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

public class TextButtonPadded extends TextButton {

    public TextButtonPadded(String text, Skin skin, float padLeft, float padRight, float padTop, float padBottom) {
        super(text, skin);
        padLeft(padLeft);
        padRight(padRight);
        padTop(padTop);
        padBottom(padBottom);
    }

    public TextButtonPadded(String text, Skin skin, float pad) {
        this(text, skin, pad, pad, pad, pad);
    }

    public TextButtonPadded(String text, Skin skin) {
        this(text, skin, 5);
    }

}
