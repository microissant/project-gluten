package view.ui.widgets;

import com.badlogic.gdx.scenes.scene2d.ui.Table;

public abstract class OptionsTable extends Table {

    public abstract void apply();

    public abstract void show();

    public abstract void reset();

}
