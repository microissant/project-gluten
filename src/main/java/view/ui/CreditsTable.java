package view.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

public class CreditsTable extends Table {

    private final Table contentTable;
    private final float sectionPadding = 100;

    public CreditsTable(Skin skin) {
        setFillParent(true);

        contentTable = new Table();
        contentTable.setWidth(750);
        add(contentTable).align(Align.center).expand();

        JsonReader jsonReader = new JsonReader();
        JsonValue creditsJson = jsonReader.parse(Gdx.files.internal("credits.json"));

        // Get and display the opener title
        String openerTitle = creditsJson.get("credits").get("opener").get(0).getString("title");
        Label openerTitleLabel = new Label(openerTitle, skin, "h0_bold");
        openerTitleLabel.setAlignment(Align.center);
        openerTitleLabel.setWrap(true);
        contentTable.add(openerTitleLabel).width(contentTable.getWidth() - 10).pad(5).padBottom(sectionPadding).align(Align.center).row();


        // Add each credit section
        JsonValue sections = creditsJson.get("credits").get("sections");
        for (JsonValue section : sections) {
            String title = null;
            if (section.has("title")) {
                title = section.getString("title");
            }
            JsonValue entries = section.get("entries");
            addSection(title, skin, entries);
            contentTable.row(); // Add line break between sections
        }
    }

    private void addSection(String title, Skin skin, JsonValue entries) {
        if (title != null) {
            Label titleLabel = new Label(title, skin, "h1_bold");
            titleLabel.setAlignment(Align.center); // Center-align the title
            titleLabel.setWrap(true); // Enable word wrapping for the title
            contentTable.add(titleLabel).width(contentTable.getWidth() - 10).pad(5).align(Align.center).padBottom(sectionPadding / 5).row();
        }

        for (int i = 0; i < entries.size; i++) {
            JsonValue entry = entries.get(i);

            String content = entry.getString("text");
            Label contentLabel = new Label(content, skin);
            contentLabel.setWrap(true);

            float padBottom = 0;
            if (i == entries.size - 1) {
                padBottom = sectionPadding;
            }

            contentTable.add(contentLabel).width(contentTable.getWidth() - 10).pad(5).align(Align.center).padBottom(padBottom).row();
        }
    }
}
