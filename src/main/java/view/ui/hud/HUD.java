package view.ui.hud;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import model.event.EventBus;
import model.event.Subscribe;
import model.event.events.game.*;
import model.event.events.item.ChestCloseEvent;
import model.event.events.item.ChestOpenEvent;
import model.event.events.ui.GameOverUIEvent;
import model.event.events.ui.InitializeHUDEvent;
import view.screens.GameView;
import view.ui.GameOverHUD;
import view.ui.PauseGameUI;
import view.ui.hud.widgets.item.ItemSelectionMenu;

/**
 * HUD represents the collection of user interface elements that are displayed
 * during the game, such as the in-game HUD, game over screen, and pause menu.
 */
public class HUD {

    private final Stage stage;
    private final SpriteBatch spriteBatch;
    private final Skin skin;
    private ItemSelectionMenu itemSelectionMenu;

    private GameHud gameHud;

    private GameOverHUD gameOverHUD;
    private PauseGameUI pauseGameUI;
    private final EventBus eventBus;

    private final GameView gameView;

    /**
     * Constructs a new HUD with the specified event bus, skin, stage, and label styles.
     *
     * @param eventBus the event bus for handling events
     * @param skin     the skin to use for the UI widgets
     * @param stage    the stage to add UI widgets to
     */
    public HUD(EventBus eventBus,
               Skin skin,
               Stage stage,
               GameView gameView,
               SpriteBatch spriteBatch){
        this.eventBus = eventBus;
        this.skin = skin;
        this.stage = stage;
        this.gameView = gameView;
        this.spriteBatch = spriteBatch;

        eventBus.register(this);
        create();
    }

    private void create() {

        gameHud = new GameHud(skin, eventBus, stage);
        gameOverHUD = new GameOverHUD(skin, stage, eventBus);
        pauseGameUI = new PauseGameUI(skin, stage, eventBus);

        // Add the table to the stage
        stage.addActor(gameHud);
        stage.addActor(gameOverHUD);
        stage.addActor(pauseGameUI);

        itemSelectionMenu = new ItemSelectionMenu(skin, eventBus);

        initialize();
    }

    public void resize() {
        gameHud.resize();
    }

    /**
     * Renders the HUD with the given HUDInformation.
     *
     * @param hudInformation the information to update the HUD with
     */
    public void render(HUDInformation hudInformation) {
        float deltaTime = Gdx.graphics.getDeltaTime();

        gameHud.update(deltaTime, hudInformation);

        // Render the stage containing the HUD
        stage.act(deltaTime);
        stage.draw();
    }

    @Subscribe
    public void addChestItemMenu(ChestOpenEvent event) {
        if (event == null) return;
        if (event.chest() == null) return;
        if (event.chest().getItems() == null) return;
        if (stage.getActors().contains(itemSelectionMenu, true)) return;

        hideGameHud();
        stage.addActor(itemSelectionMenu);
        itemSelectionMenu.addChest(event.chest());
        itemSelectionMenu.setVisible(true);

        gameView.disableControllerInput();
    }

    @Subscribe
    public void closeChestMenu(ChestCloseEvent event) {
        stage.getRoot().removeActor(itemSelectionMenu);
        showGameHud();
        eventBus.post(new ResumeGameEvent());

        gameView.enableControllerInput();
    }

    @Subscribe
    public void gameOverEvent(GameOverUIEvent event) {
        if (event == null) return;
        if (event.stats() == null) return;

        gameOverHUD.setVisible(true);
        hideGameHud();
        gameOverHUD.show(event.stats());
    }

    @Subscribe
    public void pauseGame(PauseGameEvent event) {
        hideGameHud();
        pauseGameUI.setVisible(true);
    }

    @Subscribe
    public void resumeGame(ResumeGameEvent event) {
        pauseGameUI.setVisible(false);
        pauseGameUI.startAnimations();
        showGameHud();
    }

    @Subscribe
    public void newGameEvent(NewGameEvent event) {
        initialize();
    }

    @Subscribe
    public void restartGame(RestartLevelEvent event) {
        initialize();
    }

    @Subscribe
    public void loadNextLevel(LoadNextLevelEvent event) {
        initialize();
    }

    /**
     * Initializes the HUD by resetting its state and making relevant elements visible.
     *
     * <p>
     * This method resets the game HUD, hides the game over and pause screens, and the
     * item selection menu. It then makes the game HUD visible and posts an
     * {@link InitializeHUDEvent} to inform subscribers about the initial state of the HUD,
     * such as all equipped upgrades.
     * </p>
     */
    public void initialize() {
        gameHud.reset();
        gameOverHUD.setVisible(false);
        pauseGameUI.setVisible(false);
        itemSelectionMenu.setVisible(false);
        showGameHud();
        eventBus.post(new InitializeHUDEvent());
    }

    /**
     * Disposes of the resources used by the HUD.
     */
    public void dispose() {
        stage.dispose();
        spriteBatch.dispose();
        skin.dispose();
        gameOverHUD.dispose();
    }

    private void hideGameHud() {
        gameHud.setVisible(false);
    }

    private void showGameHud() {
        gameHud.setVisible(true);
    }
}
