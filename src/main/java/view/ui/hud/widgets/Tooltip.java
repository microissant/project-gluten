package view.ui.hud.widgets;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;

public class Tooltip extends Table {

    public Tooltip(String title, String description, Skin skin) {

        Label titleLabel = new Label(title, skin, "h2");
        Label descriptionLabel = new Label(description, skin, "h3");
        descriptionLabel.setWrap(true);
        descriptionLabel.setWidth(250);
        descriptionLabel.setAlignment(Align.center);

        Table contentTable = new Table();
        contentTable.setBackground(skin.newDrawable("white", 0, 0, 0, 0.5f));

        float padding = 10;

        setBackground(skin.getDrawable("frame"));
        add(contentTable).expand().fill().pad(2);

        contentTable.add(titleLabel).padBottom(padding);
        contentTable.row();
        contentTable.add(descriptionLabel).width(descriptionLabel.getWidth()).padBottom(padding);
    }
}