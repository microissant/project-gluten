package view.ui.hud.widgets;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

public class TimeCounter extends Table {

    private final Label minutes;
    private final Label seconds;
    private final Label milliseconds;

    private final int precision;

    public TimeCounter(int precision, Skin skin) {
        this.precision = Math.max(0, precision);

        Label.LabelStyle style = skin.get("h1", Label.LabelStyle.class);
        style.fontColor = Color.DARK_GRAY;

        minutes = new Label("00:", style);
        seconds = new Label("00:", style);
        milliseconds = new Label("0", style);

        add(minutes).width(50).pad(2);
        add(seconds).width(50).pad(2);
        add(milliseconds).width(15);
    }

    /**
     * Update the timer display on the screen. Time has format MM:SS:ms
     * @param elapsedTime new time
     */
    public void updateTimer(double elapsedTime) {
        String minutes = String.format("%02d", (int) elapsedTime / 60);
        String seconds = String.format("%02d", (int) elapsedTime % 60);
        String milliseconds = String.format("%01d", (int) ((elapsedTime - (int) elapsedTime) * 10 * precision));

        this.minutes.setText(minutes + ":");
        this.seconds.setText(seconds + ":");
        this.milliseconds.setText(milliseconds);
    }


}
