package view.ui.hud.widgets;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar.ProgressBarStyle;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Align;
import media.MediaManager;
import media.NinePatchID;
import media.TextureID;


public class ResourceWidgetStack extends Stack {
    
    private final Label health;
    private final Label currency;
    private final Image background;
    private final Image foreground;
    private final WorkingProgressBar healthBar;
    private final Image deathIcon;


    public ResourceWidgetStack(Skin skin) {
        currency = new Label("5", skin);
        currency.setColor(new Color(125f/255f,217f/255f,87f/255f,1f));
        health = new Label("5", skin);
        health.setColor(new Color(232f/255f,58f/255f,75f/255f,1f));
        foreground = new Image(MediaManager.getTexture(TextureID.HEALTHBAR_CRT).getKeyFrame(0));
        background = new Image(MediaManager.getTexture(TextureID.HEALTHBAR    ).getKeyFrame(0));
        float scale = 4f;

        //needed to set the size of the stack
        Container<Image> backgroundContainer = new Container<>(background);
        background.setOriginY(26);
        foreground.setOriginY(26);

        backgroundContainer.size(scale*87, scale*28);

        currency.setAlignment(Align.right);
        health.setAlignment(Align.center);

        //needed to position the health label
        Container<Label> healthLabelContainer = new Container<>(health);
        healthLabelContainer.pad(scale*5f, scale*3f, scale*13f, scale*71f);
        
        //needed to position the currency label
        Container<Label> currencyLabelContainer = new Container<>(currency);
        currencyLabelContainer.pad(scale*17f,scale*29f,scale*2f,scale*42f);
        currency.setBounds(scale*29f, scale*2f, scale*16, scale*9);
        

        NinePatch healthNinePatch = MediaManager.getNinePatch(NinePatchID.HEALTHBAR);
        healthNinePatch.scale(scale, scale);
        NinePatchDrawable ninePatchDrawable = new NinePatchDrawable(healthNinePatch);

        ninePatchDrawable.setMinHeight(scale*5f);
        ProgressBarStyle style = new ProgressBarStyle();
        style.knobBefore = ninePatchDrawable;
        //custom progress bar because the original class has a hardlocked width for some reason
        healthBar = new WorkingProgressBar(0f, 10f, 1f, false,style);
        healthBar.setAnimateDuration(0.2f);

        Container<ProgressBar> healthBarContainer = new Container<>(healthBar);
        healthBarContainer.setBounds(0, 0, scale*87, scale*28);
        healthBarContainer.center();
        healthBarContainer.left();
        healthBar.setPrefWidth(scale*64);

        healthBarContainer.pad(scale*9f,scale*20f,scale*14,scale*3f);

        deathIcon = new Image(MediaManager.getTexture(TextureID.DEATH_ICON).getKeyFrame(0));
        deathIcon.setScale(scale);
        Container<Image> deathContainer = new Container<>(deathIcon);
        deathContainer.bottom();
        deathContainer.left();
        deathContainer.setBounds(0,0,86 * scale, 28 * scale);
        deathContainer.pad(5*scale,3*scale,13*scale,71*scale);

        add(backgroundContainer);
        add(healthLabelContainer);
        add(currencyLabelContainer);
        add(healthBarContainer);
        add(deathContainer);
        add(foreground);
    }

    /**
     * Update health value to new health value
     *
     * @param current current health value
     * @param max max health value
     */
    public void updateHealth(int current, int max) {
        health.setText(current);
        healthBar.setRange(0, max);
        healthBar.setValue(current);
        boolean isDead = current == 0;
        deathIcon.setVisible(isDead);
        healthBar.setVisible(!isDead);
    }

    /**
     * Set the displayed currency value to be the new value
     *
     * @param current new value
     */
    public void updateCurrency(int current) {
        currency.setText(current);
    }

    @Override
    public void layout() {
        super.layout();

        float heightWidthRatio = 28f/87f;
        setHeight( getWidth() * heightWidthRatio);
        background.setBounds(0, 0, getWidth(), getHeight());
        foreground.setBounds(0, 0, getWidth(), getHeight());
        foreground.validate();
        background.validate();


    }    
}
