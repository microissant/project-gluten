package view.ui.hud.widgets.upgrades;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import components.upgrade.upgrades.IUpgrade;
import model.IUpdatable;
import model.event.EventBus;
import model.event.Subscribe;
import model.event.events.upgrades.UpgradeAddedEvent;
import model.event.events.upgrades.UpgradeRemovedEvent;

import java.util.HashMap;

/**
 * The UpgradeBar class represents the user interface for displaying a collection of upgrades.
 * It manages and displays UpgradeIcons for each upgrade in the collection.
 */
public class UpgradeBar extends Table implements IUpdatable {
    private final HashMap<IUpgrade, UpgradeIcon> upgrades;
    private final int iconsPerLine;
    private final Skin skin;

    public UpgradeBar(int iconsPerLine, Skin skin, EventBus eventBus) {
        super();
        this.upgrades = new HashMap<>();
        this.iconsPerLine = iconsPerLine;
        this.skin = skin;

        if (eventBus == null) return;
        eventBus.register(this);
    }

    /**
     * Adds an UpgradeIcon for the given upgrade to the UpgradeBar.
     *
     * @param upgrade The upgrade for which the icon should be added.
     */
    private void addUpgradeIcon(IUpgrade upgrade) {
        if (upgrade == null) return;

        UpgradeIcon icon = new UpgradeIcon(upgrade, skin);
        upgrades.put(upgrade, icon);
        updateLayout();
    }

    /**
     * Removes the UpgradeIcon for the given upgrade from the UpgradeBar.
     *
     * @param upgrade The upgrade for which the icon should be removed.
     */
    private void removeUpgradeIcon(IUpgrade upgrade) {
        if (upgrade == null) return;
        if (!upgrades.containsKey(upgrade)) return;

        upgrades.remove(upgrade);
        updateLayout();
    }

    /**
     * Updates the layout of the UpgradeBar to display the current set of UpgradeIcons.
     */
    private void updateLayout() {
        clearChildren(); // Clear the previous layout
        int currentColumn = 0;

        for (UpgradeIcon icon : upgrades.values()) {
            add(icon).pad(5);

            currentColumn++;

            if (currentColumn >= iconsPerLine) {
                row();
                currentColumn = 0;
            }
        }
    }

    @Subscribe
    public void upgradeAdded(UpgradeAddedEvent event) {
        if (event == null) return;
        if (event.upgrade() == null) return;

        addUpgradeIcon(event.upgrade());
    }

    @Subscribe
    public void removeUpgrade(UpgradeRemovedEvent event) {
        if (event == null) return;
        if (event.upgrade() == null) return;

        removeUpgradeIcon(event.upgrade());
    }

    @Override
    public void update(double deltaTime) {
        for (UpgradeIcon icon: upgrades.values()) {
            icon.update(deltaTime);
        }
    }

    @Override
    public void reset() {
        upgrades.clear();
        updateLayout();
    }
}