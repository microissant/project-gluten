package view.ui.hud.widgets.upgrades;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import components.upgrade.upgrades.IUpgrade;
import media.MediaManager;
import media.TextureID;
import model.IUpdatable;
import view.ui.hud.widgets.Tooltip;

/**
 * The UpgradeIcon class represents a user interface element that displays an icon for a single upgrade.
 * It also displays a tooltip with the upgrade's title and description when hovered over.
 */
public class UpgradeIcon extends Table implements IUpdatable {
    private final Label textLabel;
    private final Tooltip tooltip;
    private final IUpgrade upgrade;

    /**
     * Creates an UpgradeIcon for the given upgrade with the specified styles and skin.
     *
     * @param upgrade           The upgrade associated with this icon.
     * @param skin              The skin used for the tooltip.
     */
    public UpgradeIcon(IUpgrade upgrade, Skin skin) {
        // Create the icon
        Image icon = new Image(upgrade.getIcon());
        icon.setSize(32, 32);
        this.upgrade = upgrade;


        textLabel = new Label("", skin, "h3");

        add(icon).width(icon.getWidth()).height(icon.getHeight()).center();
        row();
        add(textLabel).align(Align.center).padTop(2);

        // Create the tooltip
        tooltip = new Tooltip(upgrade.getTitle(), upgrade.getDescription(), skin);
        tooltip.setPosition(0, -50);
        addActor(tooltip);
        tooltip.setVisible(false);

        TextureRegion background = MediaManager.getTexture(TextureID.CUTTING_BOARD).getKeyFrame(0);
        TextureRegionDrawable drawable = new TextureRegionDrawable(background);
        setBackground(drawable);
        pad(10);

        addListener(new InputListener() {
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                tooltip.setVisible(true);
            }

            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                tooltip.setVisible(false);
            }
        });
    }

    @Override
    public void update(double deltaTime) {
        if (upgrade.getTimeRemaining() < 0 ){
            textLabel.setText("");
        } else {
            textLabel.setText((int) Math.ceil(upgrade.getTimeRemaining()));
        }
    }
}