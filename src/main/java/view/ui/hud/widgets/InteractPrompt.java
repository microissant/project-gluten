package view.ui.hud.widgets;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import item.Interactable;
import media.MediaManager;
import media.NinePatchID;
import media.TextureID;

public class InteractPrompt extends Group {
    private final TextWithIcon interactText;
    private final TextWithIcon cost;

    public InteractPrompt(Skin skin) {

        interactText = new TextWithIcon("Unlock chest", MediaManager.getTexture(TextureID.ICE_CREAM).getKeyFrame(0), false, skin);
        cost = new TextWithIcon("104", MediaManager.getTexture(TextureID.CURRENCY_ICON).getKeyFrame(0), true, skin);

        NinePatch patch = MediaManager.getNinePatch(NinePatchID.TILE_BLACK);
        patch.setColor(new Color(1, 1, 1, 0.75f));
        patch.scale(5, 5);
        NinePatchDrawable drawable = new NinePatchDrawable(patch);

        Table table = new Table();
        table.add(interactText);
        table.row();
        table.add(cost);
        table.pad(10);
        table.setBackground(drawable);

        table.pack(); // Adjust the table's size to its content
        addActor(table);
        setSize(table.getWidth(), table.getHeight());
    }

    /**
     * Set the cost dispalyed
     *
     * @param cost cost of interaction
     * @param <E> type extending Number
     */
    public <E extends Number> void setCostText(E cost) {
        this.cost.updateText(String.valueOf(cost));
    }

    /**
     * Set the interactable object. Used to set the interact prompt.
     *
     * @param interactable object that is interacted with
     */
    public void setInteractable(Interactable interactable) {
        interactText.updateText(interactable.interactPromptText());

        if (interactable.getCost() == null) {
            hideCost();
        } else {
            showCost();
            setCostText(interactable.getCost().getValue());
        }
    }

    /**
     * Hide the cost of the interaction
     */
    public void hideCost() {
        cost.setVisible(false);
    }

    /**
     * Show the cost of the interaction
     */
    public void showCost() {
        cost.setVisible(true);
    }
}
