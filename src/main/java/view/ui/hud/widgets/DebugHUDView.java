package view.ui.hud.widgets;

import app.global.Statistics;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import model.IUpdatable;
import util.CountdownTimer;

public class DebugHUDView extends Table implements IUpdatable {

    private final Label fpsCounter;

    private final Label frameTime;

    private final Label collisionCountFrame;
    private final Label collisionCountTotal;
    private final Label collisionTimeUS;
    private final Label pathNodesVisitedFrame;
    private final Label pathNodesVisitedTotal;



    private final CountdownTimer updateStatsTimer;

    public DebugHUDView(Skin skin) {
        Label.LabelStyle style = skin.get("h2", Label.LabelStyle.class);
        style.fontColor = Color.RED;

        updateStatsTimer = new CountdownTimer(0.25f, this::updateStats, true);
        updateStatsTimer.start();

        Label titleFPS = new Label("FPS", style);
        fpsCounter = new Label("0", style);

        Label titleFrameTime = new Label("Frametime (ms)", style);
        frameTime = new Label("0", style);

        Label sectionCollision = new Label("Collision detection", style);

        Label titleCollisionChecksFrame = new Label("Checks (Frame)", style);
        collisionCountFrame = new Label("", style);

        Label titleCollisionChecksTotal = new Label("Checks (Total)", style);
        collisionCountTotal = new Label("", style);

        Label titleCollisionTime = new Label("Time us", style);
        collisionTimeUS = new Label("", style);

        Label sectionPathfinding = new Label("Pathfinding", style);

        Label titlePathNodesVisitedFrame = new Label("Visited (frame)", style);
        pathNodesVisitedFrame = new Label("", style);

        Label titlePathNodesVisitedTotal = new Label("Visited (total)", style);
        pathNodesVisitedTotal = new Label("", style);


        setFillParent(true);
        add().expandX();
        add(titleFPS).width(250).align(Align.left);
        add(fpsCounter).padBottom(10).width(50).align(Align.right).row();
        add();
        add(titleFrameTime).align(Align.left);
        add(frameTime).width(50).align(Align.right).row();

        add();
        add(sectionCollision).align(Align.left).padTop(20).padBottom(5);
        add().row();

        add();
        add(titleCollisionChecksFrame).align(Align.left);
        add(collisionCountFrame).width(50).align(Align.right).row();

        add();
        add(titleCollisionChecksTotal).align(Align.left);
        add(collisionCountTotal).width(50).align(Align.right).row();

        add();
        add(titleCollisionTime).align(Align.left);
        add(collisionTimeUS).width(50).align(Align.right).row();

        add();
        add(sectionPathfinding).align(Align.left).padTop(20).padBottom(5);
        add().row();

        add();
        add(titlePathNodesVisitedFrame).align(Align.left);
        add(pathNodesVisitedFrame).width(50).align(Align.right).row();

        add();
        add(titlePathNodesVisitedTotal).align(Align.left);
        add(pathNodesVisitedTotal).width(50).align(Align.right).row();

        padRight(100);
    }

    @Override
    public void update(double deltaTime) {
        updateStatsTimer.update((float)deltaTime);
    }

    /**
     * Update the stats displayed on the HUD
     */
    private void updateStats() {
        fpsCounter.setText(Gdx.graphics.getFramesPerSecond());
        frameTime.setText(String.format("%.2f", Gdx.graphics.getDeltaTime() * 1000f));

        collisionCountFrame.setText(String.valueOf(Statistics.getCollisionsCountFrame()));
        collisionCountTotal.setText(String.format("%.2f k", Statistics.getCollisionsCountTotal() / 1000.0));
        collisionTimeUS.setText(String.format("%.2f", Statistics.getCollisionTimeNS() / 1000.0));

        pathNodesVisitedFrame.setText(String.valueOf(Statistics.getPathNodeChecksFrame()));
        pathNodesVisitedTotal.setText(String.format("%.2f k", Statistics.getPathNodeChecksTotal() / 1000.0));
    }

}
