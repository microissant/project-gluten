package view.ui.hud.widgets.item;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import item.chest.IChest;
import item.loot.ILoot;
import model.event.EventBus;
import model.event.events.item.ChestCloseEvent;
import model.event.events.item.ObtainLootEvent;

/**
 * The ItemSelectionMenu class represents a user interface element that displays a menu for selecting a loot item from a chest.
 * It shows a list of item cards, and allows the user to confirm or cancel their selection.
 */
public class ItemSelectionMenu extends Table {
    private final Skin skin;
    private final Label titleLabel;
    private final Table itemCardsTable;
    private final TextButton confirmButton;
    private final TextButton cancelButton;
    private IChest chest;

    private ItemCard selectedCard;

    /**
     * Creates an ItemSelectionMenu with the specified skin and event bus.
     *
     * @param skin     The skin used for the menu UI elements.
     * @param eventBus The event bus used for posting events related to item selection and chest interaction.
     */
    public ItemSelectionMenu(Skin skin, EventBus eventBus) {
        this.skin = skin;

        setFillParent(true);

        titleLabel = new Label("Select item", skin);

        itemCardsTable = new Table();

        confirmButton = new TextButton("Confirm", skin);
        confirmButton.setDisabled(true);
        cancelButton = new TextButton("Cancel", skin);

        layoutMenu();

        confirmButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (selectedCard == null) return;
                if (eventBus == null) return;
                if (chest == null) return;

                eventBus.post(new ObtainLootEvent(selectedCard.getLoot()));
                eventBus.post(new ChestCloseEvent());
                clearItemCards();
                chest.looted();
            }
        });

        cancelButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (eventBus == null) return;
                clearItemCards();
                eventBus.post(new ChestCloseEvent());
            }
        });
    }

    private void layoutMenu() {
        clearChildren();
        Table buttonTable = new Table();
        buttonTable.add(confirmButton).width(200).padRight(10);
        buttonTable.add(cancelButton).width(200);

        add(titleLabel).colspan(2).padBottom(10);
        row();
        add(itemCardsTable).colspan(2);
        row();
        add(buttonTable).colspan(2).padTop(10);
    }

    public void clearSelection() {
        for (Actor actor : itemCardsTable.getChildren()) {
            ItemCard itemCard = (ItemCard) actor;
            itemCard.setSelected(false);
            itemCard.setBackground(skin.getDrawable("frame"));
        }
    }

    // Method to add an item card to the menu
    public void addItemCard(ItemCard itemCard) {
        itemCard.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                clearSelection();
                itemCard.setSelected(true);
                itemCard.setBackground(skin.newDrawable("frame-selected"));
                confirmButton.setDisabled(false);
                selectedCard = itemCard;
            }
        });
        itemCardsTable.add(itemCard).padLeft(10);
    }

    public void clearItemCards() {
        itemCardsTable.clear();
        confirmButton.setDisabled(true);
        selectedCard = null;
    }

    public void addChest(IChest chest) {

        if (chest == null) return;
        if (chest.getItems() == null) return;

        for (ILoot loot: chest.getItems()) {
            addItemCard(new ItemCard(loot, skin));
        }
        this.chest = chest;
    }
}