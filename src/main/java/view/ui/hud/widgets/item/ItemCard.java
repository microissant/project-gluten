package view.ui.hud.widgets.item;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import item.loot.ILoot;
import item.loot.UpgradeLoot;

/**
 * The ItemCard class represents a user interface element that displays a single loot item's icon, title, and description.
 * It also allows the user to select or deselect the item card with a click.
 */
public class ItemCard extends Table {
    private final Actor hoverArea;
    private final ILoot loot;
    private boolean isSelected;

    /**
     * Creates an ItemCard for the given loot item with the specified skin.
     *
     * @param loot The loot item associated with this card.
     * @param skin The skin used for the card UI elements.
     */
    public ItemCard(ILoot loot, Skin skin) {
        Image itemIcon = new Image(new TextureRegionDrawable(loot.displayIcon()));
        Label titleLabel = new Label(loot.displayTitle(), skin);
        Label descriptionLabel = new Label(loot.displayDescription(), skin, "h2");
        descriptionLabel.setWrap(true);
        descriptionLabel.setWidth(250);
        descriptionLabel.setAlignment(Align.center);
        isSelected = false;
        this.loot = loot;

        Table contentTable = new Table();
        contentTable.setBackground(skin.newDrawable("white", 0, 0, 0, 0.5f));

        float padding = 20;

        add(contentTable).expand().fill().pad(2);

        contentTable.add(itemIcon).size(64, 64).padBottom(padding).padTop(padding);
        contentTable.row();
        Table textTable = new Table();
        textTable.add(titleLabel).padBottom(padding);
        textTable.row();
        textTable.add(descriptionLabel).width(descriptionLabel.getWidth()).padBottom(padding).row();

        if (loot instanceof UpgradeLoot upgrade) {
            float timeToLive = upgrade.getTimeToLive();
            if (timeToLive > 0) {
                textTable.add(new Label(String.format("Duration: %.1f s", timeToLive), skin, "h3"));
            }
        }

        contentTable.add(textTable).expand().fill();

        // Create a group to fill the entire card
        hoverArea = new Actor();
        addActor(hoverArea);

        hoverArea.addListener(new InputListener() {
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                if (!isSelected) {
                    setBackground(skin.getDrawable("frame-hover"));
                }
            }

            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                if (!isSelected) {
                    setBackground(skin.getDrawable("frame"));
                }
            }

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (isSelected) {
                    isSelected = false;
                    setBackground(skin.getDrawable("frame-hover"));
                } else {
                    isSelected = true;
                    setBackground(skin.newDrawable("frame-selected"));
                }
                return true;
            }
        });
    }

    @Override
    public void layout() {
        super.layout();
        hoverArea.setBounds(0, 0, getWidth(), getHeight());
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public ILoot getLoot() {
        return loot;
    }
}
