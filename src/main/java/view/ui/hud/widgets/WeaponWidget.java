package view.ui.hud.widgets;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import media.MediaManager;
import media.TextureID;

public class WeaponWidget extends Table {

    private final TextureRegion background;
    private final Image weaponIcon;
    private final Label ammoCounter;
    private final float scaleFactor = 3.0f;

    private TextureID currentTextureID;

    public WeaponWidget(Skin skin) {

        currentTextureID = TextureID.BLENDER;
        weaponIcon = new Image(MediaManager.getTexture(currentTextureID).getKeyFrame(0));
        weaponIcon.setScaling(Scaling.fit);

        Label.LabelStyle style = new Label.LabelStyle(skin.getFont("h3"), Color.WHITE);
        ammoCounter = new Label("10", style);
        ammoCounter.setAlignment(Align.right);

        Image crtOverlay = new Image(MediaManager.getTexture(TextureID.AMMO_BACKGROUND_CRT).getKeyFrame(0));
        crtOverlay.setScale(scaleFactor);

        Stack stack = new Stack();
        stack.add(ammoCounter);
        stack.add(crtOverlay);


        background = MediaManager.getTexture(TextureID.AMMO_BACKGROUND).getKeyFrame(0);
        TextureRegionDrawable drawable = new TextureRegionDrawable(background);
        setBackground(drawable);

        float bgWidth = background.getRegionWidth() * scaleFactor;
        float bgHeight = background.getRegionHeight() * scaleFactor;

        // Scale the table size while maintaining the background image proportions
        add(stack).width(bgWidth * 0.2f).padTop(bgHeight * 0.3f).align(Align.left);
        add(weaponIcon).expand().fill().pad(7*scaleFactor);
    }

    @Override
    public float getPrefHeight() {
        return background.getRegionHeight() * scaleFactor;
    }

    @Override
    public float getPrefWidth() {
        return background.getRegionWidth() * scaleFactor;
    }

    /**
     * Update the weapon icon of the weapon widget
     *
     * @param textureID texture id of the weapon icon
     */
    public void updateWeaponIcon(TextureID textureID) {
        if (textureID == null) return;
        if (textureID == currentTextureID) return;

        currentTextureID= textureID;
        TextureRegion newTexture = MediaManager.getTexture(textureID).getKeyFrame(0);
        TextureRegionDrawable newDrawable = new TextureRegionDrawable(newTexture);
        
        weaponIcon.setDrawable(newDrawable);
    }

    /**
     * Update the ammo count displayed on the weapon widget
     *
     * @param amount ammo count
     */
    public void updateAmmoCounter(int amount) {
        ammoCounter.setText(Math.min(amount, 999));
    }


}
