package view.ui.hud.widgets;

import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;

/**an extension of the progressbar that allows for widths that aren't 140*/
public class WorkingProgressBar extends ProgressBar{

    public WorkingProgressBar(float f, float g, float h, boolean b, ProgressBarStyle style) {
        super(f,g,h,b,style);
    }

    private float prefWidth;

    /**
     * Set the preferred width of the progressbar
     * @param prefWidth preferred width
     */
    public void setPrefWidth(float prefWidth) {
        this.prefWidth = prefWidth;
    }

    @Override
    public float getPrefWidth(){
        return prefWidth;
    }
        
}
