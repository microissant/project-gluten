package view.ui.hud.widgets;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

public class TextWithIcon extends Table {

    private final Label text;

    public TextWithIcon(String text, TextureRegion icon, boolean imageToLeft, Skin skin) {

        this.text = new Label(text, skin);
        Image image = new Image(icon);
        image.setSize(100, 100);

        if (imageToLeft) {
            add(this.text).pad(5);
            add(image).pad(5);
        } else {
            add(image).pad(5).size(32, 32);
            add(this.text).pad(5);
        }
    }

    /**
     * Update the displayed text to a new text
     *
     * @param text new text to be used
     */
    public void updateText(String text) {
        this.text.setText(text);
    }
}
