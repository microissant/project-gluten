package view.ui.hud;

import item.Interactable;
import media.TextureID;

/**
 * HUDInformation is a container class for data required to update the in-game HUD.
 * It holds information such as current health, maximum health, time elapsed, weapon
 * name, ammo count, coins, and an interactable target.
 */
public class HUDInformation {

    /** The current health of the player. */
    public int currentHealth;

    /** The maximum health of the player. */
    public int maxHealth;

    /** The time elapsed in the game, in seconds. */
    public double timeElapsed;

    /** The name of the weapon currently equipped by the player. */
    public TextureID weaponIconID;

    /** The remaining ammunition count for the equipped weapon. */
    public int ammoCounter;

    /** The number of coins collected by the player. */
    public int coins;

    /**
     * The interactable target in the game world that the player is currently
     * targeting or interacting with, or null if no target is present.
     */
    public Interactable interactableTarget;


    public double getTimeElapsed() {
        return timeElapsed;
    }

    public int getAmmoCounter() {
        return ammoCounter;
    }

    public int getCoins() {
        return coins;
    }

    public int getCurrentHealth() {
        return currentHealth;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    public Interactable getInteractableTarget() {
        return interactableTarget;
    }

    public TextureID getWeaponIconID() {
        return weaponIconID;
    }
}
