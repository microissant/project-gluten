package view.ui.hud;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import model.event.EventBus;
import view.ui.hud.widgets.InteractPrompt;
import view.ui.hud.widgets.ResourceWidgetStack;
import view.ui.hud.widgets.TimeCounter;
import view.ui.hud.widgets.WeaponWidget;
import view.ui.hud.widgets.upgrades.UpgradeBar;

/**
 * GameHud represents the in-game HUD (Heads Up Display) that provides essential
 * information to the player, such as health, currency, and weapons.
 */
public class GameHud extends Table {

    private final ResourceWidgetStack resources;
    private final WeaponWidget weaponWidget;
    private final TimeCounter timeCounter;
    private final InteractPrompt interactPrompt;
    private final UpgradeBar upgradeBar;

    /**
     * Constructs a new GameHud with the specified skin, label styles, event bus, and stage.
     *
     * @param skin     the skin to use for the UI widgets
     * @param eventBus the event bus for handling events
     * @param stage    the stage to add UI widgets to
     */
    public GameHud(Skin skin,
                   EventBus eventBus,
                   Stage stage) {
        timeCounter = new TimeCounter(1, skin);
        resources = new ResourceWidgetStack(skin);
        weaponWidget = new WeaponWidget(skin);
        interactPrompt = new InteractPrompt(skin);
        upgradeBar = new UpgradeBar(5, skin, eventBus);

        float OUTERPADDING = 20;

        setFillParent(true);
        top();
        add(resources).align(Align.topLeft).pad(OUTERPADDING);
        add().expandX();
        add(upgradeBar).align(Align.topRight).pad(OUTERPADDING);
        add(timeCounter).align(Align.topRight).pad(OUTERPADDING);

        row().expandY(); // Add a new row and expand it vertically
        add(); // Add an empty cell
        add(); // Add an empty cell
        add(); // Add an empty cell
        add(weaponWidget).align(Align.bottomRight).pad(OUTERPADDING);


        interactPrompt.setPosition((Gdx.graphics.getWidth() - interactPrompt.getWidth()) / 2, (float) (Gdx.graphics.getHeight() / 3.0));
        stage.addActor(interactPrompt);

    }

    /**
     * Updates the HUD with the given deltaTime and HUDInformation.
     *
     * @param deltaTime      the time since the last frame update
     * @param hudInformation the information to update the HUD with
     */
    public void update(double deltaTime, HUDInformation hudInformation) {
        if (!this.isVisible()) return;

        resources.updateHealth(hudInformation.getCurrentHealth(), hudInformation.getMaxHealth());
        resources.updateCurrency(hudInformation.getCoins());
        timeCounter.updateTimer(hudInformation.getTimeElapsed());
        weaponWidget.updateAmmoCounter(hudInformation.getAmmoCounter());
        weaponWidget.updateWeaponIcon(hudInformation.getWeaponIconID());
        upgradeBar.update(deltaTime);


        if (hudInformation.getInteractableTarget() == null) {
            interactPrompt.setVisible(false);
        } else {
            interactPrompt.setVisible(true);
            interactPrompt.setInteractable(hudInformation.getInteractableTarget());
        }
    }

    public void resize() {
        interactPrompt.setPosition((Gdx.graphics.getWidth() - interactPrompt.getWidth()) / 2, (float) (Gdx.graphics.getHeight() / 3.0));
    }

    @Override
    public void reset() {
        upgradeBar.reset();
    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        interactPrompt.setVisible(false);
    }
}
