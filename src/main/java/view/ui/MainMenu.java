package view.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import model.event.EventBus;
import view.screens.Gluten;
import view.ui.widgets.ControllerLayout;
import view.ui.widgets.TextButtonPadded;

/**
 * The MainMenu class represents the main menu screen in the game.
 * It provides options for starting a new game, accessing settings, and exiting the game.
 */
public class MainMenu implements Screen {

    private final Stage stage;

    private final Table mainTable;

    private final Group menuGroup;

    private final InputProcessor controller;

    private final Table controllerLayout;

    private final InputMultiplexer inputMultiplexer;

    public MainMenu(Gluten mainScreen,
                    InputProcessor controller,
                    Stage stage,
                    Skin skin,
                    EventBus eventBus){

        this.stage = stage;
        this.controller = controller;
        inputMultiplexer = new InputMultiplexer();

        controllerLayout = new Table();
        controllerLayout.setFillParent(true);
        controllerLayout.add().expandY().row();
        controllerLayout.add(new ControllerLayout(skin)).padBottom(100);

        TextButton newGameButton = new TextButtonPadded("New game", skin, 5);
        TextButton optionsButton = new TextButtonPadded("Options", skin, 5);
        TextButton creditsButton = new TextButtonPadded("Credits", skin, 5);
        TextButton exitButton = new TextButtonPadded("Exit", skin, 5);

        mainTable = new Table();
        mainTable.setFillParent(true);
        mainTable.add(new Label("Buncharted", skin, "h0_bold")).padBottom(50).row();
        mainTable.add(newGameButton).padBottom(10).row();
        mainTable.add(optionsButton).padBottom(10).row();
        mainTable.add(creditsButton).padBottom(10).row();
        mainTable.add(exitButton);

        menuGroup = new Group();
        menuGroup.addActor(mainTable);
        menuGroup.setPosition((stage.getWidth() - mainTable.getWidth()) / 2, (stage.getHeight() - mainTable.getHeight()) / 2);

        stage.addActor(menuGroup);
        stage.addActor(controllerLayout);

        newGameButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mainScreen.setGameView();
            }
        });

        optionsButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mainScreen.setOptionsView();
            }
        });

        creditsButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                mainScreen.setCredits();
            }
        });

        exitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.exit();
            }
        });
    }

    /**
     * Called when this screen becomes the current screen.
     * Sets the input processor to the stage and makes the table visible.
     */
    @Override
    public void show() {

        inputMultiplexer.clear();
        inputMultiplexer.addProcessor(stage);
        inputMultiplexer.addProcessor(controller);
        Gdx.input.setInputProcessor(inputMultiplexer);

        menuGroup.setVisible(true);
        controllerLayout.setVisible(true);
    }

    @Override
    public void render(float v) {
        // Clear the screen
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Draw the stage
        stage.act(v);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height,true);

        menuGroup.setPosition((stage.getWidth() - mainTable.getWidth()) / 2, (stage.getHeight() - mainTable.getHeight()) / 2);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        menuGroup.setVisible(false);
        controllerLayout.setVisible(false);
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

}


