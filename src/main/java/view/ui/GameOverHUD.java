package view.ui;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import model.event.EventBus;
import model.event.events.game.MainMenuActiveEvent;
import model.event.events.game.RestartLevelEvent;
import model.state.Stat;
import util.Pair;

import java.util.ArrayList;
import java.util.List;

/**
 * The GameOverHUD class represents the Game Over screen displayed when the player fails a mission.
 * It shows a summary of the mission statistics, along with buttons to restart the level or return
 * to the main menu. The class also handles animations and transitions for the Game Over screen.
 */
public class GameOverHUD extends Group {

    private final Skin skin;
    private final Stage stage;

    private final Label titleLabel;
    private final Label missionSummaryLabel;
    private final TextButton restartButton;
    private final TextButton quitButton;
    private final Table table;

    private final Table statTable;

    private final List<Pair<Label>> statRows;

    public GameOverHUD(Skin skin, Stage stage, EventBus eventBus) {

        statRows = new ArrayList<>();

        this.skin = skin;
        this.stage = stage;

        titleLabel = new Label("Mission Failed", skin);
        missionSummaryLabel = new Label("Mission summary", skin);
        restartButton = new TextButton("Restart", skin);
        quitButton = new TextButton("Main menu", skin);

        Table buttonTable = new Table();
        buttonTable.add(restartButton).padRight(20);
        buttonTable.add(quitButton);

        statTable = new Table();

        table = new Table().pad(40);
        table.setTransform(true);
        stage.addActor(table);

        table.add(titleLabel).padBottom(20).row();
        table.add(missionSummaryLabel).padBottom(20).row();
        table.add(statTable).padBottom(20).row();
        table.add(buttonTable);

        table.pack();
        table.setPosition((stage.getWidth() - table.getWidth()) / 2, (stage.getHeight() - table.getHeight()) / 2);

        Pixmap bgPixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        bgPixmap.setColor(0, 0, 0, 0.5f);
        bgPixmap.fill();
        TextureRegionDrawable backgroundFrame  = new TextureRegionDrawable(new TextureRegion(new Texture(bgPixmap)));
        table.setBackground(backgroundFrame);

        addActor(table);

        restartButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                eventBus.post(new RestartLevelEvent());
            }
        });

        quitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                // Quit the game or return to the main menu
                eventBus.post(new MainMenuActiveEvent());
            }
        });
    }

    /**
     * Displays the Game Over screen with the provided mission statistics.
     *
     * @param stats A list of Stat objects representing mission statistics.
     */
    public void show(List<Stat> stats) {
        addStats(stats);
        startAnimations();
    }

    private void addStats(List<Stat> stats) {
        statTable.clearChildren();
        statRows.clear();
        for (Stat stat: stats) {
            Label statTitle = new Label(stat.name(), skin);
            Label statValue = new Label(stat.value(), skin);

            statTable.add(statTitle).left().padBottom(5);
            statTable.add(statValue).right().padBottom(5).row();
            statRows.add(new Pair<>(statTitle, statValue));
        }
        table.pack();
        table.setPosition((stage.getWidth() - table.getWidth()) / 2, (stage.getHeight() - table.getHeight()) / 2);
    }

    public void startAnimations() {

        float duration = 1f;

        table.getColor().a = 0;
        table.addAction(Actions.fadeIn(duration));

        titleLabel.addAction(Actions.sequence(
                Actions.alpha(0),
                Actions.moveTo(titleLabel.getX(), titleLabel.getY() + 40, duration),
                Actions.parallel(
                        Actions.fadeIn(duration),
                        Actions.moveTo(titleLabel.getX(), titleLabel.getY(), duration)
                )
        ));

        missionSummaryLabel.addAction(Actions.sequence(
                Actions.alpha(0),
                Actions.moveTo(missionSummaryLabel.getX(), missionSummaryLabel.getY() + 40, duration),
                Actions.delay(duration),
                Actions.parallel(
                        Actions.fadeIn(duration),
                        Actions.moveTo(missionSummaryLabel.getX(), missionSummaryLabel.getY(), duration)
                )
        ));

        float statDuration = duration / 2;
        int i = 0;
        for (Pair<Label> label : statRows) {
            label.a().addAction(Actions.sequence(
                    Actions.alpha(0),
                    Actions.moveBy(0, 40, statDuration),
                    Actions.delay(duration * 3 + statDuration * i),
                    Actions.parallel(
                            Actions.fadeIn(statDuration),
                            Actions.moveBy(0, -40, statDuration)
                    )));
            label.b().addAction(Actions.sequence(
                    Actions.alpha(0),
                    Actions.moveBy(0, 40, statDuration),
                    Actions.delay(duration * 3 + statDuration * i),
                    Actions.parallel(
                            Actions.fadeIn(statDuration),
                            Actions.moveBy(0, -40, statDuration)
                    )));
            i++;
        }

        restartButton.addAction(Actions.sequence(
                Actions.alpha(0),
                Actions.delay(duration * 3 + statDuration * (i + 1)),
                Actions.parallel(
                        Actions.fadeIn(duration/2)
                )
        ));

        quitButton.addAction(Actions.sequence(
                Actions.alpha(0),
                Actions.delay(duration * 3 + statDuration * (i + 1)),
                Actions.parallel(
                        Actions.fadeIn(duration)
                )
        ));
    }

    public void update(float delta) {
        stage.act(delta);
    }

    public void dispose() {
        skin.dispose();
    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        table.setVisible(visible);
    }
}
