package view.ui;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import model.event.EventBus;
import model.event.events.game.MainMenuActiveEvent;
import model.event.events.game.RestartLevelEvent;
import model.event.events.input.InputESC;
import view.ui.widgets.TextButtonPadded;

/**
 * The PauseGameUI class represents the user interface for the pause menu in the game.
 * It provides options for resuming the game, restarting the mission, and quitting to the main menu.
 */
public class PauseGameUI extends Table {
    private final Skin skin;
    private final Stage stage;
    private final Label titleLabel;
    private final TextButton resumeButton;
    private final TextButton restartButton;
    private final TextButton quitButton;

    public PauseGameUI(Skin skin, Stage stage, EventBus eventBus) {

        this.skin = skin;
        this.stage = stage;

        titleLabel = new Label("Paused", skin, "h1_bold");
        resumeButton = new TextButtonPadded("Resume game", skin);
        restartButton = new TextButtonPadded("Restart mission", skin);
        quitButton = new TextButtonPadded("Quit to main menu", skin);

        setFillParent(true);
//        setTransform(true);

        add(titleLabel).padBottom(20).row();
        add(resumeButton).padBottom(20).row();
        add(restartButton).padBottom(20).row();
        add(quitButton);


        Pixmap bgPixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        bgPixmap.setColor(0, 0, 0, 0.5f);
        bgPixmap.fill();
        TextureRegionDrawable backgroundFrame = new TextureRegionDrawable(new TextureRegion(new Texture(bgPixmap)));
        setBackground(backgroundFrame);

        resumeButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                eventBus.post(new InputESC());
            }
        });

        restartButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                eventBus.post(new RestartLevelEvent());
            }
        });

        quitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                eventBus.post(new MainMenuActiveEvent());
            }
        });
    }

    /**
     * Starts animations for the pause menu elements.
     * Fade-in animations are applied to the table, titleLabel, resumeButton, restartButton, and quitButton.
     */
    public void startAnimations() {

        float duration = 0.1f;

        getColor().a = 0;
        addAction(Actions.fadeIn(duration));

        titleLabel.addAction(Actions.sequence(
                Actions.alpha(0),
                Actions.moveTo(titleLabel.getX(), titleLabel.getY() + 20, duration),
                Actions.parallel(
                        Actions.fadeIn(duration),
                        Actions.moveTo(titleLabel.getX(), titleLabel.getY(), duration)
                )
        ));

        resumeButton.addAction(Actions.sequence(
                Actions.alpha(0),
                Actions.moveTo(resumeButton.getX(), resumeButton.getY() + 20, duration),
                Actions.parallel(
                        Actions.fadeIn(duration),
                        Actions.moveTo(resumeButton.getX(), resumeButton.getY(), duration)
                )
        ));

        restartButton.addAction(Actions.sequence(
                Actions.alpha(0),
                Actions.moveTo(restartButton.getX(), restartButton.getY() + 20, duration),
                Actions.parallel(
                        Actions.fadeIn(duration),
                        Actions.moveTo(restartButton.getX(), restartButton.getY(), duration)
                )
        ));


        quitButton.addAction(Actions.sequence(
                Actions.alpha(0),
                Actions.moveTo(quitButton.getX(), quitButton.getY() + 20, duration),
                Actions.parallel(
                        Actions.fadeIn(duration),
                        Actions.moveTo(quitButton.getX(), quitButton.getY(), duration)
                )
        ));
    }

    public void dispose() {
        skin.dispose();
        stage.dispose();
    }
}
