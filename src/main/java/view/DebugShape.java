package view;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public interface DebugShape {

    default void drawDebug(ShapeRenderer renderer) { }

}
