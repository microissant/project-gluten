package entities.navigation.pathfinder;

import com.badlogic.gdx.math.Vector2;

import java.util.LinkedList;

/**
 * The IPathfinder interface defines the structure for pathfinding algorithms in the navigation system.
 * Classes implementing this interface should provide a findPath method that takes the start and end
 * positions on a grid and returns the shortest path as a LinkedList of Vector2 objects.
 */
public interface IPathfinder {

    /**
     * Finds the shortest path between the start and end positions on the grid.
     *
     * @param startPosition the starting position of the path as a Vector2
     * @param endPosition   the ending position of the path as a Vector2
     * @return a LinkedList of Vector2 objects representing the path, or null if no path is found
     */
    LinkedList<Vector2> findPath(Vector2 startPosition, Vector2 endPosition);
}
