package entities.navigation.pathfinder;

import app.global.Statistics;
import com.badlogic.gdx.math.Vector2;
import grid.GridCoordinate;
import grid.TileData.TileRuleset;
import grid.WorldGrid;

import java.util.*;

/**
 * PathAStar is an implementation of the A* pathfinding algorithm for finding the shortest path
 * between two points on a grid. It is a subclass of the IPathfinder interface.
 * <p>
 * The A* algorithm is a best-first search algorithm that uses a heuristic to guide the search.
 * It is widely used in pathfinding and graph traversal because of its performance and accuracy.
 * </p>
 * This implementation uses a WorldGrid to represent the grid and GridCoordinate for grid positions.
 */
public class PathAStar implements IPathfinder {
    private final WorldGrid grid;
    private final HashMap<GridCoordinate, GridCoordinate> cellParent;

    private final HashMap<GridCoordinate, Float> fScore;
    private final HashMap<GridCoordinate, Float> gScore;

    private final Neighbours[] neighbours;
    private final GridCoordinate[][] gridCells;
    private final TileRuleset ruleset;

    private long nodeVisitedCount;

    public PathAStar(WorldGrid grid, TileRuleset ruleset) {
        this.grid = grid;
        cellParent = new HashMap<>();
        fScore = new HashMap<>();
        gScore = new HashMap<>();
        neighbours = Neighbours.allValues();
        gridCells = grid.getPreAllocatedGridCells();
        this.ruleset =ruleset;
    }

    /**
     * Finds the shortest path between the start and end positions on the grid using the A* algorithm.
     * A* is an informed search algorithm that uses a heuristic function to estimate the cost from
     * the current node to the goal node. It explores the most promising nodes first, based on the
     * sum of the path cost and the heuristic cost.
     * <p>
     * Algorithm steps:
     * <ol>
     * <li>Initialize open and closed sets, gScore, fScore, and the start node's gScore and fScore.</li>
     * <li>Loop until the open set is empty or the goal is reached:
     *   <ol type="a">
     *     <li>Select the node with the lowest fScore from the open set as the current node.</li>
     *     <li>If the current node is the goal node, reconstruct and return the path.</li>
     *     <li>Remove the current node from the open set and add it to the closed set.</li>
     *     <li>For each valid neighbor of the current node:
     *       <ol type="i">
     *         <li>If the neighbor is in the closed set, skip it.</li>
     *         <li>Calculate the tentative gScore for the neighbor.</li>
     *         <li>If the neighbor is not in the open set, update its gScore, fScore, and parent,
     *             and add it to the open set.</li>
     *         <li>If the neighbor is already in the open set and the tentative gScore is better
     *             (lower) than its current gScore, update its gScore, fScore, and parent.</li>
     *       </ol>
     *     </li>
     *   </ol>
     * </li>
     * <li>If the loop ends without finding the goal, return null, as no valid path exists.</li>
     * </ol>
     *
     * @param startPosition the starting position of the path as a Vector2
     * @param endPosition   the ending position of the path as a Vector2
     * @return a LinkedList of Vector2 objects representing the path, or null if no path is found
     */
    public LinkedList<Vector2> findPath(Vector2 startPosition, Vector2 endPosition) {
        cellParent.clear();
        fScore.clear();
        gScore.clear();
        HashSet<GridCoordinate> visited = new HashSet<>();
        PriorityQueue<GridCoordinate> toBeSearched = new PriorityQueue<>((o1, o2) -> Float.compare(fScore.get(o1), fScore.get(o2)));
        HashSet<GridCoordinate> toBeSearchedLookup = new HashSet<>();

        GridCoordinate current = grid.getGridCoordinate(startPosition);
        GridCoordinate endCoordinate = grid.getGridCoordinate(endPosition);

        toBeSearched.add(current);
        toBeSearchedLookup.add(current);

        gScore.put(current, 0.0f);
        fScore.put(current, hCost(current, endCoordinate));

        nodeVisitedCount = 0;

        while (!toBeSearched.isEmpty()) {
            current = toBeSearched.poll();
            nodeVisitedCount ++;

            if (current.equals(endCoordinate)) return reconstructPath(current);

            visited.add(current);

            for (Neighbours neighbour : neighbours) {
                GridCoordinate neighbourCell = gridCells[current.row + neighbour.y][current.col + neighbour.x];

                if (!grid.coordinateIsOnGrid(neighbourCell)) continue;
                if (grid.checkForTileProperty(neighbourCell.row, neighbourCell.col, ruleset.val)) continue;
                if (visited.contains(neighbourCell)) continue;

                float temporaryGScore = gScore.get(current) + neighbour.cost;

                if (!toBeSearchedLookup.contains(neighbourCell) || temporaryGScore < gScore.get(neighbourCell)) {
                    cellParent.put(neighbourCell, current);
                    gScore.put(neighbourCell, temporaryGScore);
                    fScore.put(neighbourCell, gScore.get(neighbourCell) + hCost(neighbourCell, endCoordinate));

                    if (!toBeSearchedLookup.contains(neighbourCell)) {
                        toBeSearched.add(neighbourCell);
                        toBeSearchedLookup.add(neighbourCell);
                    }
                }
            }
        }

        return reconstructPath(Collections.min(fScore.entrySet(), Map.Entry.comparingByValue()).getKey());
    }

    /**
     * Reconstructs the path from the end position to the start position by following the parent pointers.
     * The path is stored as a LinkedList of Vector2 objects, with each element representing a position
     * in the grid.
     *
     * @param current the final GridCoordinate in the path
     * @return a LinkedList of Vector2 objects representing the path from the start to the end position
     */
    private LinkedList<Vector2> reconstructPath(GridCoordinate current) {
        LinkedList<Vector2> totalPath = new LinkedList<>();
        while (current != null) {
            totalPath.add(grid.getCellCenteredWorldPosition(current));
            current = cellParent.get(current);
        }
        Statistics.addPathCalculationsCount(nodeVisitedCount);
        return totalPath;
    }

    /**
     * Calculates the heuristic cost of a cell based on its current position and the target position.
     * This heuristic cost is an estimation of the cost from the current cell to the target cell.
     * In this implementation, the heuristic cost is the squared Euclidean distance between the two cells.
     *
     * @param current the current GridCoordinate
     * @param endNode the target GridCoordinate
     * @return the heuristic cost as a Float
     */
    private float hCost(GridCoordinate current, GridCoordinate endNode) {
        float x = endNode.row - current.row;
        float y = endNode.col - current.col;

        return x * x + y * y;
    }
}
