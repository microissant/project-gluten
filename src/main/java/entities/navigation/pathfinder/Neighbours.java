package entities.navigation.pathfinder;

public enum Neighbours {
    NORTH(0, -1, 2.0f),
    NORTHEAST(1, -1, 2.8284f),
    EAST(1, 0, 2.0f),
    SOUTHEAST(1, 1, 2.8284f),
    SOUTH(0, 1, 2.0f),
    SOUTHWEST(-1, 1, 2.8284f),
    WEST(-1, 0, 2.0f),
    NORTHWEST(-1, -1, 2.8284f);

    public final int x, y;
    public final float cost;
    private static final Neighbours[] allValues;

    static {
        allValues = values();
    }

    Neighbours(int x, int y, float cost) {
        this.x = x;
        this.y = y;
        this.cost = cost;
    }

    /**
     * @return all neighbours cells, with their delta row, delta col, and their distance (cost)
     */
    public static Neighbours[] allValues() {
        return allValues;
    }
}
