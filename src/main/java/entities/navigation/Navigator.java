package entities.navigation;

import com.badlogic.gdx.math.Vector2;
import entities.navigation.pathfinder.IPathfinder;
import entities.navigation.pathfinder.PathAStar;
import grid.GridCoordinate;
import grid.TileData.TileRuleset;
import grid.WorldGrid;
import model.IUpdatable;

import java.util.LinkedList;

/**
 * The Navigator class is responsible for finding and updating the path between the start and target positions
 * on a grid, using a pathfinding algorithm implemented by the IPathfinder interface.
 * It also adjusts the pathfinding update frequency based on the distance to the target.
 */
public class Navigator implements IUpdatable {
    private IPathfinder pathfinder;
    private float timeBetweenPathUpdates = 0.25f;
    private float timeSinceLastPathfind = 0.0f;
    private final Vector2 startPosition;
    private Vector2 targetPosition;
    private GridCoordinate previousCoordinate;
    private WorldGrid worldGrid;
    private LinkedList<Vector2> path;

    /**
     * Constructs a new Navigator with the specified start position.
     *
     * @param startPosition the starting position of the path as a Vector2
     */
    public Navigator(Vector2 startPosition) {
        this.startPosition = startPosition;
    }

    @Override
    public void update(double deltaTime) {
        if (worldGrid == null) return;
        if (pathfinder == null) return;
        if (targetPosition == null) return;

        adjustPathFindFrequency();

        timeSinceLastPathfind += deltaTime;

        if (timeSinceLastPathfind < timeBetweenPathUpdates) return;
        if (targetSameCell()) return;

        findPath();
    }

    private void adjustPathFindFrequency() {

        float dist2 = startPosition.dst2(targetPosition);

        if (dist2 < 6400) timeBetweenPathUpdates = 0.25f; // less than 5 cells away
        else if (dist2 < 25000) timeBetweenPathUpdates = 1f; // less than 10 cells away
        else if (dist2 < 102400) timeBetweenPathUpdates = 2.5f; // less than 20 cells away
        else if (dist2 < 409600) timeBetweenPathUpdates = 5f; // less than 40 cells away
        else timeBetweenPathUpdates = 10f; // over 40 cells away
    }

    /**
     * Returns the current path as a LinkedList of Vector2 objects.
     *
     * @return the path as a LinkedList of Vector2 objects
     */
    public LinkedList<Vector2> getPath() {
        return path;
    }

    private boolean targetSameCell() {
        if (targetPosition == null) return false;

        GridCoordinate targetCoordinate = worldGrid.getGridCoordinate(targetPosition);

        if (previousCoordinate == null) {
            previousCoordinate = targetCoordinate;
            return false;
        }

        boolean isSameCell = targetCoordinate.equals(previousCoordinate);
        if (!isSameCell) {
            previousCoordinate = targetCoordinate;
        }

        return isSameCell;
    }

    /**
     * Sets the target position and initiates the pathfinding process.
     *
     * @param targetPosition the target position as a Vector2
     */
    public void setTarget(Vector2 targetPosition) {
        this.targetPosition = targetPosition;
        findPath();
    }

    private void findPath() {
        if (pathfinder == null) return;
        path = pathfinder.findPath(startPosition, targetPosition);
        if (path != null) {
            path.removeLast();
        }
        timeSinceLastPathfind = 0.0f;
    }

    /**
     * Sets the WorldGrid object and initializes the pathfinder with the specified TileRuleset.
     *
     * @param worldGrid the WorldGrid object to use for pathfinding
     * @param ruleset   the TileRuleset to use for pathfinding
     */
    public void setWorldGrid(WorldGrid worldGrid,TileRuleset ruleset) {
        this.worldGrid = worldGrid;
        pathfinder = new PathAStar(worldGrid,ruleset);
    }

}