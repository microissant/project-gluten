package entities.player;

import app.Globals;
import com.badlogic.gdx.math.Vector2;
import components.currency.CurrencyManager;
import components.currency.ITrader;
import components.currency.coin.Coin;
import components.health.HealthManager;
import components.health.IHealable;
import components.physics.Physics;
import components.physics.PhysicsBody;
import components.upgrade.IUpgradeManager;
import components.upgrade.UpgradeManager;
import components.upgrade.upgrades.IUpgrade;
import components.weapon.Weapon;
import components.weapon.WeaponHolder;
import components.weapon.WeaponManager;
import entities.particles.ParticleTypes;
import item.loot.ILooter;
import model.IUpdatable;
import model.event.EventBus;
import model.event.Subscribe;
import model.event.events.AddParticleEvent;
import model.event.events.entities.PlayerDeathEvent;
import model.event.events.input.InputPrimary;
import model.event.events.item.ObtainLootEvent;
import model.event.events.player.SwapWeaponsEvent;
import model.event.events.ui.InitializeHUDEvent;
import model.event.events.upgrades.UpgradeAddedEvent;
import model.event.events.upgrades.UpgradeRemovedEvent;

public class PlayerManager implements IUpdatable, WeaponHolder, ILooter {

    private final PlayerCharacter character;
    private final HealthManager healthManager;
    private final WeaponManager weaponManager;
    private final IUpgradeManager upgradeManager;
    private final CurrencyManager currencyManager;
    private final Vector2 aimDirection;

    private final EventBus eventBus;
    private boolean isDead;



    public PlayerManager(Vector2 position, int startHealth, EventBus eventBus) {
        this.eventBus = eventBus;
        aimDirection = new Vector2(0, 0);
        Physics physics = new Physics(position);
        weaponManager = new WeaponManager(physics.getPosition(), aimDirection, eventBus);
        healthManager = new HealthManager(startHealth);
        healthManager.increaseMaximumHealth(5);
        character = new PlayerCharacter(physics, this, eventBus);
        currencyManager = new CurrencyManager();
        currencyManager.deposit(new Coin(10));
        upgradeManager = new UpgradeManager(
                healthManager,
                physics,
                weaponManager,
                currencyManager,
                eventBus);
        upgradeManager.setUpgradableBehavior(character.getBehaviorCollection());

        if (eventBus == null) return;
        eventBus.register(this);
        eventBus.register(character);
        isDead = character.isDead();
    }

    @Override
    public IHealable getHealable() {
        return healthManager;
    }

    public PlayerManager(Vector2 position, int startHealth) {
        this(position, startHealth, null);
    }

    /**
     * @return physics object of the player
     */
    public PhysicsBody getPhysicsBody() {
        return character;
    }

    /**
     * @return health manager of the player, responsible for all health.
     */
    protected HealthManager getHealthManager() {
        return healthManager;
    }

    /**
     * @return the character of the player, responsible for movement and graphics
     */
    public PlayerCharacter getPlayerCharacter() {
        return character;
    }

    /**
     * @return the weapon manager of the player, responsible for handling weapons
     */
    public WeaponManager getWeaponManager() {
        return weaponManager;
    }

    public Coin getCoinBalance() {
        return currencyManager.getBalance();
    }

    @Override
    public void update(double deltaTime) {
        if (character.isDead()) {
            justDied();
        }

        weaponManager.update(deltaTime);
        character.update(deltaTime);
        upgradeManager.update(deltaTime);
    }

    private void justDied() {
        if (isDead) return;
        eventBus.post(new AddParticleEvent(ParticleTypes.EXPLOSION_V2,getPlayerCharacter().getPosition()));
        eventBus.post(new PlayerDeathEvent(this));
        isDead = true;
    }

    @Override
    public void attack() {
        if (weaponManager == null) return;

        updateTargetDirection();
        weaponManager.fireWeapon();
    }

    @Override
    public void updateTargetDirection() {
        float aimOriginX = getPlayerCharacter().getPosition().x;
        float aimOriginY = getPlayerCharacter().getPosition().y;

        Weapon equippedWeapon = weaponManager.getPrimary();
        float aimDirX = 0;
        float aimDirY = 0;
        if (equippedWeapon != null) {

            aimDirX = Globals.mouseWorldPosition.x - equippedWeapon.getBarrelPosX();
            aimDirY = Globals.mouseWorldPosition.y - equippedWeapon.getBarrelPosY();

            if (aimDirX * aimDirX + aimDirY * aimDirY < 20*20) {
                aimDirX = Globals.mouseWorldPosition.x - aimOriginX;
                aimDirY = Globals.mouseWorldPosition.y - aimOriginY;
            }
        }

        switch (Globals.currentAimMode) {
            case Mouse -> {
                aimDirection.x = aimDirX;
                aimDirection.y = aimDirY;
            }
            case Keyboard -> {
                aimDirection.x = Globals.keyAimDirection.x;
                aimDirection.y = Globals.keyAimDirection.y;
            }
        }
    }

    @Override
    public void addWeapon(Weapon weapon) {
        if (weaponManager == null) return;
        weaponManager.addWeapon(weapon);
    }

    @Override
    public boolean addUpgrade(IUpgrade upgrade) {
        if (eventBus == null) return false;

        boolean hasBeenApplied = upgradeManager.addUpgrade(upgrade);
        if (!hasBeenApplied) return false;

        eventBus.post(new UpgradeAddedEvent(upgrade));
        return true;
    }

    @Override
    public void removeUpgrade(IUpgrade upgrade) {
        if (eventBus != null) {
            eventBus.post(new UpgradeRemovedEvent(upgrade));
        }

        upgradeManager.removeUpgrade(upgrade);
    }

    @Override
    public void removeAllUpgrades() {
        upgradeManager.removeAll();
    }


    @Subscribe
    public void inputPrimary(InputPrimary event) {
        attack();
    }

    @Subscribe
    public void obtainLoot(ObtainLootEvent event) {
        if (event == null) return;
        if (event.loot() == null) return;

        event.loot().obtain(this);
    }

    @Subscribe
    public void initializeHUD(InitializeHUDEvent event) {
        if (eventBus == null) return;
        for (IUpgrade upgrade: upgradeManager) {
            eventBus.post(new UpgradeAddedEvent(upgrade));
        }
    }

    @Subscribe
    public void swapWeapons(SwapWeaponsEvent event) {
        weaponManager.swapWeapons();
    }

    public EventBus getEventBus(){
        return eventBus;
    }

    @Override
    public ITrader getTrader() {
        return currencyManager;
    }

    /**
     * Remove the entity from the eventbus. After it won't receive any subscriber event calls
     */
    public void unregisterFromEventBus() {
        eventBus.unregister(this);
        character.unregisterFromEventBus();
    }
}
