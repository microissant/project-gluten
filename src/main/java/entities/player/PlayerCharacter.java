package entities.player;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType;
import components.collision.ICollidable;
import components.collision.primitives.CollisionCircle;
import components.currency.ITrader;
import components.health.*;
import components.physics.Physics;
import components.physics.PhysicsBody;
import components.physics.reaction.PlayerReaction;
import components.upgrade.behavior.BehaviorCollection;
import components.upgrade.behavior.BehaviorID;
import components.upgrade.upgrades.IUpgrade;
import components.weapon.WeaponHolder;
import entities.Entity;
import grid.TileData;
import grid.WorldGrid;
import item.InteractAgent;
import item.Interactable;
import media.DrawProperties;
import media.SoundID;
import media.TextureID;
import model.event.EventBus;
import model.event.Subscribe;
import model.event.events.DodgeStartEvent;
import model.event.events.audio.PlaySoundEvent;
import model.event.events.audio.StopSoundEvent;
import model.event.events.input.InputDodge;
import model.event.events.input.InputInteract;
import model.event.events.input.InputMove;
import model.event.events.player.DodgeInDirectionEvent;
import util.CountdownTimer;
import util.DoOnce;
import view.screens.Gluten;

import java.util.HashSet;
import java.util.Set;


public class PlayerCharacter implements Entity, PhysicsBody, IDamageable, InteractAgent {

    private final Physics physics;
    private final PlayerManager playerManager;
    private final CollisionCircle collisionCircle;
    private final DrawProperties drawProperties;
    private final BehaviorCollection behaviorCollection;
    private PState playerState;
    private Vector2 dodgeDirection;
    private double timeSinceDodgeStart=0;
    private static final double dodgeDuration = 0.41;
    private double timeSinceFallStart = 0;
    private static final double fallDuration = 0.4;
    private final Vector2 lastSafePosition = new Vector2(0,0);
    private boolean canBeHit = true;
    private final CountdownTimer invincibilityTimer;
    private Damage damageTaken;

    private final DoOnce footStepsTrigger;

    private Interactable interactableTarget;

    private final EventBus eventBus;

    private final CountdownTimer hitFlashTimer;
    private float hitFlashIntensity;

    public PlayerCharacter(Physics physics, PlayerManager playerManager, EventBus eventBus) {
        if (physics == null) physics = new Physics();

        this.physics = physics;
        physics.setAccelerationForce(100);
        physics.setMaxSpeed(100);
        physics.enableInstantMovement();
        physics.setBoxCollision(6,2,5,5);
        physics.setReaction(new PlayerReaction());
        playerState = PState.NORMAL;
        drawProperties = new DrawProperties(TextureID.ROBOCHEF_IDLE);
        this.playerManager = playerManager;
        behaviorCollection = new BehaviorCollection(this);
        invincibilityTimer = new CountdownTimer(0.5f, this::disableInvincibility);

        footStepsTrigger = new DoOnce(this::startedMoving, this::stoppedMoving);

        hitFlashTimer = new CountdownTimer(0.15f, this::disableHitEffect);
        hitFlashIntensity = 0.0f;

        collisionCircle = new CollisionCircle(
                physics.getPosition(),
                new Vector2(0, drawProperties.height / 2),
                10,
                this,
                new HashSet<>(),
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.PLAYER)
                ))
        );

        this.eventBus = eventBus;
        if (eventBus == null) return;
        eventBus.register(this);
    }

    @Override
    public void setInteractTarget(Interactable interactableTarget) {
        this.interactableTarget = interactableTarget;
    }

    @Override
    public Interactable getInteractTarget() {
        return interactableTarget;
    }

    @Override
    public void removeInteractTarget() {
        interactableTarget = null;
    }

    @Override
    public InteractAgent getInteractAgent() {
        return this;
    }

    @Override
    public IHealable getHealable() {
        return playerManager.getHealable();
    }

    @Override
    public boolean addUpgrade(IUpgrade upgrade) {
        return playerManager.addUpgrade(upgrade);
    }

    @Override
    public void removeUpgrade(IUpgrade upgrade) {
        playerManager.removeUpgrade(upgrade);
    }

    @Override
    public WeaponHolder getWeaponHolder() {
        return playerManager;
    }

    @Override
    public void removeAllUpgrades() {
        playerManager.removeAllUpgrades();
    }

    private enum PState {
        NORMAL((character, dt) -> {
            //set the animation to running if the character is moving
            if(character.physics.getVelocity().len2()!=0) character.setMoveAnimation();
            else character.setIdleAnimation();

            character.lastSafePosition.set(character.physics.getPosition());
        }),
        DODGING((character, dt) -> {
            //move in the dodgedirection
            character.move(character.dodgeDirection);
            character.timeSinceDodgeStart+=dt;
            if(character.timeSinceDodgeStart>= dodgeDuration * 0.5f) character.canBeHit = true;

            //end the dodge if the time runs out
            if(character.timeSinceDodgeStart>= dodgeDuration){
                character.endDodge();
            }

            //check if the player is going to fall
            if(character.timeSinceDodgeStart / dodgeDuration >= 0.8){
                WorldGrid grid  = character.physics.getWorldGrid();
                if(grid != null){
                    Vector2 pos = character.physics.getPosition();
                    if(grid.checkForTilePropertyAtPixel(pos.x,pos.y,TileData.TileProperty.HOLE.val)){
                        character.endDodge();
                        character.setFallingState();
                    }
                }
            }

        }),
        FALLING((character, dt) -> {
            DrawProperties props = character.getProperties();
            character.timeSinceFallStart+=dt;
            float fallFactor = (float) (1 - (character.timeSinceFallStart / fallDuration));
            props.rotation  = (float) (360.0  * fallFactor);
            props.scaleX = fallFactor;
            props.scaleY = fallFactor;

            if(character.timeSinceFallStart >= fallDuration){
                character.endFallingState();
            }

        });

        private final playerUpdater updater;
        PState(playerUpdater updater){
            this.updater = updater;
        }

    }

    private interface playerUpdater {
        void update(PlayerCharacter character, double dt);
    }

    private void disableHitEffect() {
        hitFlashIntensity = 0.0f;
    }

    @Override
    public Vector2 getPosition() {
        return physics.getPosition();
    }

    @Override
    public void push(Vector2 dir) {
        physics.applyForce(dir);
    }

    @Override
    public void move(Vector2 dir) {
        if (isDead()) return;

        physics.accelerate(dir);
        footStepsTrigger.execute();

        if (playerManager.getWeaponManager().getPrimary() == null) return;
        playerManager.getWeaponManager().getPrimary().setMoveDirection(dir);
    }

    private void startedMoving() {
        if (eventBus == null) return;
        eventBus.post(new PlaySoundEvent(SoundID.PLAYER_FOOTSTEP, true));
    }

    private void stoppedMoving() {
        if (eventBus == null) return;
        eventBus.post(new StopSoundEvent(SoundID.PLAYER_FOOTSTEP));
    }

    @Subscribe
    public void movePressed(InputMove event) {
        if (event == null) return;
        if(playerState != PState.NORMAL)return;
        move(event.direction());
    }

    @Subscribe
    public void tryDodge(InputDodge event){
        if (isDodging()) return;
        if (physics.getVelocity().isZero()) return;

        doDodge(physics.getVelocity().cpy());
    }

    @Subscribe
    public void dodgeDirection(DodgeInDirectionEvent event) {
        if (event == null) return;
        if (event.direction() == null) return;
        if (isDead()) return;
        if (isDodging()) return;
        doDodge(event.direction());
    }

    @Subscribe
    public void interactEvent(InputInteract event) {
        if (event == null) return;
        if (interactableTarget == null) return;
        interactableTarget.onInteract(this);
    }

    /**returns true if the player is currently dodging */
    public boolean isDodging(){
        return playerState  == PState.DODGING;
    }

    

    private void doDodge(Vector2 direction){
        playerState = PState.DODGING;
        canBeHit = false;

        dodgeDirection = direction;
        timeSinceDodgeStart = 0.0;

        //allow player to slide over holes
        physics.setRuleset(TileData.TileRuleset.SLIDING);

        float angle = dodgeDirection.angleDeg();


        TextureID slideTexture = TextureID.ROBOCHEF_SLIDE_SIDE;
        if(angle>225 && angle < 315) slideTexture = TextureID.ROBOCHEF_SLIDE_DOWN;
        drawProperties.setAnimation(slideTexture);

        physics.setMaxSpeed(160);

        playerManager.getEventBus().post(new DodgeStartEvent());
        footStepsTrigger.reset();

        getHealthComponent().enableImmunity();
        invincibilityTimer.restart();
        behaviorCollection.execute(BehaviorID.DASH);
    }

    @Override
    public void update(double dt) {
        physics.update(dt);
        //flip if moving left
        if(physics.getVelocity().x!=0) drawProperties.scaleX = Math.signum(physics.getVelocity().x);

        updateHitEffect(dt);

        if (physics.isStationary()) footStepsTrigger.reset();

        //update based on the current player state
        playerState.updater.update(this, dt);
        behaviorCollection.update(dt);
        invincibilityTimer.update(dt);
    }

    private void updateHitEffect(double dt) {
        if (!hitFlashTimer.isActive()) return;

        hitFlashTimer.update(dt);
        if (!hitFlashTimer.isComplete()) return;

        hitFlashIntensity = 0.0f;
        hitFlashTimer.reset();
    }

    private void endDodge(){
        physics.setMaxSpeed(100);
        setNormalState();
        physics.setRuleset(TileData.TileRuleset.WALKING);
    }

    private void setNormalState(){
        playerState = PState.NORMAL;
        setIdleAnimation();
    }
    private void setFallingState(){
        
        timeSinceFallStart = 0;
        physics.stopMovement();
        playerState = PState.FALLING;
    }

    private void endFallingState(){
        playerState = PState.NORMAL;
        drawProperties.rotation = 0;
        drawProperties.scaleX = 1;
        drawProperties.scaleY = 1;
        physics.getPosition().set(lastSafePosition);
    }

    private void setMoveAnimation(){
        if(drawProperties.animationId == TextureID.ROBOCHEF_RUN)return;
        drawProperties.setAnimation(TextureID.ROBOCHEF_RUN);
    }
    private void setIdleAnimation(){
        if(drawProperties.animationId == TextureID.ROBOCHEF_IDLE)return;
        drawProperties.setAnimation(TextureID.ROBOCHEF_IDLE);
    }

    /**
     * The behavior collection is responsible for abilities that can be added and
     * removed.
     *
     * @return the behavior collection
     */
    public BehaviorCollection getBehaviorCollection() {
        return behaviorCollection;
    }

    @Override
    public ITrader getTrader() {
        return playerManager.getTrader();
    }

    @Override
    public void collisionEnter(ICollidable target, CollisionType.Type type) {
    }

    @Override
    public HealthManager getHealthComponent() {
        if (playerManager == null) return null;
        return playerManager.getHealthManager();
    }

    @Override
    public Physics getPhysicsComponent() {
        return physics;
    }

    @Override
    public CollisionCircle getCollisionPrimitive() {
        return collisionCircle;
    }

    @Override
    public int getCurrentHealth() {
        return playerManager.getHealthManager().getCurrentHealth();
    }

    @Override
    public int getMaximumHealth() {
        return playerManager.getHealthManager().getMaximumHealth();
    }

    @Override
    public void applyHeal(Healer healer) {
        playerManager.getHealthManager().heal(healer);
    }

    @Override
    public void applyDamage(Damage damage) {
        if(!canBeHit) return;

        damageTaken = new Damage(damage);

        behaviorCollection.execute(BehaviorID.HIT);
        if (getHealthComponent().isImmuneToDamage()) return;

        playerManager.getHealthManager().applyDamage(damage);

        hitFlashIntensity = 1.0f;
        hitFlashTimer.restart();
    }

    @Override
    public boolean isDead() {
        return playerManager.getHealthManager().isDead();
    }
    @Override
    public void setPosition(Vector2 pos){
        physics.setPosition(pos);
    }

    /**
     * The world grid contains the world grid data, such as walls, holes etc
     *
     * @param worldGrid the world map
     */
    public void setWorldGrid(WorldGrid worldGrid) {
        physics.setWorldGrid(worldGrid);
    }

    @Override
    public IDamageable getDamageable() {
        return this;
    }

    @Override
    public DrawProperties getProperties() {
        return drawProperties;
    }

    @Override
    public void draw(SpriteBatch batch, float deltaTime) {
        if(isDead()) return;
        if (hitFlashIntensity > 0) {
            batch.setShader(Gluten.flashingShader);
            Gluten.flashingShader.setUniformf("u_flashIntensity", hitFlashIntensity);
        }
        getProperties().drawSelf(batch, deltaTime,getPosition());
        if (hitFlashIntensity > 0) {
            batch.setShader(null);
        }
        playerManager.getWeaponManager().getPrimary().draw(batch, deltaTime);
    }

    @Override
    public Vector2 getDirection() {
        return physics.getVelocity().cpy().nor();
    }

    @Override
    public void unregisterFromEventBus() {
        eventBus.unregister(this);
    }

    private void disableInvincibility() {
        getHealthComponent().disableImmunity();
    }

    @Override
    public Damage damageTaken() {
        return damageTaken;
    }
}
