package entities;

import com.badlogic.gdx.math.Vector2;
import components.collision.ICollidable;
import components.health.Killable;
import components.physics.PositionedBody;
import grid.WorldGrid;
import media.Renderable;
import model.IUpdatable;

/**
 * The Entity interface represents a game object that has various components and behaviors,
 * including physics, collision, health, rendering, and updates over time.
 * This interface extends PositionedBody, Killable, IUpdatable, ICollidable, and Renderable interfaces.
 */
public interface Entity extends PositionedBody, Killable, IUpdatable, ICollidable, Renderable {

    /**
     * Sets the WorldGrid for the Entity. Default implementation does nothing.
     * Can be overridden by implementing classes if needed.
     *
     * @param worldGrid the WorldGrid to set for the Entity
     */
    default void setWorldGrid(WorldGrid worldGrid) { }

    /**
     * Retrieves the direction vector of the Entity.
     * Default implementation returns a new Vector2 instance.
     * Can be overridden by implementing classes to provide specific direction behavior.
     *
     * @return a Vector2 representing the direction of the Entity
     */
    default Vector2 getDirection() { return new Vector2(); }

    /**
     * Unregisters the Entity from the EventBus.
     * An unregistered entity won't receive further event calls from the EventBus
     */
    void unregisterFromEventBus();
}
