package entities.inanimate;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType;
import components.collision.ICollidable;
import components.collision.primitives.CollisionCircle;
import entities.Entity;
import media.DrawProperties;
import model.event.EventBus;
import model.event.events.collision.CloseDoorsEvent;
import util.DoOnce;

import java.util.HashSet;
import java.util.Set;

public class CloseDoorsTrigger implements Entity {

    private final Vector2 position;
    private final CollisionCircle collisionCircle;
    private final EventBus eventBus;
    private boolean dead= false;
    private final DoOnce collisionEvent = new DoOnce(this::closeDoors);
    
    public CloseDoorsTrigger(EventBus eventBus, Vector2 position) {
        this.eventBus = eventBus;
        this.position = position;
        collisionCircle = new CollisionCircle(
                position,
                new Vector2(0, 5),
                16,
                this,
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.PLAYER)
                )),
                new HashSet<>()
        );
    }


    private void closeDoors(){
        eventBus.post(new CloseDoorsEvent(this.position));
        dead= true;
    }

    @Override
    public void collisionEnter(ICollidable target, CollisionType.Type type) {
      collisionEvent.execute();
       
    }

    @Override
    public CollisionCircle getCollisionPrimitive() {
        return collisionCircle;
    }

    @Override
    public boolean isDead() {
        return dead;
    }

    @Override
    public DrawProperties getProperties() {
        return null;
    }
    

    @Override
    public void draw(SpriteBatch batch, float deltaTime) {
    }


    @Override
    public Vector2 getPosition() {
        return position;
    }

    @Override
    public void setPosition(Vector2 pos) {
        this.position.x = pos.x;
        this.position.y = pos.y;
    }

    @Override
    public void unregisterFromEventBus() {
        eventBus.unregister(this);
    }
}
