package entities.inanimate;

import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType;
import components.collision.ICollidable;
import components.collision.primitives.CollisionCircle;
import entities.Entity;
import media.DrawProperties;
import media.SoundID;
import media.TextureID;
import model.event.EventBus;
import model.event.events.audio.PlaySoundEvent;
import model.event.events.game.EndLevelEvent;
import model.event.events.game.GameCompleteEvent;

import java.util.HashSet;
import java.util.Set;

public class EndPortal implements Entity {

    private final Vector2 position;
    private final CollisionCircle collisionCircle;
    private final EventBus eventBus;
    private final boolean completedGame;
    private final DrawProperties drawProperties;

    public EndPortal(EventBus eventBus, Vector2 position, boolean completedGame) {
        this.eventBus = eventBus;
        this.position = position;
        this.completedGame = completedGame;
        drawProperties = new DrawProperties(TextureID.PORTAL);

        collisionCircle = new CollisionCircle(
                position,
                new Vector2(0, 5),
                10,
                this,
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.PLAYER)
                )),
                new HashSet<>()
        );
    }


    @Override
    public void collisionEnter(ICollidable target, CollisionType.Type type) {
        if (completedGame) {
            eventBus.post(new GameCompleteEvent());
        } else {
            eventBus.post(new EndLevelEvent());
        }
        eventBus.post(new PlaySoundEvent(SoundID.LEVEL_COMPLETE));
        eventBus.post(new PlaySoundEvent(SoundID.HAPPY_CHEF));
    }

    @Override
    public CollisionCircle getCollisionPrimitive() {
        return collisionCircle;
    }

    @Override
    public boolean isDead() {
        return false;
    }

    @Override
    public DrawProperties getProperties() {
        return drawProperties;
    }

    @Override
    public Vector2 getPosition() {
        return position;
    }

    @Override
    public void setPosition(Vector2 pos) {
        this.position.x = pos.x;
        this.position.y = pos.y;
    }

    @Override
    public void unregisterFromEventBus() {
        eventBus.unregister(this);
    }
}
