package entities.enemy.state;

import com.badlogic.gdx.math.Vector2;
import entities.Entity;
import entities.ITargeter;

public interface IStatefulEntity extends ITargeter<Entity> {

    /**
     * @return the maximum distance (range) the entity should pursuit a target
     */
    float getPursuitRange();

    /**
     * @return the maximum distance (range) the entity should attack a target
     */
    float getAttackRange();

    /**
     * @return the position of the entity
     */
    Vector2 getPosition();

}
