package entities.enemy.state;

import model.IUpdatable;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *     Class for controlling, checking and executing state behaviors
 * </p>
 *
 * <p>
 *     Each time the {@code StateManager} is updated, it will check the condition
 *     of all the currently defined states in the {@code StateManager}. It goes
 *     through each {@code State} and check if its condition is met, if so,
 *     it switches to that state, and starts executing the states behavior.
 * </p>
 *
 * <p>
 *     All states defined in the {@code StateManager} is sorted by their priority
 *     level (from low to high, 1 = highest priority). When checking the
 *     {@code State} condition, the states are checked in the sorted order.
 *     The highest priority {@code State} with a condition that is met, is
 *     always executed.
 * </p>
 */
public class StateManager implements IUpdatable {

    private StateDefinition currentState;

    private final HashMap<State, StateDefinition> stateDefinitions;
    private final List<State> sortedStates;
    private final Comparator<State> comparator;

    public StateManager() {
        this(null);
    }

    public StateManager(List<StateDefinition> definitions) {
        
        stateDefinitions = new HashMap<>();
        sortedStates = new ArrayList<>();
        comparator = new State.StateComparator();
        if (definitions == null) return;

        for (StateDefinition definition: definitions) {
            stateDefinitions.put(definition.state(), definition);
        }

        sortedStates.addAll(stateDefinitions.keySet());
        sortedStates.sort(comparator);
    }

    /**
     * <p>
     *     Add a {@code StateDefinition} to the {@code StateMachine}.
     * </p>
     *
     * <p>
     *     When the {@code StateMachine} has a {@code StateDefinition} defined
     *     it will check for its condition and execute if possible each update
     *     cycle, prioritizing the state by their priority level
     * </p>
     *
     * @param stateDefinition that defines the condition and behavior of a {@code State}
     */
    public void addStateDefinition(StateDefinition stateDefinition) {
        if (stateDefinition == null) return;

        if (!sortedStates.contains(stateDefinition.state())){
            sortedStates.add(stateDefinition.state());
            sortedStates.sort(comparator);
        }
        stateDefinitions.put(stateDefinition.state(), stateDefinition);
    }

    /**
     * <p>
     *     Add a list of {@code StateDefinition} to the {@code StateMachine}.
     * </p>
     *
     * <p>
     *     When the {@code StateMachine} has a {@code StateDefinition} defined
     *     it will check for its condition and execute if possible each update
     *     cycle, prioritizing the state by their priority level
     * </p>
     *
     * @param stateDefinitions that defines the condition and behavior of a {@code State}
     */
    public void addStateDefinition(List<StateDefinition> stateDefinitions) {
        if (stateDefinitions == null) return;
        for (StateDefinition definition: stateDefinitions) {
            addStateDefinition(definition);
        }
    }

    /**
     * <p>
     *     Remove the a {@code StateDefinition} from the {@code StateManager}
     *     if there is currently such has such a state defined. If no such state
     *     is found, do nothing.
     * </p>
     *
     * <p>
     *     By removing a state from the {@code StateManager} it will no longer
     *     execute the behaviour defined in the State's {@code StateDefinition}.
     * </p>
     *
     * @param state to be removed
     */
    public void removeStateDefinition(State state) {
        if (!stateDefinitions.containsKey(state)) return;

        stateDefinitions.remove(state);
        sortedStates.remove(state);
        sortedStates.sort(comparator);
    }

    @Override
    public void update(double deltaTime) {
        if (stateDefinitions.isEmpty()) return;
        StateDefinition previousState = currentState;
        if (currentState == null) {
            findNextState();
        } else if( currentState.behaviour().isComplete()){
            currentState.completeState();
            findNextState();
        }
        if (previousState != null && currentState.state() != previousState.state()) {
            previousState.behaviour().reset();
        }

        if (!stateDefinitions.containsKey(currentState.state())) return;
        stateDefinitions.get(currentState.state()).behaviour().execute(deltaTime);
    }

    private void findNextState(){
        for (State state: sortedStates) {
            if (checkStateCondition(stateDefinitions.get(state))) return;
        }
    }
    
    private boolean checkStateCondition(StateDefinition stateDefinition){
        if (stateDefinition == null) return false;
        if (!stateDefinition.behaviourChecker().stateConditionMet()) return false;
        currentState = stateDefinition;
        return true; 
    }

    /**
     * @return the current state of the state manager
     */
    public State getState() {
        return currentState.state();
    }
}
