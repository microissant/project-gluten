package entities.enemy.state.behaviour;

import com.badlogic.gdx.math.Vector2;
import components.physics.Physics;
import entities.BehaviourHelper;
import entities.navigation.Navigator;
import media.DrawProperties;
import media.TextureID;

import java.util.LinkedList;

public class PursuitPath implements IStateBehaviour {

    private final Navigator navigator;
    private final Physics physics;
    private Vector2 targetPosition;

    private final DrawProperties drawProperties;
    private final TextureID activePersuitID;
    private final TextureID stationaryPursuitID;

    public PursuitPath(Navigator navigator, Physics physics, DrawProperties drawProperties, TextureID activePersuitID, TextureID stationaryPursuitID) {
        this.navigator = navigator;
        this.physics = physics;
        this.drawProperties = drawProperties;
        this.activePersuitID = activePersuitID;
        this.stationaryPursuitID = stationaryPursuitID;
    }

    @Override
    public void execute(double deltaTime) {
        if (physics == null) return;

        if (physics.isStationary()) {
            if (drawProperties.getTextureID() != stationaryPursuitID) {
                drawProperties.setAnimation(stationaryPursuitID);
            }
        } else {
            if (drawProperties.getTextureID() != activePersuitID) {
                drawProperties.setAnimation(activePersuitID);
            }
        }

        LinkedList<Vector2> path = navigator.getPath();
        if (path == null) {
            return;
        }
        getNextTarget(path);
        if (targetPosition == null) {
            return;
        }

        if (path.size() < 1 && targetPosition.dst2(physics.getPosition()) < 1){
            return;
        }

        float dirX = targetPosition.x - physics.getPosition().x;
        float dirY = targetPosition.y - physics.getPosition().y;

        BehaviourHelper.setAnimationDirection(drawProperties, dirX);
        physics.accelerate(new Vector2(dirX, dirY));
    }

    private void getNextTarget(LinkedList<Vector2> path) {
        if (targetPosition == null) {
            targetPosition = path.pollLast();
            return;
        }
        if (physics.getPosition().dst2(targetPosition) < 50.0 && path.size() > 0) {
            targetPosition = path.pollLast();
        }
    }
}
