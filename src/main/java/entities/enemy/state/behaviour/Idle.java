package entities.enemy.state.behaviour;

import media.DrawProperties;
import media.TextureID;

/** This behaviour does nothing*/
public class Idle implements IStateBehaviour {

    private boolean hasExecuted;
    private final DrawProperties drawProperties;
    private final TextureID textureID;

    public Idle(DrawProperties drawProperties, TextureID textureID) {
        reset();
        this.drawProperties = drawProperties;
        this.textureID = textureID;
    }

    @Override
    public void execute(double deltaTime) {
        if (!hasExecuted) {
            drawProperties.setAnimation(textureID);
        }

        hasExecuted = true;
    }

    @Override
    public void reset() {
        hasExecuted = false;
    }
}
