package entities.enemy.state.behaviour;

public interface IStateBehaviour {
    
    /**
     * Execute a certain behaviour
     */
    default void execute(double deltaTime) {}

    /**
     * @return true if the behavior has been executed and is completed
     */
    default boolean isComplete() { return true; }

    /**
     * reset the behavior state
     */
    default void reset() { }
}
