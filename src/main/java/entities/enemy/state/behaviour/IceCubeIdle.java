package entities.enemy.state.behaviour;

import com.badlogic.gdx.math.Vector2;
import entities.enemy.state.behaviour.dash.DashHelper;

import java.util.concurrent.ThreadLocalRandom;

/** This behaviour does nothing*/
public class IceCubeIdle implements IStateBehaviour {

    private float timeUntilRandomDash = 0.f;
    private final DashHelper dashHelper;
    public final float dashStrength = 60f;


    public IceCubeIdle(DashHelper dashHelper) {
        reset();
        this.dashHelper = dashHelper;
    }

    @Override
    public void execute(double deltaTime) {
        timeUntilRandomDash-=deltaTime;

        if(timeUntilRandomDash <=0){
            Vector2 vec = new Vector2();
            vec.setToRandomDirection();
            dashHelper.doDash(vec, dashStrength);
        }

    }

    @Override
    public void reset() {
        timeUntilRandomDash = ThreadLocalRandom.current().nextFloat(2f);
    }
}
