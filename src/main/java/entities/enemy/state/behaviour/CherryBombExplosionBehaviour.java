package entities.enemy.state.behaviour;

import entities.enemy.IEnemy;
import media.DrawProperties;
import media.SoundID;
import media.TextureID;
import model.event.EventBus;
import model.event.events.audio.PlaySoundEvent;
import util.CountdownTimer;
import util.DoOnce;

public class CherryBombExplosionBehaviour implements IStateBehaviour {
    private boolean completed = false;
    private final IEnemy owner;
    private final DrawProperties drawProperties;
    private final EventBus eventBus;
    
    private final CountdownTimer explosionTimer;
    private final DoOnce startedExploding;

    public CherryBombExplosionBehaviour(IEnemy owner,DrawProperties drawProperties, EventBus bus){
        if (owner == null){
            throw new NullPointerException("Owner can't be null");
        }
        this.owner = owner;
        this.drawProperties = drawProperties;
        this.eventBus = bus;

        explosionTimer = new CountdownTimer(0.6f, this::triggerExplosion);
        explosionTimer.start();
        startedExploding = new DoOnce(this::startExploding);

        }


    @Override
    public void execute(double deltaTime) {
        startedExploding.execute();
        explosionTimer.update(deltaTime);
    }

    private void startExploding() {
        drawProperties.setAnimation(TextureID.CHERRYBOMB_EXPLOSION);
        eventBus.post(new PlaySoundEvent(SoundID.CHERRY_SCREAM));
    }

    private void triggerExplosion() {
        completed = true;
        owner.destroy();
        explosionTimer.reset();
    }

    @Override
    public boolean isComplete() {
        return completed;
    }
    
}
