package entities.enemy.state.behaviour;

import com.badlogic.gdx.math.Vector2;
import components.weapon.WeaponManager;
import entities.BehaviourHelper;
import media.DrawProperties;
import media.TextureID;

public class Attack implements IStateBehaviour {

    private final Vector2 aim;
    private final WeaponManager weaponManager;
    private boolean attackStarted;
    private boolean attackComplete;
    private int attackCountBefore;
    private final TextureID textureID;
    private final DrawProperties drawProperties;

    public Attack(Vector2 aim, WeaponManager weaponManager, DrawProperties drawProperties, TextureID textureID){
        if (aim == null || weaponManager == null){
            throw new NullPointerException("Attack: Aim, Target or WeaponManager is null");
        }
        this.aim = aim;
        this.weaponManager = weaponManager;
        this.textureID = textureID;
        this.drawProperties = drawProperties;

        reset();
    }

    @Override
    public void execute(double deltaTime){
        if (!attackStarted) {
            drawProperties.setAnimation(textureID);
            attackStarted = true;
            attackComplete = false;
            attackCountBefore = weaponManager.shotsFiredPrimaryWeapon();
        }

        BehaviourHelper.setAnimationDirection(drawProperties, aim.x);

        weaponManager.fireWeapon();

        if (weaponManager.shotsFiredPrimaryWeapon() - attackCountBefore < 1) return;

        attackComplete = true;
        attackStarted = false;
    }

    @Override
    public boolean isComplete() {
        return attackComplete;
    }

    @Override
    public void reset() {
        attackStarted = false;
        attackComplete = false;
    }
}
