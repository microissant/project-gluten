package entities.enemy.state.behaviour;

import com.badlogic.gdx.math.Vector2;

import entities.enemy.IEnemy;
import entities.particles.ParticleTypes;
import model.event.EventBus;
import model.event.events.AddParticleEvent;
import model.event.events.EnemyDeathEvent;

public class CherryDeath implements IStateBehaviour {
    private boolean hasDied;
    private final EventBus eventBus;
    private final IEnemy enemy;
    private final Vector2 position;

    public CherryDeath(EventBus eventBus, Boolean hasDied, IEnemy enemy,Vector2 position){
        this.eventBus = eventBus;
        this.hasDied = hasDied;
        this.enemy = enemy;
        this.position = position;
    }

    @Override
    public void execute(double deltaTime) {
        if (hasDied) return;
        hasDied = true;
        eventBus.post(new AddParticleEvent(ParticleTypes.EXPLOSION_V2,position));
        eventBus.post(new EnemyDeathEvent(enemy));
    }


}
