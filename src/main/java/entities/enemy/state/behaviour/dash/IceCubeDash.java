package entities.enemy.state.behaviour.dash;

import entities.enemy.state.behaviour.IStateBehaviour;

public class IceCubeDash implements IStateBehaviour{

    private final DashHelper dashHelper;

    public IceCubeDash(DashHelper dashHelper){
        this.dashHelper = dashHelper;
    }

    @Override
    public void execute(double deltaTime) {
        dashHelper.setAngleToMovingDirection();
    }

    @Override
    public boolean isComplete() {
        // the squared speed at which the dash will end
        float endDashStrength = 256;
        if(dashHelper.getCurrentSpeed2()< endDashStrength){
            dashHelper.endDash();
            return true;
        }
        return false;
    }
}
