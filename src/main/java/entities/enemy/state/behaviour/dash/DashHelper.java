package entities.enemy.state.behaviour.dash;

import com.badlogic.gdx.math.Vector2;

import components.physics.Physics;
import entities.BehaviourHelper;
import entities.enemy.IEnemy;


/**class used to communicate between states that are about dashing */
public class DashHelper {
    private boolean wantToDash = false;
    private final IEnemy owner;
    private float angle;

    public DashHelper(IEnemy owner){
        this.owner=owner;
        angle = 0f;
      
    }

    /**returns true if the enemy is either about to dash or is currently dashing */
    public boolean wantToDash(){
        return wantToDash;
    }

    /** initiates a dash in the given direction with the given strength
     * 
     * @param dashDirection the dash direction
     * @param strength the dash strength (speed)
    */
    public void doDash(Vector2 dashDirection, float strength){
        if (dashDirection == null) return;
        wantToDash = true;
        Physics physics = owner.getPhysicsComponent();
        physics.disableInstantMovement();
        physics.enableFriction();
        physics.setFriction(80f);
        physics.setAccelerationForce(strength*strength);
        physics.accelerate(dashDirection);
        
    }
    /** initiates a dash in the direction created by making a vector that goes from "from" to "to"
     * 
     * @param from where the dash direction goes from
     * @param to where the dash direction goes to
     * @param strength the dash strength (speed)
    */
    public void doDash(Vector2 from, Vector2 to, float strength){
        if (from == null || to == null) return;
        Vector2 dashDirection = new Vector2(to);
        dashDirection.sub(from);
        doDash(dashDirection,strength);        
    }

    /**returns the position of the enemy */
    public Vector2 getPosition(){
        return owner.getPosition();
    }

    /**returns the square of the current speed */
    public float getCurrentSpeed2(){
        return owner.getPhysicsComponent().getVelocity().len2();
    }

    /**sets the angle of the enemy to the direction that it is moving in */
    public void setAngleToMovingDirection(){
        angle = owner.getPhysicsComponent().getVelocity().angleDeg();
        updateAngle();
    }

    /**adds a value to the angle of the enemy */
    public void addToAngle(float angleDifference){
        angle+=angleDifference;
        updateAngle();
    }
    /**ends the current dash */
    public void endDash(){
        wantToDash = false;
    }

    /**updates the texture so that the enemy has the correct rotation frame */
    private void updateAngle(){
        BehaviourHelper.setFrameOfRotatingAnimation(owner.getProperties(), angle);
    }
}


