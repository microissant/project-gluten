package entities.enemy.state.behaviour;

import com.badlogic.gdx.math.Vector2;

import components.physics.Physics;

public class PursuitTarget implements IStateBehaviour {
    private final Vector2 targetDirection;
    private final Physics physics;

    public PursuitTarget(Vector2 targetDirection, Physics physics){
        this.targetDirection = targetDirection;
        this.physics = physics;
    }

    @Override
    public void execute(double deltaTime){
        physics.accelerate(targetDirection);
    }
}
