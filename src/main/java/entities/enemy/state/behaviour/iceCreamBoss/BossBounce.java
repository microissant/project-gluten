package entities.enemy.state.behaviour.iceCreamBoss;

import com.badlogic.gdx.math.Vector2;
import entities.BehaviourHelper;
import entities.enemy.IEnemy;
import entities.enemy.state.behaviour.IStateBehaviour;
import media.DrawProperties;
import media.TextureID;

public class BossBounce implements IStateBehaviour {
    private final Vector2 aim;
    private boolean attackComplete;
    private final TextureID textureID;
    private final DrawProperties drawProperties;
    private final IEnemy owner;
    private double dashTimeRemaining = 0;
    

    public BossBounce(Vector2 aim,IEnemy owner, DrawProperties drawProperties, TextureID textureID){
        if (aim == null || owner == null || drawProperties == null){
            throw new NullPointerException("Aim, owner or drawProperties is null");
        }
        this.aim = aim;
        this.textureID = textureID;
        this.drawProperties = drawProperties;
        this.owner = owner;
        
        reset();
    }

    @Override
    public void execute(double deltaTime){
        drawProperties.setAnimation(textureID);
        BehaviourHelper.setFrameOfRotatingAnimation(drawProperties, owner.getPhysicsComponent().getVelocity().angleDeg());

        dashTimeRemaining-=deltaTime;
        if(dashTimeRemaining<=0f){
            attackComplete =true;
        }

    }


    @Override
    public boolean isComplete() {
        return attackComplete;
    }

    @Override
    public void reset() {
        dashTimeRemaining = 5f;
        attackComplete = false;
        owner.getPhysicsComponent().setLinearMovement(aim, 150);
    }
}
