package entities.enemy.state.behaviour.iceCreamBoss;

import com.badlogic.gdx.math.Vector2;
import components.weapon.WeaponManager;
import entities.enemy.IEnemy;
import entities.enemy.state.behaviour.IStateBehaviour;
import media.DrawProperties;
import media.TextureID;

public class BossShoot implements IStateBehaviour {

    private final Vector2 aim;
    private final WeaponManager weaponManager;
    private boolean attackComplete;
    private final TextureID textureID;
    private final DrawProperties drawProperties;
    private double timeUntilShoot = 0f;
    private final IEnemy owner;
    private int bouncesRemaining = 4;


    public BossShoot(Vector2 aim, IEnemy owner, WeaponManager weaponManager, DrawProperties drawProperties, TextureID textureID){
        if (aim == null || weaponManager == null || owner == null || drawProperties == null) {
            throw new NullPointerException("BossShoot: Aim, owner, drawProperties or WeaponManager is null");
        }
        this.aim = aim;
        this.weaponManager = weaponManager;
        this.textureID = textureID;
        this.drawProperties = drawProperties;
        this.owner = owner;
        
        reset();
    }

    @Override
    public void execute(double deltaTime){
        timeUntilShoot-=deltaTime;

        if (timeUntilShoot<0) {
            aim.set(1, 0);
            //finish discharge
            weaponManager.update(1f);
            weaponManager.fireWeapon();
            weaponManager.update(1f);
            
            bounce();
        }

        if(bouncesRemaining<=0){
            attackComplete = true;
        }
    }

    private void bounce(){

        bouncesRemaining--;
        drawProperties.setAnimation(textureID);
        drawProperties.setAnimationSpeed(1);
        Vector2 moveDir = new Vector2();
        moveDir.setToRandomDirection();
        owner.getPhysicsComponent().setLinearMovement(moveDir, 80);
        timeUntilShoot = drawProperties.getAnimationDuration();
    }

    @Override
    public boolean isComplete() {
        return attackComplete;
    }

    @Override
    public void reset() {
        bouncesRemaining = 4;
        attackComplete = false;
        bounce();
    }
}
