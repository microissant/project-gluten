package entities.enemy.state.behaviour;

import com.badlogic.gdx.math.Vector2;
import entities.enemy.state.behaviour.dash.DashHelper;

public class IceCubeSpin implements IStateBehaviour {

    private final DashHelper dashHelper;
    private float timeLeft = 0f;
    private final Vector2 targetPosition;

    public static final float dashStrength = 120f;
    public static final float spinTime = 1f;


    public IceCubeSpin(DashHelper dashHelper,Vector2 targetPosition ){
        this.dashHelper = dashHelper;
        this.targetPosition =targetPosition;
        reset();
    }


    @Override
    public void execute(double deltaTime) {
        timeLeft-=deltaTime;
        if(timeLeft <= 0){

            dashHelper.doDash(dashHelper.getPosition(),targetPosition,dashStrength);
        }
        float spinSpeed = 600f;
        dashHelper.addToAngle((float) deltaTime * spinSpeed * (1-timeLeft)*3f);
    }

    @Override
    public boolean isComplete() {
        return timeLeft<=0;
    }

    @Override
    public void reset() {
        timeLeft  = spinTime;
    }
    
}
