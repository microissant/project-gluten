package entities.enemy.state.behaviour;

import entities.enemy.IEnemy;
import model.event.EventBus;
import model.event.events.EnemyDeathEvent;

public class Dead implements IStateBehaviour {
    private boolean hasDied;
    private final EventBus eventBus;
    private final IEnemy enemy;

    public Dead(EventBus eventBus, Boolean hasDied, IEnemy enemy){
        this.eventBus = eventBus;
        this.hasDied = hasDied;
        this.enemy = enemy;

    }

    @Override
    public void execute(double deltaTime) {
        if (hasDied) return;
        hasDied = true;
        eventBus.post(new EnemyDeathEvent(enemy));
    }


}
