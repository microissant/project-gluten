package entities.enemy.state;

import java.io.Serializable;
import java.util.Comparator;

public enum State {
    ATTACK(2),
    IDLE(100),
    PURSUIT(3),
    DEAD(1);

    private final int priority;
    State(int priority) {
        this.priority = priority;
    }

    public static class StateComparator implements Comparator<State>, Serializable {
        @Override
        public int compare(State o1, State o2) {
            return Integer.compare(o1.priority, o2.priority);
        }
    }
}
