package entities.enemy.state.condition;

import com.badlogic.gdx.math.Vector2;
import entities.enemy.state.IStatefulEntity;
import entities.enemy.state.State;
import grid.WorldGrid;

public class PursuitStateCondition implements IStateBehaviourCondition {

    private final Vector2 vectorTowardTarget;
    private final IStatefulEntity owner;

    public PursuitStateCondition(IStatefulEntity owner, Vector2 entityPosition, Vector2 targetPosition, Vector2 vectorTowardTarget, float range, WorldGrid worldGrid){
        this.owner = owner;
        this.vectorTowardTarget = vectorTowardTarget;
    }

    @Override
    public boolean stateConditionMet() {
        return vectorTowardTarget.len2() < Math.pow(owner.getPursuitRange(), 2);
    }
    
    @Override
    public State getState() {
        return State.PURSUIT;
    }
}
