package entities.enemy.state.condition;

import entities.enemy.state.State;

public interface IStateBehaviourCondition {

    /**
     * @return true if the condition to execute the behavior is meet
     */
    boolean stateConditionMet();

    /**
     * @return the state which should be switched to if the condition is meet
     */
    State getState();
}


