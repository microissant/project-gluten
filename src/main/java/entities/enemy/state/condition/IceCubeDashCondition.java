package entities.enemy.state.condition;

import entities.enemy.state.State;
import entities.enemy.state.behaviour.dash.DashHelper;

public class IceCubeDashCondition implements IStateBehaviourCondition{

    private final DashHelper dashHelper;


    public IceCubeDashCondition(DashHelper dashHelper){
        this.dashHelper = dashHelper;
    }


    @Override
    public boolean stateConditionMet() {
        return dashHelper.wantToDash();
    }

    @Override
    public State getState() {
        return State.IDLE;
    }
    
}
