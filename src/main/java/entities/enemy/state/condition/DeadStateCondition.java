package entities.enemy.state.condition;

import components.health.Killable;
import entities.enemy.state.State;

public class DeadStateCondition implements IStateBehaviourCondition {
    private final Killable killable;

    public DeadStateCondition(Killable killable){
        this.killable = killable;

    }

    @Override
    public boolean stateConditionMet() {
        return killable.isDead();
    }

    @Override
    public State getState() {
        return State.DEAD;
    }
    
}
