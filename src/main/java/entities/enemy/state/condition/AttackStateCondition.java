package entities.enemy.state.condition;

import com.badlogic.gdx.math.Vector2;
import entities.BehaviourHelper;
import entities.enemy.state.IStatefulEntity;
import entities.enemy.state.State;
import grid.TileData.TileRuleset;
import grid.WorldGrid;

public class AttackStateCondition implements IStateBehaviourCondition {
    private final Vector2 targetPosition;
    private final IStatefulEntity owner;
    private final Vector2 ownerPosition;
    private final TileRuleset ruleset;
    private final WorldGrid worldGrid;

    public AttackStateCondition(IStatefulEntity owner, Vector2 targetPosition, Vector2 ownerPosition,TileRuleset ruleset, WorldGrid worldGrid){
        if (owner == null) throw new IllegalArgumentException("owner can't be null");  
        if (targetPosition == null) throw new IllegalArgumentException("targetPosition can't be null");  
        if (ownerPosition == null) throw new IllegalArgumentException("ownerPosition can't be null");  
        
        this.owner = owner;
        this.targetPosition = targetPosition;
        this.ownerPosition = ownerPosition;
        this.ruleset = ruleset;
        this.worldGrid = worldGrid;
    }
    
    @Override
    public boolean stateConditionMet() {
        if (!owner.hasTarget()) return false;
        if (owner.getTarget().isDead()) return false;

        float distanceX = targetPosition.x - ownerPosition.x;
        float distanceY = targetPosition.y - ownerPosition.y;
        float distance = distanceX * distanceX + distanceY * distanceY;
        if(distance > Math.pow(owner.getAttackRange(), 2)) return false;
        return(BehaviourHelper.lineOfSightCheck(worldGrid, targetPosition, ownerPosition, ruleset.val));
       
    }

    @Override
    public State getState() {
        return State.ATTACK;
    }
    
}
