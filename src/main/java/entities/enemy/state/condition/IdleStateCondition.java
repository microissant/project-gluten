package entities.enemy.state.condition;

import entities.enemy.state.State;

public class IdleStateCondition implements IStateBehaviourCondition {

    @Override
    public boolean stateConditionMet() {
        return true;
    }

    @Override
    public State getState() {
        return State.IDLE;
    }
    
}
