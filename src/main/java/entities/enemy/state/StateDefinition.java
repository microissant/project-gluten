package entities.enemy.state;

import entities.enemy.state.behaviour.IStateBehaviour;
import entities.enemy.state.condition.IStateBehaviourCondition;

import java.util.concurrent.ThreadLocalRandom;


public class StateDefinition {


    private final State state;
    private final IStateBehaviourCondition behaviourChecker;
    private final IStateBehaviour[] behaviours;
    private int currentBehaviour = 0;
    private final BehaviourPickMode pickMode;

    public enum BehaviourPickMode{
        RANDOM,
        SEQUENCE
    }


    /**
     * Definition of a state, its condition, and its behavior
     * @param state defines the state of an object
     * @param behaviourChecker defines and checks when the state conditions are met
     * @param pickMode defines how the next behaviour will be picked. If random, then the next one will be picked at random, if sequence, then the next one will be the next one in the list and then loop around when the list has finished.
     * @param behaviours defines how the object should behave when the state is active. If multiple are given, then one will be picked according to the pickmode when the current one finishes. 
     */
    public StateDefinition(State state, IStateBehaviourCondition behaviourChecker,BehaviourPickMode pickMode, IStateBehaviour ... behaviours) {
        this.state = state;
        this.behaviourChecker = behaviourChecker;
        this.behaviours = behaviours;
        this.pickMode = pickMode;
    }
    /**     * Definition of a state, its condition, and its behavior
     * @param state defines the state of an object
     * @param behaviourChecker defines and checks when the state conditions are met
     * @param behaviours defines how the object should behave when the state is active. If multiple are given, then one will be picked at random when the current one finishes. 
     */
    public StateDefinition(State state, IStateBehaviourCondition behaviourChecker, IStateBehaviour ... behaviours) {
        this(state,behaviourChecker,BehaviourPickMode.RANDOM,behaviours);
    }

    /** defines the state of an object
     */
    public State state(){
        return state;
    }
    /** defines and checks when the state conditions are met */
    public IStateBehaviourCondition behaviourChecker(){
        return behaviourChecker;
    }
    /**defines how the object should behave when the state is active */
    public IStateBehaviour behaviour(){
        return behaviours[currentBehaviour];
    }

    /**
     * Should be called when this state definition's behaviour finishes. This is the part that randomly picks the next behaviour
     */
    public void completeState(){
        switch (pickMode) {
            case RANDOM -> currentBehaviour = ThreadLocalRandom.current().nextInt(behaviours.length);
            case SEQUENCE -> {
                currentBehaviour++;
                currentBehaviour = currentBehaviour % behaviours.length;
            }
        }
        behaviour().reset();
        
    }

    
}
