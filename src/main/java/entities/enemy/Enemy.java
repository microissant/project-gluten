package entities.enemy;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import components.health.Damage;
import components.health.Healer;
import components.health.HealthManager;
import components.health.IDamageable;
import components.physics.Physics;
import components.weapon.Weapon;
import components.weapon.WeaponManager;
import entities.Entity;
import entities.enemy.state.StateDefinition;
import entities.enemy.state.StateManager;
import entities.navigation.Navigator;
import grid.TileData.TileRuleset;
import grid.WorldGrid;
import item.Item;
import item.items.CoinItem;
import media.DrawProperties;
import media.SoundID;
import model.event.EventBus;
import model.event.events.EnemyDeathEvent;
import model.event.events.audio.PlaySoundEvent;
import model.event.events.entities.SpawnItemEvent;
import util.CountdownTimer;
import util.DoOnce;
import view.screens.Gluten;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class Enemy implements IEnemy {

    protected final HealthManager healthManager;
    protected final Physics physics;
    protected final WeaponManager weaponManager;
    protected final Vector2 originToTarget = new Vector2(1, 0);
    private final StateManager stateManager;
    private Entity target;
    protected final Vector2 targetPosition = new Vector2();
    protected final EventBus eventBus;
    protected final Navigator navigator;

    private final boolean isRequiredTarget;
    protected SoundID deathSoundID;
    private final Item lootItem;
    private float lootDropChance;

    private final CountdownTimer combatTimer;

    private final CountdownTimer hitFlashTimer;
    private final DoOnce deathTrigger;

    private float hitFlashIntensity;

    private TileRuleset ruleset = TileRuleset.WALKING;
    
    protected Enemy(Vector2 position, int startHealth, boolean isRequiredTarget, EventBus eventBus, int lootMin, int lootMax, float lootDropChance) {
        healthManager = new HealthManager(startHealth);
        physics = new Physics(position);
        physics.enableInstantMovement();
        physics.setBoxCollision(6,2,5,5);
        weaponManager = new WeaponManager(physics.getPosition(), originToTarget, eventBus);
        stateManager = new StateManager();
        this.isRequiredTarget = isRequiredTarget;
        combatTimer = new CountdownTimer(10, this::disableCombat);
        hitFlashTimer = new CountdownTimer(0.15f, this::disableHitEffect);
        hitFlashIntensity = 0;
        deathTrigger = new DoOnce(this::death);

        this.eventBus = eventBus;

        navigator = new Navigator(physics.getPosition());

        lootItem = new CoinItem(physics.getPosition(), lootMin, lootMax, eventBus);
        this.lootDropChance = Math.min(1, lootDropChance);
        this.lootDropChance = Math.max(0, this.lootDropChance);
    }

    @Override
    public boolean isDead() {
        return healthManager.isDead() || deathTrigger.hasExecuted();
    }

    @Override
    public Vector2 getPosition() {
        return physics.getPosition();
    }

    @Override
    public void setPosition(Vector2 pos) {
        physics.setPosition(pos);
    }

    @Override
    public void update(double dt) {
        physics.update(dt);
        updateTargetPosition();
        updateTargetDirection();
        stateManager.update(dt);
        weaponManager.update(dt);
        combatTimer.update(dt);
        hitFlashTimer.update(dt);

        if (navigator != null) navigator.update(dt);

        if (isDead()) deathTrigger.execute();
    }

    protected SoundID getDeathSoundID(){
        return deathSoundID;
    }

    protected void death() {
        eventBus.post(new EnemyDeathEvent(this));
        eventBus.post(new PlaySoundEvent(getDeathSoundID()));

        Random random = new Random();
        float roll = random.nextFloat();
        if (roll > lootDropChance) return;
        eventBus.post(new SpawnItemEvent(lootItem));
    }

    @Override
    public int getCurrentHealth() {
        return healthManager.getCurrentHealth();
    }

    @Override
    public int getMaximumHealth() {
        return healthManager.getMaximumHealth();
    }

    @Override
    public void applyHeal(Healer healer) {
        healthManager.heal(healer);
    }

    @Override
    public void applyDamage(Damage damage) {
        healthManager.applyDamage(damage);

        activateCombat();
        hitFlashIntensity = 1f;
        hitFlashTimer.restart();
    }

    private void disableHitEffect() {
        hitFlashIntensity = 0;
    }

    protected void activateCombat() {
        combatTimer.restart();
    }

    protected void disableCombat() {
        combatTimer.stop();
    }

    @Override
    public IDamageable getDamageable() {
        return this;
    }

    @Override
    public void attack() {

    }

    private void updateTargetPosition(){
        if (target == null) return;
        targetPosition.x = target.getPosition().x;
        targetPosition.y = target.getPosition().y;
    }
    
    @Override
    public void updateTargetDirection() {
        if (target == null) return;
        float x = target.getPosition().x - getPosition().x;
        float y = target.getPosition().y - getPosition().y;

        originToTarget.x = x;
        originToTarget.y = y;
    }

    @Override
    public void addWeapon(Weapon weapon) {
        weaponManager.addWeapon(weapon);
    }

    @Override
    public void setTarget(Entity target) {
        if (target == null) return;

        this.target = target;
        navigator.setTarget(target.getPosition());
    }

    @Override
    public boolean hasTarget(){
        return target != null;
    }

    @Override
    public Entity getTarget(){
        return target;
    }

    protected void addStateDefinitions(List<StateDefinition> stateDefinitions) {
        stateManager.addStateDefinition(stateDefinitions);
    }

    /**
     * @return the draw properties of the enemy.
     * Determines the texture and animation of the enemy
     */
    public abstract DrawProperties getProperties();

    @Override
    public void setWorldGrid(WorldGrid worldGrid) {
        if (worldGrid == null) return;
        physics.setWorldGrid(worldGrid);
        navigator.setWorldGrid(worldGrid,getTileRuleset());
        StateDefinition pursuitState  = getPursuitPathState(navigator, worldGrid);
        if(pursuitState != null) addStateDefinitions(new ArrayList<>(List.of(pursuitState)));
    }

    protected abstract StateDefinition getPursuitPathState(Navigator navigator, WorldGrid worldGrid);

    @Override
    public Physics getPhysicsComponent() {
        return physics;
    }

    @Override
    public HealthManager getHealthComponent() {
        return healthManager;
    }

    @Override
    public boolean isRequiredTarget() {
        return isRequiredTarget;
    }

    @Override
    public void unregisterFromEventBus() {
        eventBus.unregister(this);
    }

    @Override
    public void draw(SpriteBatch batch, float deltaTime) {
        if (hitFlashIntensity > 0) {
            batch.setShader(Gluten.flashingShader);
            Gluten.flashingShader.setUniformf("u_flashIntensity", hitFlashIntensity);
        }
        getProperties().drawSelf(batch, deltaTime,getPosition());
        if (hitFlashIntensity > 0) {
            batch.setShader(null);
        }
    }

    @Override
    public List<Vector2> getCurrentPath() {
        if (navigator == null) return null;
        if (navigator.getPath() == null) return null;

        return new ArrayList<>(navigator.getPath());
    }

    protected void setRuleset(TileRuleset ruleset){
        this.ruleset=ruleset;
        physics.setRuleset(ruleset);
    }

    /**
     * @return the ruleset on the grid. If flying, the enemy can fly over holes.
     */
    public TileRuleset getTileRuleset(){
        return ruleset;
    }


    @Override
    public void destroy(){
        healthManager.applyDamage(new Damage(healthManager.getMaximumHealth()));
    }
}
