package entities.enemy;

import com.badlogic.gdx.math.Vector2;
import components.collision.ICollidable;
import components.health.IDamageable;
import components.weapon.WeaponHolder;
import entities.Entity;
import entities.enemy.state.IStatefulEntity;
import media.Renderable;

import java.util.List;

public interface IEnemy extends Entity, ICollidable, IDamageable, WeaponHolder, IStatefulEntity, Renderable {

    /**
     *
     * @return true if the enemy is tagged to
     */
    default boolean isRequiredTarget() { return false; }

    /**
     * @return the path found from the pathfinder
     */
    List<Vector2> getCurrentPath();

    /**
     * Instantly kills this enemy
     */
    void destroy();

}
