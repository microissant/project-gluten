package entities.enemy.enemies;

import java.util.HashSet;
import java.util.Set;

import com.badlogic.gdx.math.Vector2;


import components.collision.primitives.CollisionCircle;
import components.collision.CollisionType;
import components.collision.ICollidable;
import entities.enemy.Enemy;
import entities.enemy.state.StateDefinition;
import entities.navigation.Navigator;
import grid.WorldGrid;
import media.DrawProperties;
import media.TextureID;
import model.event.EventBus;
import model.event.events.entities.CreateCrateEvent;
import model.event.events.entities.DestroyCrateEvent;

public class Crate extends Enemy {
    
    private final DrawProperties drawProperties;

    public Crate(Vector2 position, EventBus eventBus) {
        super(position, 3, false, eventBus, 1, 5, 0.25f);
        getPhysicsComponent().freeze();
        drawProperties = new DrawProperties(TextureID.CRATE);
        drawProperties.setRandomFrame();
        drawProperties.setAnimationSpeed(0);
    }

    @Override
    protected void activateCombat() {
    }

    @Override
    protected void disableCombat() {
    }

    @Override
    public DrawProperties getProperties() {
        return drawProperties;
    }

    @Override
    protected StateDefinition getPursuitPathState(Navigator navigator, WorldGrid worldGrid) {
        return null;
    }

    @Override
    public void collisionEnter(ICollidable target, CollisionType.Type type) {
    }

    

    @Override
    protected void death() {
        eventBus.post(new DestroyCrateEvent(getPosition()));
        super.death();
    }

    

    @Override
    public void setWorldGrid(WorldGrid worldGrid) {
        if (worldGrid == null) return;
        physics.setWorldGrid(worldGrid);
        eventBus.post(new CreateCrateEvent(this));
    }

    @Override
    public CollisionCircle getCollisionPrimitive() {
        return new CollisionCircle(
                physics.getPosition(),
                new Vector2(0, 0),
                8,
                this,
                new HashSet<>(),
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.ENEMY)
                ))
        );
    }

    @Override
    public float getPursuitRange() {
        return 0;
    }

    @Override
    public float getAttackRange() {
        return 0;
    }

}
