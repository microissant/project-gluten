package entities.enemy.enemies;

import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType;
import components.collision.ICollidable;
import components.collision.primitives.CollisionCircle;
import components.weapon.Weapon;
import components.weapon.weapons.Melee;
import entities.enemy.Enemy;
import entities.enemy.state.State;
import entities.enemy.state.StateDefinition;
import entities.enemy.state.behaviour.Attack;
import entities.enemy.state.behaviour.Dead;
import entities.enemy.state.behaviour.Idle;
import entities.enemy.state.behaviour.PursuitPath;
import entities.enemy.state.condition.AttackStateCondition;
import entities.enemy.state.condition.DeadStateCondition;
import entities.enemy.state.condition.IdleStateCondition;
import entities.enemy.state.condition.PursuitStateCondition;
import entities.navigation.Navigator;
import grid.WorldGrid;
import media.DrawProperties;
import media.SoundID;
import media.TextureID;
import model.event.EventBus;

import java.util.*;

public class EvilCookie extends Enemy{
    private final DrawProperties drawProperties;
    private float pursuitRange;
    private final float attackRange;

    public EvilCookie(Vector2 position, boolean required, EventBus eventBus) {
        super(position, 3, required, eventBus, 1, 5, 0.25f);
        this.drawProperties = new DrawProperties();
        attackRange = 10;
        pursuitRange = 150;

        Weapon weapon = new Melee(CollisionType.Type.PLAYER, eventBus, SoundID.GUN_SHOT);
        weaponManager.addWeapon(weapon);



        addStateDefinitions(new ArrayList<>(Arrays.asList(
                new StateDefinition(
                    State.DEAD, 
                    new DeadStateCondition(this),
                    new Dead(eventBus, healthManager.isDead(), this)),
                new StateDefinition(
                    State.IDLE, 
                    new IdleStateCondition(), 
                    new Idle(drawProperties, TextureID.COOKIE_ENEMY_IDLE))
        )));

        drawProperties.setAnimation(TextureID.COOKIE_ENEMY_IDLE);
        super.deathSoundID = SoundID.COOKIE_DEATH;
    }

    @Override
    protected void activateCombat() {
        super.activateCombat();
        pursuitRange = 250;
    }

    @Override
    protected void disableCombat() {
        super.disableCombat();
        pursuitRange = 150;
    }

    @Override
    public DrawProperties getProperties() {
        return drawProperties;
    }

    @Override
    protected StateDefinition getPursuitPathState(Navigator navigator, WorldGrid worldGrid) {
        return new StateDefinition(
                State.PURSUIT,
                new PursuitStateCondition(this, physics.getPosition(), targetPosition, originToTarget, pursuitRange, worldGrid),
                new PursuitPath(navigator, physics, drawProperties, TextureID.COOKIE_ENEMY_WALK, TextureID.COOKIE_ENEMY_IDLE)
        );
    }

    @Override
    public void update(double dt) {
        super.update(dt);


    }

    @Override
    public void collisionEnter(ICollidable target, CollisionType.Type type) {

    }

    @Override
    public void collisionLeave(ICollidable target) {
        super.collisionLeave(target);
    }

    @Override
    public CollisionCircle getCollisionPrimitive() {
        return new CollisionCircle(
                physics.getPosition(),
                new Vector2(0, drawProperties.height / 2),
                10,
                this,
                new HashSet<>(),
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.ENEMY)
                ))
        );
    }

    @Override
    public float getPursuitRange() {
        return pursuitRange;
    }

    @Override
    public float getAttackRange() {
        return attackRange;
    }

    @Override
    public void setWorldGrid(WorldGrid worldGrid) {
        super.setWorldGrid(worldGrid);

        addStateDefinitions( new ArrayList<>(List.of(
                new StateDefinition(
                        State.ATTACK,
                        new AttackStateCondition(this, targetPosition, physics.getPosition(), getTileRuleset(), worldGrid),
                        new Attack(originToTarget, weaponManager, drawProperties, TextureID.COOKIE_ENEMY_IDLE)))));
    }
}
