package entities.enemy.enemies;

import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType;
import components.collision.ICollidable;
import components.collision.primitives.CollisionCircle;
import components.weapon.Weapon;
import components.weapon.weapons.GhostGun;
import entities.enemy.Enemy;
import entities.enemy.state.State;
import entities.enemy.state.StateDefinition;
import entities.enemy.state.behaviour.Attack;
import entities.enemy.state.behaviour.Dead;
import entities.enemy.state.behaviour.Idle;
import entities.enemy.state.behaviour.PursuitPath;
import entities.enemy.state.condition.AttackStateCondition;
import entities.enemy.state.condition.DeadStateCondition;
import entities.enemy.state.condition.IdleStateCondition;
import entities.enemy.state.condition.PursuitStateCondition;
import entities.navigation.Navigator;
import grid.TileData.TileRuleset;
import grid.WorldGrid;
import media.DrawProperties;
import media.SoundID;
import media.TextureID;
import model.event.EventBus;

import java.util.*;

public class GhostPepperEnemy extends Enemy{
    private final DrawProperties drawProperties;
    private float pursuitRange;
    private float attackRange;

    public GhostPepperEnemy(Vector2 position, boolean required, EventBus eventBus) {
        super(position, 3, required, eventBus, 1, 5, 0.25f);
        this.drawProperties = new DrawProperties();
        attackRange = 150;
        pursuitRange = 250;
        setRuleset(TileRuleset.FLYING);
        
        Weapon weapon = new GhostGun(CollisionType.Type.PLAYER, eventBus, SoundID.GUN_SHOT);
        weaponManager.addWeapon(weapon);

        addStateDefinitions(new ArrayList<>(Arrays.asList(
                new StateDefinition(
                    State.DEAD, 
                    new DeadStateCondition(this),
                    new Dead(eventBus, healthManager.isDead(), this)),
                new StateDefinition(
                    State.IDLE, 
                    new IdleStateCondition(), 
                    new Idle(drawProperties, TextureID.GHOST_PEPPER_IDLE))
        )));

        drawProperties.setAnimation(TextureID.GHOST_PEPPER_IDLE);
        super.deathSoundID = SoundID.GHOST_DEATH;
    }

    @Override
    protected void activateCombat() {
        super.activateCombat();
        attackRange = 150;
        pursuitRange = 250;
    }

    @Override
    protected void disableCombat() {
        super.disableCombat();
        attackRange = 150;
        pursuitRange = 250;
    }

    @Override
    public DrawProperties getProperties() {
        return drawProperties;
    }

    @Override
    protected StateDefinition getPursuitPathState(Navigator navigator, WorldGrid worldGrid) {
        return new StateDefinition(
                State.PURSUIT,
                new PursuitStateCondition(this, physics.getPosition(), targetPosition, originToTarget, pursuitRange, worldGrid),
                new PursuitPath(navigator, physics, drawProperties, TextureID.GHOST_PEPPER_IDLE, TextureID.GHOST_PEPPER_IDLE)
        );
    }

    @Override
    public void collisionEnter(ICollidable target, CollisionType.Type type) {
    }

    @Override
    public CollisionCircle getCollisionPrimitive() {
        return new CollisionCircle(
                physics.getPosition(),
                new Vector2(0, 12),
                10,
                this,
                new HashSet<>(),
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.ENEMY)
                ))
        );
    }

    @Override
    public float getPursuitRange() {
        return pursuitRange;
    }

    @Override
    public float getAttackRange() {
        return attackRange;
    }

    @Override
    public void setWorldGrid(WorldGrid worldGrid) {
        super.setWorldGrid(worldGrid);
        addStateDefinitions( new ArrayList<>(List.of(
                new StateDefinition(
                        State.ATTACK,
                        new AttackStateCondition(this, targetPosition, physics.getPosition(), getTileRuleset(), worldGrid),
                        new Attack(originToTarget, weaponManager, drawProperties, TextureID.GHOST_PEPPER_SHOOT)))));
       
    }
}

