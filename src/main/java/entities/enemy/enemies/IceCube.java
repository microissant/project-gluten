package entities.enemy.enemies;

import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType;
import components.collision.ICollidable;
import components.collision.primitives.CollisionCircle;
import components.collision.trigger.damage.DamageRadius;
import components.collision.trigger.damage.FalloffConstant;
import components.health.Damage;
import components.physics.reaction.StaticReactions;
import entities.enemy.Enemy;
import entities.enemy.state.State;
import entities.enemy.state.StateDefinition;
import entities.enemy.state.behaviour.Dead;
import entities.enemy.state.behaviour.IceCubeIdle;
import entities.enemy.state.behaviour.IceCubeSpin;
import entities.enemy.state.behaviour.dash.DashHelper;
import entities.enemy.state.behaviour.dash.IceCubeDash;
import entities.enemy.state.condition.AttackStateCondition;
import entities.enemy.state.condition.DeadStateCondition;
import entities.enemy.state.condition.IceCubeDashCondition;
import entities.enemy.state.condition.IdleStateCondition;
import entities.navigation.Navigator;
import grid.WorldGrid;
import media.DrawProperties;
import media.SoundID;
import media.TextureID;
import model.event.EventBus;
import model.event.events.collision.SpawnTriggerEvent;

import java.util.*;

public class IceCube extends Enemy{
    private final DrawProperties drawProperties;
    private float pursuitRange;
    private float attackRange;
    private final DamageRadius impactDamage;
    private final DashHelper dashHelper;

    public IceCube(Vector2 position,boolean required, EventBus eventBus) {
        super(position, 3, required, eventBus, 1, 5, 0.25f);
        this.drawProperties = new DrawProperties(TextureID.ICECUBE_SPIN);
        drawProperties.setAnimationSpeed(0);
        physics.setReaction(StaticReactions.StaticReact.Bounce.reaction);
        attackRange = 150;
        pursuitRange = 150;

        dashHelper = new DashHelper(this);
        addStateDefinitions(new ArrayList<>(Arrays.asList(
                new StateDefinition(
                    State.ATTACK, 
                    new IceCubeDashCondition(dashHelper),
                    new IceCubeDash(dashHelper)),
                new StateDefinition(
                    State.DEAD, 
                    new DeadStateCondition(this),
                    new Dead(eventBus, healthManager.isDead(), this)),
                new StateDefinition(
                    State.IDLE, 
                    new IdleStateCondition(), 
                    new IceCubeIdle(dashHelper))
        )));
        super.deathSoundID = SoundID.CUBE_DEATH;

        impactDamage = new DamageRadius(physics.getPosition(), 15, new Damage(1), 100, new FalloffConstant(), new CollisionType(CollisionType.Type.PLAYER), eventBus);
    }

    @Override
    protected void activateCombat() {
        super.activateCombat();
        attackRange = 250;
        pursuitRange = 250;
    }

    @Override
    protected void disableCombat() {
        super.disableCombat();
        attackRange = 150;
        pursuitRange = 150;
    }

    @Override
    public DrawProperties getProperties() {
        return drawProperties;
    }

    @Override
    protected StateDefinition getPursuitPathState(Navigator navigator, WorldGrid worldGrid) {
        return null;
    }

    @Override
    public void collisionEnter(ICollidable target, CollisionType.Type type) {
        if (physics.getVelocity().len2() < 1) return;
        eventBus.post(new SpawnTriggerEvent(new DamageRadius(impactDamage)));
    }

    @Override
    public CollisionCircle getCollisionPrimitive() {
        return new CollisionCircle(
                physics.getPosition(),
                new Vector2(0, drawProperties.height / 2),
                10,
                this,
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.PLAYER))),
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.ENEMY)
                ))
        );
    }



    @Override
    public float getPursuitRange() {
        return pursuitRange;
    }

    @Override
    public float getAttackRange() {
        return attackRange;
    }

    @Override
    public void setWorldGrid(WorldGrid worldGrid) {
        super.setWorldGrid(worldGrid);
        addStateDefinitions( new ArrayList<>(List.of(
                new StateDefinition(
                        State.PURSUIT,
                        new AttackStateCondition(this, targetPosition, physics.getPosition(), getTileRuleset(), worldGrid),
                        new IceCubeSpin(dashHelper, targetPosition)))
        ));
       
    }

}
