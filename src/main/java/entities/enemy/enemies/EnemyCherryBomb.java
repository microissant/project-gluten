package entities.enemy.enemies;

import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType;
import components.collision.ICollidable;
import components.collision.primitives.CollisionCircle;
import components.collision.trigger.damage.DamageRadius;
import components.collision.trigger.damage.FalloffConstant;
import components.health.Damage;
import entities.enemy.Enemy;
import entities.enemy.state.State;
import entities.enemy.state.StateDefinition;
import entities.enemy.state.behaviour.CherryBombExplosionBehaviour;
import entities.enemy.state.behaviour.CherryDeath;
import entities.enemy.state.behaviour.Idle;
import entities.enemy.state.behaviour.PursuitPath;
import entities.enemy.state.condition.AttackStateCondition;
import entities.enemy.state.condition.DeadStateCondition;
import entities.enemy.state.condition.IdleStateCondition;
import entities.enemy.state.condition.PursuitStateCondition;
import entities.navigation.Navigator;
import entities.particles.ParticleTypes;
import grid.WorldGrid;
import media.DrawProperties;
import media.SoundID;
import media.TextureID;
import model.event.EventBus;
import model.event.events.AddParticleEvent;
import model.event.events.DestructiveExplosionEvent;
import model.event.events.collision.SpawnTriggerEvent;

import java.util.*;

public class EnemyCherryBomb extends Enemy{

    private final DrawProperties drawProperties;
    private float pursuitRange;
    private float attackRange;
    private final DamageRadius explosionDamage;

    public EnemyCherryBomb(Vector2 position, boolean required, EventBus eventBus) {
        super(position, 3, required, eventBus, 1, 5, 0.25f);
        this.drawProperties = new DrawProperties();
        attackRange = 10;
        pursuitRange = 150;
        this.physics.setMaxSpeed(75);

        explosionDamage = new DamageRadius(getPosition(), 10, new Damage(3), 100, new FalloffConstant(), new CollisionType(CollisionType.Type.PLAYER), eventBus);
    

        addStateDefinitions(new ArrayList<>(Arrays.asList(
                new StateDefinition(
                    State.DEAD, 
                    new DeadStateCondition(this),
                    new CherryDeath(eventBus, healthManager.isDead(), this,getPosition())),
                new StateDefinition(
                    State.IDLE, 
                    new IdleStateCondition(), 
                    new Idle(drawProperties, TextureID.CHERRYBOMB_IDLE))
        )));

        drawProperties.setAnimation(TextureID.CHERRYBOMB_IDLE);
        super.deathSoundID = SoundID.CHERRY_DEATH;
    }

    @Override
    protected void activateCombat() {
        super.activateCombat();
        attackRange = 10;
        pursuitRange = 250;
    }

    @Override
    protected void disableCombat() {
        super.disableCombat();
        attackRange = 50;
        pursuitRange = 150;
    }

    @Override
    public DrawProperties getProperties() {
        return drawProperties;
    }

    @Override
    protected StateDefinition getPursuitPathState(Navigator navigator, WorldGrid worldGrid) {
        return new StateDefinition(
                State.PURSUIT,
                new PursuitStateCondition(this, physics.getPosition(), targetPosition, originToTarget, pursuitRange, worldGrid),
                new PursuitPath(navigator, physics, drawProperties, TextureID.CHERRYBOMB_WALK, TextureID.CHERRYBOMB_IDLE)
        );
    }

    @Override
    public void collisionEnter(ICollidable target, CollisionType.Type type) {
    }

    @Override
    public CollisionCircle getCollisionPrimitive() {
        return new CollisionCircle(
                physics.getPosition(),
                new Vector2(0, drawProperties.height / 2),
                10,
                this,
                new HashSet<>(),
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.ENEMY)
                ))
        );
    }

    @Override
    public float getPursuitRange() {
        return pursuitRange;
    }

    @Override
    public float getAttackRange() {
        return attackRange;
    }
    @Override
    public void setWorldGrid(WorldGrid worldGrid) {
        super.setWorldGrid(worldGrid);
        addStateDefinitions( new ArrayList<>(List.of(
                new StateDefinition(
                        State.ATTACK,
                        new AttackStateCondition(this, targetPosition, physics.getPosition(), getTileRuleset(), worldGrid),
                        new CherryBombExplosionBehaviour(this, getProperties(), eventBus)))));
    }

    @Override 
    protected void death() {
        eventBus.post(new AddParticleEvent(ParticleTypes.EXPLOSION_V2,getPosition()));
        eventBus.post(new SpawnTriggerEvent(new DamageRadius(explosionDamage)));
        eventBus.post(new DestructiveExplosionEvent(getPosition(),3));
        super.death();
    }
    
}
