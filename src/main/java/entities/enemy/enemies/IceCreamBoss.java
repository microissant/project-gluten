package entities.enemy.enemies;

import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType;
import components.collision.ICollidable;
import components.collision.primitives.CollisionCircle;
import components.collision.trigger.damage.DamageRadius;
import components.collision.trigger.damage.FalloffConstant;
import components.health.Damage;
import components.health.Healer;
import components.physics.reaction.StaticReactions;
import components.weapon.weapons.IceCreamBossGun;
import entities.enemy.Enemy;
import entities.enemy.state.State;
import entities.enemy.state.StateDefinition;
import entities.enemy.state.behaviour.Dead;
import entities.enemy.state.behaviour.Idle;
import entities.enemy.state.behaviour.iceCreamBoss.BossBounce;
import entities.enemy.state.behaviour.iceCreamBoss.BossShoot;
import entities.enemy.state.condition.AttackStateCondition;
import entities.enemy.state.condition.DeadStateCondition;
import entities.enemy.state.condition.IdleStateCondition;
import entities.navigation.Navigator;
import grid.WorldGrid;
import media.DrawProperties;
import media.SoundID;
import media.TextureID;
import model.event.EventBus;
import model.event.Subscribe;
import model.event.events.collision.CloseDoorsEvent;
import model.event.events.collision.SpawnTriggerEvent;

import java.util.*;

public class IceCreamBoss extends Enemy{
    private final DrawProperties drawProperties;
    private float pursuitRange;
    private float attackRange;
    private final DamageRadius impactDamage;

    public IceCreamBoss(Vector2 position,boolean required, EventBus eventBus) {
        super(position, 3, required, eventBus, 1, 5, 0.25f);
        this.drawProperties = new DrawProperties(TextureID.ICECREAMBOSS_IDLE);
        physics.setReaction(StaticReactions.StaticReact.Bounce.reaction);
        physics.setBoxCollision(16*3, 4, 16, 16);
        
        this.healthManager.increaseMaximumHealth(30);
        healthManager.heal(new Healer(100));
        healthManager.enableImmunity();
        this.weaponManager.addWeapon(new IceCreamBossGun(CollisionType.Type.PLAYER, eventBus, null));
        addStateDefinitions(new ArrayList<>(Arrays.asList(
                new StateDefinition(
                    State.DEAD, 
                    new DeadStateCondition(this),
                    new Dead(eventBus, healthManager.isDead(), this)),
                new StateDefinition(
                    State.IDLE, 
                    new IdleStateCondition(), 
                    new Idle(drawProperties, TextureID.ICECREAMBOSS_IDLE))
        )));
        super.deathSoundID = SoundID.CUBE_DEATH;
        eventBus.register(this);
        attackRange = 0;
        pursuitRange = 0;
        impactDamage = new DamageRadius(physics.getPosition(), 20, new Damage(3), 100, new FalloffConstant(), new CollisionType(CollisionType.Type.PLAYER), eventBus);
    }

    @Override
    protected void activateCombat() {
        super.activateCombat();
    }

    @Override
    protected void disableCombat() {
        super.disableCombat();
    }

    @Override
    public DrawProperties getProperties() {
        return drawProperties;
    }

    @Override
    protected StateDefinition getPursuitPathState(Navigator navigator, WorldGrid worldGrid) {
        return null;
    }

    @Override
    public void collisionEnter(ICollidable target, CollisionType.Type type) {
        if (physics.getVelocity().len2() < 1) return;
        eventBus.post(new SpawnTriggerEvent(new DamageRadius(impactDamage)));
    }

    @Override
    public CollisionCircle getCollisionPrimitive() {
        return new CollisionCircle(
                physics.getPosition(),
                new Vector2(0, drawProperties.height / 2),
                20,
                this,
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.PLAYER))),
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.ENEMY)
                ))
        );
    }


    @Subscribe
    public void activate(CloseDoorsEvent event){
        if(event == null) return;
        healthManager.disableImmunity();
        attackRange = 450;
        pursuitRange = 450;
        
    }

    @Override
    public float getPursuitRange() {
        return pursuitRange;
    }

    @Override
    public float getAttackRange() {
        return attackRange;
    }

    @Override
    public void setWorldGrid(WorldGrid worldGrid) {
        super.setWorldGrid(worldGrid);
        
        addStateDefinitions( new ArrayList<>(List.of(
                new StateDefinition(
                        State.ATTACK,
                        new AttackStateCondition(this, targetPosition, physics.getPosition(), getTileRuleset(), worldGrid),
                        new BossShoot(originToTarget, this, weaponManager, drawProperties, TextureID.ICECREAMBOSS_JUMP),
                        new BossBounce(originToTarget, this, drawProperties, TextureID.ICECREAMBOSS_SPIN))))
            );
    }

}
