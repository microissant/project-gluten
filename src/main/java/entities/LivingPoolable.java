package entities;

import com.badlogic.gdx.utils.Pool.Poolable;

public interface LivingPoolable extends Poolable{

    /**
     * returns whether this object is currently active
     */
    boolean isAlive();
}
