package entities;

public interface ITargeter<E> {

    /**
     * @return true if the targeter has a target aquired
     */
    boolean hasTarget();

    /**
     * @param target target for the entity
     */
    void setTarget(E target);

    /**
     * @return the current target or null if target is not set
     */
    E getTarget();
    
}
