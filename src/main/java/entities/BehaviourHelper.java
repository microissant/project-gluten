package entities;

import com.badlogic.gdx.math.Vector2;
import grid.WorldGrid;
import media.DrawProperties;

/**
 * BehaviourHelper is a utility class providing helper methods for various entity behaviors,
 * including line of sight checks, setting animation direction, and setting the frame of a rotating animation.
 */
public class BehaviourHelper {

    /** checks if there is a tile with a property specified by the ruleset argument between the start and end position
     *  typically used to see if an enemy can currently see the player or reach the player by walking/flying in a straight line.
     *  the start and end of the line are interchangable
     * @param grid the grid containing the walls
     * @param start the start position of the line
     * @param end the end position of the line
     * @param ruleset the set of properties to check for
     * @return true if none of the specified properties are between the end and start of the line, and false otherwise
     */
    public static boolean lineOfSightCheck(WorldGrid grid,Vector2 start, Vector2 end, int ruleset){
        float dx = end.x-start.x;
        float dy = end.y-start.y;

        int curCellX = toCellCoordinate(start.x,grid);
        int curCellY = toCellCoordinate(start.y,grid);

        int targetCellX = toCellCoordinate(end.x, grid);
        int targetCellY = toCellCoordinate(end.y, grid);

        if(curCellX==targetCellX && curCellY==targetCellY) return true;
        int dirX  = (int) Math.signum(targetCellX-curCellX);

        //edge case
        if(dirX == 0){
            int minY = Math.min(curCellY,targetCellY);
            int maxY = Math.max(curCellY,targetCellY);
            for(int cellY = minY; cellY <= maxY; cellY++){
                if(grid.checkForTileProperty(cellY,curCellX, ruleset)){
                    return false;
                }
            }
            return true;
        }

        //get the coefficients for the equation y = mx +b
        float m = dy/dx;
        float b = start.y - start.x*m;

        int smallestX = Math.min(curCellX,targetCellX);
        int biggestX = Math.max(curCellX,targetCellX);
        int minY = Math.min(curCellY,targetCellY);

        curCellY = toCellCoordinate(m*smallestX*grid.cellSize+b,grid);

        for(int xCell = smallestX; xCell<=biggestX; xCell++){
            float nextY = m * ((xCell+1) * grid.cellSize ) + b;
            int nextYCell = toCellCoordinate(nextY, grid);
            
            int smallest = Math.min(curCellY,nextYCell);
            smallest = Math.max(smallest,minY);
            int biggest = Math.max(curCellY, nextYCell);


            for(int yCell = smallest; yCell<=biggest; yCell++){
                if(grid.checkForTileProperty(yCell,xCell, ruleset)){
                    return false;
                }
            }

            curCellY = nextYCell;
        }

        return true;
    }

    private static int toCellCoordinate(float pos, WorldGrid grid){
        return (int) pos/grid.cellSize;
    }


    /**
     * Sets the direction of an animation based on the provided direction value (dirX).
     *
     * @param drawProperties the DrawProperties object associated with the animation
     * @param dirX           the direction value used to determine animation direction (positive for right, negative for left)
     */
    public static void setAnimationDirection(DrawProperties drawProperties, float dirX) {
        if (drawProperties == null) return;

        if (dirX > 0) {
            drawProperties.scaleX = 1;
        } else {
            drawProperties.scaleX = -1;
        }
    }

    /**
     * Sets the frame of a rotating animation based on the provided angle.
     *
     * @param properties the DrawProperties object associated with the animation
     * @param angle      the angle (in degrees) used to determine the frame of the rotating animation
     */
    public static void setFrameOfRotatingAnimation(DrawProperties properties, float angle){
        angle /=360;
        angle+=0.5 / (float)properties.numFrames();
        angle = (angle) % 1f;
        if(angle<0) angle+=1;
        properties.setAnimationProgress(angle);
    }
}
