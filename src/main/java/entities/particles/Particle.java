package entities.particles;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import entities.LivingPoolable;
import media.DrawProperties;
import media.Renderable;
import model.IUpdatable;
import model.event.EventBus;

public class Particle implements Renderable, LivingPoolable, IUpdatable{

    private final DrawProperties drawProperties;
    private final Vector2 position;
    private ParticleBehaviour behaviour;
    public boolean alive;


    public Particle(){
        drawProperties = new DrawProperties();
        position = new Vector2();
        alive = false;
        behaviour = null;
    }


    /**
     * Initialize the particle with default values
     *
     * @param position world position of the particle
     * @param type particle type
     * @param bus eventbus
     */
    public void init(Vector2 position, ParticleTypes type, EventBus bus){
        drawProperties.setAnimation(type.texture);
        this.position.set(position);
        behaviour = type.behaviour;
        behaviour.start(bus);
        alive = true;
    }


    @Override
    public DrawProperties getProperties() {
        return drawProperties;
    }

    @Override
    public Vector2 getPosition() {
        return position;
    }

    @Override
    public void reset() {
        alive = false;
        behaviour = null;
    }

    @Override 
    public void draw(SpriteBatch batch, float deltaTime){
        behaviour.update(drawProperties, deltaTime, position);
        if(drawProperties.getTimeSinceAnimationStart()+deltaTime>=drawProperties.getAnimationDuration()) alive = false;
        else Renderable.super.draw(batch, deltaTime);
    }


    @Override
    public boolean isAlive() {
        return alive;
    }

}
