package entities.particles;

import media.TextureID;

public enum ParticleTypes {
    EXPLOSION_V1(TextureID.EXPLOSION,new ExplosionParticleBehaviour()),
    EXPLOSION_V2(TextureID.EXPLOSION_V2,new ExplosionParticleBehaviour()),
    BULLETDEATH(TextureID.BULLET_END,new NoParticleBehaviour());


    public final TextureID texture; 
    public final ParticleBehaviour behaviour;
    ParticleTypes(TextureID texture, ParticleBehaviour behaviour){
        this.texture = texture;
        this.behaviour = behaviour;
    }

}
