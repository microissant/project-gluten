package entities.particles;

import com.badlogic.gdx.math.Vector2;
import media.DrawProperties;
import media.SoundID;
import model.event.EventBus;
import model.event.events.ExplosionEvent;
import model.event.events.audio.PlaySoundEvent;

public class ExplosionParticleBehaviour implements ParticleBehaviour {

    @Override
    public void update(DrawProperties drawProperties, float deltaTime, Vector2 position) {
    }

    @Override
    public void start(EventBus bus) {
        bus.post(new ExplosionEvent());
        bus.post(new PlaySoundEvent(SoundID.EXPLOSION));
    }
    
}
