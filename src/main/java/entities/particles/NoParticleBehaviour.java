package entities.particles;

import com.badlogic.gdx.math.Vector2;
import media.DrawProperties;

public class NoParticleBehaviour implements ParticleBehaviour{

    @Override
    public void update(DrawProperties drawProperties, float deltaTime, Vector2 position) {
    }
}
