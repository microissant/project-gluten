package entities.particles;

import com.badlogic.gdx.math.Vector2;
import media.DrawProperties;
import model.event.EventBus;

public interface ParticleBehaviour {
    /**
     * determines the way this projectile will behave
     */
    void update(DrawProperties drawProperties, float deltaTime, Vector2 position);

    /**
     * Start the particle behavior
     *
     * @param bus eventbus
     */
    default void start(EventBus bus){}
}

