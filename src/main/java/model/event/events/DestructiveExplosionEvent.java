package model.event.events;

import com.badlogic.gdx.math.Vector2;

public record DestructiveExplosionEvent(Vector2 position, int radius) implements Event{
    
}
