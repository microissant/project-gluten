package model.event.events.entities;

import item.Item;
import model.event.events.Event;

public record SpawnItemEvent(Item item) implements Event {
}
