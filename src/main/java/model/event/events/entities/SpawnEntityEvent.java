package model.event.events.entities;

import entities.Entity;
import model.event.events.Event;

public record SpawnEntityEvent<E extends Entity>(E entity) implements Event {
}
