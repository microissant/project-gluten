package model.event.events.entities;

import entities.enemy.IEnemy;
import model.event.events.Event;

public record CreateCrateEvent(IEnemy crate) implements Event{
    
}
