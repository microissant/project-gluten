package model.event.events.entities;

import components.weapon.projectile.IProjectile;
import model.event.events.Event;

public record ProjectileDeathEvent(IProjectile projectile) implements Event {
}
