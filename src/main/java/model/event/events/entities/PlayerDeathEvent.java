package model.event.events.entities;

import entities.player.PlayerManager;
import model.event.events.Event;

public record PlayerDeathEvent(PlayerManager playerManager) implements Event {
}
