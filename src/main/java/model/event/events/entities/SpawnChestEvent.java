package model.event.events.entities;

import item.chest.IChest;
import model.event.events.Event;

public record SpawnChestEvent(IChest chest) implements Event {
}
