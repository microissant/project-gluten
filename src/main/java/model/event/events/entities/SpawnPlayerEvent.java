package model.event.events.entities;

import entities.player.PlayerManager;
import model.event.events.Event;

public record SpawnPlayerEvent(PlayerManager playerManager) implements Event {
}
