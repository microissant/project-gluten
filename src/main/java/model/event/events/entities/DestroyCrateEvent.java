package model.event.events.entities;

import com.badlogic.gdx.math.Vector2;

import model.event.events.Event;

public record DestroyCrateEvent(Vector2 position) implements Event{
    
}
