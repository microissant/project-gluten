package model.event.events.entities;

import entities.inanimate.EndPortal;
import model.event.events.Event;

public record SpawnEndPortalEvent(EndPortal endPortal) implements Event {
}
