package model.event.events;

import components.weapon.projectile.IProjectile;

public record AddedProjectilesEvent(Iterable<IProjectile> projectiles) implements Event{
    
}
