package model.event.events;

import grid.WorldGrid;

public record WorldGridCreatedEvent(WorldGrid worldGrid) implements Event {
    
}
