package model.event.events.audio;

import media.SoundID;
import model.event.events.Event;

public record StopSoundEvent(SoundID soundID) implements Event {
}
