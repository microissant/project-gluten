package model.event.events.audio;

import media.SoundID;
import model.event.events.Event;

public record PlaySoundEvent(SoundID soundID, boolean isLooping) implements Event {

    public PlaySoundEvent(SoundID soundID) {
        this(soundID, false);
    }

}
