package model.event.events;

import com.badlogic.gdx.math.Vector2;

import entities.particles.ParticleTypes;

public record AddParticleEvent(ParticleTypes particleType, Vector2 position) implements Event{
    
}
