package model.event.events.ui;

import model.event.events.Event;

public record GameCompleteStats(String time) implements Event {
}
