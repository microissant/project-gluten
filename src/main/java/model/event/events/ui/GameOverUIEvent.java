package model.event.events.ui;

import model.event.events.Event;
import model.state.Stat;

import java.util.List;

public record GameOverUIEvent(List<Stat> stats) implements Event {
}
