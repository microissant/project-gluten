package model.event.events.ui;

import model.event.events.Event;

public record MenuInputEvent(int keycode) implements Event {
}
