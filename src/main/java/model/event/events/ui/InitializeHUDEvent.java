package model.event.events.ui;

import model.event.events.Event;

public record InitializeHUDEvent() implements Event {
}
