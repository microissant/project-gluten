package model.event.events.ui;

import model.event.events.Event;

public record LevelStatsEvent(String time) implements Event {
}
