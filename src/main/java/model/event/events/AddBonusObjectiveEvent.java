package model.event.events;

import model.state.objective.IObjective;

public record AddBonusObjectiveEvent(IObjective objective) implements Event {
}
