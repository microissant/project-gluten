package model.event.events;

import components.weapon.Weapon;
import components.weapon.projectile.emitter.ProjectileStruct;
import entities.Entity;

import java.util.List;

public record FireWeaponEvent(Entity originEntity, Weapon originWeapon, List<ProjectileStruct> projectiles) implements Event {
}
