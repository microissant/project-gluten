package model.event.events;

import model.state.objective.IObjective;

public record BonusObjectiveCompleteEvent(IObjective objective) implements Event {
}
