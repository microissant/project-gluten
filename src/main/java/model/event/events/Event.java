package model.event.events;

/**
 * <p>Base type of all event.</p>
 *
 * <p>
 *     Every event object must implement this interface for the {@code EventBus} to work with the event.
 * </p>
 */
public interface Event {}
