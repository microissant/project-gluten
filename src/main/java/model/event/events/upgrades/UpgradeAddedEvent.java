package model.event.events.upgrades;

import components.upgrade.upgrades.IUpgrade;
import model.event.events.Event;

public record UpgradeAddedEvent(IUpgrade upgrade) implements Event {
}
