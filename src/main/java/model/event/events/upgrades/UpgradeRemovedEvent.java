package model.event.events.upgrades;

import components.upgrade.upgrades.IUpgrade;
import model.event.events.Event;

public record UpgradeRemovedEvent(IUpgrade upgrade) implements Event {
}
