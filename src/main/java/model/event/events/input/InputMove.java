package model.event.events.input;

import com.badlogic.gdx.math.Vector2;
import model.event.events.Event;

public record InputMove(Vector2 direction) implements Event {}
