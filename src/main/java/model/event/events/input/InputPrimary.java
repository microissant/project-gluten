package model.event.events.input;

import com.badlogic.gdx.math.Vector2;
import model.event.events.Event;

public record InputPrimary(Vector2 position) implements Event {}
