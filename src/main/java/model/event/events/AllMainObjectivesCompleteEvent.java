package model.event.events;

public record AllMainObjectivesCompleteEvent() implements Event {
}
