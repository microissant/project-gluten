package model.event.events.player;

import com.badlogic.gdx.math.Vector2;

import model.event.events.Event;

public record DodgeInDirectionEvent(Vector2 direction) implements Event {
    public DodgeInDirectionEvent(Vector2 direction) {
        this.direction = new Vector2(direction);
    }
}
