package model.event.events.item;


import model.event.events.Event;

public record ChestCloseEvent() implements Event {
}
