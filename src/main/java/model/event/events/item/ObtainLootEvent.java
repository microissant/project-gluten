package model.event.events.item;

import item.loot.ILoot;
import model.event.events.Event;

public record ObtainLootEvent(ILoot loot) implements Event {
}
