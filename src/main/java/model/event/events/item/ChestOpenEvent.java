package model.event.events.item;

import item.chest.IChest;
import model.event.events.Event;

public record ChestOpenEvent(IChest chest) implements Event {
}
