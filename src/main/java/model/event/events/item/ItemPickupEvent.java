package model.event.events.item;

import item.InteractAgent;
import item.Item;
import model.event.events.Event;

public record ItemPickupEvent(Item item, InteractAgent agent) implements Event {
}
