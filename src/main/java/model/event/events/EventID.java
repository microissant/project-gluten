package model.event.events;

public enum EventID {

    INPUT_PRIMARY,
    INPUT_SECONDARY,
    FIRE_WEAPON,
    DEATH_PLAYER,
    DEATH_ENEMY,
    ITEM_PICKUP,
    MOVE_PLAYER

}
