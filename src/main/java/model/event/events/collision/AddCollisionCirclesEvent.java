package model.event.events.collision;

import com.badlogic.gdx.utils.Array;
import components.collision.primitives.CollisionCircle;
import model.event.events.Event;

public record AddCollisionCirclesEvent(Array<CollisionCircle> circles) implements Event {
}
