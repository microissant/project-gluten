package model.event.events.collision;

import entities.inanimate.CloseDoorsTrigger;
import model.event.events.Event;

public record CreateDoorEvent(CloseDoorsTrigger doorTrigger) implements Event{
    
}
