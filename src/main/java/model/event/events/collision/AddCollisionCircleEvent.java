package model.event.events.collision;

import components.collision.primitives.CollisionCircle;
import model.event.events.Event;

public record AddCollisionCircleEvent(CollisionCircle circle) implements Event {
}
