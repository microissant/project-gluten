package model.event.events.collision;

import components.collision.trigger.ITrigger;
import model.event.events.Event;

public record RemoveTriggerEvent(ITrigger trigger) implements Event {
}
