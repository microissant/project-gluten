package model.event.events.collision;

import com.badlogic.gdx.math.Vector2;

import model.event.events.Event;

public record CloseDoorsEvent(Vector2 position) implements Event{
    
}
