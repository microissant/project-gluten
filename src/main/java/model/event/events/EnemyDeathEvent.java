package model.event.events;


import entities.enemy.IEnemy;

public record EnemyDeathEvent(IEnemy enemy) implements Event {
}
