package model.event.events.game;

import model.event.events.Event;

public record LoadNextLevelEvent() implements Event {
}
