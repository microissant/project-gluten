package model.event.events.game;

import model.event.events.Event;
import model.state.objective.IObjective;

public record AddMainObjectiveEvent(IObjective objective) implements Event {
}
