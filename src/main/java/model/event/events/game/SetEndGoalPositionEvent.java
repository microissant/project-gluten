package model.event.events.game;

import com.badlogic.gdx.math.Vector2;
import model.event.events.Event;

public record SetEndGoalPositionEvent(Vector2 position) implements Event {

    public SetEndGoalPositionEvent(Vector2 position) {
        this.position = new Vector2(position);
    }
}
