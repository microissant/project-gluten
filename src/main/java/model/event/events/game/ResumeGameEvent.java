package model.event.events.game;

import model.event.events.Event;

public record ResumeGameEvent() implements Event {
}
