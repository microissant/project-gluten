package model.event.events;

import components.health.Damage;
import entities.Entity;

public record DamageAppliedEvent(Damage damage, Entity target) {
}
