package model.event;

import model.event.events.Event;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;


/**
 * <p>
 *     The central dispatcher for events. It maintains a registry of subscribers
 *     and their corresponding event handler methods. The EventBus class typically
 *     provides methods for registering and unregistering subscribers, as well as
 *     for posting events.
 * </p>
 *
 * <p>
 *     A subscriber (a receiver of an event), must register itself to the event bus
 *     and include a method which is called post is called. This method in the subscriber
 *     must include a @Subscribe tag, and have a parameter of type Event in order to be
 *     registered by the event bus.
 * </p>
 *
 * <p>
 *     A publisher (object that post an event) must only call {@code EventBus::post()} in
 *     order for the currently active subscribers to be notified.
 * </p>
 *
 * <p>
 *     <p><b>How to use, example:</b></p>
 *     Given the two classes: {@code Controller} and {@code Player}. We want the {@code Controller}
 *     to tell the {@code Player} to fire his weapon:
 *
 *     <pre>
 *     {@code
 *     public class Player {
 *         Weapon weapon;
 *
 *         public Player(EventBus eventBus) {
 *             eventBus.register(this);
 *         }
 *
 *         private void fireWeapon(){
 *             weapon.fire();
 *         }
 *
 *         @Subscribe
 *         public void listenEvent(FireWeaponEvent event) {
 *             fireWeapon();
 *         }
 *     }
 *
 *     public class Controller {
 *         EventBus eventBus;
 *
 *         public Controller(EventBus eventBus) {
 *             this.eventBus = eventBus;
 *         }
 *
 *         public void handleInput() {
 *             if (input.Mouse.Left)
 *                 eventBus.post(new FireWeaponEvent());
 *         }
 *     }
 *     }
 *     </pre>
 *
 *
 * </p>
 *
 */
public class EventBus {

    private final Map<Class<?>, List<SubscriberMethod>> subscribers;

    public EventBus() {
        subscribers = new HashMap<>();
    }

    /**
     * <p>Register an subscriber</p>
     *
     * <p>
     *     Te be registered the subscriber needs to have a method with the tag
     *     {@code @Subscribe} and must have a {@code Event} type in their first parameter
     *     list. When post is called with the same {@code Event} type is called, the method
     *     is triggered.
     * </p>
     *
     * <p>
     *     <pre>
     *         {@code
     *          @Subscribe
     *          public void eventListener(MyEventType event) {
     *              -- event logic here --
     *          }
     *         }
     *     </pre>
     *
     * </p>
     *
     * @param subscriber object with a {@code @Subscribe} method that will be triggered upon {@code post()}
     */
    public void register(Object subscriber) {
        if (subscriber == null) return;

        for (Method method : subscriber.getClass().getDeclaredMethods()) {
            if (!method.isAnnotationPresent(Subscribe.class)) continue;

            Class<?> eventType = method.getParameterTypes()[0];

            subscribers.computeIfAbsent(eventType, k -> new CopyOnWriteArrayList<>())
                    .add(new SubscriberMethod(subscriber, method));
        }
    }

    /**
     * Post to all subscribers of the event. All subscribers that has been registered with
     * the same {@code Event} type in their {@code @Subscribe} method parameter list
     * will trigger.
     *
     * @param event event to be sent to the subscribers
     */
    public void post(Event event) {
        if (event == null) return;
        List<SubscriberMethod> subscriberMethods = subscribers.get(event.getClass());
        if (subscriberMethods == null) return;
        for (SubscriberMethod subscriberMethod : subscriberMethods) {
            try {
                subscriberMethod.method.invoke(subscriberMethod.subscriber, event);
            } catch (InvocationTargetException e) {
                Throwable cause = e.getCause();
                System.err.println("Error while invoking subscriber method:");
                cause.printStackTrace();
                throw new RuntimeException("Event cannot be triggered: " + event, cause);
            } catch (Exception e) {
                System.err.println("Error while invoking subscriber method:");
                e.printStackTrace();
                throw new RuntimeException("Event cannot be triggered: " + event, e);
            }
        }
    }

    private record SubscriberMethod(Object subscriber, Method method) {
    }

    /**
     * Remove the given subscriber from every {@code Event}. The subscriber will no
     * longer trigger it's {@code @Subscribe} method when {@code post()} is called
     *
     * @param subscriber to be removed
     */
    public void unregister(Object subscriber) {
        for (List<SubscriberMethod> subscriberMethods : subscribers.values()) {
            subscriberMethods.removeIf(subscriberMethod ->
                    subscriberMethod.subscriber.equals(subscriber));
        }
    }

}
