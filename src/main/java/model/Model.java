package model;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import components.collision.CollisionManager;
import components.collision.CollisionType;
import components.weapon.weapons.ShotGun;
import components.weapon.weapons.TopperGun;
import entities.Entity;
import entities.enemy.IEnemy;
import entities.particles.Particle;
import entities.player.PlayerManager;
import grid.WorldGrid;
import item.items.WeaponItem;
import media.Renderable;
import media.SoundID;
import model.event.EventBus;
import model.event.Subscribe;
import model.event.events.DestructiveExplosionEvent;
import model.event.events.entities.SpawnItemEvent;
import model.event.events.game.LoadNextLevelEvent;
import model.event.events.game.NewGameEvent;
import model.event.events.game.RestartLevelEvent;
import model.level.LevelManager;
import model.state.GameState;
import model.state.GameStateManager;
import view.DebugShape;
import view.ui.hud.HUDInformation;

import java.util.Collection;
import java.util.List;

public class Model implements IUpdatable {

    private final EntityManager entityManager;
    private final LevelManager levelManager;
    private final GameStateManager gameStateManager;
    private final CollisionManager collisionManager;
    private final EventBus eventBus;
    private final HUDInformation hudInformation;

    /**
     * Constructs the model with an event bus, level manager, entity manager, collision manager, and game state manager.
     * Registers this model as an event subscriber.
     *
     * @param eventBus the event bus used for game events
     */
    public Model(EventBus eventBus) {
        this.eventBus = eventBus;
        eventBus.register(this);
        levelManager = new LevelManager(eventBus);
        entityManager = new EntityManager(eventBus);
        collisionManager = new CollisionManager(100, 100, 25, eventBus);
        gameStateManager = new GameStateManager(eventBus, levelManager);
        hudInformation = new HUDInformation();
    }

    /**
     * Event subscriber method that is called when a new game event is posted.
     * Initializes the game by clearing all entities and loading the first level.
     *
     * @param event the new game event
     */
    @Subscribe
    public void newGameEvent(NewGameEvent event) {
        initializeGame();
    }

    private void initializeGame() {
        clearAllEntities();
        levelManager.clear();
        loadNextLevel();
    }

    private void clearAllEntities() {
        entityManager.clear();
        collisionManager.clear();
    }

    /**
     * Event subscriber method that is called when a load next level event is posted.
     * Clears all entities, the game state manager, and loads the next level.
     *
     * @param event the load next level event
     */
    @Subscribe
    public void nextLevel(LoadNextLevelEvent event) {
        gameStateManager.clear();
        clearAllEntities();
        loadNextLevel();
    }

    /**
     * Event subscriber method that is called when a restart level event is posted.
     * Resets the current level and loads it again.
     *
     * @param event the restart level event
     */
    @Subscribe
    public void restartLevel(RestartLevelEvent event) {
        levelManager.resetCurrentLevel();
        nextLevel(null);
    }

    private void loadNextLevel() {
        levelManager.loadNextLevel();
        entityManager.setWorldGrid(getWorldGrid());

        eventBus.post(new SpawnItemEvent(
                new WeaponItem(
                        new Vector2(1240, 130),
                        new ShotGun(
                                CollisionType.Type.ENEMY,
                                eventBus, SoundID.GUN_SHOT),
                        eventBus
                        )));

        eventBus.post(new SpawnItemEvent(
                new WeaponItem(
                        new Vector2(1200, 150),
                        new TopperGun(
                                CollisionType.Type.ENEMY,
                                eventBus, SoundID.GUN_SHOT),
                        eventBus
                )));
    }

    /**
     * @return the world grid representing the game world
     */
    public WorldGrid getWorldGrid() {
        return levelManager.getWorldGrid();
    }


    @Override
    public void update(double deltaTime) {
        if (gameStateManager.getGameState() != GameState.ACTIVE) return;
        entityManager.update(deltaTime);
        collisionManager.update(deltaTime);
        gameStateManager.update(deltaTime);
    }

    /**
     * HUD information contains all information to be displayed on the game HUD
     *
     * @return HUDInformation with information from various parts of the model to be displayed
     */
    public HUDInformation getHUDInformation() {
        hudInformation.currentHealth = getPlayer().getPlayerCharacter().getCurrentHealth();
        hudInformation.maxHealth = getPlayer().getPlayerCharacter().getMaximumHealth();
        hudInformation.coins = getPlayer().getCoinBalance().getValue();
        hudInformation.timeElapsed = gameStateManager.getStatistics().getCurrentLevelTimeElapsed();

        if (getPlayer().getWeaponManager().getPrimary() != null) {
            hudInformation.ammoCounter = getPlayer().getWeaponManager().getPrimary().getCurrentAmmo();
            hudInformation.weaponIconID = getPlayer().getWeaponManager().getPrimary().getDisplayIcon();
        }
        hudInformation.interactableTarget = getPlayer().getPlayerCharacter().getInteractTarget();

        return hudInformation;
    }

    /**
     * @return iterable of all projectiles in the game
     */
    public Iterable<? extends Entity> getProjectiles() {
        return entityManager.getProjectiles();
    }

    /**
     * @return current player manager in the game
     */
    public PlayerManager getPlayer() {
        return entityManager.getPlayerManager();
    }

    /**
     * @return list of all enemies in the game
     */
    public List<IEnemy> getEnemies() {
        return entityManager.getEnemies();
    }

    /**
     * @return list of all various entities in the game
     */
    public List<Entity> getVariousEntities() {
        return entityManager.getVariousEntities();
    }

    /**
     * @return array of all the particles in the game
     */
    public Array<Particle> getParticles(){
        return entityManager.getParticles();
    }

    /**
     * @return collection of all the items in the game
     */
    public Collection<? extends Renderable> getItems() {
        return entityManager.getItems();
    }

    /**
     * @return collection of all the debug shapes in the game
     */
    public Collection<DebugShape> getDebugShapes() {
        return collisionManager.getCollisionCircles();
    }

    /**
     * remove tiles from the world grid if they are inside an explosion radius
     *
     * @param event event describing the tiles to be removed
     */
    @Subscribe 
    public void destructiveExplosion(DestructiveExplosionEvent event){
        if(event == null) return;
        if(event.position() == null) return;
        getWorldGrid().explodeTiles(event.position(), event.radius());
    }
}
