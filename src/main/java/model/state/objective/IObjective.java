package model.state.objective;

import model.IUpdatable;
import model.event.EventBus;
import model.state.GameStatistics;

/**
 * Interface representing an objective in a game that can be updated and provides various
 * information about the objective's state.
 */
public interface IObjective extends IUpdatable {

    /**
     * Sets the game statistics for the objective.
     *
     * @param statistics The GameStatistics instance to be used by the objective.
     */
    default void setStatistics(GameStatistics statistics) {}

    /**
     * Returns the current progression of the objective as a float value between 0 and 1.
     *
     * @return The progression of the objective (0.0f to 1.0f).
     */
    float getProgression();

    /**
     * Returns the current count of the objective.
     *
     * @return The current count of the objective.
     */
    int currentCount();

    /**
     * Returns the maximum count of the objective.
     *
     * @return The maximum count of the objective.
     */
    int maxCount();

    /**
     * Returns whether the objective is complete or not.
     *
     * @return true if the objective is complete, false otherwise.
     */
    boolean isComplete();

    /**
     * Returns the title of the objective.
     *
     * @return The title of the objective as a String.
     */
    String title();

    /**
     * Returns the description of the objective.
     *
     * @return The description of the objective as a String.
     */
    String description();

    /**
     * Sets the event bus for the objective.
     *
     * @param eventBus The EventBus instance to be used by the objective.
     */
    default void setEventBus(EventBus eventBus) {}

    /**
     * Clear current conditions and set them to default
     */
    void reset();

    /**
     * @return a copy of the objective
     */
    IObjective copy();
}
