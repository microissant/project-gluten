package model.state.objective;

import components.health.Killable;
import entities.enemy.IEnemy;
import model.event.Subscribe;
import model.event.events.entities.SpawnEntityEvent;

import java.util.HashSet;
import java.util.Set;

/**
 * KillRequiredTargets is a class that implements the IObjective interface, representing an
 * objective in which the player must eliminate a specific set of targets to complete it.
 *
 * <p>
 *     Each enemy with a required tag will be included in the target set.
 * </p>
 *
 * <p>
 *     Note that enemies added to the game before this game mode is activated will not be added
 *     to the target set.
 * </p>
 */
public class KillRequiredTargets implements IObjective {

    private final Set<Killable> targets;

    private boolean hasStarted;

    private int currentCount;
    private int maxCount;

    public KillRequiredTargets() {
        this.targets = new HashSet<>();
        reset();
    }

    @Override
    public float getProgression() {
        return (float) currentCount() / (float) maxCount();
    }

    @Override
    public int currentCount() {
        return currentCount;
    }

    @Override
    public void update(double deltaTime) {
        Set<Killable> toBeRemoved = new HashSet<>();

        for (Killable target: targets) {
            if (!target.isDead()) continue;
            toBeRemoved.add(target);
            currentCount++;
        }

        targets.removeAll(toBeRemoved);
    }

    @Override
    public int maxCount() {
        return maxCount;
    }

    @Override
    public boolean isComplete() {
        return hasStarted && targets.isEmpty();
    }

    @Override
    public String title() {
        return "Kill targets";
    }

    @Override
    public String description() {
        return targets.size() + " targets remains";
    }

    @Override
    public void reset() {
        hasStarted = false;
        targets.clear();
    }

    @Override
    public IObjective copy() {
        return new KillRequiredTargets();
    }

    /**
     * Register an enemy if it is tagged as required
     *
     * @param event enemy spawn event
     */
    @Subscribe
    public void registerEnemy(SpawnEntityEvent<IEnemy> event) {
        if (event == null) return;
        if (event.entity() == null) return;

        if (!event.entity().isRequiredTarget()) return;

        maxCount++;
        hasStarted = true;
        targets.add(event.entity());
    }

}
