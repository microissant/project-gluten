package model.state.objective;

import model.IUpdatable;
import model.event.EventBus;
import model.event.events.AllMainObjectivesCompleteEvent;

/**
 * ObjectiveManager is a class that manages the main objective of the game.
 * It updates the main objective and posts an event when it is completed.
 * The class checks if the main objective is complete during the update process,
 * and if so, it unregisters the objective from the EventBus, posts an event,
 * and sets the main objective to null.
 */
public class ObjectiveManager implements IUpdatable {

    private IObjective mainObjective;
    private final EventBus eventBus;

    public ObjectiveManager(EventBus eventBus) {
        this.eventBus = eventBus;
    }

    /**
     * Sets the main objective of the game.
     * If the given objective is not null, it is set as the main objective,
     * and the EventBus registers the objective for event handling.
     *
     * @param objective The IObjective instance to be set as the main objective.
     */
    public void setMainObjective(IObjective objective) {
        if (objective == null) return;
        mainObjective = objective.copy();
        eventBus.register(mainObjective);
    }

    /**
     * Updates the main objective and checks if it is completed.
     * If the main objective is not null and not already completed,
     * it updates the objective with the given deltaTime.
     * After the update, if the main objective is completed,
     * it calls the mainObjectiveJustCompleted() method.
     *
     * @param deltaTime The time elapsed since the last update.
     */
    @Override
    public void update(double deltaTime) {
        if (mainObjective == null) return;
        if (mainObjective.isComplete()) return;

        mainObjective.update(deltaTime);

        if (mainObjective.isComplete()) {
            mainObjectiveJustCompleted();
        }
    }

    /**
     * Handles the completion of the main objective by unregistering it
     * from the EventBus, posting an AllMainObjectivesCompleteEvent,
     * and setting the main objective to null.
     * This method is called when the main objective is completed
     * during the update process.
     */
    private void mainObjectiveJustCompleted() {
        eventBus.unregister(mainObjective);
        eventBus.post(new AllMainObjectivesCompleteEvent());
        mainObjective = null;
    }

    /**
     * Unregister the main objective from the eventbus and remove it
     */
    public void clear() {
        eventBus.unregister(mainObjective);
        mainObjective = null;
    }
}
