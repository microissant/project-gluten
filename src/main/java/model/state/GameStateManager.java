package model.state;

import com.badlogic.gdx.math.Vector2;
import entities.inanimate.EndPortal;
import model.IUpdatable;
import model.event.EventBus;
import model.event.Subscribe;
import model.event.events.AllMainObjectivesCompleteEvent;
import model.event.events.entities.PlayerDeathEvent;
import model.event.events.entities.SpawnEndPortalEvent;
import model.event.events.game.*;
import model.event.events.input.InputESC;
import model.event.events.ui.GameOverUIEvent;
import model.event.events.ui.LevelStatsEvent;
import model.level.LevelManager;
import model.state.objective.ObjectiveManager;
import util.CountdownTimer;

import java.util.ArrayList;
import java.util.List;

/**
 * Manages the game state transitions and updates the game logic based on the
 * current state. Also handles game-related events and updates the game statistics
 * and objectives.
 */
public class GameStateManager implements IUpdatable {

    private final GameStatistics statistics;
    private final ObjectiveManager objectiveManager;
    private static GameState gameState;
    private final EventBus eventBus;
    private final CountdownTimer playerDeathTimer;
    private final LevelManager levelManager;

    private Vector2 levelEndPosition;

    public GameStateManager(EventBus eventBus, LevelManager levelManager) {
        this.eventBus = eventBus;
        eventBus.register(this);
        this.levelManager = levelManager;
        playerDeathTimer = new CountdownTimer(1.0f, this::gameOver);

        gameState = GameState.MAIN_MENU;
        statistics = new GameStatistics(eventBus);
        objectiveManager = new ObjectiveManager(eventBus);
        initialize();
    }

    private void initialize() {
        playerDeathTimer.reset();
        statistics.resetCurrent();
    }

    /**
     * Returns the current GameState.
     *
     * @return The current GameState.
     */
    public GameState getGameState() {
        return gameState;
    }

    @Override
    public void update(double deltaTime) {
        playerDeathTimer.update(deltaTime);

        statistics.update(deltaTime);
        objectiveManager.update(deltaTime);
    }

    /**
     * Handles the completion of all main objectives by spawning an EndPortal.
     *
     * @param event The event triggered when all main objectives are complete.
     */
    @Subscribe
    public void completedAllObjectives(AllMainObjectivesCompleteEvent event) {
        if (event == null) return;
        if (eventBus == null) return;

        if (levelEndPosition == null) {
            levelEndPosition = new Vector2();
        }

        eventBus.post(new SpawnEndPortalEvent(new EndPortal(eventBus, levelEndPosition, levelManager.levelsRemaining() < 1)));
    }

    /**
     * Handles the event of setting the end goal position. The eng goal position
     * determines where the end portal is spawned when the main objective is complete
     *
     * @param event The event triggered when the end goal position is set.
     */
    @Subscribe
    public void setEndPosition(SetEndGoalPositionEvent event) {
        if (event == null) return;
        if (event.position() == null) return;

        levelEndPosition = event.position();
    }

    /**
     * Handles the event of adding a main objective to the game.
     *
     * @param event The event triggered when a main objective is added.
     */
    @Subscribe
    public void addMainObjective(AddMainObjectiveEvent event) {
        if (event == null) return;
        if (event.objective() == null) return;

        event.objective().setStatistics(statistics);
        objectiveManager.setMainObjective(event.objective());
    }

    /**
     * Handles the event of opening the main menu. Setting the current state to
     * Main Menu
     *
     * @param event The event triggered when the main menu is opened.
     */
    @Subscribe
    public void mainMenuOpened(MainMenuActiveEvent event) {
        gameState = GameState.MAIN_MENU;
    }

    /**
     * Pauses or resumes the game based on the current game state when the
     * InputESC event is triggered. If the game is active, it will pause,
     * otherwise, if the game is paused, it will resume.
     *
     * @param event The InputESC event triggered by a user input.
     */
    @Subscribe
    public void pauseGame(InputESC event) {
        if (gameState == GameState.ACTIVE) {
            gameState = GameState.PAUSED;
            eventBus.post(new PauseGameEvent());
        } else if (gameState == GameState.PAUSED) {
            gameState = GameState.ACTIVE;
            eventBus.post(new ResumeGameEvent());
        }
    }

    /**
     * Initiates a countdown timer when the player dies, as indicated by the
     * PlayerDeathEvent. When the timer reaches zero, the game is set to
     * the game over state and a GameOverUIEvent is posted.
     *
     * @param event The PlayerDeathEvent triggered when the player dies.
     */
    @Subscribe
    public void playerDeath(PlayerDeathEvent event) {
        playerDeathTimer.restart();
    }

    /**
     * Restarts the current level when a RestartLevelEvent is triggered.
     * This resets the player death timer and other game statistics for the level.
     *
     * @param event The RestartLevelEvent triggered when the current level is restarted.
     */
    @Subscribe
    public void restartLevel(RestartLevelEvent event) {
        gameState = GameState.ACTIVE;
        initialize();
    }

    /**
     * Restarts the current level when a RestartLevelEvent is triggered.
     * This resets the player death timer and other game statistics for the level.
     *
     * @param event The RestartLevelEvent triggered when the current level is restarted.
     */
    @Subscribe
    public void levelCompleted(EndLevelEvent event) {
        gameState = GameState.LEVEL_COMPLETE;
        eventBus.post(new LevelStatsEvent(statistics.getFormattedLevelTime()));
    }

    /**
     * Posts a LevelStatsEvent containing the formatted game time when the game is completed,
     * as indicated by the GameCompleteEvent.
     *
     * @param event The GameCompleteEvent triggered when the game is completed.
     */
    @Subscribe
    public void gameCompleted(GameCompleteEvent event) {
        eventBus.post(new LevelStatsEvent(statistics.getFormattedGameTime()));
    }

    /**
     * Handles the activation of the game view when the GameViewActiveEvent is triggered.
     * Depending on the current game state, this can start a new game, load the next level,
     * or resume the game from a paused state.
     *
     * @param event The GameViewActiveEvent triggered when the game view is activated.
     */
    @Subscribe
    public void activeView(GameViewActiveEvent event) {
        if (gameState == GameState.MAIN_MENU) {
            eventBus.post(new NewGameEvent());
            initialize();
            gameState = GameState.ACTIVE;
        }

        else if (gameState == GameState.LEVEL_COMPLETE) {
            eventBus.post(new LoadNextLevelEvent());
            initialize();
            gameState = GameState.ACTIVE;
        }
    }

    /**
     * Returns the GameStatistics instance managed by this GameStateManager.
     *
     * @return The GameStatistics instance.
     */
    public GameStatistics getStatistics() {
        return statistics;
    }

    /**
     * Returns the current game state.
     *
     * @return The current GameState.
     */
    public static GameState state() {
        return gameState;
    }

    private void gameOver() {
        gameState = GameState.GAME_OVER;

        List<Stat> gameStats = new ArrayList<>();
        gameStats.add(new Stat("Time elapsed", String.valueOf(statistics.getFormattedLevelTime())));
        gameStats.add(new Stat("Enemies killed", String.valueOf(statistics.getEnemiesKilled())));

        eventBus.post(new GameOverUIEvent(gameStats));
    }

    public void clear() {
        objectiveManager.clear();
    }
}
