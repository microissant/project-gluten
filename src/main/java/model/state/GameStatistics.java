package model.state;

import model.IUpdatable;
import model.event.EventBus;
import model.event.Subscribe;
import model.event.events.EnemyDeathEvent;

/**
 * GameStatistics is a class that tracks various statistics throughout the game, such as
 * time elapsed, number of enemies killed, projectiles fired, and total damage done.
 *
 * <p>
 *     This class implements the IUpdatable interface to update the time-related statistics.
 * </p>
 */
public class GameStatistics implements IUpdatable {

    private double totalTimeElapsed = 0;
    private double currentLevelTimeElapsed = 0;
    private int enemiesKilled = 0;

    public GameStatistics(EventBus eventBus) {
        eventBus.register(this);
    }

    /**
     * Returns the time elapsed in the current level, which represents the
     * duration a player has spent playing the current level.
     *
     * @return The time elapsed in the current level in seconds.
     */
    public double getCurrentLevelTimeElapsed() {
        return currentLevelTimeElapsed;
    }

    /**
     * Returns the total time elapsed since the game started, which represents
     * the overall duration a player has spent playing the game.
     *
     * @return The total time elapsed since the game started in seconds.
     */
    public double getTotalTimeElapsed() {
        return totalTimeElapsed;
    }

    /**
     * Returns the total number of enemies killed by the player throughout the game.
     * This statistic can be used to track the player's progress or skill.
     *
     * @return The total number of enemies killed.
     */
    public int getEnemiesKilled() {
        return enemiesKilled;
    }

    @Override
    public void update(double deltaTime) {
        totalTimeElapsed += deltaTime;
        currentLevelTimeElapsed += deltaTime;
    }

    /**
     * Registers an enemy killed and updates the enemies killed statistic.
     *
     * @param event The event triggered when an enemy is killed.
     */
    @Subscribe
    public void registerEnemyKilled(EnemyDeathEvent event) {
        if (event == null) return;

        enemiesKilled ++;
    }

    private String getFormattedTime(double time) {
        String minutes = String.format("%02d", (int) time / 60);
        String seconds = String.format("%02d", (int) time % 60);
        String milliseconds = String.format("%01d", (int) ((time - (int) time) * 10 * 2));
        return minutes + " : " + seconds + " : " + milliseconds;
    }

    /**
     * Returns the formatted elapsed time of the current level as "MM : SS : ms".
     * This method is useful for displaying the current level time to the player.
     *
     * @return The formatted elapsed time on the current level.
     */
    public String getFormattedLevelTime() {
        return getFormattedTime(currentLevelTimeElapsed);
    }

    /**
     * Returns the formatted total elapsed time since the game started as "MM : SS : ms".
     * This method is useful for displaying the overall game time to the player.
     *
     * @return The formatted total elapsed time since the game started.
     */
    public String getFormattedGameTime() {
        return getFormattedTime(totalTimeElapsed);
    }

    /**
     * Resets the current level time and enemies killed statistics. This method is
     * called when starting a new level or restarting the current level.
     */
    public void resetCurrent() {
        currentLevelTimeElapsed = 0;
        enemiesKilled = 0;
    }
}
