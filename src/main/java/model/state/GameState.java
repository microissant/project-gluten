package model.state;

/**
 * Represents the various game states that the application can be in.
 * Each state is associated with different behaviors and interactions.
 */
public enum GameState {

    ACTIVE,
    PAUSED,
    GAME_OVER,
    LEVEL_COMPLETE,
    MAIN_MENU
}
