package model.state;

public record Stat(String name, String value) {
}
