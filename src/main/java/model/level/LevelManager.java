package model.level;

import grid.GridCoordinate;
import grid.WorldGrid;
import model.event.EventBus;
import model.generation.WFCworldGenerator;
import model.state.objective.KillRequiredTargets;

import java.util.Arrays;
import java.util.LinkedList;

/**
 * Manages the levels in the game, including loading, resetting, and transitioning between levels.
 */
public class LevelManager {
    
    private final LinkedList<LevelStruct> levelStructList = new LinkedList<>(Arrays.asList(
        new LevelStruct(
                5,9, new WFCworldGenerator(),
                16,
                new GridCoordinate(0, 4),
                5,
                new KillRequiredTargets()),
        new LevelStruct(
                5,9, new WFCworldGenerator(),
                16,
                new GridCoordinate(0, 4),
                5,
                new KillRequiredTargets())
    ));

    private final LinkedList<LevelStruct> currentLevelStructs;

    private Level currentLevel;
    private final EventBus eventBus;
    private LevelStruct currentLevelStruct;


    public LevelManager(EventBus eventBus) {
        this.eventBus = eventBus;
        currentLevelStructs = new LinkedList<>(levelStructList);
    }

    /**
     * Returns the current level's WorldGrid.
     *
     * @return The current level's WorldGrid.
     */
    public WorldGrid getWorldGrid() {
        return currentLevel.worldGrid();
    }

    /**
     * Loads the next level in the list of levels.
     */
    public void loadNextLevel() {
        if (currentLevelStructs.isEmpty()) return;

        loadLevel(currentLevelStructs.poll());
    }

    /**
     * Resets the current level, placing it back in the list of levels to be loaded.
     */
    public void resetCurrentLevel() {
        currentLevelStructs.addFirst(currentLevelStruct);
    }


    private void loadLevel(LevelStruct levelStruct) {
        if (levelStruct == null) return;

        currentLevel = LevelFabricator.constructLevel(levelStruct, eventBus);
        currentLevelStruct = levelStruct;
    }

    /**
     * Clears the list of levels and resets it to the initial list of levels.
     */
    public void clear() {
        currentLevelStructs.clear();
        currentLevelStructs.addAll(levelStructList);
    }

    /**
     * Returns the number of levels remaining in the list.
     *
     * @return The number of levels remaining in the list.
     */
    public int levelsRemaining() {
        return currentLevelStructs.size();
    }
}