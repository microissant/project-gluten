package model.level;

import grid.GridCoordinate;
import model.generation.IWorldGenerator;
import model.state.objective.IObjective;

public record LevelStruct(
        int rows, int cols,
        IWorldGenerator worldGenerator,
        int cellSize,
        GridCoordinate playerSpawn,
        int playerSpawnHealth,
        IObjective mainObjective) {

}
