package model.level;

import components.collision.CollisionType;
import components.weapon.weapons.BlenderGun;
import entities.player.PlayerManager;
import grid.WorldGrid;
import media.TileSetManager.TileSetID;
import model.event.EventBus;
import model.event.events.WorldGridCreatedEvent;
import model.event.events.entities.SpawnPlayerEvent;
import model.event.events.game.AddMainObjectiveEvent;
import model.generation.IWorldGenerator;
import model.generation.WorldStruct;


public class LevelFabricator {

    protected static Level constructLevel(LevelStruct levelStruct, EventBus eventBus) {
        eventBus.post(new AddMainObjectiveEvent(levelStruct.mainObjective()));

        //generate the world
        IWorldGenerator worldGenerator = levelStruct.worldGenerator();
        WorldStruct worldStruct = worldGenerator.generateWorld(levelStruct.rows(), levelStruct.cols(), eventBus,levelStruct.cellSize(),levelStruct.playerSpawn());
        WorldGrid worldGrid = worldStruct.worldGrid();
        eventBus.register(worldGrid);
        worldGrid.initTileMap(TileSetID.DEFAULT_TILESET);
        eventBus.post(new WorldGridCreatedEvent(worldGrid));

        //initialize the player
        PlayerManager playerManager = new PlayerManager(worldStruct.playeSpawnPosition(), levelStruct.playerSpawnHealth(), eventBus);
        playerManager.getPlayerCharacter().setWorldGrid(worldGrid);
        playerManager.addWeapon(new BlenderGun( CollisionType.Type.ENEMY, eventBus));
        eventBus.post(new SpawnPlayerEvent(playerManager));

        return new Level(worldGrid);
    }

}
