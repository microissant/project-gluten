package model;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import components.collision.primitives.CollisionCircle;
import components.weapon.projectile.IProjectile;
import components.weapon.projectile.PoolableProjectile;
import components.weapon.projectile.emitter.ProjectileStruct;
import entities.Entity;
import entities.LivingPoolable;
import entities.enemy.IEnemy;
import entities.particles.Particle;
import entities.particles.ParticleTypes;
import entities.player.PlayerManager;
import grid.WorldGrid;
import item.Item;
import model.event.EventBus;
import model.event.Subscribe;
import model.event.events.AddParticleEvent;
import model.event.events.EnemyDeathEvent;
import model.event.events.FireWeaponEvent;
import model.event.events.WorldGridCreatedEvent;
import model.event.events.collision.AddCollisionCircleEvent;
import model.event.events.collision.AddCollisionCirclesEvent;
import model.event.events.collision.CreateDoorEvent;
import model.event.events.collision.RemoveCollisionCircleEvent;
import model.event.events.entities.*;
import model.event.events.item.ItemPickupEvent;
import util.UpdatableList;

import java.util.List;

/**
 * EntityManager is a class responsible for managing various types of entities within the game, such as
 * projectiles, particles, enemies, and items. It handles their creation, update, and
 * removal, as well as connecting them to the game's event bus. EntityManager also takes care of
 * managing the WorldGrid and PlayerManager.
 */
public class EntityManager implements IUpdatable {

    private final Array<IProjectile> projectiles;
    private final Pool<Particle> particlePool;
    private final Pool<IProjectile> projectilePool;
    private final Array<Particle> particles;
    private final UpdatableList<IEnemy> enemies;
    private final UpdatableList<Item> items;

    private final UpdatableList<Entity> variousEntities;
    private WorldGrid worldGrid = null;

    private PlayerManager playerManager;

    private final EventBus eventBus;

    /**
     * Constructs a new EntityManager with the given event bus.
     * Initializes the entity lists, pools and registers the event listeners.
     *
     * @param eventBus The EventBus to use for handling events related to entities.
     */
    public EntityManager(EventBus eventBus) {
        this.eventBus = eventBus;
        projectiles = new Array<>();
        projectiles.ordered = false;
        enemies = new UpdatableList<>();
        items = new UpdatableList<>();
        variousEntities = new UpdatableList<>();
        particles = new Array<>();
        particles.ordered =false;
        particlePool = new Pool<>() {

            @Override
            protected Particle newObject() {
                return new Particle();
            }

        };
        projectilePool = new Pool<>() {

            @Override
            protected IProjectile newObject() {
                return new PoolableProjectile(eventBus);
            }

        };
        if (eventBus == null) return;

        eventBus.register(this);
    }

    /**
     * Adds the specified projectile to the EntityManager, and sets its WorldGrid.
     *
     * @param projectile The IProjectile instance to be added and managed.
     */
    public void addProjectile(IProjectile projectile) {
        projectiles.add(projectile);
        projectile.setWorldGrid(getWorldGrid());
    }

    private void addAllProjectiles(List<ProjectileStruct> projectiles){
        Array<CollisionCircle> toBeAddedCollision = new Array<>();
        toBeAddedCollision.ordered = false;
        for(ProjectileStruct p : projectiles){
            IProjectile projectile = projectilePool.obtain();
            if(projectile instanceof PoolableProjectile poolable){
                poolable.init(p.position(),p.direction(),p.speed(),p.collisionType(),p.projectileType());
            }
            addProjectile(projectile);

            if (projectile.getCollisionPrimitive() == null) continue;
            toBeAddedCollision.add(projectile.getCollisionPrimitive());
        }

        eventBus.post(new AddCollisionCirclesEvent(toBeAddedCollision));
    }

    private void addParticle(Vector2 position, ParticleTypes type){
        Particle particle =  particlePool.obtain();
        particle.init(position, type, eventBus);
        particles.add(particle);
    }


    /**
     * Listens to the AddParticleEvent to create a new Particle at the given position with the specified ParticleType.
     *
     * @param event The AddParticleEvent carrying information about the particle to be created.
     */
    @Subscribe
    public void createParticle(AddParticleEvent event){
        if(event == null) return;
        if(event.position() == null) return;
        if(event.particleType() == null) return;
        addParticle(event.position(), event.particleType());
    }

    /**
     * Retrieves an Array of Particle instances managed by the EntityManager.
     *
     * @return An Array of Particle instances.
     */
    public Array<Particle> getParticles(){
        return particles;
    }

    /**method for updating a poolable list and removing dead elements */
    private <T extends LivingPoolable & IUpdatable> void updatePoolable(double deltaTime, Array<T> poolableList, Pool<T> pool){
        T poolable;
        for(int i = poolableList.size; --i>=0;){
            poolable = poolableList.get(i);
            poolable.update(deltaTime);
            if(!poolable.isAlive()){
                poolableList.removeIndex(i);
                pool.free(poolable);
            }
        }
    }

    /**
     * Provides an Iterable of IProjectile instances managed by the EntityManager.
     *
     * @return An Iterable of IProjectile instances.
     */
    public Iterable<IProjectile> getProjectiles() {
        return projectiles;
    }

    /**
     * Retrieves the PlayerManager instance managed by the EntityManager.
     *
     * @return The PlayerManager instance, which handles the player's character and related information.
     */
    public PlayerManager getPlayerManager() {
        return playerManager;
    }

    @Override
    public void update(double deltaTime) {
        if (playerManager != null) {
            playerManager.update(deltaTime);
        }

        updatePoolable(deltaTime, projectiles, projectilePool);
        enemies.update(deltaTime);
        items.update(deltaTime);
        variousEntities.update(deltaTime);
        updatePoolable(deltaTime, particles, particlePool);
    }

    /**
     * Retrieves the PlayerManager instance managed by the EntityManager.
     *
     * @return The PlayerManager instance, which handles the player's character and related information.
     */
    public List<IEnemy> getEnemies() {
        return enemies.getList();
    }

    /**
     * Listens to the SpawnEntityEvent to spawn a new enemy.
     * Adds the enemy to the EntityManager, sets its target, WorldGrid, and registers its collision primitive.
     *
     * @param event The SpawnEntityEvent carrying information about the enemy to be spawned.
     */
    @Subscribe
    public void spawnEnemy(SpawnEntityEvent<IEnemy> event) {
        if (event == null) return;
        if (event.entity() == null) return;
        if (playerManager != null) {
            
            if (playerManager.getPlayerCharacter() != null){ 
                event.entity().setTarget(playerManager.getPlayerCharacter());
            }
        }

        enemies.add(event.entity());


        if (worldGrid != null) {
            event.entity().setWorldGrid(worldGrid);
        }

        if (event.entity().getCollisionPrimitive() == null) return;
        eventBus.post(new AddCollisionCircleEvent(event.entity().getCollisionPrimitive()));
    }

    /**
     * Listens to the FireWeaponEvent to add projectiles fired.
     * Adds all projectiles contained in the event to the EntityManager and registers their collision primitives.
     *
     * @param event The FireWeaponEvent carrying information about the projectiles to be added.
     */
    @Subscribe
    public void addProjectileFired(FireWeaponEvent event) {
        if (event == null) return;
        if (event.projectiles() == null) return;

        addAllProjectiles(event.projectiles());
    }

    /**
     * Listens to the EnemyDeathEvent to remove a dead enemy.
     * Unregisters the enemy from the event bus, removes it from the EntityManager, and removes its collision primitive.
     *
     * @param event The EnemyDeathEvent carrying information about the enemy to be removed.
     */
    @Subscribe
    public void removeEnemy(EnemyDeathEvent event) {
        if (event == null) return;
        if (event.enemy() == null) return;

        event.enemy().unregisterFromEventBus();
        enemies.remove(event.enemy());

        if (event.enemy().getCollisionPrimitive() == null) return;
        eventBus.post(new RemoveCollisionCircleEvent(event.enemy().getCollisionPrimitive()));
    }

    /**
     * Listens to the ItemPickupEvent to remove an item when picked up.
     * Removes the item from the EntityManager and removes its collision primitive.
     *
     * @param event The ItemPickupEvent carrying information about the item to be removed.
     */
    @Subscribe
    public void removeItem(ItemPickupEvent event) {
        if (event == null) return;
        if (event.item() == null) return;

        items.remove(event.item());

        if (event.item().getCollisionPrimitive() == null) return;
        eventBus.post(new RemoveCollisionCircleEvent(event.item().getCollisionPrimitive()));
    }

    /**
     * Retrieves the WorldGrid instance managed by the EntityManager.
     * The WorldGrid contains information about the game world, such as grid cells, walls, and paths.
     *
     * @return The WorldGrid instance.
     */
    public WorldGrid getWorldGrid(){
        return worldGrid;
    }

    /**
     * Sets the WorldGrid instance managed by the EntityManager.
     * Updates the WorldGrid for all IEnemy instances managed by the EntityManager.
     *
     * @param worldGrid The WorldGrid instance to set.
     */
    public void setWorldGrid(WorldGrid worldGrid){
        this.worldGrid = worldGrid;
        for(IEnemy enemy: enemies.getList()){
            enemy.setWorldGrid(worldGrid);
        }
    }

    /**
     * Listens to the SpawnPlayerEvent to spawn a player.
     * Sets the PlayerManager instance, updates the target for all IEnemy instances, and registers the player's collision primitive.
     *
     * @param event The SpawnPlayerEvent carrying information about the player to be spawned.
     */
    @Subscribe
    public void spawnPlayer(SpawnPlayerEvent event) {
        if (event == null) return;
        this.playerManager = event.playerManager();

        for (IEnemy enemy: enemies.getList()) {
            enemy.setTarget(playerManager.getPlayerCharacter());
        }

        if (event.playerManager().getPlayerCharacter().getCollisionPrimitive() == null) return;
        eventBus.post(new AddCollisionCircleEvent(event.playerManager().getPlayerCharacter().getCollisionPrimitive()));
    }

    /**
     * Listens to the WorldGridCreatedEvent to set the WorldGrid for the EntityManager.
     *
     * @param event The WorldGridCreatedEvent carrying information about the created WorldGrid.
     */
    @Subscribe
    public void worldGridCreated(WorldGridCreatedEvent event){
        if (event == null) return;
        if(event.worldGrid()==null) return;
        setWorldGrid(worldGrid);
    }

    /**
     * Listens to the SpawnEndPortalEvent to spawn an end portal.
     * Adds the end portal to the EntityManager and registers its collision primitive.
     *
     * @param event The SpawnEndPortalEvent carrying information about the end portal to be spawned.
     */
    @Subscribe
    public void spawnEndPortal(SpawnEndPortalEvent event) {
        if (event == null) return;
        if (event.endPortal() == null) return;

        variousEntities.add(event.endPortal());

        if (event.endPortal().getCollisionPrimitive() == null) return;
        eventBus.post(new AddCollisionCircleEvent(event.endPortal().getCollisionPrimitive()));
    }

    /**
     * Listens to the CreateDoorEvent to spawn a door trigger.
     * Adds the door trigger to the EntityManager and registers its collision primitive.
     *
     * @param event The CreateDoorEvent carrying information about the door trigger to be spawned.
     */
    @Subscribe
    public void spawnDoorTrigger(CreateDoorEvent event){
        if(event == null) return;
        if(event.doorTrigger()==null) return;
        
        variousEntities.add(event.doorTrigger());
        if (event.doorTrigger().getCollisionPrimitive() == null) return;
        eventBus.post(new AddCollisionCircleEvent(event.doorTrigger().getCollisionPrimitive()));

    }

    /**
     * Listens to the SpawnChestEvent to spawn a chest.
     * Adds the chest to the EntityManager and registers its collision primitive.
     *
     * @param event The SpawnChestEvent carrying information about the chest to be spawned.
     */

    @Subscribe
    public void spawnChest(SpawnChestEvent event) {
        if (event == null) return;
        if (event.chest() == null) return;

        variousEntities.add(event.chest());

        if (event.chest().getCollisionPrimitive() == null) return;
        eventBus.post(new AddCollisionCircleEvent(event.chest().getCollisionPrimitive()));
    }

    /**
     * This method is called when a SpawnItemEvent is posted on the event bus. It adds the spawned
     * item to the list of items and posts an AddCollisionCircleEvent if the item has a collision primitive.
     *
     * @param event The SpawnItemEvent that was posted on the event bus.
     */
    @Subscribe
    public void spawnItem(SpawnItemEvent event) {
        if (event == null) return;
        if (event.item() == null) return;

        items.add(event.item());

        if (event.item().getCollisionPrimitive() == null) return;
        eventBus.post(new AddCollisionCircleEvent(event.item().getCollisionPrimitive()));
    }

    /**
     * Retrieves a List of Item instances managed by the EntityManager.
     *
     * @return A List of Item instances.
     */
    public List<Item> getItems() {
        return items.getList();
    }

    /**
     * Retrieves a List of various Entity instances managed by the EntityManager.
     * This includes entities such as end portals, door triggers, and chests.
     *
     * @return A List of various Entity instances.
     */
    public List<Entity> getVariousEntities() {
        return variousEntities.getList();
    }

    /**
     * clears all entities, removing them from the game and unregistering them from the eventbus
     */
    public void clear() {
        if (playerManager != null) {
            playerManager.unregisterFromEventBus();
            playerManager = null;
        }

        unregisterAll(enemies.getList());
        unregisterAll(projectiles);
        unregisterAll(variousEntities.getList());

        enemies.clear();
        projectiles.clear();
        particles.clear();
        variousEntities.clear();
        items.clear();
    }

    private void unregisterAll(Iterable<? extends Entity> entities) {
        for (Entity entity: entities) {
            entity.unregisterFromEventBus();
        }
    }
}
