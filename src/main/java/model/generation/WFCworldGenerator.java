package model.generation;

import com.badlogic.gdx.utils.Queue;
import grid.CoordinateItem;
import grid.Grid;
import grid.GridCoordinate;
import model.event.EventBus;

import java.util.*;


/**creates a WorldGrid using the wave function collapse algorithm*/
public class WFCworldGenerator implements IWorldGenerator {

    @Override
    public WorldStruct generateWorld(int rows, int columns,EventBus eventBus, int cellSize, GridCoordinate playerSpawn) {
        RoomPlacer roomPlacer = new RoomPlacer();
        GridCoordinate bossRoom  = new GridCoordinate(rows-2,columns/2);
        Grid<PathBitmask> paths = generatePaths(rows, columns,playerSpawn,bossRoom);
        return roomPlacer.createWorldGrid(paths, eventBus,cellSize,playerSpawn,bossRoom );

    }

    /** generates a grid of pathbitmasks used to determine where walls should go with a guaranteed path between playerspawn and bossroom.
     * 
     * @param rows the amount of rows of rooms in the grid
     * @param columns the amount of columns of rooms in the grid
     * @param playerSpawn the room that the player should spawn in, Null value will disable the safe path
     * @param bossRoom the room where the boss is, Null value will disable the safe path
     * @return a grid of pathbitmasks 
     */
    public Grid<PathBitmask> generatePaths(int rows, int columns, GridCoordinate playerSpawn, GridCoordinate bossRoom){
        //create the PathBitmask grid
        Grid<PathBitmask> grid= new Grid<>(rows, columns);
        for(CoordinateItem<PathBitmask> cItem : grid){
            //need individual PathBitmasks
            grid.set(cItem.coordinate(), new PathBitmask(0, 0));
        }

        if(playerSpawn != null && bossRoom != null){
            Grid<Boolean> searchGrid = new Grid<>(rows, columns, false);
            generateStartAndBossRoom(grid, searchGrid, playerSpawn, bossRoom);
            //ensure that there is a path from the start to the target
            generateSafePath(grid, searchGrid, playerSpawn, bossRoom);
        }
        //determine the shape of every room
        waveFunctionCollapse(grid);

        return grid;
    }
    
    private void generateStartAndBossRoom(Grid<PathBitmask> grid, Grid<Boolean> searchGrid, GridCoordinate playerSpawn, GridCoordinate bossRoom){
        //dont make a path through the boss room
        searchGrid.set(bossRoom,true);
        searchGrid.set(bossRoom.row,bossRoom.col+1,true);
        searchGrid.set(bossRoom.row,bossRoom.col-1,true);
        searchGrid.set(bossRoom.row+1,bossRoom.col+1,true);
        searchGrid.set(bossRoom.row+1,bossRoom.col-1,true);
        searchGrid.set(bossRoom.row+1,bossRoom.col,true);
        //set the walls of the boss room
        grid.set(playerSpawn,new PathBitmask("001001010010", "111111111111"));
        grid.set(bossRoom                     ,new PathBitmask("111111110010", "111111111111"));
        grid.set(bossRoom.row  ,bossRoom.col+1,new PathBitmask("111011000000", "111111111111"));
        grid.set(bossRoom.row  ,bossRoom.col-1,new PathBitmask("011100110000", "111111111111"));
        grid.set(bossRoom.row+1,bossRoom.col+1,new PathBitmask("000011001110", "111111111111"));
        grid.set(bossRoom.row+1,bossRoom.col-1,new PathBitmask("000000110111", "111111111111"));
        grid.set(bossRoom.row+1,bossRoom.col  ,new PathBitmask("000011111111", "111111111111"));
    }


    /** ensures that it is possible to get from the start coordinate to the finish coordinate.
     * This implementation makes a random path from the start to the target position in order to ensure that
     * the result does not always have a straight line path to the target.
     */
    public void generateSafePath(Grid<PathBitmask> grid, Grid<Boolean> searchGrid, GridCoordinate start, GridCoordinate target){
        //do random dfs
        Queue<GridCoordinate> path = createDFSPath(searchGrid,start,target);

        //back track and set determined bits
        GridCoordinate lastPos = target;
        for(GridCoordinate coord :  path){
            int rowDifference = lastPos.row-coord.row;
            int colDifference = lastPos.col-coord.col;
            PathBitmask currentCell =  grid.get(coord);
            PathBitmask lastCell =  grid.get(lastPos);
            currentCell.setVerticalPath(rowDifference);
            lastCell.setVerticalPath(-rowDifference);
            currentCell.setHorizontalPath(colDifference);
            lastCell.setHorizontalPath(-colDifference);
            lastPos = coord;
        }
    }

    /**creates a list of gridcoordinates from the target room to the start room. Not necessarily the most optimal path */
    private Queue<GridCoordinate> createDFSPath(Grid<Boolean> searchGrid, GridCoordinate start, GridCoordinate target){
        Random rng = new Random();
        GridCoordinate currentPos = start;

        Queue<GridCoordinate> currentPath = new Queue<>();
        currentPath.addFirst(currentPos);

        List<GridCoordinate> available = new ArrayList<>(4);

        while (!currentPos.equals(target)){
            searchGrid.set(currentPos, true); 
            int rowOffset = 0;
            int colOffset = 1;
            for(int i =0; i < 4; i++){
                //rotate 90 degrees counter clockwise
                int swapTemp  = colOffset;
                colOffset = -rowOffset;
                rowOffset = swapTemp;
                //get neighbor coordinate
                
                GridCoordinate candidate = new GridCoordinate(currentPos.row+rowOffset,currentPos.col+colOffset);
                if(candidate.equals(target)) {
                    currentPath.addFirst(currentPos);
                    return currentPath;
                }
                if(searchGrid.coordinateIsOnGrid(candidate) && !searchGrid.get(candidate)){
                    available.add(candidate);
                }
            }
            //backtrack if there are no possible moves
            if(available.isEmpty()){
                //this will only happen if the target coordinate is impossible to get to
                if(currentPath.isEmpty()) throw new IllegalArgumentException("target is unreachable");
                currentPos = currentPath.removeFirst();
            } else {
                currentPath.addFirst(currentPos);
                //randomly pick the next position
                currentPos  = available.get(rng.nextInt(available.size()));
            }
            available.clear();
        }
        return currentPath; //this line should only run if start is equal to target.
    }


    private void waveFunctionCollapse(Grid<PathBitmask> grid){
        //a pathbitmask where all of the determined bits are 0, used for the edge of the grid
        PathBitmask empty = new PathBitmask(0,(1<<PathBitmask.size)-1);

        //set all of the determined bits at the edge of the grid
        for(int i  = 0; i < grid.numRows(); i++){
            grid.get(i,0).setHorizontalDetermined(empty, -1);
        }
        for(int i  = 0; i < grid.numRows(); i++){
            grid.get(i,grid.numCols()-1).setHorizontalDetermined(empty, 1);
        }
        for(int i  = 0; i < grid.numCols(); i++){
            grid.get(0,i).setVerticalDetermined(empty, -1);
        }
        for(int i  = 0; i < grid.numCols(); i++){
            grid.get(grid.numRows()-1,i).setVerticalDetermined(empty, 1);
        }

        List<CoordinateItem<PathBitmask>> toDo = new ArrayList<>(grid.numRows() * grid.numCols());
        
        //add all to the list
        for(CoordinateItem<PathBitmask> cItem : grid){
            toDo.add(cItem);
        }

        while(!toDo.isEmpty()){
            //get the grid-coordinate with the least amount of uncertainty
            CoordinateItem<PathBitmask> mostCertain = Collections.max(toDo, Comparator.comparingInt(o -> Integer.bitCount(o.item().determinedBits)));
            mostCertain.item().determine(0.7f);
            //change the neighbouring states

            for(int r =-1; r <= 1; r++){
                for(int c =-1; c <= 1; c++){
                    if(c==0 && r == 0) continue;
                    GridCoordinate neighbor = new GridCoordinate(mostCertain.coordinate().row+r, mostCertain.coordinate().col+c);
                    if(grid.coordinateIsOnGrid(neighbor)) {
                        PathBitmask neighborBitmask = grid.get(neighbor);

                        //if not in a corner
                        if(Math.abs(r)+Math.abs(c)<2){
                            neighborBitmask.setHorizontalDetermined(mostCertain.item(), -c);
                            neighborBitmask.setVerticalDetermined(mostCertain.item(), -r);
                        } else neighborBitmask.setDiagonalDetermined(mostCertain.item(), -c,-r);
                    }
                }
            }

            toDo.remove(mostCertain);
        }

    }


}
