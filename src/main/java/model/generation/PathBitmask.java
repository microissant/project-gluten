package model.generation;

import grid.autotiling.Bitmask;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Class for determining where pathways can go when making the worldgrid.
 */
public class PathBitmask {

    static public final int TOPLEFTCORNER = 0;
    static public final int TOPWALL = 1;
    static public final int TOPDOOR = 2;
    static public final int TOPRIGHTCORNER = 3;
    static public final int LEFTWALL = 4;
    static public final int LEFTDOOR = 5;
    static public final int RIGHTWALL = 6;
    static public final int RIGHTDOOR = 7;
    static public final int BOTTOMLEFTCORNER = 8;
    static public final int BOTTOMWALL = 9;
    static public final int BOTTOMDOOR = 10;
    static public final int BOTTOMRIGHTCORNER = 11;
    static public final int size = 12;

    static private final int[] diagonals = new int[] { BOTTOMLEFTCORNER, BOTTOMRIGHTCORNER, TOPLEFTCORNER,
            TOPRIGHTCORNER };

    /**
     * The bits containing information about possible paths. 0 means no path and 1
     * means path.
     */
    public int bits;
    /**
     * The bitmask determining which bits in the bits variable that are currently
     * determined.
     */
    public int determinedBits;

    /**
     * Converts Strings into bitmasks.
     * The strings are the binary representation of each bit in each position.
     * <p>
     * NOTE:
     * <p>
     * The first position in the string represents the bit in the 1s position.
     * This means that the binary representation of the numbers that are produced
     * are REVERSED versions of the arguments.
     * 
     * @param bits           the bits of the bitmask in binary form
     * @param determinedBits the determined bits of the bitmask in binary form
     */
    public PathBitmask(String bits, String determinedBits) {
        int len = bits.length();
        this.bits = 0;
        this.determinedBits = 0;
        for (int i = 0; i < len; i++) {
            this.bits |= (Integer.parseInt(String.valueOf(bits.charAt(i)))) << i;
            this.determinedBits |= (Integer.parseInt(String.valueOf(determinedBits.charAt(i)))) << i;
        }
    }

    public PathBitmask(int bits, int determinedBits) {
        this.bits = bits;
        this.determinedBits = determinedBits;
    }

    /**
     * Sets the corresponding bit and determinedbit such that there must be a path
     * in that direction.
     * 
     * @param dir the vertical direction
     */
    public void setVerticalPath(int dir) {
        if (dir == 0)
            return;
        // top when dir = 1 and bottom when dir = -1
        int dirPos = 1 << (((TOPDOOR + BOTTOMDOOR) + (TOPDOOR - BOTTOMDOOR) * dir) >> 1);
        determinedBits |= dirPos;
        bits |= dirPos;
    }

    /**
     * Sets the corresponding bit and determinedbit such that there must be a path
     * in that direction.
     * 
     * @param dir the horizontal direction
     */
    public void setHorizontalPath(int dir) {
        if (dir == 0)
            return;
        // right when dir = 1 and left when dir = -1
        int dirPos = 1 << (((RIGHTDOOR + LEFTDOOR) + (RIGHTDOOR - LEFTDOOR) * dir) >> 1);
        determinedBits |= dirPos;
        bits |= dirPos;
    }

    /**
     * Changes the state of certain bits in order to get a path bitmask out of an
     * illegal state.
     * <p>
     * Rules:
     * <ul>
     * <li>a corner can not be missing if one of the two adjacent walls are not
     * missing,
     * <li>a wall must exist if its corresponding door exists
     * </ul>
     * 
     * @param bitmask the number to fix
     * @return the fixed number
     */
    public static int fixIllegalState(int bitmask) {
        // apply wall rule
        bitmask = Bitmask.makeDependent(bitmask, TOPWALL, TOPDOOR);
        bitmask = Bitmask.makeDependent(bitmask, LEFTWALL, LEFTDOOR);
        bitmask = Bitmask.makeDependent(bitmask, RIGHTWALL, RIGHTDOOR);
        bitmask = Bitmask.makeDependent(bitmask, BOTTOMWALL, BOTTOMDOOR);
        // apply corner rule
        bitmask = Bitmask.removeCornerBit(bitmask, TOPLEFTCORNER, TOPWALL, LEFTWALL);
        bitmask = Bitmask.removeCornerBit(bitmask, TOPRIGHTCORNER, TOPWALL, RIGHTWALL);
        bitmask = Bitmask.removeCornerBit(bitmask, BOTTOMLEFTCORNER, BOTTOMWALL, LEFTWALL);
        bitmask = Bitmask.removeCornerBit(bitmask, BOTTOMRIGHTCORNER, BOTTOMWALL, RIGHTWALL);
        return bitmask;
    }

    /**
     * Determines the state of this path bitmask by randomly filling out the
     * remaining bits in a way that is legal.
     */
    public void determine(float wallProbability) {
        // generate a random number with "size" number of bits
        int randomNumber = 0;
        for (int i = 0; i < size; i++) {
            randomNumber = randomNumber << 1;
            if (ThreadLocalRandom.current().nextFloat() >= wallProbability)
                randomNumber += 1;

        }
        // int randomNumber = rng.nextInt((1<<size)-1);
        // set determined bits
        bits = (randomNumber & ~(determinedBits)) | (bits & determinedBits);
        bits = fixIllegalState(bits);
        determinedBits = (1 << size) - 1;
    }

    /**
     * Sets the bits of this bitmask to the determined bits of the other bitmask.
     * 
     * @param other          the adjacent bitmask
     * @param thisPositions  the positions of the bits in this bitmask
     * @param otherPositions the positions of the bits in the other bitmask
     */
    private void setAdjacentDetermined(PathBitmask other, int[] thisPositions, int[] otherPositions) {
        for (int i = 0; i < thisPositions.length; i++) {
            if (Bitmask.getBitAt(other.determinedBits, otherPositions[i]) == 1) {
                this.bits = (this.bits & ~(1 << thisPositions[i]))
                        | (((other.bits & (1 << otherPositions[i])) >> otherPositions[i]) << thisPositions[i]);
                this.determinedBits |= 1 << thisPositions[i];
            }
        }
    }

    /**
     * Sets the bits of this bitmask to the determined bits of the adjacent bitmask
     * in the given direction.
     * 
     * @param other the adjacent bitmask
     * @param dir   the direction of the other bitmask
     */
    public void setHorizontalDetermined(PathBitmask other, int dir) {
        if (dir == 0)
            return;
        int[] leftPos = new int[] { TOPLEFTCORNER, LEFTWALL, LEFTDOOR, BOTTOMLEFTCORNER };
        int[] rightPos = new int[] { TOPRIGHTCORNER, RIGHTWALL, RIGHTDOOR, BOTTOMRIGHTCORNER };
        int[] thisPositions;
        int[] otherPositions;
        if (dir == 1) {
            thisPositions = rightPos;
            otherPositions = leftPos;

        } else {
            thisPositions = leftPos;
            otherPositions = rightPos;
        }
        setAdjacentDetermined(other, thisPositions, otherPositions);

    }

    /**
     * Sets the bits of this bitmask to the determined bits of the adjacent bitmask
     * in the given direction.
     * 
     * @param other the adjacent bitmask
     * @param dir   the direction of the other bitmask
     */
    public void setVerticalDetermined(PathBitmask other, int dir) {
        if (dir == 0)
            return;
        int[] topPos = new int[] { TOPLEFTCORNER, TOPWALL, TOPDOOR, TOPRIGHTCORNER };
        int[] bottomPos = new int[] { BOTTOMLEFTCORNER, BOTTOMWALL, BOTTOMDOOR, BOTTOMRIGHTCORNER };
        int[] thisPositions;
        int[] otherPositions;
        if (dir == 1) {
            thisPositions = topPos;
            otherPositions = bottomPos;

        } else {
            thisPositions = bottomPos;
            otherPositions = topPos;
        }
        setAdjacentDetermined(other, thisPositions, otherPositions);
    }

    /**
     * Sets the diagonal bit of the other bitmask to the corresponding diagonal bit
     * of this bitmask.
     * 
     * @param other the adjacent bitmask
     * @param xdir  the x direction of the other bitmask
     * @param ydir  the y direction of the other bitmask
     */
    public void setDiagonalDetermined(PathBitmask other, int xdir, int ydir) {
        if (xdir == 0 || ydir == 0)
            return;
        int thisPosition = diagonals[((xdir + 1) >> 1) + (ydir + 1)];
        int otherPosition = diagonals[((-xdir + 1) >> 1) + (-ydir + 1)];

        if (Bitmask.getBitAt(other.determinedBits, otherPosition) == 0)
            return;
        this.bits = (this.bits & ~(1 << thisPosition))
                | (((other.bits & (1 << otherPosition)) >> otherPosition) << thisPosition);
        this.determinedBits |= 1 << thisPosition;
    }

    /**
     * Checks if this pathbitmask can be placed on the input pathbitmask.
     * 
     * @param other the pathbitmask to check
     * @return true if this pathbitmask can be placed on the input pathbitmask
     */
    public boolean isCompatible(PathBitmask other) {
        return ((this.bits & this.determinedBits) ^ (other.bits & this.determinedBits)) == 0;

    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + bits;
        result = prime * result + determinedBits;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PathBitmask other = (PathBitmask) obj;
        if (bits != other.bits)
            return false;
        return determinedBits == other.determinedBits;
    }

}
