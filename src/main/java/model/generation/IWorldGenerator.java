package model.generation;

import grid.GridCoordinate;
import model.event.EventBus;


public interface IWorldGenerator {
    
    /**creates a worldgrid with {@code rows * columns} rooms
     * 
     * @param rows the amount of rows in the grid
     * @param columns the amount of columns in the grid
     * @param bus the eventbus that will spawn entities from the grid
     * @param cellSize the size of each cell in the grid
     * @return a struct containing the generated worldGrid and the player spawn position
     */
    WorldStruct generateWorld(int rows, int columns,EventBus bus,int cellSize, GridCoordinate playerSpawn);
}
