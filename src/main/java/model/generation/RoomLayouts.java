package model.generation;

import app.global.RuntimeOptions;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import grid.factory.RoomFactory;
import grid.factory.RoomLayout;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;



public class RoomLayouts {
    
    private static final List<RoomLayout> bigLayouts;
    private static final List<RoomLayout> tallLayouts;
    private static final List<RoomLayout> wideLayouts;
    private static final List<RoomLayout> bossLayouts;
    private static final HashMap<Integer,List<RoomLayout>> smallLayouts;

    private static final Random rng = new Random();
    static {
        //load all layouts
        RoomFactory factory = new RoomFactory();
        HashMap<String, RoomType> typeMap = new HashMap<>();
        JsonValue rooms;


        typeMap.put("SMALL_ROOM", new RoomType() {
            @Override
            public void add(JsonValue val) {
                PathBitmask bitmask = new PathBitmask(val.getInt("bitmask"),val.getInt("determined"));
                String roomString = getRoomString(val);
                int key = (bitmask.bits | (~bitmask.determinedBits) ) & ((1<<PathBitmask.size)-1);

                smallLayouts.computeIfAbsent(key, k -> new ArrayList<>());

                smallLayouts.get(key).add(factory.createLayout(roomString));
            }
            
        });
        typeMap.put("BIG_ROOM", new RoomType() {
            @Override
            public void add(JsonValue val) {
                String roomString = getRoomString(val);
                bigLayouts.add(factory.createLayout(roomString));
            }
            
        });
        typeMap.put("TALL_ROOM", new RoomType() {
            @Override
            public void add(JsonValue val) {
                String roomString = getRoomString(val);
                tallLayouts.add(factory.createLayout(roomString));
            }
            
        });
        typeMap.put("WIDE_ROOM", new RoomType() {
            @Override
            public void add(JsonValue val) {
                String roomString = getRoomString(val);

                wideLayouts.add(factory.createLayout(roomString));
            }
        }); 
        typeMap.put("BOSS_ROOM", new RoomType() {
            @Override
            public void add(JsonValue val) {
                String roomString = getRoomString(val);

                bossLayouts.add(factory.createLayout(roomString));
            }
        }); 
        


        if(RuntimeOptions.renderingEnabled()){
            //this is faster but doesn't work for tests
            FileHandle file = Gdx.files.internal("src/main/resources/rooms.json");
            JsonReader reader = new JsonReader();
            rooms = reader.parse(file).get("rooms");
        } else {
            //this makes the game startup way slower, but it works for tests
            String jsonString;
            try{
                jsonString = Files.readString(Paths.get("src/main/resources/rooms.json"));
            } catch(Exception iOEXException){
                throw new IllegalAccessError("rooms.json cannot be accessed");
            }
            JsonReader reader = new JsonReader();
            rooms = reader.parse(jsonString).get("rooms");
        }
        
        

        
        
        smallLayouts = new HashMap<>();
        bigLayouts = new ArrayList<>();
        wideLayouts = new ArrayList<>();
        tallLayouts = new ArrayList<>();
        bossLayouts = new ArrayList<>();
        for(int i = 0; i < rooms.size; i++){
            JsonValue room = rooms.get(i);
            typeMap.get(room.getString("roomType")).add(room);
        }
        
        
    }
        
    /** finds a small room that is compatible with the given bitmask
     * 
     * @param rules the bitmask with information about which doors are open
     * @return room layout if found, null otherwise
     */
    public static RoomLayout getSmallRoom(PathBitmask rules){

        if(smallLayouts==null || smallLayouts.isEmpty()) return null;

        int bits = rules.bits;
        int nextCompatible = bits;
        while(nextCompatible <= (1<<PathBitmask.size)-1){

            nextCompatible +=1;
            nextCompatible |= bits;
            
            List<RoomLayout> rooms = smallLayouts.get(PathBitmask.fixIllegalState(nextCompatible));
            if(rooms == null) continue;
            int index = ThreadLocalRandom.current().nextInt(rooms.size());

            return rooms.get(index);

        }
        //no rooms found
        return null;
    }
    /** returns a random big room layout
     */
    public static RoomLayout getBigRoom(){
        if(bigLayouts==null || bigLayouts.isEmpty()) return null;
        return bigLayouts.get(rng.nextInt(bigLayouts.size()));
    }

    /**returns a random wide room layout */
    public static RoomLayout getWideRoom(){
        if(wideLayouts==null || wideLayouts.isEmpty()) return null;
        return wideLayouts.get(rng.nextInt(wideLayouts.size()));
    }
    /**returns a random tall room layout */
    public static RoomLayout getTallRoom(){
        if(tallLayouts==null || tallLayouts.isEmpty()) return null;
        return tallLayouts.get(rng.nextInt(tallLayouts.size()));
    }
    /**returns a random tall room layout */
    public static RoomLayout getBossRoom(){
        if(bossLayouts==null || bossLayouts.isEmpty()) return null;
        return bossLayouts.get(rng.nextInt(bossLayouts.size()));
    }

    /**takes an array of strings as an argument and creates a roomlayout for the roomtype that implements this interface */
    private interface RoomType {
        void add(JsonValue value);
    }

    /**
     * Constructs a room string from a given JsonValue.
     *
     * @param val The JsonValue containing the room information.
     * @return A room string representing the room layout and enemy list.
     */
    private static String getRoomString(JsonValue val){
        StringBuilder roomString = new StringBuilder();
        for(JsonValue rowString : val.get("rows")){
            roomString.append(rowString).append(",");
        }
        for(JsonValue enemy : val.get("enemyList")){
            roomString.append(enemy).append(" ");
        }
        
        return roomString.toString();
    }


}  
