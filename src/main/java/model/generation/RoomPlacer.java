package model.generation;

import com.badlogic.gdx.math.Vector2;
import grid.*;
import grid.TileData.TileType;
import grid.autotiling.Bitmask;
import grid.factory.RoomFactory;
import grid.factory.RoomLayout;
import model.event.EventBus;

import java.util.Objects;

/**takes a grid returned by a world generator and turns it into a worldgrid  */
public class RoomPlacer {
    public static final int roomWidth = 16;
    public static final int roomHeight = 10;
    public static final int margin = 2;

    /**iterates through the path grid and places room layouts from RoomLayouts */
    public WorldStruct createWorldGrid(Grid<PathBitmask> grid, EventBus bus, int cellSize, GridCoordinate playerSpawn,GridCoordinate bossRoom){
        WorldGrid result = new WorldGrid(grid.numRows() * roomHeight + 2*margin, grid.numCols() * roomWidth + 2*margin, cellSize, new TileData(TileType.INDESTRUCTIBLEWALL));


        Grid<Boolean> canEdit = new Grid<>(grid.numRows(), grid.numCols(), true);
        //dont edit any of the boss rooms or the player spawn room
        canEdit.set(playerSpawn,false);
        canEdit.set(bossRoom,false);
        canEdit.set(bossRoom.row,bossRoom.col+1,false);
        canEdit.set(bossRoom.row,bossRoom.col-1,false);
        canEdit.set(bossRoom.row+1,bossRoom.col+1,false);
        canEdit.set(bossRoom.row+1,bossRoom.col-1,false);
        canEdit.set(bossRoom.row+1,bossRoom.col,false);
        
        

        setAllWalls(result, grid, bus);
        placeLargerRoom(Objects.requireNonNull(RoomLayouts.getBossRoom()), bossRoom.row, bossRoom.col-1, result, bus);

        //place larger rooms
        for(int row =0; row < grid.numRows(); row++){
            for(int col =0; col < grid.numCols(); col++){
                if(!canEdit.get(row,col)) continue;
                boolean notLastRow = row<grid.numRows()-1;
                boolean notLastCol = col<grid.numCols()-1;
                if(notLastCol && notLastRow) {
                    RoomLayout roomResult = checkBigRoom(grid, row, col);
                    if(roomResult != null){
                        canEdit.set(row,col,false);
                        canEdit.set(row,col+1,false);
                        canEdit.set(row+1,col,false);
                        canEdit.set(row+1,col+1,false);
                        placeLargerRoom(roomResult,row,col,result,bus);
                        break;
                    } 
                    
                }
                if(notLastCol){
                    RoomLayout roomResult = checkWideRoom(grid, row, col);
                    if(roomResult != null) {
                        canEdit.set(row,col,false);
                        canEdit.set(row,col+1,false);
                        placeLargerRoom(roomResult,row,col,result,bus);
                        break;
                    }
                }
                if(notLastRow){
                    RoomLayout roomResult = checkTallRoom(grid, row, col);
                    if(roomResult != null){
                        canEdit.set(row,col,false);
                        canEdit.set(row+1,col,false);
                        placeLargerRoom(roomResult,row,col,result,bus);
                        break;
                    } 
                }


            }
        }


        //place small rooms
        for(CoordinateItem<PathBitmask> cItem : grid){
            if(!canEdit.get(cItem.coordinate())) continue;
            GridCoordinate roomCoordinate = new GridCoordinate(roomHeight * cItem.coordinate().row + margin, roomWidth * cItem.coordinate().col + margin);
            RoomLayout roomLayout = RoomLayouts.getSmallRoom(cItem.item());
            if(roomLayout==null) continue;
            
            roomLayout.paste(result,new GridCoordinate(roomCoordinate.row +1, roomCoordinate.col+1), bus);
            
        }


        float playerSpawnX = converToWorldCoordinate(playerSpawn.col, roomWidth, cellSize);
        float playerSpawnY = converToWorldCoordinate(playerSpawn.row, roomHeight, cellSize);
        return new WorldStruct(result, new Vector2(playerSpawnX,playerSpawnY));   
    }
    private void placeLargerRoom(RoomLayout roomLayout, int row, int col,WorldGrid result, EventBus bus){
        roomLayout.paste(result,new GridCoordinate(margin +(roomHeight * row)+1,margin + (roomWidth * col)+1), bus);
    }

    private float converToWorldCoordinate(int coordinate, int roomDimension, int cellSize){
        float result = ((float) coordinate) +0.5f;
        result *= (float) roomDimension;
        result += (float) margin;
        result *= (float) cellSize;
        return result;
    }

    private void setAllWalls(WorldGrid result,Grid<PathBitmask> grid, EventBus bus){
        RoomFactory factory = new RoomFactory();
        for(CoordinateItem<PathBitmask> cItem : grid){
            RoomLayout layout =  createWalls(cItem.item(), factory);
            GridCoordinate roomCoordinate = new GridCoordinate(roomHeight * cItem.coordinate().row + margin, roomWidth * cItem.coordinate().col + margin);
            layout.paste(result,roomCoordinate, bus);
        }
    }



    private RoomLayout checkBigRoom(Grid<PathBitmask> grid, int row, int col){
        if(!(new PathBitmask(206, 2907)).isCompatible(grid.get(row,col))) return null;
        if(!(new PathBitmask(1840, 2907)).isCompatible(grid.get(row+1,col+1))) return null;
        if(!(new PathBitmask(3776, 2907)).isCompatible(grid.get(row+1,col))) return null;
        if(!(new PathBitmask(55, 2907)).isCompatible(grid.get(row,col+1))) return null;
        return RoomLayouts.getBigRoom();    
    }

    private RoomLayout checkWideRoom(Grid<PathBitmask> grid, int row, int col){
        if(!(new PathBitmask(192, 2907)).isCompatible(grid.get(row,col))) return null;
        if(!(new PathBitmask(48, 2907)).isCompatible(grid.get(row,col+1))) return null;
        return RoomLayouts.getWideRoom();
    }
    private RoomLayout checkTallRoom(Grid<PathBitmask> grid, int row, int col){
        if(!(new PathBitmask(6, 2907)).isCompatible(grid.get(row,col))) return null;
        if(!(new PathBitmask(1536, 2907)).isCompatible(grid.get(row+1,col))) return null;
        return RoomLayouts.getTallRoom();
    }   


    private RoomLayout createWalls(PathBitmask bitmask, RoomFactory factory){
        String topLeft = " ";
        if(Bitmask.getBitAt(bitmask.bits,PathBitmask.TOPLEFTCORNER)==0) topLeft = "w";
        String topDoor = " ";
        if(Bitmask.getBitAt(bitmask.bits,PathBitmask.TOPDOOR)==0) topDoor = "w"; 
        topDoor = topDoor.repeat(2);
        String topWall = " ";
        if(Bitmask.getBitAt(bitmask.bits,PathBitmask.TOPWALL)==0) topWall = "w"; 
        topWall = topWall.repeat((roomWidth - 4)/2);
        String topRight = " ";
        if(Bitmask.getBitAt(bitmask.bits,PathBitmask.TOPRIGHTCORNER)==0) topRight = "w";
        String leftDoor = " ";
        if(Bitmask.getBitAt(bitmask.bits,PathBitmask.LEFTDOOR)==0) leftDoor = "w"; 
        String leftWall = " ";
        if(Bitmask.getBitAt(bitmask.bits,PathBitmask.LEFTWALL)==0) leftWall = "w"; 
        String rightDoor = " ";
        if(Bitmask.getBitAt(bitmask.bits,PathBitmask.RIGHTDOOR)==0) rightDoor = "w";
        String rightWall = " ";
        if(Bitmask.getBitAt(bitmask.bits,PathBitmask.RIGHTWALL)==0) rightWall = "w";
        String bottomLeft = " ";
        if(Bitmask.getBitAt(bitmask.bits,PathBitmask.BOTTOMLEFTCORNER)==0) bottomLeft = "w"; 
        String bottomDoor = " ";
        if(Bitmask.getBitAt(bitmask.bits,PathBitmask.BOTTOMDOOR)==0) bottomDoor = "w";
        bottomDoor = bottomDoor.repeat(2);
        String bottomWall = " ";
        if(Bitmask.getBitAt(bitmask.bits,PathBitmask.BOTTOMWALL)==0) bottomWall = "w";
        bottomWall = bottomWall.repeat((roomWidth - 4)/2);
        String bottomRight = " ";
        if(Bitmask.getBitAt(bitmask.bits,PathBitmask.BOTTOMRIGHTCORNER)==0) bottomRight = "w"; 
        
        String inside = " ";
        if(bitmask.bits == 0) inside = "w";
        String result;
        String top = topLeft + topWall + topDoor + topWall + topRight + ",";
        String bottom = bottomLeft + bottomWall + bottomDoor +bottomWall + bottomRight+ ", ";
        String wall = (leftWall + inside.repeat(roomWidth-2) + rightWall+ ",").repeat((roomHeight - 4)/2);
        String door = (leftDoor + inside.repeat(roomWidth-2) + rightDoor+ ",").repeat(2);
        result = top +wall+door+wall+ bottom;

        return factory.createLayout(result);
    }

}