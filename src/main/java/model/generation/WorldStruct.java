package model.generation;

import com.badlogic.gdx.math.Vector2;
import grid.WorldGrid;

/**contains information necessary initialize a level */
public record WorldStruct(WorldGrid worldGrid, Vector2 playeSpawnPosition) {

    
}
