package model;

public interface IUpdatable {

    /**
     * Update all the objects items which require to be updated every frame
     *
     * @param deltaTime time since last update
     */
    default void update(double deltaTime) {}
}
