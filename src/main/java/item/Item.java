package item;

import components.collision.ICollidable;
import media.Renderable;
import model.IUpdatable;

public interface Item extends Interactable, Renderable, IUpdatable, ICollidable {


}
