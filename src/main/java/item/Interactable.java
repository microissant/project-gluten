package item;

import components.currency.coin.Coin;
import components.physics.PositionedBody;

/**
 * Interactable objects can be interacted with by {@code InteractAgents}
 */
public interface Interactable extends PositionedBody {

    /**
     * @return the text which should be displayed on the HUD
     */
    default String interactPromptText() { return "Interact"; }

    /**
     * @return cost of interaction
     */
    default Coin getCost() { return null; }

    /**
     * Action to be performed when an interaction occurs. If agent is null, nothing
     * happens
     *
     * @param agent that interacts with the interactable object
     */
    void onInteract(InteractAgent agent);

}
