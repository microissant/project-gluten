package item.loot;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import media.MediaManager;
import media.TextureID;

/**
 * Interface representing a loot item in the game, which can be obtained by looters.
 */
public interface ILoot {

    /**
     * Adds the loot item to the looter's inventory or applies the effect to the looter.
     *
     * @param looter The looter who obtains the loot item.
     */
    void obtain(ILooter looter);

    /**
     * Returns the display title of the loot item.
     *
     * @return A string representing the title of the loot item.
     */
    String displayTitle();

    /**
     * Returns the display description of the loot item.
     *
     * @return A string representing the description of the loot item.
     */
    String displayDescription();

    /**
     * Returns the display icon of the loot item as a TextureRegion.
     *
     * @return A {@link TextureRegion} representing the icon of the loot item.
     */
    default TextureRegion displayIcon() {
        return MediaManager.getTexture(TextureID.HEALTH_ICON).getKeyFrame(0);
    }
}
