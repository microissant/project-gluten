package item.loot;

import components.health.Healer;

import java.util.Random;

public class HealthLoot implements ILoot {

    private final Healer healer;

    public HealthLoot(int min, int max) {
        Random random = new Random();
        int value = min + random.nextInt(max - min);
        healer = new Healer(value);
    }

    public HealthLoot(int healValue) {
        healer = new Healer(healValue);
    }

    @Override
    public void obtain(ILooter looter) {
        if (looter == null) return;
        if (looter.getHealable() == null) return;

        looter.getHealable().heal(healer);
    }

    @Override
    public String displayTitle() {
        return "+" + healer.getHealValue() + " health";
    }

    @Override
    public String displayDescription() {
        return null;
    }

}
