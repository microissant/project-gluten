package item.loot;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import components.currency.ITrader;
import components.currency.coin.Coin;
import media.MediaManager;
import media.TextureID;

import java.util.Random;

public class CoinLoot implements ILoot {

    private final Coin coin;

    public CoinLoot(int min, int max) {
        Random random = new Random();
        int value = min + random.nextInt(max - min);

        coin = new Coin(value);
    }

    public CoinLoot(int value) {
        coin = new Coin(value);
    }

    @Override
    public void obtain(ILooter looter) {
        ITrader trader = looter.getTrader();
        if (trader == null) return;

        trader.deposit(coin);
    }

    @Override
    public String displayTitle() {
        return "+" + coin.getValue() + " coins";
    }

    @Override
    public String displayDescription() {
        return "";
    }

    @Override
    public TextureRegion displayIcon() {
        return MediaManager.getTexture(TextureID.CURRENCY_ICON).getKeyFrame(0);
    }
}
