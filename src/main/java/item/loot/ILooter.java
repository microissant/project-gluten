package item.loot;

import components.currency.ITradeAgent;
import components.health.IHealable;
import components.upgrade.IUpgradableEntity;
import components.weapon.WeaponHolder;

public interface ILooter extends ITradeAgent, IUpgradableEntity {

    /**
     *
     * @return a object which has health and can be healed
     */
    default IHealable getHealable() { return null; }

    /**
     * @return an object which can equip a weapon
     */
    default WeaponHolder getWeaponHolder() { return null; }

}
