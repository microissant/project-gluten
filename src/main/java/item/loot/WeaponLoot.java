package item.loot;

import components.weapon.Weapon;

public class WeaponLoot implements ILoot {

    private final Weapon weapon;

    public WeaponLoot(Weapon weapon) {
        this.weapon = weapon;
    }

    @Override
    public void obtain(ILooter looter) {
        if (looter == null) return;
        if (looter.getWeaponHolder() == null) return;

        looter.getWeaponHolder().addWeapon(weapon);
    }

    @Override
    public String displayTitle() {
        return weapon.getName();
    }

    @Override
    public String displayDescription() {
        return "";
    }
}
