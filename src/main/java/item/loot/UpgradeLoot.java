package item.loot;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import components.upgrade.upgrades.IUpgrade;

public class UpgradeLoot implements ILoot {

    private final IUpgrade upgrade;

    public UpgradeLoot(IUpgrade upgrade) {
        this.upgrade = upgrade;
    }

    @Override
    public void obtain(ILooter looter) {
        looter.addUpgrade(upgrade);
    }

    @Override
    public String displayTitle() {
        return upgrade.getTitle();
    }

    @Override
    public String displayDescription() {
        return upgrade.getDescription();
    }

    @Override
    public TextureRegion displayIcon() {
        return upgrade.getIcon();
    }

    public float getTimeToLive() {
        return upgrade.getTimeToLive();
    }
}
