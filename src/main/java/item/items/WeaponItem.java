package item.items;

import com.badlogic.gdx.math.Vector2;
import components.weapon.Weapon;
import item.loot.WeaponLoot;
import model.event.EventBus;

public class WeaponItem extends BaseItem {
    public WeaponItem(Vector2 position, Weapon weapon, EventBus eventBus) {
        super(position, new WeaponLoot(weapon), weapon.getProperties().getTextureID(), eventBus);
    }
}
