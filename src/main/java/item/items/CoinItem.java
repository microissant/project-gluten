package item.items;

import com.badlogic.gdx.math.Vector2;
import item.loot.CoinLoot;
import media.TextureID;
import model.event.EventBus;

public class CoinItem extends BaseItem {

    public CoinItem(Vector2 position, int min, int max, EventBus eventBus) {
        super(position, new CoinLoot(min, max), TextureID.CURRENCY_ICON, eventBus);
    }
}
