package item.items;

import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType;
import components.collision.ICollidable;
import components.collision.primitives.CollisionCircle;
import components.physics.Physics;
import item.InteractAgent;
import item.Item;
import item.loot.ILoot;
import media.DrawProperties;
import media.SoundID;
import media.TextureID;
import model.event.EventBus;
import model.event.events.audio.PlaySoundEvent;
import model.event.events.item.ItemPickupEvent;

import java.util.HashSet;
import java.util.Set;

public abstract class BaseItem implements Item {

    private final Physics physics;
    private final CollisionCircle collisionCircle;
    private final ILoot loot;
    private final DrawProperties drawProperties;
    private final EventBus eventBus;

    public BaseItem(Vector2 position, ILoot loot, TextureID textureID, EventBus eventBus) {
        physics = new Physics(position);
        drawProperties = new DrawProperties(textureID);
        collisionCircle = new CollisionCircle(
                physics.getPosition(),
                new Vector2(0, getProperties().height / 2),
                10,
                this,
                new HashSet<>(Set.of(new CollisionType(CollisionType.Type.PLAYER))),
                new HashSet<>()
        );

        this.loot = loot;
        this.eventBus = eventBus;
    }


    @Override
    public void collisionEnter(ICollidable target, CollisionType.Type type) {
        InteractAgent agent = target.getInteractAgent();
        if (agent == null) return;

        loot.obtain(agent);
        if (eventBus == null) return;
        eventBus.post(new ItemPickupEvent(this, agent));
        eventBus.post(new PlaySoundEvent(SoundID.COIN));
    }

    @Override
    public CollisionCircle getCollisionPrimitive() {
        return collisionCircle;
    }

    @Override
    public DrawProperties getProperties() {
        return drawProperties;
    }

    @Override
    public Vector2 getPosition() {
        return physics.getPosition();
    }

    @Override
    public void setPosition(Vector2 pos) {
        physics.setPosition(pos);
    }

    @Override
    public void onInteract(InteractAgent agent) {
        loot.obtain(agent);
    }
}
