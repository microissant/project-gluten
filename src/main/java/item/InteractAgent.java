package item;

import item.loot.ILooter;

/**
 * Interface representing an agent that can interact with {@link Interactable} objects in the game.
 */
public interface InteractAgent extends ILooter {

    /**
     * Sets the interactable target for this agent.
     *
     * @param interactableTarget The {@link Interactable} object that the agent can interact with.
     */
    void setInteractTarget(Interactable interactableTarget);

    /**
     * Returns the current interactable target of this agent.
     *
     * @return The {@link Interactable} object that the agent is currently targeting.
     */
    Interactable getInteractTarget();

    /**
     * Removes the current interactable target from this agent.
     */
    void removeInteractTarget();
}
