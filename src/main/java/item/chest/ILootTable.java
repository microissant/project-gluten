package item.chest;

import item.loot.ILoot;

import java.util.List;

public interface ILootTable {

    /**
     * Returns a list of loot items based on the loot table, with the specified number
     * of items.
     *
     * @param numberOfItems The number of items to retrieve from the loot table.
     * @return A list of {@link ILoot} objects based on the loot table.
     */
    List<ILoot> getItems(int numberOfItems);
}
