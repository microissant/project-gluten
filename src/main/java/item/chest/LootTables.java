package item.chest;

import components.upgrade.upgrades.behavior.AutoDodgeUpgrade;
import components.upgrade.upgrades.behavior.FireBehindOnDash;
import components.upgrade.upgrades.currency.IncreaseCurrencyGain;
import components.upgrade.upgrades.health.IncreaseMaxHealthValue;
import components.upgrade.upgrades.health.Invincibility;
import components.upgrade.upgrades.physics.IncreaseRunSpeed;
import components.upgrade.upgrades.weapons.UnlimitedAmmo;
import item.loot.CoinLoot;
import item.loot.HealthLoot;
import item.loot.UpgradeLoot;
import model.event.EventBus;

import java.util.HashMap;
import java.util.Map;

public class LootTables {

    /**
     * @param eventBus eventbus responsible for handeling events
     * @return the standard loot table
     */
    public static LootTable standardLootTable(EventBus eventBus) {
        return new LootTable(
                new HashMap<>(Map.of(
                        new CoinLoot(3, 10), 10,
                        new HealthLoot(3, 10), 10,
                        new UpgradeLoot(new IncreaseMaxHealthValue(5)), 5,
                        new UpgradeLoot(new Invincibility(20)), 5,
                        new UpgradeLoot(new UnlimitedAmmo(10)), 5,
                        new UpgradeLoot(new FireBehindOnDash(eventBus)), 5,
                        new UpgradeLoot(new AutoDodgeUpgrade(eventBus)), 5,
                        new UpgradeLoot(new IncreaseRunSpeed(10)), 5,
                        new UpgradeLoot(new IncreaseCurrencyGain(25, 30)), 5
                ))
        );
    }
}
