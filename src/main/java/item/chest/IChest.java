package item.chest;

import entities.Entity;
import item.Interactable;
import item.loot.ILoot;

import java.util.List;

public interface IChest extends Entity, Interactable {

    /**
     * Retrieves the list of items contained in the chest.
     *
     * @return a List of ILoot objects representing the items in the chest
     */
    List<ILoot> getItems();

    /**
     * Checks if the chest is locked.
     *
     * @return true if the chest is locked, false otherwise
     */
    boolean isLocked();

    /**
     * Updates the state of the chest to looted.
     */
    void looted();
}
