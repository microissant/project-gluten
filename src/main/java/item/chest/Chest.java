package item.chest;

import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType;
import components.collision.ICollidable;
import components.collision.primitives.CollisionCircle;
import components.currency.ITrader;
import components.currency.coin.Coin;
import item.InteractAgent;
import item.loot.ILoot;
import media.DrawProperties;
import media.SoundID;
import model.event.EventBus;
import model.event.events.audio.PlaySoundEvent;
import model.event.events.collision.RemoveCollisionCircleEvent;
import model.event.events.item.ChestOpenEvent;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Chest implements IChest {

    private final CollisionCircle collisionCircle;
    private final DrawProperties drawProperties;
    private final Vector2 position;

    private final List<ILoot> loot;
    private boolean locked;
    private Coin cost;
    private final EventBus eventBus;
    private InteractAgent interactAgent;

    public Chest(Vector2 position, LootTable lootTable, int lootCount, int cost, EventBus eventBus) {
        this.position = new Vector2(position);
        drawProperties = new DrawProperties(null);
        collisionCircle = new CollisionCircle(
                position,
                new Vector2(0, getProperties().height / 2),
                10,
                this,
                new HashSet<>(Set.of(new CollisionType(CollisionType.Type.PLAYER))),
                new HashSet<>()
        );
        this.cost = new Coin(cost);
        locked = true;

        this.loot = lootTable.getItems(lootCount);

        this.eventBus = eventBus;
    }

    @Override
    public void collisionEnter(ICollidable target, CollisionType.Type type) {
        InteractAgent agent = target.getInteractAgent();
        if (agent == null) return;

        agent.setInteractTarget(this);
    }

    @Override
    public void collisionLeave(ICollidable target) {
        InteractAgent agent = target.getInteractAgent();
        if (agent == null) return;

        agent.removeInteractTarget();
    }

    @Override
    public CollisionCircle getCollisionPrimitive() {
        return collisionCircle;
    }

    @Override
    public boolean isDead() {
        return false;
    }

    @Override
    public DrawProperties getProperties() {
        return drawProperties;
    }

    @Override
    public Vector2 getPosition() {
        return position;
    }

    @Override
    public void setPosition(Vector2 pos) {
        this.position.x = pos.x;
        this.position.y = pos.y;
    }

    @Override
    public List<ILoot> getItems() {
        if (locked) return null;
        return loot;
    }

    @Override
    public Coin getCost() {
        return cost;
    }

    @Override
    public boolean isLocked() {
        return locked;
    }

    @Override
    public void looted() {
        eventBus.post(new RemoveCollisionCircleEvent(collisionCircle));
        interactAgent.removeInteractTarget();
    }

    @Override
    public String interactPromptText() {
        if (locked) return "Unlock chest";
        return "Open chest";
    }

    @Override
    public void onInteract(InteractAgent agent) {
        if (agent == null) return;
        interactAgent = agent;

        if (isLocked()) {
            unlock(interactAgent.getTrader());
        }

        if (eventBus == null) return;

        eventBus.post(new ChestOpenEvent(this));
        eventBus.post(new PlaySoundEvent(SoundID.OPEN_CHEST));
    }

    private void unlock(ITrader trader) {
        if (trader == null) return;
        if (!trader.canPurchase(cost)) return;

        trader.purchase(cost);
        locked = false;
        cost = null;
    }

    @Override
    public void unregisterFromEventBus() {
        eventBus.unregister(this);
    }
}
