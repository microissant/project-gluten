package item.chest;

import item.loot.ILoot;

import java.util.*;

public class LootTable implements ILootTable {

    private final HashMap<ILoot, Integer> table;
    private int totalTickets;
    private final Random random;

    public LootTable(HashMap<ILoot, Integer> table) {
        this.table = new HashMap<>(table);
        random = new Random();
        for (Integer ticket: table.values()) {
            totalTickets += ticket;
        }
    }

    @Override
    public List<ILoot> getItems(int numberOfItems) {
        List<ILoot> selectedItems = new ArrayList<>();
        HashMap<ILoot, Integer> tableCopy = new HashMap<>(table);
        numberOfItems = Math.min(numberOfItems, table.size());
        while (selectedItems.size() < numberOfItems) {
            selectedItems.add(getRandomItem(tableCopy));
        }
        return selectedItems;
    }

    private ILoot getRandomItem(HashMap<ILoot, Integer> table) {
        int roll = random.nextInt(totalTickets);
        int accumulatedTickets = 0;
        ILoot current = null;
        for (HashMap.Entry<ILoot, Integer> entry : table.entrySet()) {
            current = entry.getKey();
            accumulatedTickets += entry.getValue();
            if (accumulatedTickets > roll) break;
        }
        totalTickets -= table.get(current);
        table.remove(current);
        return current;
    }
}
