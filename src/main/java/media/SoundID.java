package media;

public enum SoundID {
    HAPPY_CHEF("src/main/resources/Audio/Sounds/FrenchLaugh.mp3"),
    LEVEL_COMPLETE("src/main/resources/Audio/Sounds/Level Complete_bip.mp3"),
    COOKIE_DEATH("src/main/resources/Audio/Sounds/Cookie Death_bip.mp3"),
    GHOST_DEATH("src/main/resources/Audio/Sounds/Ghost Pepper Death_exhale.mp3"),
    CUBE_DEATH("src/main/resources/Audio/Sounds/Ice Cube Death_bip.mp3"),
    GUN_SHOT("src/main/resources/Audio/Sounds/Gun shot_bip_1.mp3"),
    COIN("src/main/resources/Audio/Sounds/Coin.mp3"),
    CHERRY_SCREAM("src/main/resources/Audio/Sounds/cherrybombScream.mp3"),
    CHERRY_DEATH("src/main/resources/Audio/Sounds/cherrybombDeathScream.mp3"),
    ENEMY_KILL("src/main/resources/Audio/Sounds/enemy_kill.mp3"),
    EXPLOSION("src/main/resources/Audio/Sounds/explosion.mp3"),
    OPEN_CHEST("src/main/resources/Audio/Sounds/ChestOpen.mp3"),
    PLAYER_FOOTSTEP("src/main/resources/Audio/Sounds/playerstep_01.mp3");
   

    public final String path;

    SoundID(String path){
        this.path = path;
    }
    
}
