package media;

import app.global.AudioSettings;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import model.IUpdatable;
import model.event.EventBus;
import model.event.Subscribe;
import model.event.events.audio.AdjustVolumeEvent;
import model.event.events.audio.PauseSoundEvent;
import model.event.events.audio.PlaySoundEvent;
import model.event.events.audio.StopSoundEvent;
import model.event.events.entities.PlayerDeathEvent;
import model.event.events.game.*;
import model.state.GameState;
import model.state.GameStateManager;

import java.util.*;

/**Class responsible for sounds and music*/

public class AudioPlayer implements IUpdatable {
    public Music musicLevel;
    public Music musicPause;
    public Music musicMain;

    private List<Music> allMusic;
    public Music musicPlayerDeath;
    public Sound sound;
    private Map<SoundID, Sound> activeSounds;


    public AudioPlayer(EventBus eventbus){
        if (eventbus == null) return;
        eventbus.register(this);

        musicLevel = MediaManager.getMusic(MusicID.LEVEL_MUSIC);
        musicLevel.setVolume(0.5f);
        musicLevel.setLooping(true);

        musicPause = MediaManager.getMusic(MusicID.PAUSE_MUSIC);
        musicPause.setVolume(0.6f);
        musicPause.setLooping(true);
        
        musicMain = MediaManager.getMusic(MusicID.MAIN_MENU_MUSIC);
        musicMain.setVolume(0.6f);
        musicMain.setLooping(true);
        
        musicPlayerDeath = MediaManager.getMusic(MusicID.PLAYER_DEATH_MUSIC);
        musicPlayerDeath.setVolume(0.6f);

        allMusic = new ArrayList<>(Arrays.asList(
                musicMain, musicLevel, musicPlayerDeath
        ));

        activeSounds = new HashMap<>();

        if (GameStateManager.state() == GameState.MAIN_MENU){
            musicMain.play();
        }

        adjustVolume(null);
    }

    /**
     * Adjusts the volume of music and sounds according to the current audio settings.
     *
     * @param event The AdjustVolumeEvent triggering this method.
     */
    @Subscribe
    public void adjustVolume(AdjustVolumeEvent event) {

        int musicMute = AudioSettings.getMusicMute() || AudioSettings.getMasterMute() ? 0 : 1;
        int sfxMute = AudioSettings.getSFXMute() || AudioSettings.getMasterMute() ? 0 : 1;

        for (Music music: allMusic) {
            music.setVolume(AudioSettings.getMusicVolume() * AudioSettings.getMasterVolume() * musicMute);
        }

        for (Sound sound: activeSounds.values()) {
            sound.play(AudioSettings.getSFXVolume() * AudioSettings.getMasterVolume() * sfxMute);
        }
    }

    private void pauseGameAudio() {
        for (Sound sound: activeSounds.values()) {
            sound.pause();
        }
    }

    private void stopAllMusic() {
        for (Music music: allMusic) {
            music.stop();
        }
    }

    private void removeAllActiveSounds() {
        for (Sound sound: activeSounds.values()) {
            sound.stop();
        }
        activeSounds.clear();
    }

    /**
     * Handles the NewGameEvent and starts playing the level music.
     *
     * @param event The NewGameEvent triggering this method.
     */
    @Subscribe
    public void newGameEvent(NewGameEvent event) {
        if (musicLevel.isPlaying()) return;
        restartLevel(null);
    }

    /**
     * Handles the EndLevelEvent and stops the level music and all active sounds.
     *
     * @param event The EndLevelEvent triggering this method.
     */
    @Subscribe
    public void levelEnd(EndLevelEvent event){
        musicLevel.stop();
        removeAllActiveSounds();
    }

    /**
     * Handles the GameCompleteEvent and stops the level music and all active sounds.
     *
     * @param event The GameCompleteEvent triggering this method.
     */
    @Subscribe
    public void gameComplete(GameCompleteEvent event) {
        levelEnd(null);
    }

    /**
     * Handles the GameCompleteEvent and stops the level music and all active sounds.
     *
     * @param event The GameCompleteEvent triggering this method.
     */
    @Subscribe
    public void pauseGame(PauseGameEvent event) {
        pauseGameAudio();
    }

    /**
     * Handles the LoadNextLevelEvent and starts playing the level music.
     *
     * @param event The LoadNextLevelEvent triggering this method.
     */
    @Subscribe
    public void nextLevelEvent(LoadNextLevelEvent event) {
        removeAllActiveSounds();
        playLevelMusic();
    }

    /**
     * Handles the RestartLevelEvent and starts playing the level music.
     *
     * @param event The RestartLevelEvent triggering this method.
     */
    @Subscribe
    public void restartLevel(RestartLevelEvent event) {
        playLevelMusic();
    }

    /**
     * Handles the PlayerDeathEvent and starts playing the player death music.
     *
     * @param event The PlayerDeathEvent triggering this method.
     */
    @Subscribe
    public void playerDeath(PlayerDeathEvent event){
        musicLevel.stop();
        pauseGameAudio();
        playPlayerDeathMusic();
    }

    /**
     * Handles the MainMenuActiveEvent and starts playing the main menu music.
     *
     * @param event The MainMenuActiveEvent triggering this method.
     */
    @Subscribe
    public void mainMenu(MainMenuActiveEvent event){
        removeAllActiveSounds();
        playMainMenuMusic();
    }

    private void playPlayerDeathMusic(){
        musicPlayerDeath.play();
    }
    
    private void playMainMenuMusic(){
        stopAllMusic();
        musicMain.play();
    }
    
    public void playLevelMusic(){
        stopAllMusic();
        musicLevel.play();
    }

    /**
     * Handles the PlaySoundEvent and plays the specified sound.
     *
     * @param event The PlaySoundEvent triggering this method.
     */
    @Subscribe
    public void playSound(PlaySoundEvent event) {
        if (event == null) return;
        if (event.soundID() == null) return;

        Sound sound = MediaManager.getSound(event.soundID());
        if (sound == null) return;

        int mute = AudioSettings.getSFXMute() || AudioSettings.getMasterMute() ? 0 : 1;

        if (event.isLooping()) {
            activeSounds.put(event.soundID(), sound);
            sound.loop(AudioSettings.getSFXVolume() * AudioSettings.getMasterVolume() * mute);
        } else {
            sound.play(AudioSettings.getSFXVolume() * AudioSettings.getMasterVolume() * mute);
        }
    }

    /**
     * Handles the StopSoundEvent and stops the specified sound.
     *
     * @param event The StopSoundEvent triggering this method.
     */
    @Subscribe
    public void stopSound(StopSoundEvent event) {
        if (event == null) return;
        if (event.soundID() == null) return;

        if (!activeSounds.containsKey(event.soundID())) return;

        Sound sound = activeSounds.get(event.soundID());
        sound.stop();
    }

    /**
     * Handles the PauseSoundEvent and pauses the specified sound.
     *
     * @param event The PauseSoundEvent triggering this method.
     */
    @Subscribe
    public void pauseSound(PauseSoundEvent event) {
        if (event == null) return;
        if (event.soundID() == null) return;

        if (!activeSounds.containsKey(event.soundID())) return;
        Sound sound = activeSounds.get(event.soundID());
        sound.pause();
    }

    /**
     * Disposes of all sound and music resources.
     */
    public void dispose(){
        musicLevel.dispose();
        musicPause.dispose();
        musicMain.dispose();
        musicPlayerDeath.dispose();        
    }
}
