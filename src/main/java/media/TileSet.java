package media;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;

import java.util.ArrayList;
import java.util.List;

/**
 * A class representing a set of tiles from a texture.
 * The texture is split into tiles of equal dimensions specified by width and height.
 */
public class TileSet {
    private final List<StaticTiledMapTile> tiles;

    /**
     * Constructs a TileSet from the given texture, splitting it into tiles of the specified width and height.
     *
     * @param texture The TextureRegion object representing the texture to be split.
     * @param width   The width of a single tile in the TileSet.
     * @param height  The height of a single tile in the TileSet.
     */
    public TileSet(TextureRegion texture , int width, int height){
        tiles = new ArrayList<>();
        TextureRegion [][] splitTiles = texture.split(width,height);
        int cols = splitTiles[0].length;

        for (TextureRegion[] splitTile : splitTiles) {
            for (int col = 0; col < cols; col++) {
                tiles.add(new StaticTiledMapTile(splitTile[col]));
            }
        }
    }

    /**
     * Retrieves a StaticTiledMapTile at the specified index.
     *
     * @param index The index of the desired StaticTiledMapTile within the TileSet.
     * @return The StaticTiledMapTile object at the specified index.
     */
    public StaticTiledMapTile get(int index){
        return tiles.get(index);
    }
}
