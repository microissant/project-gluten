package media;
import java.util.ArrayList;
import java.util.List;

public class TileSetManager {
  

  public enum TileSetID{
    DEFAULT_TILESET(TextureID.DEFAULT_TILESET);
    final TextureID texture;
   TileSetID(TextureID texture){
     this.texture=texture;
   }
  }
  static final List<TileSet> tileSets = new ArrayList<>();

  static {
    for(TileSetID id: TileSetID.values()){
      tileSets.add(new TileSet(MediaManager.getTexture(id.texture).getKeyFrame(0),16,16));
    }
  }

  /**
   * Retrieves the TileSet corresponding to the specified TileSetID.
   *
   * @param id The TileSetID for which the TileSet is requested.
   * @return The TileSet instance associated with the specified TileSetID.
   */
  public static TileSet getTileSet(TileSetID id){
    return tileSets.get(id.ordinal());
  }
}
