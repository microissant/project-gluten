package media;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.ArrayList;

/**
 * A class responsible for managing all media assets, such as textures, sounds, and music.
 * It stores, initializes, and provides access to these assets for the entire application.
 */
public class MediaManager {

   static final ArrayList<Animation<TextureRegion> >textureList = new ArrayList<>();
   static final ArrayList<NinePatch> ninePatches = new ArrayList<>();
   static final ArrayList<Sound> soundList = new ArrayList<>();
   static final ArrayList<Music> musicList = new ArrayList<>();

   /**
    * Initializes all media resources by loading them from their respective files.
    */
   public static void initResources() {
      TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("src/main/resources/packed/TextureAtlas.atlas"));
      for (TextureID t : TextureID.values()) {
         textureList.add( new Animation<>(t.animSpeed, atlas.findRegions(t.fileName),t.loop ? Animation.PlayMode.LOOP : Animation.PlayMode.NORMAL));
      }
      
      for(NinePatchID n : NinePatchID.values()){
         ninePatches.add(new NinePatch(atlas.createPatch(n.filename)));
      }

      for (SoundID s : SoundID.values()){
         soundList.add(Gdx.audio.newSound(Gdx.files.internal(s.path))); 
                  
      }
      
      for (MusicID s : MusicID.values()){
         musicList.add(Gdx.audio.newMusic(Gdx.files.internal(s.path)));                
      }
   }

   /**
    * Retrieves the texture animation associated with the given TextureID.
    *
    * @param id The TextureID for the desired texture animation.
    * @return The Animation<TextureRegion> object associated with the specified TextureID.
    */
   public static Animation<TextureRegion> getTexture (TextureID id){
      if(id==null) return null;
      return textureList.get(id.ordinal());

   }

   /**
    * Retrieves the sound associated with the given SoundID.
    *
    * @param sound The SoundID for the desired sound.
    * @return The Sound object associated with the specified SoundID.
    */
   public static Sound getSound(SoundID sound){
      if(sound == null)return null;
      return soundList.get(sound.ordinal());
   }

   /**
    * Retrieves the music associated with the given MusicID.
    *
    * @param music The MusicID for the desired music.
    * @return The Music object associated with the specified MusicID.
    */
   public static Music getMusic(MusicID music){
      return musicList.get(music.ordinal());
   }

   /**
    * Retrieves the NinePatch associated with the given NinePatchID.
    *
    * @param id The NinePatchID for the desired NinePatch.
    * @return The NinePatch object associated with the specified NinePatchID.
    */
   public static NinePatch getNinePatch(NinePatchID id){
      return ninePatches.get(id.ordinal());
   }


}
