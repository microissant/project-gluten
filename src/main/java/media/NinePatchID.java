package media;

public enum NinePatchID {
    HEALTHBAR("healthbarnp"),
    BUTTON_FRAME_01("ui_border"),
    TILE_BLACK("ui_border_black"),
    BUTTON_STANDARD_UP("ui_border"),
    BUTTON_STANDARD_DOWN("ui_border_down"),
    BUTTON_STANDARD_CHECKED("ui_border_checked"),
    BUTTON_STANDARD_OVER("ui_border_down");


    public final String filename;
    NinePatchID(String filename){
        this.filename = filename;
    }
}
