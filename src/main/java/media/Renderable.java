package media;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * An interface for objects that can be rendered on the screen using a SpriteBatch.
 * Implementing classes should provide DrawProperties and a position to draw the image.
 */
public interface Renderable {

    /**
     * Retrieves the DrawProperties object containing information needed for rendering.
     *
     * @return The DrawProperties object associated with this Renderable object.
     */
    DrawProperties getProperties();

    /**
     * Retrieves the current position of the Renderable object.
     *
     * @return The Vector2 object representing the position of the Renderable object.
     */
    Vector2 getPosition();

    /**
     * Renders the image using the given SpriteBatch and deltaTime.
     *
     * @param batch The SpriteBatch to draw on.
     * @param deltaTime The time since the last frame.
     */
    default void draw(SpriteBatch batch, float deltaTime) {
        getProperties().drawSelf(batch, deltaTime, getPosition());
    }
}
