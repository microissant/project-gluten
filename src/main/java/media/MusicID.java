package media;

public enum MusicID {
    
    
    LEVEL_MUSIC("src/main/resources/Audio/Music/LevelMusic2Test.mp3"),
    PAUSE_MUSIC("src/main/resources/Audio/Music/PauseLullaby.mp3"),
    MAIN_MENU_MUSIC("src/main/resources/Audio/Music/Chefs_Theme.mp3"),
    PLAYER_DEATH_MUSIC("src/main/resources/Audio/Music/PlayerDeath.mp3");

    public final String path;

    MusicID(String path){
        this.path = path;
    }
    
}

