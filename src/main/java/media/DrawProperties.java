package media;


import app.global.RuntimeOptions;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import java.util.concurrent.ThreadLocalRandom;

/**a class that stores all of information needed to display a sprite/animation at a given position. Inteded to be used by classes that implement renderable. */
public class DrawProperties {
    private Animation<TextureRegion> animation;
    public float width=1;
    public float height=1;
    public float scaleX=1;
    public float scaleY=1;
    public float rotation = 0;
    private float timeSinceAnimationStart = 0;
    public TextureID animationId;
    public float xOffset = 0;
    public float yOffset = 0;
    public float animationSpeed =1;
    private TextureID textureID;


    public DrawProperties() {
    }

    public DrawProperties(TextureID textureID) {
        this();
        this.textureID = textureID;
        setAnimation(textureID);       
    }

    /**
     * Gets the TextureID associated with this DrawProperties instance.
     *
     * @return The TextureID.
     */
    public TextureID getTextureID() {
        return textureID;
    }

    /**
     * Gets the current texture based on the time since the animation started.
     *
     * @param deltaTime The time elapsed since the last frame.
     * @return The current TextureRegion.
     */
    public TextureRegion getTexture(float deltaTime){
        timeSinceAnimationStart+=deltaTime * animationSpeed;
        return animation.getKeyFrame(timeSinceAnimationStart);
    }

    /**
     * Sets the animation for this DrawProperties instance.
     *
     * @param textureID The TextureID to set as the animation.
     */
    public void setAnimation(TextureID textureID){
        if(!RuntimeOptions.renderingEnabled())return;
        this.textureID = textureID;
        animation= MediaManager.getTexture(textureID);
        setAnimationSpeed(1f);
        animationId = textureID;
        if(animation == null) return;
        timeSinceAnimationStart = 0;
        width = animation.getKeyFrame(0).getRegionWidth();
        height = animation.getKeyFrame(0).getRegionHeight();
    }

    /**sets the animation speed of the animation.
     * 
     * @param animationSpeed a multiplier of the default animation speed for this animation. A value of 2 will make the animation end twice as fast.A value of 0 will make the animation stop.
     */
    public void setAnimationSpeed(float animationSpeed){
        this.animationSpeed = animationSpeed;
    }

    /**
     * Sets the current frame to a random frame in the animation.
     */
    public void setRandomFrame(){
        if(animation == null) return;
        timeSinceAnimationStart = ThreadLocalRandom.current().nextFloat(getAnimationDuration());
    }

    /** returns the amount of seconds between each frame of the animation
     *  the reciprocal of the returned value is the amount of frames per second
     * @return the amount of seconds between each frame
     */
    public float getAnimationSpeed(){
        if(animation ==null) return 1;
        return animation.getFrameDuration();
    }

    /**
     * @return The animation duration in seconds.
     */
    public float getAnimationDuration(){
        if(animation ==null) return 1;
        return animation.getAnimationDuration();
    }

    /**
     * returns the amount of keyframes in the animation
     */
    public int numFrames(){
        return animation.getKeyFrames().length;
    }

    /**
     * @return The time since the animation started in seconds.
     */
    public float getTimeSinceAnimationStart(){
        return timeSinceAnimationStart;
    }

    /**
     * sets the progress of the current animation. 0 is the first frame of the animation and 1 is the last frame of the animation
     */
    public void setAnimationProgress(float progress){
        if(animation ==null) return;
        progress = Math.min(Math.max(0,progress),1);
        float numFrames =(float) animation.getKeyFrames().length;
        timeSinceAnimationStart = getAnimationDuration()*((numFrames-1)/numFrames) * progress;
    }

    /**
     * Draws the current frame of the animation at the given position.
     *
     * @param batch    The SpriteBatch used for drawing.
     * @param deltaTime The time elapsed since the last frame.
     * @param position  The position to draw the animation.
     */
    public void drawSelf(SpriteBatch batch, float deltaTime, Vector2 position){
        if(animation ==null) return;
        batch.draw(
            getTexture(deltaTime),
            position.x-animationId.xOrigin + xOffset,
            position.y-animationId.yOrigin + yOffset,
            animationId.xOrigin,
            animationId.yOrigin,
            width,
            height,
            scaleX,
            scaleY,
            rotation
            );
    }
}
