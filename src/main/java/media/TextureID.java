package media;

public enum TextureID {

    BULLET("bullet",3,3),
    EXPLOSION("explosion",31,19,0.1f,false),
    COOKIE_ENEMY_IDLE("cookie_enemy_idle",9,1),
    COOKIE_ENEMY_WALK("cookie_enemy_walk",10,1,0.1f),
    ROBOCHEF_ROLL_SIDE("chef_roll_side",14,8,0.044f,false),
    ROBOCHEF_SLIDE_SIDE("robochef_slide",16,2,0.052f,false),
    ROBOCHEF_SLIDE_DOWN("robochef_slide_down",9,6,0.052f,false),
    ROBOCHEF_IDLE("robochef_idle",9,1),
    ROBOCHEF_ROLL_DOWN("robochef_roll_down",8,0,0.044f,false), 
    ROBOCHEF_RUN("robochef_run",9,2,0.1f),
    DEFAULT_TILESET("default_tileset"),
    BAGUETTE("baguette",2,2),
    DISH("dish",6,3),
    ICE_CREAM("icecream",2,3),
    ROCKET("rocket",8,3),
    ICECUBE_SPIN("icecube_spin",9,4),
    GHOST_PEPPER_SHOOT("ghost_pepper_shoot",9,6,0.075f),
    GHOST_PEPPER_IDLE("ghost_pepper_idle",8,7,0.1f),
    SHOT_GUN("shotgun",4,3),
    HEALTH_ICON("health_icon"),
    BULLET_END("bullet_end",5,5,0.065f),
    HEALTHBAR("healthbar",0,26),
    HEALTHBAR_CRT("healthbar_crt_effect",0,26),
    DEATH_ICON("death_icon"),
    TOPPINGS("toppings",5,5,1,false),
    TOPPERGUN("toppergun",6,6),
    JAR_OF_HONEY("jar_of_honey",5,4),
    CROISSANT("croissant",7,6),
    PIZZA_PEEL("pizza_peel",4,6),
    PIZZA("pizza",8,7),
    ITEM_ICONS("item_icons",0,0,1f,false),
    DONUT("donut",6,6),
    BLENDER("blender",3,6,1f,false),
    CHERRYBOMB_IDLE("cherrybomb_idle",9,1),
    ICECREAMBOSS_IDLE("icecreamboss_idle",17,4,0.1f),
    ICECREAMBOSS_JUMP("icecreamboss_jump",17,4,0.075f,false),
    ICECREAMBOSS_SPIN("icecreamboss_spin",17,4,0.075f,false),
    CHERRYBOMB_EXPLOSION("cherrybombexplosion",9,1,0.1f,false),
    CHERRYBOMB_WALK("cherrybomb_walk",9,1,0.12f),
    EXPLOSION_V2("explosion2",27,18,0.1f,false),
    CUTTING_BOARD("cuttingboard"),
    MEATCLEAVER("meatcleaver",11,8),
    AMMO_BACKGROUND("ammo_background"),
    AMMO_BACKGROUND_CRT("ammo_background_crt"),
    PORTAL("portal",13,6,0.125f,true),
    CRATE("crate",8,8,10000f,false),
    CURRENCY_ICON("currency_icon",4,3,0.1f);


  public final String fileName;
  public final int xOrigin;
  public final int yOrigin;
  public final float animSpeed;
  public final boolean loop;
  TextureID( String fileName, int xOrigin,int yOrigin, float animSpeed, boolean loop) {
    this.fileName= fileName;
    this.xOrigin=xOrigin;
    this.yOrigin=yOrigin;
    this.animSpeed=animSpeed;
    this.loop = loop;
}
  TextureID( String fileName, int xOrigin,int yOrigin, float animSpeed) {
      this(fileName,xOrigin,yOrigin,animSpeed,true);

  }
  TextureID( String fileName, int xOrigin,int yOrigin) {
    this(fileName,xOrigin,yOrigin,1,true);
  }
  TextureID( String fileName) {
    this(fileName,0,0,1,true);
  }
}

