package controller;

import app.Globals;
import app.global.ControllerSettings;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import model.event.EventBus;
import model.event.events.input.*;
import model.event.events.player.SwapWeaponsEvent;
import model.state.GameState;
import model.state.GameStateManager;

public class GameController implements InputProcessor{

    private final EventBus eventBus;
    protected boolean isEnabled;

    public GameController(EventBus eventBus) {
        this.eventBus = eventBus;
        isEnabled = true;
    }

    public void handleInput() {
        if (!isEnabled) return;
        if (GameStateManager.state() != GameState.ACTIVE) return;

        int x = 0;
        int y = 0;
        int aimx = 0;
        int aimy = 0;
        boolean keyboardFireFlag = false;

        if (Gdx.input.isKeyPressed(ControllerSettings.MOVE_UP.getCurrentOption())) {
            y += 1;
        }
        if (Gdx.input.isKeyPressed(ControllerSettings.MOVE_RIGHT.getCurrentOption())) {
            x += 1;
        }
        if (Gdx.input.isKeyPressed(ControllerSettings.MOVE_DOWN.getCurrentOption())) {
            y -= 1;
        }
        if (Gdx.input.isKeyPressed(ControllerSettings.MOVE_LEFT.getCurrentOption())) {
            x -= 1;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
            aimy += 1;
            keyboardFireFlag=true;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            aimy -= 1;
            keyboardFireFlag = true;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            aimx -= 1;
            keyboardFireFlag=true;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            aimx += 1;
            keyboardFireFlag = true;
        }

        if(keyboardFireFlag){
            Globals.currentAimMode = Globals.AimMode.Keyboard;
            Globals.keyAimDirection.set(aimx,aimy);
        }

        if (Gdx.input.isButtonPressed(Input.Buttons.LEFT) || keyboardFireFlag)    
            eventBus.post(new InputPrimary(null));
            

        if (x != 0 || y != 0)
            eventBus.post(new InputMove(new Vector2(x, y)));

        if(Gdx.input.isKeyJustPressed(ControllerSettings.DASH.getCurrentOption()))
            eventBus.post(new InputDodge());

        else if (Gdx.input.isKeyJustPressed(ControllerSettings.INTERACT.getCurrentOption()))
            eventBus.post(new InputInteract());

        else if (Gdx.input.isKeyJustPressed(ControllerSettings.PAUSE.getCurrentOption())) {
            eventBus.post(new InputESC());
        }



        
    }

    @Override
    public boolean keyDown(int keycode) {

        if (keycode == Input.Keys.NUM_1 || keycode == Input.Keys.NUM_2) {
            eventBus.post(new SwapWeaponsEvent());
        }



        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        return true;
    }

    @Override
    public boolean keyTyped(char character) {
        return true;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        Globals.currentAimMode = Globals.AimMode.Mouse;
        return true;
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        return true;
    }

    public void disable() {
        isEnabled = false;
    }

    public void enable() {
        isEnabled = true;
    }
}
