package controller;

import com.badlogic.gdx.InputProcessor;
import model.event.EventBus;
import model.event.events.ui.MenuInputEvent;

public class MenuController implements InputProcessor {


    private final EventBus eventBus;

    public MenuController(EventBus eventBus) {
        this.eventBus = eventBus;
    }

    @Override
    public boolean keyDown(int keycode) {

        eventBus.post(new MenuInputEvent(keycode));

        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        return false;
    }
}
