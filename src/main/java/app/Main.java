package app;

import app.global.RuntimeOptions;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import view.screens.Gluten;



public class Main {
    public static void main(String[] args) {
        Lwjgl3ApplicationConfiguration cfg = new Lwjgl3ApplicationConfiguration();

        RuntimeOptions.enableRendering();

        cfg.setTitle("Project Gluten");
        cfg.setWindowedMode(480*3, 320*3);


        /* 'flattenPaths' detects subdirectories as input for process()
         * and extracts the files they conatin */
        TexturePacker.Settings settings = new TexturePacker.Settings();
        settings.flattenPaths = true;        
        settings.combineSubdirectories = true;  
        TexturePacker.process(settings,"src/main/resources/images", "src/main/resources/packed/", "TextureAtlas");
        

        new Lwjgl3Application(new Gluten(), cfg);
    }
}