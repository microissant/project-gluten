package app.global;

public class RuntimeOptions {
    private static boolean doRendering=false;

    /** boolean to check if parts of the program that require runtime can run
     */
    public static boolean renderingEnabled(){
        return doRendering;
    }

    public static void enableRendering(){
        doRendering=true;
    }
}
