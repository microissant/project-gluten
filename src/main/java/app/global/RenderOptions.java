package app.global;

import util.Option;

import java.util.ArrayList;
import java.util.List;

public class RenderOptions {

    // DEFAULT SETTINGS

    public static final Option<String> resolution = new Option<>("1440x960");
    public static final Option<DisplayOption> displayMode = new Option<>(DisplayOption.WINDOWED);
    public static final Option<Boolean> vSync = new Option<>(true);
    public static final Option<Boolean> uncappedFPS = new Option<>(false);
    public static final Option<Integer> maxFPS = new Option<>(60);
    public static final Option<Boolean> displayDebugStats = new Option<>(false);
    public static final Option<Boolean> enableDrawDebug = new Option<>(false);
    public static final Option<Boolean> enableDebugCollision = new Option<>(false);
    public static final Option<Boolean> enableDebugPath = new Option<>(false);


    private static final List<Option<?>> allOptions;

    static {
        allOptions = new ArrayList<>();
        allOptions.add(resolution);
        allOptions.add(displayMode);
        allOptions.add(vSync);
        allOptions.add(uncappedFPS);
        allOptions.add(maxFPS);
        allOptions.add(displayDebugStats);
        allOptions.add(enableDrawDebug);
        allOptions.add(enableDebugCollision);
        allOptions.add(enableDebugPath);
    }

    public static void resetToDefault() {
        for (Option<?> option: allOptions) {
            option.reset();
        }
    }

    public enum DisplayOption {

        FULLSCREEN,
        WINDOWED

    }
}
