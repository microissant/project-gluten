package app.global;

import com.badlogic.gdx.Input;
import util.Option;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>ControllerSettings is a utility class for managing global controller settings in an application.
 * It provides methods to manage key bindings for various in-game actions, such as movement, interaction, and pausing.</p>
 *
 * <p>Key bindings are stored as {@link Option} objects, with default values assigned based on the {@link Input} class constants.
 * The class also provides utility methods for converting keys and buttons to human-readable strings.</p>
 */
public class ControllerSettings {

    private static final Map<Integer, String> inputToStringMap = new HashMap<>();
    private static final Map<String, Integer> stringToInputMap = new HashMap<>();

    public static final Option<Integer> ATTACK = new Option<>(Input.Buttons.LEFT);
    public static final Option<Integer> MOVE_UP = new Option<>(Input.Keys.W);
    public static final Option<Integer> MOVE_LEFT = new Option<>(Input.Keys.A);
    public static final Option<Integer> MOVE_RIGHT = new Option<>(Input.Keys.D);
    public static final Option<Integer> MOVE_DOWN = new Option<>(Input.Keys.S);
    public static final Option<Integer> DASH = new Option<>(Input.Keys.SPACE);
    public static final Option<Integer> INTERACT = new Option<>(Input.Keys.E);
    public static final Option<Integer> PAUSE = new Option<>(Input.Keys.ESCAPE);

    private static final List<Option<?>> allOptions;

    static {
        inputToStringMap.put(Input.Buttons.LEFT, "Left mouse");
        inputToStringMap.put(Input.Buttons.RIGHT, "Right mouse");
        inputToStringMap.put(Input.Buttons.MIDDLE, "Middle mouse");
        inputToStringMap.put(Input.Buttons.BACK, "Back mouse");
        inputToStringMap.put(Input.Buttons.FORWARD, "Forward mouse");

        for (char ch = 'A'; ch <= 'Z'; ch++) {
            inputToStringMap.put(Input.Keys.valueOf(Character.toString(ch)), Character.toString(ch));
        }
        for (int num = 0; num <= 9; num++) {
            inputToStringMap.put(Input.Keys.valueOf("NUM_" + num), Integer.toString(num));
        }

        for (int num = 0; num <= 9; num++) {
            inputToStringMap.put(Input.Keys.valueOf(Character.toString((char) ('0' + num))), Integer.toString(num));
        }

        inputToStringMap.put(Input.Keys.SPACE, "Space");
        inputToStringMap.put(Input.Keys.ESCAPE, "ESC");
        inputToStringMap.put(Input.Keys.TAB, "Tab");
        inputToStringMap.put(Input.Keys.SHIFT_LEFT, "Shift (Left)");
        inputToStringMap.put(Input.Keys.SHIFT_RIGHT, "Shift (Right)");
        inputToStringMap.put(Input.Keys.CONTROL_LEFT, "Ctrl (Left)");
        inputToStringMap.put(Input.Keys.CONTROL_RIGHT, "Ctrl (Right)");
        inputToStringMap.put(Input.Keys.ALT_LEFT, "Alt (Left)");
        inputToStringMap.put(Input.Keys.ALT_RIGHT, "Alt (Right)");
        inputToStringMap.put(Input.Keys.BACKSPACE, "Backspace");
        inputToStringMap.put(Input.Keys.ENTER, "Enter");
        inputToStringMap.put(Input.Keys.CAPS_LOCK, "Caps Lock");
        inputToStringMap.put(Input.Keys.SCROLL_LOCK, "Scroll Lock");
        inputToStringMap.put(Input.Keys.NUM_LOCK, "Num Lock");
        inputToStringMap.put(Input.Keys.INSERT, "Insert");
        inputToStringMap.put(Input.Keys.HOME, "Home");
        inputToStringMap.put(Input.Keys.END, "End");
        inputToStringMap.put(Input.Keys.PAGE_UP, "Page Up");
        inputToStringMap.put(Input.Keys.PAGE_DOWN, "Page Down");

        for (int fn = 1; fn <= 12; fn++) {
            inputToStringMap.put(Input.Keys.valueOf("F" + fn), "F" + fn);
        }

        inputToStringMap.put(Input.Keys.UP, "Up");
        inputToStringMap.put(Input.Keys.DOWN, "Down");
        inputToStringMap.put(Input.Keys.LEFT, "Left");
        inputToStringMap.put(Input.Keys.RIGHT, "Right");

        // Add more keys if needed

        for (Map.Entry<Integer, String> entry : inputToStringMap.entrySet()) {
            stringToInputMap.put(entry.getValue(), entry.getKey());
        }


        allOptions = new ArrayList<>();
        allOptions.add(ATTACK);
        allOptions.add(MOVE_UP);
        allOptions.add(MOVE_DOWN);
        allOptions.add(MOVE_LEFT);
        allOptions.add(MOVE_RIGHT);
        allOptions.add(DASH);
        allOptions.add(INTERACT);
        allOptions.add(PAUSE);
    }

    /**
     * Resets all options to their default values.
     */
    public static void resetToDefault() {
        for (Option<?> option: allOptions) {
            option.reset();
        }
    }

    /**
     * Converts an input key or button code to a human-readable string representation.
     *
     * @param input the input key or button code, as defined in {@link Input}
     * @return a human-readable string representation of the input key or button, or null if the input code is not recognized
     */
    public static String convertInputToString(int input) {
        return inputToStringMap.getOrDefault(input, null);
    }

    /**
     * Converts a human-readable string representation of an input key or button to its corresponding code.
     *
     * @param inputString the human-readable string representation of the input key or button
     * @return the input key or button code, as defined in {@link Input}, or null if the input string is not recognized
     */
    public static Integer convertStringToInput(String inputString) {
        return stringToInputMap.getOrDefault(inputString, null);
    }

}
