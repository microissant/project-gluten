package app.global;

/**
 * <p>Statistics is a utility class for tracking performance-related metrics in an application.
 * It provides methods to manage and retrieve statistics for various aspects of the application,
 * such as collision detection and pathfinding.</p>
 *
 * <p>Each statistic is stored as a long value, with separate values for per-frame and total counts
 * (where applicable).</p>
 */
public class Statistics {


    // Collision detection
    private static long collisionsCountFrame;
    private static long collisionTimeNS;
    private static long collisionsCountTotal;

    // Pathfinding
    private static long pathNodeChecksFrame;
    private static long pathNodeChecksTotal;


    /**
     * Adds a specified count of collision checks to both the per-frame and total counters.
     *
     * @param count the number of collision checks to add
     */
    public static void addCollisionChecksCount(long count) {
        collisionsCountFrame = count;
        collisionsCountTotal += count;
    }

    /**
     * Sets the collision detection time in nanoseconds.
     *
     * @param time the collision detection time in nanoseconds
     */
    public static void setCollisionTimeNS(long time) {
        collisionTimeNS = time;
    }

    /**
     * @return the collision detection time in nanoseconds
     */
    public static long getCollisionTimeNS() {
        return collisionTimeNS;
    }

    /**
     * @return the number of collision checks for the current frame
     */
    public static long getCollisionsCountFrame() {
        return collisionsCountFrame;
    }

    /**
     * @return the total number of collision checks
     */
    public static long getCollisionsCountTotal() {
        return collisionsCountTotal;
    }

    /**
     * Adds a specified count of path calculations to both the per-frame and total counters.
     *
     * @param count the number of path calculations to add
     */
    public static void addPathCalculationsCount(long count) {
        pathNodeChecksFrame += count;
        pathNodeChecksTotal += count;
    }

    /**
     * @return the number of path node checks for the current frame and resets the per-frame counter
     */
    public static long getPathNodeChecksFrame() {
        long temp = pathNodeChecksFrame;
        pathNodeChecksFrame = 0;
        return temp;
    }

    /**
     * @return the total number of path node checks
     */
    public static long getPathNodeChecksTotal() {
        return pathNodeChecksTotal;
    }

}
