package app.global;

import util.Option;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>AudioSettings is a utility class for managing global audio settings in an application.
 * It provides methods to manage master volume, music volume, sound effect volume, and mute settings.</p>
 *
 * <p>All volume levels are represented by float values between 0 and 1, where 1 is 100% and 0 is 0%.</p>
 *
 * <p>The mute settings are independent for master, music, and sound effects.
 * If the master mute is enabled, all sounds will be muted regardless of their individual mute settings.</p>
 */
public class AudioSettings {

    private static final Option<Boolean> masterMute = new Option<>(false);
    private static final Option<Float> masterVolume = new Option<>(1.0f);
    private static final Option<Boolean> musicMute = new Option<>(false);
    private static final Option<Float> musicVolume = new Option<>(0.6f);
    private static final Option<Boolean> SFXMute = new Option<>(false);
    private static final Option<Float> SFXVolume = new Option<>(0.6f);

    private static final List<Option<?>> allOptions;

    static {
        allOptions = new ArrayList<>();
        allOptions.add(masterVolume);
        allOptions.add(musicVolume);
        allOptions.add(SFXVolume);
    }

    /**
     * Reset all options to their default setting
     */
    public static void resetToDefault() {
        for (Option<?> option: allOptions) {
            option.reset();
        }
    }

    /**
     * @return master volume level. A value between 0-1. 1 = 100%, 0 = 0%
     */
    public static float getMasterVolume() {
        return masterVolume.getCurrentOption();
    }

    /**
     * @return music volume level. A value between 0-1. 1 = 100%, 0 = 0%
     */
    public static float getMusicVolume() {
        return musicVolume.getCurrentOption();
    }

    /**
     * @return sound effect volume level. A value between 0-1. 1 = 100%, 0 = 0%
     */
    public static float getSFXVolume() {
        return SFXVolume.getCurrentOption();
    }

    /**
     * Set the mute option for master. Master mute overrides all other mutes. If master mute is
     * enabled, all sounds is muted.
     *
     * @param isMute true to mute all audio, false to unmute
     */
    public static void setMasterMute(boolean isMute) {
        masterMute.setCurrentOption(isMute);
    }

    /**
     * Set the mute option for music. If music mute is enabled, only music will be muted.
     *
     * @param isMute true to mute music, false to unmute
     */
    public static void setMusicMute(boolean isMute) {
        musicMute.setCurrentOption(isMute);
    }

    /**
     * Set the master volume level.
     *
     * @param volume a value between 0-1, where 1 = 100% and 0 = 0%
     */
    public static void setMasterVolume(float volume) {
        masterVolume.setCurrentOption(volume);
    }

    /**
     * Set the music volume level.
     *
     * @param volume a value between 0-1, where 1 = 100% and 0 = 0%
     */
    public static void setMusicVolume(float volume) {
        musicVolume.setCurrentOption(volume);
    }

    /**
     * Set the mute option for sound effects. If sound effect mute is enabled, only sound effects will be muted.
     *
     * @param isMute true to mute sound effects, false to unmute
     */
    public static void setSFXMute(boolean isMute) {
        SFXMute.setCurrentOption(isMute);
    }

    /**
     * Set the sound effect volume level.
     *
     * @param volume a value between 0-1, where 1 = 100% and 0 = 0%
     */
    public static void setSFXVolume(float volume) {
        SFXVolume.setCurrentOption(volume);
    }

    /**
     * @return true if master mute is enabled, false otherwise
     */
    public static boolean getMasterMute() {
        return masterMute.getCurrentOption();
    }

    /**
     * @return true if music mute is enabled, false otherwise
     */
    public static boolean getMusicMute() {
        return musicMute.getCurrentOption();
    }

    /**
     * @return true if sound effect mute is enabled, false otherwise
     */
    public static boolean getSFXMute() {
        return SFXMute.getCurrentOption();
    }
}
