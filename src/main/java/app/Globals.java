package app;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class Globals {

    public static Vector3 mouseWorldPosition;
    public static final Vector2 keyAimDirection = new Vector2();

    public enum AimMode {
        Mouse,
        Keyboard
    }

    public static AimMode currentAimMode = AimMode.Keyboard;

    /////// RENDERING OPTIONS

    public static final int WINDOW_WIDTH = 1440;
    public static final int WINDOW_HEIGHT = 960;

    public static final int VIEWPORT_WIDTH = (int)(360*1.20f);
    public static final int VIEWPORT_HEIGHT = (int)(240*1.20f);

    ////// DEBUG OPTIONS



}
