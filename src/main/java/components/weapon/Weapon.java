package components.weapon;


import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import components.weapon.magazine.IMagazine;
import components.weapon.magazine.Magazine;
import components.weapon.projectile.ProjectileTypes;
import components.weapon.projectile.emitter.IProjectileEmitter;
import media.DrawProperties;
import media.SoundID;
import media.TextureID;
import util.MathHelper;

public abstract class Weapon implements IWeapon {

    private Vector2 originPosition;
    private final Vector2 positionOffset;

    protected WeaponManager manager;
    private boolean triggerHeldDown;
    private final FireController fireController;
    private final IMagazine magazine;
    private final String name;
    private int totalShotsFired;
    private Vector2 aimDirection;
    private final DrawProperties drawProperties;
    private final SoundID soundID;
    private Vector2 moveDirection;
    private final float aimAnimDelay;
    private float timeSinceLastTrigger;
    private final float distanceFromOwner;
    private final Vector2 barrelPosition;
    private final Vector2 rotationOffset;
    private final Vector2 barrelOffset;
    private final TextureID displayIcon;

    /**
     * Constructs a new Weapon object with the given parameters.
     *
     * @param positionOffset   The weapon's position offset relative to its owner.
     * @param barrelOffset     The weapon's barrel position offset.
     * @param spawnAmmo        The initial amount of ammunition in the weapon.
     * @param maxAmmo          The maximum amount of ammunition the weapon can hold.
     * @param details          The details of the weapon's fire controller.
     * @param name             The name of the weapon.
     * @param projectileEmitter The projectile emitter for the weapon.
     * @param textureID        The texture ID of the weapon.
     * @param distanceFromOwner The distance between the weapon and its owner.
     * @param soundID          The sound ID of the weapon.
     * @param projectileType   The type of projectile fired by the weapon.
     */
    public Weapon(
            Vector2 positionOffset,
            Vector2 barrelOffset,
            int spawnAmmo, int maxAmmo,
            FireControllerDetails details,
            String name,
            IProjectileEmitter projectileEmitter,
            TextureID textureID,
            float distanceFromOwner, 
            SoundID soundID,
            ProjectileTypes projectileType) {

        this.positionOffset = positionOffset;
        moveDirection = new Vector2();
        aimAnimDelay = 0.3f;
        timeSinceLastTrigger = 1000;
        this.distanceFromOwner = distanceFromOwner;
        barrelPosition = new Vector2();
        rotationOffset = new Vector2();
        this.name = name;
        this.barrelOffset = barrelOffset;
        projectileEmitter.setOriginWeapon(this);
        this.displayIcon = textureID;

        triggerHeldDown = false;

        magazine = new Magazine(spawnAmmo, maxAmmo, this);
        fireController = new FireController(details, projectileEmitter, magazine, this, projectileType);
        drawProperties = new DrawProperties(textureID);
        this.soundID = soundID;
    }

    /**
     * Set the origin position of the object that owns the weapon.
     *
     * <p>The world position of the weapons origin position</p>
     *
     * <p>The projectile spawn point is affected by the origin</p>
     *
     * @param originPosition position object of the owner
     */
    @Override
    public void setOriginPosition(Vector2 originPosition) {
        this.originPosition = originPosition;
        fireController.setOriginPosition(barrelPosition);
    }

    @Override
    public void attack() {
        triggerHeldDown = true;
        fireController.activate();
        rotateWeaponTowardsAim();
        timeSinceLastTrigger = 0;
    }

    @Override
    public int getCurrentAmmo() {
        return magazine.currentAmmo();
    }

    @Override
    public int getMaxAmmo() {
        return magazine.maxAmmo();
    }

    /**
     * Set the owner of the weapon. The owner is a WeaponManager.
     *
     * @param weaponManager object that has the weapon equipped
     */
    protected void setManager(WeaponManager weaponManager) {
        manager = weaponManager;
    }

    @Override
    public boolean hasUnlimitedAmmoOverride() {
        if (manager == null) return false;
        return manager.hasUnlimitedAmmo();
    }

    @Override
    public void setAimDirection(Vector2 aimDirection) {
        this.aimDirection = aimDirection;
        fireController.setAimDirection(aimDirection);
    }

    @Override
    public void update(double deltaTime) {
        //if player is not shooting, move the gun towards where the player is moving
        //if(!triggerHeldDown && timeSinceLastTrigger > aimAnimDelay) aimDirection.set(moveDirection);
        rotateWeaponTowardsAim();
        
        //needs to be normalized in order to not change length
        aimDirection.nor();
        if(aimDirection.isZero()) aimDirection.set(1,0);

        //set the position where the gun is held
        rotationOffset.set(distanceFromOwner,0);
        //rotate the rotation offset by aimdirections angle
        MathHelper.complexProduct(aimDirection,rotationOffset,rotationOffset);
        rotationOffset.add(originPosition);
        rotationOffset.add(positionOffset);
        
        //set the position of the barrel
        barrelPosition.set(barrelOffset);
        barrelPosition.y*=drawProperties.scaleY;
        //rotate the barrel offset by aimdirections angle
        MathHelper.complexProduct(aimDirection, barrelPosition, barrelPosition);
        barrelPosition.add(rotationOffset);

        if (!triggerHeldDown) fireController.reset();

        fireController.update(deltaTime);
        magazine.update(deltaTime);

        triggerHeldDown = false;
        timeSinceLastTrigger += deltaTime;
    }

    private void rotateWeaponTowardsAim() {
        if (aimDirection == null) return;
        rotateWeapon(aimDirection.angleDeg());
    }

    private void rotateWeaponTowardsMoveDirection() {
        if (triggerHeldDown) return;
        if (timeSinceLastTrigger < aimAnimDelay) return;

        rotateWeapon(moveDirection.angleDeg());
    }

    private void rotateWeapon(float angle) {
        if (angle < 90 || angle > 270) {
            drawProperties.scaleY = 1;
        } else {
            drawProperties.scaleY = -1;
        }
        drawProperties.rotation = angle;
    }


    @Override
    public String getName() {
        if (name == null) return "Weapon";
        return name;
    }

    @Override
    public void countShots(int shotsFired) {
        totalShotsFired += shotsFired;
    }

    @Override
    public int shotsFiredCount() {
        return totalShotsFired;
    }

    @Override
    public boolean isIdle() {
        return fireController.getCurrentState() == FireController.State.IDLE;
    }

    @Override
    public Vector2 getPosition() {
        return new Vector2(originPosition.x + positionOffset.x, originPosition.y + positionOffset.y);
    }

    @Override
    public DrawProperties getProperties() {
        return drawProperties;
    }

    @Override
    public void setMoveDirection(Vector2 moveDir) {
        this.moveDirection = new Vector2(moveDir);
    }

    @Override
    public void draw(SpriteBatch batch, float deltaTime) {
        drawProperties.xOffset = distanceFromOwner * aimDirection.x;
        drawProperties.yOffset = distanceFromOwner * aimDirection.y;
        drawProperties.drawSelf(batch, deltaTime, getPosition());
    }

    /**
     * Returns the X-coordinate of the weapon's barrel position.
     *
     * @return The X-coordinate of the barrel position.
     */
    public float getBarrelPosX() {
        return barrelPosition.x;
    }

    /**
     * Returns the Y-coordinate of the weapon's barrel position.
     *
     * @return The Y-coordinate of the barrel position.
     */
    public float getBarrelPosY() {
        return barrelPosition.y;
    }

    /**
     * Returns the aim direction of the weapon as a Vector2.
     *
     * @return The aim direction of the weapon.
     */
    public Vector2 getAimDirection(){
        return aimDirection;
    }

    /**
     * @return The charge factor of the weapon.
     */
    public float getChargeFactor(){
        return fireController.getChargeFactor();
    }

    /**
     * Returns the time between shots factor for the weapon.
     *
     * @return The time between shots factor.
     */
    public float getTimeBetweenShotsFactor(){
        return fireController.getTimeBetweenShotsFactor();
    }

    /**
     * Returns the TextureID of the weapon's display icon.
     * The display icon is what's displayed on the HUD
     *
     * @return The TextureID of the display icon.
     */
    public TextureID getDisplayIcon() {
        return displayIcon;
    }
   
}
