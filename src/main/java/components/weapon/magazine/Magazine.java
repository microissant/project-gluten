package components.weapon.magazine;

import components.weapon.IWeapon;
import components.weapon.projectile.emitter.IProjectileEmitter;

public class Magazine implements IMagazine{

    private int currentAmmo;
    private final int maxAmmo;

    private final IWeapon ownerWeapon;

    public Magazine(int currentAmmo, int maxAmmo, IWeapon ownerWeapon) {
        this.maxAmmo = Math.max(maxAmmo, 0);
        this.currentAmmo = Math.min(currentAmmo, this.maxAmmo);
        this.currentAmmo = Math.max(this.currentAmmo, 0);
        this.ownerWeapon = ownerWeapon;
    }

    @Override
    public boolean sufficientAmmunition(IProjectileEmitter emitter) {
        if (emitter == null) return false;

        if (ownerWeapon != null && ownerWeapon.hasUnlimitedAmmoOverride()) return true;
        return currentAmmo >= emitter.minAmmoCost();
    }

    @Override
    public int retrieveAmmo(IProjectileEmitter emitter) {
        if (emitter == null) return 0;
        if (ownerWeapon != null && ownerWeapon.hasUnlimitedAmmoOverride()) return emitter.maxAmmoCost();

        int result;

        if (currentAmmo >= emitter.maxAmmoCost()) result = emitter.maxAmmoCost();
        else if (currentAmmo >= emitter.minAmmoCost()) result = currentAmmo;
        else return 0;

        currentAmmo -= result;
        return result;
    }

    @Override
    public void addBullets(int ammo) {
        if (ammo < 0) return;

        currentAmmo += ammo;
        currentAmmo = Math.min(currentAmmo, maxAmmo);
    }

    @Override
    public int currentAmmo() {
        return currentAmmo;
    }

    @Override
    public int maxAmmo() {
        return maxAmmo;
    }
}
