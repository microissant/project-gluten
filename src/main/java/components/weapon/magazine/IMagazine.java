package components.weapon.magazine;

import components.weapon.projectile.emitter.IProjectileEmitter;
import model.IUpdatable;

public interface IMagazine extends IUpdatable {

    /**
     * Checks if there is sufficient ammunition for the given projectile emitter.
     *
     * @param emitter the projectile emitter to check for ammunition
     * @return true if there is enough ammunition for the emitter, false otherwise
     */
    boolean sufficientAmmunition(IProjectileEmitter emitter);

    /**
     * Retrieves the required amount of ammo for the given projectile emitter.
     *
     * @param emitter the projectile emitter to retrieve ammo for
     * @return the amount of ammo consumed by the projectile emitter
     */
    int retrieveAmmo(IProjectileEmitter emitter);

    /**
     * Adds the specified number of bullets to the magazine.
     *
     * @param ammo the number of bullets to add
     */
    void addBullets(int ammo);

    /**
     * Gets the current amount of ammunition in the magazine.
     *
     * @return the current amount of ammo
     */
    int currentAmmo();

    /**
     * Gets the maximum amount of ammunition the magazine can hold.
     *
     * @return the maximum amount of ammo
     */
    int maxAmmo();

}
