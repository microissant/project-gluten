package components.weapon.projectile;

import components.collision.CollisionType;
import components.collision.trigger.damage.FalloffConstant;
import components.health.Damage;
import entities.particles.ParticleTypes;
import media.TextureID;
import model.event.EventBus;

public class RocketProjectileSetter implements IProjectileSetter{

    @Override
    public void set(PoolableProjectile projectile, EventBus bus) {
        projectile.getProperties().setAnimation(TextureID.ROCKET);
        projectile.setDeathParticle(ParticleTypes.EXPLOSION_V2);
        projectile.setRotateTowardsDir(true);
        projectile.removeDamageRadius(20, new Damage(1), 150, new FalloffConstant(), new CollisionType(CollisionType.Type.ENEMY));
        projectile.setLifeSpanTimer(5);

    }
    
}
