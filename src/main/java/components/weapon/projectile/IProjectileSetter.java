package components.weapon.projectile;

import model.event.EventBus;

/**
 * interface for turning a poolable projectile into a specific type of projectile
 */
public interface IProjectileSetter {

    /**
     * sets the field variables to values specific for this projectile type
     */
    void set(PoolableProjectile projectile, EventBus bus);
}
