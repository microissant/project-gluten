package components.weapon.projectile;

import components.physics.reaction.StaticReactions.StaticReact;
import media.TextureID;

import java.util.concurrent.ThreadLocalRandom;

public enum ProjectileTypes {
    Rocket(new RocketProjectileSetter()),
    Donut((projectile, bus) -> {
        projectile.getProperties().setAnimation(TextureID.DONUT);
        projectile.getProperties().rotation = ThreadLocalRandom.current().nextFloat(360);
        projectile.setReaction(StaticReact.Bounce.reaction);
        projectile.setRemainingGridCollisions(5);
        projectile.setRotationSpeed(ThreadLocalRandom.current().nextFloat(90)+270);
    }),
    TOPPING((projectile, bus) -> {
        projectile.getProperties().setAnimation(TextureID.TOPPINGS);
        projectile.getProperties().setRandomFrame();
        projectile.getProperties().setAnimationSpeed(0);
        projectile.getProperties().rotation = ThreadLocalRandom.current().nextFloat(360);
        projectile.setRotationSpeed(ThreadLocalRandom.current().nextFloat(90)+270);
    }),
    BasicBullet(new DefaultProjectileSetter()),

    MELEE(new MeleeProjectileSetter());
    public final IProjectileSetter setter;
    ProjectileTypes(IProjectileSetter setter){
        this.setter = setter;
    }
}
