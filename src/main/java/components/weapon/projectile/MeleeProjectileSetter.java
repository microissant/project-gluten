package components.weapon.projectile;

import components.collision.CollisionType;
import components.collision.trigger.damage.FalloffConstant;
import components.health.Damage;
import model.event.EventBus;

public class MeleeProjectileSetter implements IProjectileSetter{

    @Override
    public void set(PoolableProjectile projectile, EventBus bus) {
        projectile.removeDamageRadius(10, new Damage(1), 0, new FalloffConstant(), new CollisionType(CollisionType.Type.PLAYER));
        projectile.setLifeSpanTimer(0.01f);
    }
}
