package components.weapon.projectile;

import components.collision.ICollidable;
import components.health.IDamageable;
import entities.Entity;
import entities.LivingPoolable;
import grid.WorldGrid;

/**
 * Interface representing a projectile in the game.
 * A projectile is an entity that can collide with other game objects, be pooled, and deal damage.
 */
public interface IProjectile extends Entity, ICollidable, LivingPoolable, IDamageable {

    /**
     * Sets the world grid for the projectile.
     *
     * @param grid The WorldGrid in which the projectile exists.
     */
    void setWorldGrid(WorldGrid grid);
}
