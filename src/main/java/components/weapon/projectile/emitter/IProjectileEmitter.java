package components.weapon.projectile.emitter;

import com.badlogic.gdx.math.Vector2;
import components.weapon.Weapon;
import components.weapon.magazine.IMagazine;
import components.weapon.projectile.ProjectileTypes;
import model.IUpdatable;

public interface IProjectileEmitter extends IUpdatable {

    /**
     * Gets the minimum ammo cost for emitting projectiles.
     *
     * @return the minimum ammo cost
     */
    int minAmmoCost();

    /**
     * Gets the maximum ammo cost for emitting projectiles.
     *
     * @return the maximum ammo cost
     */
    int maxAmmoCost();

    /**
     * Emits the specified number of projectiles with the specified parameters.
     *
     * @param magazine the magazine providing the ammunition
     * @param direction the direction of the emitted projectiles
     * @param originPosition the origin position of the emitted projectiles
     * @param type the type of the projectiles to emit
     * @param accuracy the accuracy of the emitted projectiles
     * @return the number of projectiles emitted
     */
    int emitProjectiles(IMagazine magazine, Vector2 direction, Vector2 originPosition, ProjectileTypes type,float accuracy);

    /**
     * Sets the weapon from which the projectiles are emitted.
     *
     * @param originWeapon the weapon from which the projectiles are emitted
     */
    void setOriginWeapon(Weapon originWeapon);

}
