package components.weapon.projectile.emitter;

import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType;
import components.weapon.Weapon;
import components.weapon.magazine.IMagazine;
import components.weapon.projectile.ProjectileTypes;
import media.SoundID;
import model.event.EventBus;
import model.event.events.FireWeaponEvent;
import model.event.events.audio.PlaySoundEvent;
import util.MathHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class StandardEmitter implements IProjectileEmitter {

    private final CollisionType.Type collisionType;
    private final int count;
    private final float angleSpread;
    private Weapon origin;
    private final Vector2 accuracyOffset;
    private final Vector2 offsetRotator;

    private final EventBus eventBus;

    public StandardEmitter(CollisionType.Type collisionType, EventBus eventBus) {
        this(collisionType, 1, 0, eventBus);
    }

    public StandardEmitter(CollisionType.Type collisionType, int count, float angleSpread, EventBus eventBus) {
        this.collisionType = collisionType;
        this.count = count;
        this.angleSpread = angleSpread;
        this.eventBus = eventBus;

        accuracyOffset = new Vector2();
        offsetRotator = new Vector2(1,-1);
    }


    @Override
    public int minAmmoCost() {
        return 1;
    }

    @Override
    public int maxAmmoCost() {
        return count;
    }

    @Override
    public int emitProjectiles(IMagazine magazine, Vector2 direction, Vector2 originPosition, ProjectileTypes projType, float accuracy) {
        if (eventBus == null) return 0;

        List<ProjectileStruct> projectiles = new ArrayList<>();

        int ammo = magazine.retrieveAmmo(this);
        if (ammo == 0) return 0;

        float angleStep = angleSpread /(float) count;
        float accuracyConst = (1/accuracy);
        for (int i = 0; i < ammo; i++) {
            Vector2 projectileDirection = new Vector2(direction).rotateDeg(angleStep * i - angleSpread*0.5f);
            
            if(accuracyConst!=1){
                float xAccuracy = ThreadLocalRandom.current().nextFloat(accuracyConst,1);
                float yAccuracy = ThreadLocalRandom.current().nextFloat(accuracyConst,1);
                accuracyOffset.set(xAccuracy,yAccuracy);
                MathHelper.complexProduct(accuracyOffset, offsetRotator, accuracyOffset);
                accuracyOffset.nor();
                if(accuracyOffset.isZero())accuracyOffset.set(1,0);
                MathHelper.complexProduct(projectileDirection, accuracyOffset, projectileDirection);
            }
            
            //projectiles.add(new BasicProjectile(new Vector2(originPosition), projectileDirection, 150, collisionType, eventBus));
            projectiles.add(new ProjectileStruct(new Vector2(originPosition),projectileDirection,150,collisionType,projType));
        }

        eventBus.post(new FireWeaponEvent(null, origin, projectiles));
        eventBus.post(new PlaySoundEvent(SoundID.GUN_SHOT));

        return projectiles.size();
    }

    @Override
    public void setOriginWeapon(Weapon originWeapon) {        
        this.origin = originWeapon;        
    }

    
}
