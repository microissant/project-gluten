package components.weapon.projectile.emitter;

import com.badlogic.gdx.math.Vector2;

import components.collision.CollisionType;
import components.weapon.projectile.ProjectileTypes;

public record ProjectileStruct(Vector2 position, Vector2 direction, float speed,CollisionType.Type collisionType, ProjectileTypes projectileType) {
    
}
