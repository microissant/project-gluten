package components.weapon.projectile;

import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType;
import components.collision.ICollidable;
import components.collision.event.ICollisionEventDefinition;
import components.collision.event.definitions.ProjectileCollisionEventDefinition;
import components.collision.primitives.CollisionCircle;
import components.collision.trigger.ITrigger;
import components.collision.trigger.damage.DamageRadius;
import components.collision.trigger.damage.IFalloffType;
import components.health.Damage;
import components.health.Healer;
import components.health.HealthManager;
import components.health.IDamageable;
import components.physics.Physics;
import components.physics.reaction.CollisionReaction;
import entities.particles.ParticleTypes;
import grid.TileData.TileRuleset;
import grid.WorldGrid;
import media.DrawProperties;
import model.event.EventBus;
import model.event.events.AddParticleEvent;
import model.event.events.collision.RemoveCollisionCircleEvent;
import model.event.events.collision.SpawnTriggerEvent;
import util.CountdownTimer;
import util.DoOnce;

import java.util.HashSet;
import java.util.Set;

/**
 * Class representing a poolable projectile in the game.
 * A projectile can collide with other game objects, be pooled, and deal damage.
 */
public class PoolableProjectile implements IProjectile {

    private static final DefaultProjectileSetter defaultSetter = new DefaultProjectileSetter();
    private HealthManager healthManager;
    private final Physics physics;
    private final EventBus eventBus;

    private ParticleTypes deathParticle;

    private final DrawProperties drawProperties;

    private ICollisionEventDefinition collisionDefinition;

    private CollisionCircle collisionCircle;

    private int remainingGridCollisions;
    private boolean rotateTowardsDir = false;

    private ITrigger damageRadius;
    private float rotationSpeed = 0;

    private final DoOnce deathTrigger;
    private final CountdownTimer lifeSpanTimer;


    public PoolableProjectile(EventBus bus){
        physics = new Physics();
        physics.setProjectileOwner(this);
        this.eventBus = bus;
        healthManager = new HealthManager();
        remainingGridCollisions = 1;
        drawProperties = new DrawProperties();

        deathTrigger = new DoOnce(this::death);
        float maxLifeSpan = 5;
        lifeSpanTimer = new CountdownTimer(maxLifeSpan, deathTrigger::execute);
        lifeSpanTimer.start();
    }
    
    @Override
    public void update(double dt) {
        physics.update(dt);

        getProperties().rotation += rotationSpeed * dt;
        lifeSpanTimer.update(dt);

        if (isDead()) deathTrigger.execute();
    }

    private void death() {
        eventBus.post(new AddParticleEvent(deathParticle,getPosition()));
        eventBus.post(new RemoveCollisionCircleEvent(getCollisionPrimitive()));

        if (damageRadius == null) return;
        eventBus.post(new SpawnTriggerEvent(damageRadius));
        lifeSpanTimer.stop();
    }

    @Override
    public boolean isDead() {
        return healthManager.isDead() || deathTrigger.hasExecuted();
    }

    @Override
    public Vector2 getPosition() {
        return physics.getPosition();
    }

    @Override
    public void setPosition(Vector2 pos) {
        physics.setPosition(pos);
    }

    @Override
    public void collisionEnter(ICollidable target, CollisionType.Type type) {
        if (!isAlive()) return;
        collisionDefinition.triggerCollisionEvent(this, target, type);
    }

    @Override
    public CollisionCircle getCollisionPrimitive() {
        return collisionCircle;
    }

    @Override
    public HealthManager getHealthComponent() {
        return healthManager;
    }

    @Override
    public DrawProperties getProperties() {
       return drawProperties;
    }

    @Override
    public void setWorldGrid(WorldGrid grid) {
        physics.setWorldGrid(grid);
    }

    @Override
    public void unregisterFromEventBus() {
        eventBus.unregister(this);
    }
    
    /** enables/disables rotating the sprite towards the moving direction in init event
     * @param value enables / disables rotating the sprite towards the moving direction
     */
    public void setRotateTowardsDir(boolean value){
        rotateTowardsDir = value;   
    }

    /**rotates the sprite in the moving direction
     * @param dir the moving direction
     */
    public void rotateTowardsDir(Vector2 dir){
        getProperties().rotation = dir.angleDeg();
    }

    /** gives this projectile a box collider for colliding with the grid
     * @param topToPos the distance from this projectiles origin to the top of the box. must be positive
     * @param bottomToPos the distance from this projectiles origin to the bottom of the box. must be positive
     * @param leftToPos the distance from this projectiles origin to the left edge of the box. must be positive
     * @param rightToPos the distance from this projectiles origin to the right edge of the box. must be positive
     */
    public void setBoxCollision(float topToPos, float bottomToPos, float leftToPos, float rightToPos){
        physics.setBoxCollision(topToPos, bottomToPos, leftToPos, rightToPos);
    }
    /** decides how this projectile will react to colliding with the grid
     * 
     * @param reaction the reaction that determines how this projectile will collide with the grid
     */
    public void setReaction(CollisionReaction reaction){
        physics.setReaction(reaction);
    }

    /** determines what kind of tiles on the world grid this projectile will collide with
     * 
     * @param ruleset the set of tile properties to collide with
     */
    public void setRuleset(TileRuleset ruleset){
        physics.setRuleset(ruleset);
    }

    /** the method that is called when this projectile hits a wall
     *  currently reduces the amount of times this projectile can collide with the grid
     */
    public void hitWall(){
        remainingGridCollisions --;
        if(remainingGridCollisions <= 0) deathTrigger.execute();
    }

    /** sets the amount of times this projectile can collide with the grid
     *  when this number hits 0, the projectile is destroyed.
     * @param collisionsLeft the amount of collisions remaining
     */
    public void setRemainingGridCollisions(int collisionsLeft){
        remainingGridCollisions = collisionsLeft;
    }

    /** sets the particle that will be created when this projectile is destroyed
     * 
     * @param type the type of particle that will be created
     */
    public void setDeathParticle(ParticleTypes type){
        deathParticle = type;
    }

    /** sets the rotationspeed of this projectile
     * 
     * @param rotationSpeed the speed it should rotate at (degrees per second)
     */
    public void setRotationSpeed(float rotationSpeed){
        this.rotationSpeed = rotationSpeed;
    }


    /** initializes this projectile
     * 
     * @param position the position that the projectile will have when spawned
     * @param dir the direction that the projectile will move in when spawned
     * @param speed the speed that this projectile will move at
     * @param entityCollisionType the collision primitives this projectile should collide with
     * @param type the type of projectile that this should be
     */
    public void init(Vector2 position, Vector2 dir, float speed, CollisionType.Type entityCollisionType, ProjectileTypes type){
        defaultSetter.set(this,eventBus);
        physics.setPosition(position);
        physics.setLinearMovement(dir, speed);
        healthManager = new HealthManager();
        collisionCircle = new CollisionCircle(
                physics.getPosition(),
                new Vector2(),
                5,
                this,
                new HashSet<>(Set.of(
                        new CollisionType(entityCollisionType),
                        new CollisionType(CollisionType.Type.PROP)
                )),
                new HashSet<>()
        );

        collisionDefinition = new ProjectileCollisionEventDefinition(entityCollisionType, new Damage(), new Damage());
        type.setter.set(this, eventBus);


        if(rotateTowardsDir) rotateTowardsDir(dir);

        deathTrigger.reset();
        lifeSpanTimer.restart();
    }

    @Override
    public void reset() {
        
        
    }

    @Override
    public boolean isAlive() {
        return !isDead();
    }

    public void removeDamageRadius(float radius, Damage damage, float pushForce, IFalloffType falloffType, CollisionType collisionType) {
        damageRadius = new DamageRadius(physics.getPosition(), radius, damage, pushForce, falloffType, collisionType, eventBus);
    }

    public void removeDamageRadius() {
        damageRadius = null;
    }

    @Override
    public int getCurrentHealth() {
        return healthManager.getCurrentHealth();
    }

    @Override
    public int getMaximumHealth() {
        return healthManager.getMaximumHealth();
    }

    @Override
    public void applyHeal(Healer healer) {
        if (healer == null) return;
        healthManager.heal(healer);
    }

    @Override
    public void applyDamage(Damage damage) {
        if (damage == null) return;

        healthManager.applyDamage(damage);
    }

    @Override
    public IDamageable getDamageable() {
        return this;
    }

    /**
     * Life span timer determines the amount of time a projectile will live. When
     * Timer is finished, the projectile dies and is removed from the game
     *
     * @param time amount of seconds to live
     */
    public void setLifeSpanTimer(float time) {
        lifeSpanTimer.setTime(time);
    }
}
