package components.weapon.projectile;

import components.physics.reaction.StaticReactions.StaticReact;
import entities.particles.ParticleTypes;
import grid.TileData.TileRuleset;
import media.TextureID;
import model.event.EventBus;

/**class for setting the default values of a projectile */
public class DefaultProjectileSetter implements IProjectileSetter {

    @Override
    public void set(PoolableProjectile projectile, EventBus bus) {
        projectile.setBoxCollision(0, 0, 3, 3);
        projectile.setRuleset(TileRuleset.BULLET);
        projectile.setReaction(StaticReact.Stop.reaction);
        projectile.getProperties().setAnimation(TextureID.BULLET);
        projectile.setRemainingGridCollisions(1);
        projectile.setDeathParticle(ParticleTypes.BULLETDEATH);
        projectile.setRotateTowardsDir(false);
        projectile.removeDamageRadius();
        projectile.getProperties().rotation = 0;
        projectile.setRotationSpeed(0);
        projectile.setLifeSpanTimer(5);
    }
    
}
