package components.weapon;

import com.badlogic.gdx.math.Vector2;
import components.weapon.magazine.IMagazine;
import components.weapon.projectile.ProjectileTypes;
import components.weapon.projectile.emitter.IProjectileEmitter;
import model.IUpdatable;

import java.util.HashMap;
import java.util.Map;

/**
 * Class responsible for controlling the firing mechanism of a weapon.
 * This class handles the firing states, timing, and projectile emission of a weapon.
 */
public class FireController implements IUpdatable {

    private final HashMap<State, Double> stateTime;
    private State currentState;
    private double stateElapsedTime;
    private double timeSinceLastShot;
    private final IMagazine magazine;
    private final IProjectileEmitter projectileEmitter;
    private Vector2 aimDirection, originPosition;
    private boolean triggerHeldDown = false;
    private final int burstNumber;
    private int burstShotsRemaining = 0;
    private final boolean automaticRelease;
    private final boolean cancelFireIfNotFull;
    private final float accuracy;
    private final boolean oneShotPerTap;
    private final IWeapon ownerWeapon;

    private final ProjectileTypes projectileType;
    private boolean canFireAgain;


    public FireController(FireControllerDetails details, IProjectileEmitter projectileEmitter, IMagazine magazine, IWeapon ownerWeapon, ProjectileTypes projectileType) {
        this.currentState = FireController.State.IDLE;
        this.magazine = magazine;
        this.projectileEmitter = projectileEmitter;
        this.aimDirection = new Vector2(1, 0);
        this.originPosition = new Vector2();
        this.ownerWeapon = ownerWeapon;
        this.projectileType = projectileType;
        this.burstNumber = details.burstNumber();
        accuracy = details.accuracy();
        this.automaticRelease = details.automaticRelease();
        this.cancelFireIfNotFull = details.cancelFireIfNotFull();
        this.oneShotPerTap = details.oneShotPerTap();
        stateTime = new HashMap<>(Map.of(
                State.CHARGE, details.chargeTime(),
                State.BURST, details.burstFirerate(),
                //State.BETWEENFIRE, details.timeBetweenShots(),
                State.DISCHARGE, details.timeBetweenShots()
        ));

        stateElapsedTime = 0.0;
        canFireAgain = true;
    }

    /**
     * Activates the firing mechanism.
     */
    public void activate() {
        triggerHeldDown = true;        
        if(canFireAgain && currentState == State.IDLE) currentState = nextState();
        if(oneShotPerTap) canFireAgain =false;
        //timeSinceLastShot = timeBetweenShots;
    }

    /**
     * Returns the current firing state of the controller.
     *
     * @return The current state of the controller.
     */
    public State getCurrentState() {
        return currentState;
    }

    private void releaseFire(){
        canFireAgain = true;
        if (currentState == State.CHARGE){
            if (!automaticRelease){
                if (stateElapsedTime>stateTime.get(State.CHARGE)){
                    currentState = nextState();
                } else {
                    if (cancelFireIfNotFull) currentState = State.IDLE;
                }
            } else currentState = State.IDLE;
            
        } else if(currentState == State.BETWEENFIRE && automaticRelease){
            currentState = nextState();
        }

        
    }

    /**
     * Resets the firing controller to its initial state.
     */
    public void reset() {
        if(triggerHeldDown){
            triggerHeldDown = false;
            releaseFire();
        }
    }

    @Override
    public void update(double deltaTime) {
        stateElapsedTime += deltaTime;
        if (currentState == State.IDLE) return;
        if (currentState == State.BURST) {
            fire(deltaTime);
            return;
        }

        if (canSwitchStates(currentState, stateElapsedTime)) {
            currentState = nextState();
        }
    }

    /**
     * Sets the aim direction for the firing mechanism.
     *
     * @param aimDirection The direction to aim the weapon.
     */
    public void setAimDirection(Vector2 aimDirection) {
        this.aimDirection = aimDirection;
    }

    /**
     * Sets the position where the projectile originates.
     *
     * @param originPosition The position of the projectile's origin.
     */
    public void setOriginPosition(Vector2 originPosition) {
        this.originPosition = originPosition;
    }

    private void fire(double deltaTime) {
        timeSinceLastShot += deltaTime;
        double burstTime = stateTime.get(State.BURST);
        if (timeSinceLastShot < burstTime) return;

        while(timeSinceLastShot>burstTime && burstShotsRemaining > 0){

            int shotsFired = projectileEmitter.emitProjectiles(magazine, aimDirection, originPosition, projectileType,accuracy);
            if (ownerWeapon != null) {
                ownerWeapon.countShots(shotsFired);
            }

            if (!magazine.sufficientAmmunition(projectileEmitter)){
                currentState = nextState();
                break;
            }
                

            timeSinceLastShot -=burstTime;
            burstShotsRemaining--;
        }

        if (burstShotsRemaining <= 0) currentState =  nextState();
    }

    private State setBurstState(){
        currentState = State.BURST;
        burstShotsRemaining = burstNumber;
        return State.BURST;
    }

    /** if currently charging, returns a number between 0 and 1 that represents how close the weapon is to being fully charged. 1 is fully charged and 0 is not at all.
     * if the weapon does not automatically release fire, then the number can go above 1.
     */
    public float getChargeFactor(){
        if(currentState != State.CHARGE) return 0;
        else return (float)( stateElapsedTime / stateTime.get(State.CHARGE));
    }



    private State nextState() {
        stateElapsedTime = 0;
        if (currentState == State.IDLE) {
            if (stateTime.get(State.CHARGE) > 0) return State.CHARGE;
            return setBurstState();
        }
        if (currentState == State.CHARGE)return setBurstState();
        if(currentState == State.BURST)return State.DISCHARGE;
        if (currentState == State.DISCHARGE){
            if(automaticRelease && triggerHeldDown && !oneShotPerTap)return setBurstState();
            
            return State.IDLE;
        } 
        
        return State.IDLE;
    }

    private boolean canSwitchStates(State state, double elapsedTime) {
        if (!stateTime.containsKey(state)) return false;
        if (!automaticRelease && state == State.CHARGE) return false;
        return elapsedTime > stateTime.get(state);
    }

    public enum State {

        IDLE,
        CHARGE,
        BURST,
        BETWEENFIRE,
        DISCHARGE
    }

    /**
     * Returns the time factor between shots when the weapon is discharging.
     *
     * @return The time factor between shots.
     */
    public float getTimeBetweenShotsFactor() {
        if(currentState != State.DISCHARGE) return 0;
        else return (float)( stateElapsedTime / stateTime.get(State.DISCHARGE));
    }
}
