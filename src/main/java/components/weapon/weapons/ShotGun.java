package components.weapon.weapons;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType;
import components.weapon.FireControllerDetails;
import components.weapon.Weapon;
import components.weapon.projectile.ProjectileTypes;
import components.weapon.projectile.emitter.StandardEmitter;
import media.DrawProperties;
import media.SoundID;
import media.TextureID;
import model.event.EventBus;

public class ShotGun extends Weapon {

    public ShotGun( CollisionType.Type collisionType, EventBus eventBus, SoundID sound) {
        super(
                new Vector2(0,5),
                new Vector2(10, 3),
                1000, 1000,
                new FireControllerDetails(0f, 0.3f,true,false,true,0,1,2),
                "Gun",
                new StandardEmitter(collisionType, 4,15f,eventBus),
                TextureID.SHOT_GUN,
                8f,
                SoundID.GUN_SHOT,
                ProjectileTypes.BasicBullet
                );
                
    }

    private float recoilFactor(float t){
        float t1 = 2*t - 0.7937f; // qube root of 0.5
        float t2 = Math.min(t1*t1*t1 +0.5f,1f);
        return -t2 * (t2-1);
    }
    @Override
    public void draw(SpriteBatch batch, float deltaTime) {
        
        DrawProperties drawProperties = getProperties();
        Vector2 aimDirection = getAimDirection();
        drawProperties.setAnimationSpeed(0);
        float rechargeTime = recoilFactor(getTimeBetweenShotsFactor());

        drawProperties.rotation = aimDirection.angleDeg();
        drawProperties.rotation += rechargeTime * drawProperties.scaleY * 40f * 4f;
        
        super.draw(batch, deltaTime);
    }

    
}