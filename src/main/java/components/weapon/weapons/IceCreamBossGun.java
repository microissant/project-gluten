package components.weapon.weapons;

import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType;
import components.weapon.FireControllerDetails;
import components.weapon.Weapon;
import components.weapon.projectile.ProjectileTypes;
import components.weapon.projectile.emitter.StandardEmitter;
import media.SoundID;
import model.event.EventBus;

public class IceCreamBossGun extends Weapon {
    public IceCreamBossGun( CollisionType.Type collisionType, EventBus eventBus, SoundID sound) {
        super(
            new Vector2(0,0),
            new Vector2(0, 0),
            1000, 1000,
            new FireControllerDetails(0.0, 1.,true,true,true,0,1,1),
            "Gun",
            new StandardEmitter(collisionType,10,360f, eventBus),
            null,
            8f,
            null,
            ProjectileTypes.BasicBullet
                );
                
    }
}
