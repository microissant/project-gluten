package components.weapon.weapons;

import com.badlogic.gdx.math.Vector2;

import components.collision.CollisionType;
import components.weapon.FireControllerDetails;
import components.weapon.Weapon;
import components.weapon.projectile.ProjectileTypes;
import components.weapon.projectile.emitter.StandardEmitter;
import media.SoundID;
import media.TextureID;
import model.event.EventBus;

public class TopperGun extends Weapon{
    public TopperGun(CollisionType.Type collisionType, EventBus eventBus, SoundID sound){
        super(
                new Vector2(0,5),
                new Vector2(20, 0),
                1000, 1000,
                new FireControllerDetails(0f, 0.3f,true,true,false,0,1,1.5f),
                "Gun",
                new StandardEmitter(collisionType, eventBus),
                TextureID.TOPPERGUN,
                8f,
                SoundID.GUN_SHOT,
                ProjectileTypes.TOPPING
                );
    }
}
