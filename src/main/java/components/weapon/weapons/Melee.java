package components.weapon.weapons;

import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType;
import components.weapon.FireControllerDetails;
import components.weapon.Weapon;
import components.weapon.projectile.ProjectileTypes;
import components.weapon.projectile.emitter.StandardEmitter;
import media.SoundID;
import model.event.EventBus;

public class Melee extends Weapon {
    public Melee(CollisionType.Type collisionType, EventBus eventBus, SoundID sound) {
        super(
                new Vector2(),
                new Vector2(),
                10000, 10000,
                new FireControllerDetails(0, 1),
                "Melee",
                new StandardEmitter(collisionType, eventBus),
                null,
                0,
                SoundID.GUN_SHOT,
                ProjectileTypes.MELEE);
    }
}
