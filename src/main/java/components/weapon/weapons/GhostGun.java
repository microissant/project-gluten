package components.weapon.weapons;

import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType;
import components.weapon.FireControllerDetails;
import components.weapon.Weapon;
import components.weapon.projectile.ProjectileTypes;
import components.weapon.projectile.emitter.StandardEmitter;
import media.SoundID;
import media.TextureID;
import model.event.EventBus;

/**a gun that should only be used by ghost peppers and not players */
public class GhostGun extends Weapon {

    public GhostGun(CollisionType.Type collisionType, EventBus eventBus, SoundID sound) {
        super(
                new Vector2(0,5),
                new Vector2(10, 3),
                1000, 1000,
                new FireControllerDetails(0.15, 2.5,true,true,false,0.2,3,1),
                "Gun",
                new StandardEmitter(collisionType, eventBus),
                TextureID.SHOT_GUN,
                8f,
                SoundID.GUN_SHOT,
                ProjectileTypes.BasicBullet
                );
                
    }
}