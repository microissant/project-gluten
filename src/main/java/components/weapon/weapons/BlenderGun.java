package components.weapon.weapons;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType;
import components.weapon.FireControllerDetails;
import components.weapon.Weapon;
import components.weapon.projectile.ProjectileTypes;
import components.weapon.projectile.emitter.StandardEmitter;
import media.DrawProperties;
import media.SoundID;
import media.TextureID;
import model.event.EventBus;
import util.MathHelper;

public class BlenderGun extends Weapon {

    public BlenderGun(CollisionType.Type collisionType, EventBus eventBus) {
        super(
                new Vector2(0,5),
                new Vector2(14, 0),
                1000, 1000,
                new FireControllerDetails(1f,0f,false,true,false,0.1f,3,1),
                "Gun",
                new StandardEmitter(collisionType, eventBus),
                TextureID.BLENDER,
                6f,
                SoundID.GUN_SHOT,
                ProjectileTypes.Donut
                );
                
    }
    @Override
    public void draw(SpriteBatch batch, float deltaTime){
        DrawProperties drawProperties = getProperties();
        Vector2 aimDirection = getAimDirection();
        drawProperties.setAnimationSpeed(0);
        float chargeTime = getChargeFactor();
        float cappedChargeTime = Math.min(chargeTime,1);
        drawProperties.setAnimationProgress(cappedChargeTime);
        drawProperties.rotation = aimDirection.angleDeg();
        drawProperties.rotation += Math.sin(chargeTime * 30f) * MathHelper.smoothStep(cappedChargeTime) * 6f;
        super.draw(batch,deltaTime);
    }
}