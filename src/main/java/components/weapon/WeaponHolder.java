package components.weapon;

/**
 * Interface representing an object that can hold and manage weapons.
 */
public interface WeaponHolder {

    /**
     * Attack with the weapon held by the weapon holder.
     */
    void attack();

    /**
     * Update the aim direction of the weapon held by the weapon holder.
     */
    void updateTargetDirection();

    /**
     * Add a weapon to the weapon holder.
     *
     * @param weapon The weapon to be added to the weapon holder.
     */
    void addWeapon(Weapon weapon);
}
