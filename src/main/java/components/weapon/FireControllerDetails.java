package components.weapon;


public record FireControllerDetails(double chargeTime, double timeBetweenShots,boolean automaticRelease, boolean cancelFireIfNotFull,boolean oneShotPerTap, double burstFirerate, int burstNumber,float accuracy) {

    public FireControllerDetails(float chargeTime,float timeBetweenShots){
        this(
            chargeTime,
            timeBetweenShots,
            true,
            true,
            false,
            0,      //time between each shot during burst
            1,      //number of shots fired during burst
            1      //perfect accuracy
        );
    }
}
