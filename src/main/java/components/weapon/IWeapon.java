package components.weapon;

import com.badlogic.gdx.math.Vector2;
import media.Renderable;
import model.IUpdatable;

public interface IWeapon extends IUpdatable, Renderable {

    /**
     * Initiates an attack using the weapon.
     */
    void attack();

    /**
     * Sets the aim direction for the weapon.
     *
     * @param aimDirection The direction to aim the weapon.
     */
    void setAimDirection(Vector2 aimDirection);

    /**
     * Returns the current amount of ammunition available in the weapon.
     *
     * @return The current ammunition count.
     */
    int getCurrentAmmo();

    /**
     * Returns the maximum amount of ammunition the weapon can hold.
     *
     * @return The maximum ammunition capacity.
     */
    int getMaxAmmo();

    /**
     * Set the origin position of the object that owns the weapon.
     *
     * <p>The world position of the weapon is the origin + the offset</p>
     *
     * <p>The projectile spawn point is affected by the origin</p>
     *
     * @param originPosition position object of the owner
     */
    void setOriginPosition(Vector2 originPosition);

    /**
     * @return The weapon's name.
     */
    String getName();

    /**
     * Adds the number of shots fired to the weapon's total shot count.
     *
     * @param shotsFired The number of shots fired.
     */
    void countShots(int shotsFired);

    /**
     * Returns the total number of shots fired by the weapon.
     *
     * @return The total number of shots fired.
     */
    int shotsFiredCount();

    /**
     * Returns whether the weapon is in an idle state.
     *
     * @return True if the weapon is idle, false otherwise.
     */
    boolean isIdle();

    /**
     * Sets the move direction of the weapon.
     *
     * @param moveDir The direction in which the weapon is moving.
     */
    void setMoveDirection(Vector2 moveDir);

    /**
     * Returns whether the weapon has an unlimited ammunition override.
     *
     * @return True if the weapon has unlimited ammunition, false otherwise.
     */
    boolean hasUnlimitedAmmoOverride();
}
