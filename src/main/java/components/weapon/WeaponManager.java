package components.weapon;

import com.badlogic.gdx.math.Vector2;
import components.upgrade.upgrades.weapons.IUpgradableWeaponManager;
import model.IUpdatable;
import model.event.EventBus;

/**
 * Class responsible for managing weapons in primary and secondary slots.
 */
public class WeaponManager implements IUpdatable, IUpgradableWeaponManager {

    private Weapon primary;
    private Weapon secondary;
    private final Vector2 originPosition;

    private final Vector2 aimDirection;
    private boolean hasUnlimitedAmmo;

    /**
     * Constructor for creating a new WeaponManager.
     *
     * @param originPosition The origin position of the weapon manager.
     * @param aimDirection   The initial aim direction of the weapon manager.
     * @param eventBus       The event bus to communicate with other game components.
     */
    public WeaponManager(Vector2 originPosition, Vector2 aimDirection, EventBus eventBus) {
        this.originPosition = originPosition;
        this.aimDirection = aimDirection;
        hasUnlimitedAmmo = false;
    }

    /**
     * @return weapon currently in the primary weapon slot
     */
    public Weapon getPrimary() {
        return primary;
    }

    /**
     * @return weapon currently in the secondary weapon slot
     */
    public Weapon getSecondary() {
        return secondary;
    }

    /**
     * <p>Adding a weapon to a free weapon slot.</p>
     *
     * <p>If both weapon slots occupied, replace with primary</p>
     *
     * @param weapon to add to either weapon slots
     */
    public void addWeapon(Weapon weapon) {
        if (weapon == null) return;

        weapon.setOriginPosition(originPosition);
        weapon.setManager(this);
        weapon.setAimDirection(aimDirection);

        if (secondary == null) {
            secondary = primary;
        }

        primary = weapon;
    }

    /**
     * <p>Swap primary and secondary weapons.</p>
     *
     * <p>If no weapons are equipped or secondary slot is empty,
     * it has no effect on the weapon slots</p>
     */
    public void swapWeapons() {
        if (secondary == null) return;

        Weapon temp = primary;
        primary = secondary;
        secondary = temp;
    }

    /**
     * Fire the weapon in the primary weapon slot if possible
     */
    public void fireWeapon() {
        if (primary == null) return;

        primary.attack();
    }

    @Override
    public void update(double deltaTime) {
        if (primary == null) return;
        primary.update(deltaTime);
    }


    /**
     * Returns the number of shots fired by the primary weapon.
     *
     * @return The total number of shots fired by the primary weapon.
     */
    public int shotsFiredPrimaryWeapon() {
        if (primary == null) return 0;
        return primary.shotsFiredCount();
    }

    /**
     * Checks if the primary weapon is idle.
     *
     * @return true if the primary weapon is idle, false otherwise.
     */
    public boolean isWeaponIdle() {
        if (primary == null) return true;
        return primary.isIdle();
    }

    /**
     * Checks if the weapon manager has unlimited ammo enabled.
     *
     * @return true if unlimited ammo is enabled, false otherwise.
     */
    public boolean hasUnlimitedAmmo() {
        return hasUnlimitedAmmo;
    }

    @Override
    public void enableUnlimitedAmmo() {
        hasUnlimitedAmmo = true;
    }

    @Override
    public void disableUnlimitedAmmo() {
        hasUnlimitedAmmo = false;
    }
}
