package components.physics.reaction;

public class StaticReactions {
    
    public enum StaticReact {
        Stop(new StopReaction()),
        Bounce(new BounceReaction());

        public final CollisionReaction reaction;
        StaticReact(CollisionReaction reaction){
            this.reaction=reaction;
        }        
    } 
}
