package components.physics.reaction;

import com.badlogic.gdx.math.Vector2;
import components.physics.Physics;
import grid.TileData;
import grid.WorldGrid;
import grid.collision.Collider;
import grid.collision.GridCollider;

/**implementation of collisionreaction meant to only be used by the player object */
public class PlayerReaction implements CollisionReaction{
    private final Vector2 result = new Vector2();
    private final Vector2 candidate = new Vector2();
    @Override
    public boolean implementation(Physics physics, Vector2 velocityDelta, WorldGrid grid, int ruleset) {
        
        Collider pointCollider = GridCollider.StaticColliders.PointCollider.object;
        Collider boxCollider = physics.getCollider();
        
        //use a point collider to collide with hole tiles
        boolean hitHole = pointCollider.collide(physics, velocityDelta, grid, ruleset,candidate);
        candidate.sub(physics.getPosition());

        // use a box collider to collide with walls
        boolean hitWall = boxCollider.collide(physics, candidate, grid, TileData.TileRuleset.SLIDING.val,result);
        hitWall = hitWall || hitHole;
        physics.getPosition().set(result);
        return hitWall;
    }
    
}
