package components.physics.reaction;

import com.badlogic.gdx.math.Vector2;
import components.physics.Physics;
import grid.WorldGrid;

public interface CollisionReaction {
    
    /**uses the physics object's gridcollider to collide with the grid and then react appropriately based on the implementation of the reaction.
     * 
     * @param physics the physics object that will collide with the grid
     * @param velocityDelta the difference between the positions current position and the position after updating
     * @param grid the grid to collide with
     * @param ruleset the ruleset determining which tile properties to collide with
     * @return true if the physics object collided with the grid
     */
    default boolean react(Physics physics, Vector2 velocityDelta, WorldGrid grid, int ruleset){
        if(grid==null) {
            return noReaction(physics, velocityDelta);
        } else {
            return implementation(physics, velocityDelta, grid, ruleset);
        }
    }

    /**the specific implementation of the reaction */
    boolean implementation(Physics physics, Vector2 velocityDelta, WorldGrid grid, int ruleset);

    /**
     * moves as if the grid simply does not exist
     *
     * @return false
     */
    default boolean noReaction(Physics physics, Vector2 velocityDelta){
        physics.getPosition().add(velocityDelta);
        return false;
    }
}
