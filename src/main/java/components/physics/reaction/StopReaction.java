package components.physics.reaction;

import com.badlogic.gdx.math.Vector2;
import components.physics.Physics;
import grid.WorldGrid;
import grid.collision.Collider;

public class StopReaction implements CollisionReaction {
    private final Vector2 result = new Vector2();
    @Override
    public boolean implementation(Physics physics, Vector2 velocityDelta, WorldGrid grid, int ruleset) {

        Collider collider = physics.getCollider();
        
        boolean hitWall = collider.collide(physics, velocityDelta, grid, ruleset,result);
        
        Vector2 difference = result.cpy();
        difference.sub(physics.getPosition());

        Vector2 velocity = physics.getVelocity();
        
        physics.getPosition().set(result);
        if(Math.abs(difference.x)<Math.abs(velocityDelta.x)-0.1) velocity.x = 0;
        if(Math.abs(difference.y)<Math.abs(velocityDelta.y)-0.1) velocity.y = 0;
        return hitWall;
    }
    
}
