package components.physics;

import com.badlogic.gdx.math.Vector2;
import components.physics.reaction.CollisionReaction;
import components.physics.reaction.StaticReactions;
import components.upgrade.upgrades.physics.IUpgradablePhysics;
import components.weapon.projectile.PoolableProjectile;
import grid.TileData.TileRuleset;
import grid.WorldGrid;
import grid.collision.BoxCollider;
import grid.collision.Collider;
import grid.collision.GridCollider;

/**
 * Represents the physics of a game object, including position, velocity, acceleration, and collisions.
 * This class supports various features like setting maximum speed, friction, and collision reaction behavior.
 * It also provides methods to control the object's movement and update its state based on the provided world grid.
 */
public class Physics implements IUpgradablePhysics {

    private final Vector2 position, velocity, acceleration;
    private float maxSpeed = 50;
    private float accelerationForce = 250;
    private float friction = 250;

    private boolean instantMovement = false;

    private boolean frictionEnabled = true;
    private boolean scaledFriction = true;
    private boolean isMoving = false;

    private Collider collider = GridCollider.StaticColliders.PointCollider.object;

    private CollisionReaction reaction = StaticReactions.StaticReact.Stop.reaction;

    
    private TileRuleset ruleset = TileRuleset.WALKING;

    private PoolableProjectile projectileOwner = null;
    private boolean canMove = true;

    // Collision detection
    private WorldGrid worldGrid;
    private final Vector2 appliedVelocity = new Vector2();

    public Physics() {
        this(new Vector2(0,0));
    }

    public Physics(Vector2 position) {
        this(position, null);
    }

    public Physics(Vector2 position, WorldGrid worldGrid) {
        this.position = position;
        this.worldGrid = worldGrid;
        velocity = new Vector2(0,0);
        acceleration = new Vector2(0, 0);
    }

    /**
     * @return current position of physics object
     */
    public Vector2 getPosition() {
        return position;
    }

    /**
     * Sets the position of the physics object.
     *
     * @param position the new position of the object
     */
    public void setPosition(Vector2 position) {
        this.position.x = position.x;
        this.position.y = position.y;
    }

    /**
     * @param velocity new velocity of physics object
     */
    public void setVelocity(Vector2 velocity) {
        velocity.limit(maxSpeed);

        this.velocity.x = velocity.x;
        this.velocity.y = velocity.y;
    }

    /**
     * Gets the velocity of the physics object.
     *
     * @return the current velocity of the object
     */
    public Vector2 getVelocity() {
        return velocity;
    }

    /**
     * Start moving at a constant speed on a linear path.
     *
     * @param dir direction of movement
     * @param speed speed of movement
     */
    public void setLinearMovement(Vector2 dir, float speed) {
        frictionEnabled = false;
        dir.nor();
        setMaxSpeed(speed);
        setVelocity(new Vector2(dir.x * speed, dir.y * speed));
    }

    /**
     * <p>Accelerate object in a given direction with the force of
     * the maximum acceleration.</p>
     *
     * <p>If instant movement is enabled, velocity is affected, not
     * acceleration</p>
     *
     * <p>When speed limit is hit, object is no longer accelerating
     * in the velocity direction</p>
     *
     * @param dir direction to be moved
     */
    public void accelerate(Vector2 dir) {
        isMoving = true;

        if (instantMovement) {
            velocity.x = dir.x;
            velocity.y = dir.y;
            velocity.clamp(maxSpeed, maxSpeed);
            return;
        }

        float speed = velocity.len();
        if (speed >= maxSpeed) {
            dir = velocity.cpy().scl(-1);
        }

        dir.nor();

        acceleration.x = dir.x * accelerationForce;
        acceleration.y = dir.y * accelerationForce;
    }


    /**
     * <p>Apply a force to the physics object that pushed the object</p>
     *
     * <p>Speed limit of object does not affect the speed reached after
     * force is applied.</p>
     *
     * @param force to be added to acceleration
     */
    public void applyForce(Vector2 force) {
        if (force == null) return;
        appliedVelocity.x = force.x;
        appliedVelocity.y = force.y;
    }

    /**
     * Update the physics. Calculate new position, velocity and acceleration.
     *
     * @param dt delta time - time since last update
     */
    public void update(double dt) {
        if (isStationary()) return;

        double deltaX = (velocity.x + appliedVelocity.x) * dt;
        double deltaY = (velocity.y + appliedVelocity.y) * dt;

        // the difference between the current position and the candidate for the next position
        Vector2 velocityDeltaTime = new Vector2((float) deltaX,(float) deltaY);

        boolean hitWall = reaction.react(this,velocityDeltaTime,worldGrid,ruleset.val);
        if(projectileOwner != null && hitWall){
            projectileOwner.hitWall();
        }

        velocity.x += acceleration.x * dt;
        velocity.y += acceleration.y * dt;

        // Break applied velocity
        float breakingFactor = 0.95f;
        appliedVelocity.scl(breakingFactor);

        if (appliedVelocity.len2() < 1) {
            appliedVelocity.setZero();
        }

        acceleration.x = 0;
        acceleration.y = 0;

        if (!isMoving) calculateBreakingAcceleration();

        isMoving = false;
    }

    /**
     * @return true if physics is not moving, has no velocity, no acceleration, and no applied velocity from a push
     */
    public boolean isStationary() {
        return !canMove ||( velocity.x == 0 && velocity.y == 0 && acceleration.x == 0 && acceleration.y == 0 && appliedVelocity.x == 0 && appliedVelocity.y == 0);
    }

    private void calculateBreakingAcceleration() {
        if (!frictionEnabled) return;

        if (instantMovement) {
            stopMovement();
            return;
        }

        float speed = velocity.len();
        if (speed < 1) {
            stopMovement();
            return;
        }

        Vector2 negVelocity = velocity.cpy().scl(-1).nor();

        float adjustedForce = friction;
        if (scaledFriction) adjustedForce *= speed / maxSpeed;

        acceleration.x = negVelocity.x * adjustedForce;
        acceleration.y = negVelocity.y * adjustedForce;
    }

    /**
     * Set velocity and acceleration of physics object to 0.
     */
    public void stopMovement() {
        velocity.x = 0;
        velocity.y = 0;
        acceleration.x = 0;
        acceleration.y = 0;
    }

    /**
     * <p>Set the speed limit of the physics object.</p>
     *
     * <p>When the maximum speed limit is reached, the physics object
     * will no longer accelerate to go any faster in that direction.</p>
     *
     * <p>The speed limit does not affect the speed of the object if pushed
     * with a high force using ´applyForce´</p>
     * @param maxSpeed max speed of velocity
     */
    public void setMaxSpeed(float maxSpeed) {
        this.maxSpeed = (maxSpeed > 0) ? maxSpeed : 0;
    }

    @Override
    public void increaseMaxSpeed(float deltaMaxSpeed) {
        if (deltaMaxSpeed < 0) return;

        setMaxSpeed(maxSpeed + deltaMaxSpeed);
    }

    @Override
    public void decreaseMaxSpeed(float deltaMaxSpeed) {
        if (deltaMaxSpeed < 0) return;

        setMaxSpeed(maxSpeed - deltaMaxSpeed);
    }

    /**
     * <p>Adjust the strength of the automatic breaking when the physics object
     * no longer accelerates. The stronger the force, the faster the object stops moving.</p>
     *
     * @param force of the breaking
     */
    public void setFriction(float force) {
        friction = force;
    }

    /**
     * <p>Adjust the maximum acceleration force when calling `accelerate`.</p>
     *
     * <p>The force determines how fast the physics object accelerates.</p>
     *
     * @param force strength of the accelerating. The bigger force, the faster acceleration.
     */
    public void setAccelerationForce(float force) {
        accelerationForce = force;
    }

    /**
     * <p>Enable automatic breaking when object is no longer accelerating or pushed.</p></br>
     *
     * <p>When breaking is enabled it will apply the breaking force in the opposite
     * movement-direction.</p>
     */
    public void enableFriction() {
        frictionEnabled = true;
    }

    /**
     * <p>Disable automatic breaking when object is no longer accelerating or pushed.</p></br>
     *
     * <p>When breaking is enabled it will apply the breaking force in the opposite
     * movement-direction.</p>
     */
    public void disableFriction() {
        frictionEnabled = false;
    }

    /**
     * Enables acceleration when moving.
     *
     * <p>When instant movement, the object reaches max speed instantly</p>
     */
    public void enableInstantMovement() {
        instantMovement = true;
    }

    /**
     * Disable acceleration when moving.
     *
     * <p>Movement will be smoother, based on acceleration force.</p>
     *
     * <p>When instant movement is enabled,
     * the object reaches max speed instantly</p>
     */
    public void disableInstantMovement() {
        instantMovement = false;
    }

    /**
     * <p>Enables scaled friction</p>
     *
     * <p>Scaled friction means that the friction force
     * is scaled based on you speed / maxSpeed. The higher
     * speed, the more friction. This makes for a smoother
     * slowdown of movement</p>
     */
    public void enableScaledFriction() {
        scaledFriction = true;
    }

    /**
     * Rotate the velocity by an angle (in radians)
     *
     * @param angle in radians
     */
    public void rotateVelocity(float angle) {
        velocity.rotateRad(angle);
    }

    /**
     * Updating the world grid object for the physics.
     *
     * <p>World grid is used to check for collision when moving</p>
     *
     * <p>If no world grid is defined, then the object will
     * not collide with the world.</p>
     *
     * @param worldGrid world represented as a grid
     */
    public void setWorldGrid(WorldGrid worldGrid) {
        this.worldGrid = worldGrid;
    }

    /**
     * Gets the WorldGrid used for collision detection.
     *
     * @return the WorldGrid instance
     */
    public WorldGrid getWorldGrid() {
        return worldGrid;
    }

    /**
     * Sets the box collider for the physics object.
     *
     * @param topToPos the distance from the top edge of the collider to the object's position
     * @param bottomToPos the distance from the bottom edge of the collider to the object's position
     * @param leftToPos the distance from the left edge of the collider to the object's position
     * @param rightToPos the distance from the right edge of the collider to the object's position
     */
    public void setBoxCollision(float topToPos, float bottomToPos, float leftToPos,float rightToPos) {
        if(collider instanceof BoxCollider) return;
        this.collider = GridCollider.makeBoxCollider(topToPos,bottomToPos,leftToPos,rightToPos);
    }

    /**
     * <p>Maximum speed determines the speed limit of the objects velocity when accelerating.</p>
     *
     * <p>Object will not reach higher velocity then the speed limit. However,
     * if the object is pushed, it can reach higher speeds above the speed limit</p>
     *
     * @return maximum magnitude of velocity vector
     */
    public float getMaxSpeed() {
        return maxSpeed;
    }

    /**
     * Gets the collider associated with the physics object.
     *
     * @return the collider instance
     */
    public Collider getCollider(){
        return collider;
    }

    /**
     * Sets the collision reaction behavior of the physics object.
     *
     * @param reaction the CollisionReaction instance
     */
    public void setReaction(CollisionReaction reaction) {
        this.reaction = reaction;
    }

    /**
     * Sets the tile ruleset for the physics object.
     *
     * @param ruleset the TileRuleset instance
     */
    public void setRuleset(TileRuleset ruleset) {
        this.ruleset = ruleset;
    }

    /**
     * Associates the physics object with a projectile.
     *
     * @param projectile the PoolableProjectile instance
     */
    public void setProjectileOwner(PoolableProjectile projectile){
        projectileOwner = projectile; 
    }

    /**makes this object unable to move */
    public void freeze(){
        canMove = false;
    }

}

