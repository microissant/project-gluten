package components.physics;

import com.badlogic.gdx.math.Vector2;

public interface PhysicsBody extends PositionedBody {

    /**
     * Push the physics body in a certain direction and force
     *
     * @param dir direction vector with force
     */
    void push(Vector2 dir);

    /**
     * Move the physics body in a certain direction. The force
     * of the movement is controlled by the physics acceleration force
     * @param dir direction of the movement
     */
    void move(Vector2 dir);

}
