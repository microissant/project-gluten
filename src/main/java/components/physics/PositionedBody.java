package components.physics;

import com.badlogic.gdx.math.Vector2;

public interface PositionedBody {

    /**
     * @return Position of body
     */
    Vector2 getPosition();

    /**change the position of the body
     * 
     * @param pos the new position
     */
    void setPosition(Vector2 pos);
}
