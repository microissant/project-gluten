package components.collision.event;

import components.collision.CollisionType;
import components.collision.ICollidable;

public interface ICollisionEventDefinition {

    /**
     * Trigger a collision event based on the given type of collision.
     *
     * <p>
     *      For both the eventOrigin and the target, check if they have a
     *      defined collision event defined for the given {@code CollisionType}.
     *      If so, execute the defined event.
     * </p>
     *
     * @param eventOrigin object that triggers the event
     * @param target object that the eventOrigin has collided with
     * @param type of the collision
     */
    void triggerCollisionEvent(ICollidable eventOrigin, ICollidable target, CollisionType.Type type);


}
