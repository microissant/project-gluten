package components.collision.event.definitions;

import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType;
import components.collision.ICollidable;
import components.collision.event.CollisionDefinitionsEvent;
import components.collision.event.ICollisionEvent;
import components.collision.event.ICollisionEventDefinition;
import components.collision.event.events.CollisionDamageEvent;
import components.collision.event.events.CollisionPushEvent;
import components.collision.event.events.CompositeCollisionEvent;
import components.health.Damage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *     Template of the projectile collision events.
 * </p>
 *
 * <p>
 *     Is setup to handle both self damage and target damage when
 *     collision event is triggered. Support for impact force vector,
 *     which will push the target object in the vectors direction when
 *     triggering collision event.
 * </p>
 */
public class ProjectileCollisionEventDefinition implements ICollisionEventDefinition {

    private final CollisionDefinitionsEvent collisionDefinitions;

    public ProjectileCollisionEventDefinition(CollisionType.Type entityCollisionType, Damage selfDamage, Damage targetDamage, Vector2 impactForce) {
        ICollisionEvent applySelfDamage = new CollisionDamageEvent(selfDamage);
        ICollisionEvent applyTargetDamage = new CompositeCollisionEvent(
                new ArrayList<>(Arrays.asList(
                        new CollisionDamageEvent(targetDamage),
                        new CollisionPushEvent(impactForce)
                )));

        HashMap<CollisionType.Type, ICollisionEvent> selfCollision = new HashMap<>(Map.of(
                entityCollisionType, applySelfDamage,
                CollisionType.Type.PROP, applySelfDamage
        ));
        HashMap<CollisionType.Type, ICollisionEvent> targetCollision = new HashMap<>(Map.of(
                entityCollisionType, applyTargetDamage,
                CollisionType.Type.PROP, applyTargetDamage
        ));

        collisionDefinitions = new CollisionDefinitionsEvent(selfCollision, targetCollision);
    }

    public ProjectileCollisionEventDefinition(CollisionType.Type entityCollisionType, Damage selfDamage, Damage targetDamage) {
        this(entityCollisionType, selfDamage, targetDamage, new Vector2());
    }

    @Override
    public void triggerCollisionEvent(ICollidable eventOrigin, ICollidable target, CollisionType.Type type) {
        collisionDefinitions.triggerCollisionEvent(eventOrigin, target, type);
    }
}
