package components.collision.event;

import components.collision.ICollidable;

public interface ICollisionEvent {

    /**
     * Perform the event behavior
     *
     * <p>
     *     Check if the target has the required component to complete
     *     the event behavior. If so, retrieve it and perform the event
     *     on the component.
     * </p>
     *
     * @param target object the event behavior should affect
     */
    void execute(ICollidable target);

}
