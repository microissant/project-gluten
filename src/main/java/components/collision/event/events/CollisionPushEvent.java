package components.collision.event.events;

import com.badlogic.gdx.math.Vector2;
import components.collision.ICollidable;
import components.collision.event.ICollisionEvent;
import components.physics.Physics;

/**
 * Event for applying a push force to a target.
 *
 * <p>
 *     If the given target has a physics component, it will
 *     apply the defined push force to the physics
 * </p>
 *
 * @param pushForce to be applied upon executing event
 */
public record CollisionPushEvent(Vector2 pushForce) implements ICollisionEvent {

    @Override
    public void execute(ICollidable target) {
        if (target == null) return;
        if (pushForce == null) return;

        Physics targetPhysics = target.getPhysicsComponent();
        if (targetPhysics == null) return;

        targetPhysics.applyForce(pushForce);
    }
}
