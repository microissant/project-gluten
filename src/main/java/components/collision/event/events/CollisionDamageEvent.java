package components.collision.event.events;

import components.collision.ICollidable;
import components.collision.event.ICollisionEvent;
import components.health.Damage;
import components.health.IDamageable;

/**
 * Event for applying damage to a target.
 *
 * <p>
 *     If the given target has a health manager, it will
 *     apply the defined damage to the health manager
 * </p>
 *
 * @param damage to be applied upon executing event
 */
public record CollisionDamageEvent(Damage damage) implements ICollisionEvent {

    @Override
    public void execute(ICollidable target) {
        if (target == null) return;
        if (damage == null) return;

        IDamageable damageable = target.getDamageable();
        if (damageable == null) return;

        damageable.applyDamage(damage);
    }
}
