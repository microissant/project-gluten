package components.collision.event.events;

import components.collision.ICollidable;
import components.collision.event.ICollisionEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for compositing multiple collision events in one.
 *
 * <p>
 *     Each time execute is called, each event in the event list
 *     will trigger their execute method
 * </p>
 *
 * @param events list of event to be executed
 */
public record CompositeCollisionEvent(List<ICollisionEvent> events) implements ICollisionEvent {

    public CompositeCollisionEvent(List<ICollisionEvent> events) {
        this.events = new ArrayList<>(events);
    }

    @Override
    public void execute(ICollidable target) {
        for (ICollisionEvent event: events) {
            event.execute(target);
        }
    }
}
