package components.collision.event;

import components.collision.CollisionType;
import components.collision.ICollidable;

import java.util.HashMap;

/**
 * Class for controlling and triggering events based on specific collision types.
 * <p>
 *     This class contains two maps of collision types to collision events. One map is for events
 *     to be triggered on the owner of the {@code CollisionDefinitionEvent}, and another map is for
 *     the events to affect the other target (colliding object).
 * </p>
 * <p>
 *     When a specific collision type occurs, the class finds the appropriate event by looking in the
 *     maps. If the map contains an entry for the type, it triggers the corresponding event.
 * </p>
 */
public class CollisionDefinitionsEvent implements ICollisionEventDefinition {

    private final HashMap<CollisionType.Type, ICollisionEvent> selfCollisionEvents;
    private final HashMap<CollisionType.Type, ICollisionEvent> targetCollisionEvents;


    public CollisionDefinitionsEvent(HashMap<CollisionType.Type, ICollisionEvent> selfCollisionEvents, HashMap<CollisionType.Type, ICollisionEvent> targetCollisionEvents) {
        this.selfCollisionEvents = new HashMap<>(selfCollisionEvents);
        this.targetCollisionEvents = new HashMap<>(targetCollisionEvents);
    }

    @Override
    public void triggerCollisionEvent(ICollidable eventOrigin, ICollidable target, CollisionType.Type type) {
        triggerIfDefined(eventOrigin, selfCollisionEvents, type);
        triggerIfDefined(target, targetCollisionEvents, type);
    }

    private void triggerIfDefined(ICollidable target, HashMap<CollisionType.Type, ICollisionEvent> collisionBehavior, CollisionType.Type type) {
        if (!collisionBehavior.containsKey(type)) return;
        collisionBehavior.get(type).execute(target);
    }
}
