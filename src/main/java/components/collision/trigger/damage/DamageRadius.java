package components.collision.trigger.damage;

import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType;
import components.collision.ICollidable;
import components.collision.primitives.CollisionCircle;
import components.collision.trigger.ITrigger;
import components.health.Damage;
import components.health.IDamageable;
import media.DrawProperties;
import model.event.EventBus;
import model.event.events.collision.RemoveTriggerEvent;

import java.util.HashSet;
import java.util.Set;

/**
 * Represents a damage radius trigger, which damages and applies forces to
 * nearby collidable objects based on their distance from the origin position.
 */
public class DamageRadius implements ITrigger {

    private final CollisionCircle collisionCircle;
    private final IFalloffType falloffType;
    private final CollisionType collisionType;
    private final Damage damage;
    private final float force;
    private final float radius;
    private final Vector2 originPosition;
    private final EventBus eventBus;

    public DamageRadius(
            Vector2 originPosition,
            float radius,
            Damage damage,
            float pushForce,
            IFalloffType falloffType,
            CollisionType collisionType,
            EventBus eventBus) {

        this.originPosition = originPosition;
        this.radius = radius;
        this.collisionType = collisionType;
        collisionCircle = new CollisionCircle(
                originPosition,
                new Vector2(),
                radius,
                this,
                new HashSet<>(Set.of(collisionType)),
                new HashSet<>()
        );
        this.damage = damage;
        this.falloffType = falloffType;
        this.force = pushForce;
        this.eventBus = eventBus;
    }

    public DamageRadius(DamageRadius damageRadius) {
        this(damageRadius.originPosition, damageRadius.radius, damageRadius.damage, damageRadius.force, damageRadius.falloffType, damageRadius.collisionType, damageRadius.eventBus);
    }

    @Override
    public void update(double deltaTime) {
        if (!collisionCircle.hasCheckedCollision()) return;
        eventBus.post(new RemoveTriggerEvent(this));
    }

    @Override
    public void collisionEnter(ICollidable target, CollisionType.Type type) {
        if (target == null) return;
        if (target.getPosition() == null) return;

        float distanceToTarget = target.getPosition().dst(originPosition);
        float falloffScale = falloffType.calculateFalloff(distanceToTarget / radius);

        Vector2 directionToTarget = new Vector2(target.getPosition().x - originPosition.x, target.getPosition().y - originPosition.y);

        if (target.getPhysicsComponent() != null) {
            target.getPhysicsComponent().applyForce(directionToTarget.setLength(force * falloffScale));
        }


        IDamageable damageable = target.getDamageable();
        if (damageable == null) return;

        damageable.applyDamage(new Damage((int) (damage.getDamageValue() * falloffScale)));
    }

    @Override
    public CollisionCircle getCollisionPrimitive() {
        return collisionCircle;
    }

    @Override
    public DrawProperties getProperties() {
        return null;
    }

    @Override
    public Vector2 getPosition() {
        return originPosition;
    }

    @Override
    public void setPosition(Vector2 pos) {
        originPosition.x = pos.x;
        originPosition.y = pos.y;
    }

    @Override
    public boolean isDead() {
        return false;
    }

    @Override
    public void unregisterFromEventBus() {
        eventBus.unregister(this);
    }
}
