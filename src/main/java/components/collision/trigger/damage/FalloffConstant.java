package components.collision.trigger.damage;

/**
 * Represents a constant falloff type, which does not apply any falloff to the
 * damage or force calculations in the DamageRadius class.
 */
public class FalloffConstant implements IFalloffType {

    @Override
    public float calculateFalloff(float value) {
        return 1;
    }
}
