package components.collision.trigger.damage;

public interface IFalloffType {

    /**
     * Calculate a falloff value based on a percentage input
     *
     * @param value a value between 0-1
     * @return a float with a value from 0-1
     */
    float calculateFalloff(float value);

}
