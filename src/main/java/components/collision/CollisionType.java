package components.collision;

import java.util.HashSet;
import java.util.Set;

/**
 * Record for defining the type of collision a collision
 * primitive should react to.
 *
 * @param type the specific type which the collision primitive
 *             should react to.
 * @param ignore set of object which should be ignored even
 *               if they have the matching type
 */
public record CollisionType(Type type, Set<ICollidable> ignore) {

    public CollisionType(Type type, Set<ICollidable> ignore) {
        this.type = type;
        if (ignore == null) {
            this.ignore = null;
        } else {
            this.ignore = new HashSet<>(ignore);

        }
    }

    public CollisionType(Type type) {
        this(type, null);
    }

    public enum Type {
        PROJECTILE,
        PLAYER,
        ENEMY,
        PROP
    }
}
