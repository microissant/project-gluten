package components.collision.primitives;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionSolver;
import components.collision.CollisionType;
import components.collision.ICollidable;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Record for collision circle, containing information
 * important to collision detection and collision response.
 */
public class CollisionCircle implements ICollisionPrimitive {

    private final Vector2 originPosition;
    private final Vector2 positionOffset;
    private final float radius;
    private final ICollidable owner;
    private final Set<CollisionType> activeTypes;
    private final Set<CollisionType> passiveTypes;
    private final Set<ICollidable> collidedObjects;
    private final int hashCode;

    private int collisionChecks;

    public CollisionCircle(Vector2 originPosition, Vector2 positionOffset, float radius, ICollidable owner, Set<CollisionType> activeTypes, Set<CollisionType> passiveTypes) {
        this.originPosition = originPosition;
        this.positionOffset = positionOffset;
        this.radius = radius;
        this.owner = owner;

        if (activeTypes == null) {
            this.activeTypes = new HashSet<>();
        } else {
            this.activeTypes = new HashSet<>(activeTypes);
        }

        if (passiveTypes == null) {
            this.passiveTypes = new HashSet<>();
        } else {
            this.passiveTypes = new HashSet<>(passiveTypes);
        }


        hashCode = Objects.hash(radius, owner, activeTypes, passiveTypes);

        collidedObjects = new HashSet<>();
    }

    @Override
    public Set<CollisionType> getActiveTypes() {
        return activeTypes;
    }

    @Override
    public Set<CollisionType> getPassiveTypes() {
        return passiveTypes;
    }

    @Override
    public ICollidable getOwner() {
        return owner;
    }

    public void checkCollision(CollisionCircle target, CollisionType.Type type) {
        collisionChecks ++;
        boolean hasCollision = CollisionSolver.checkCollision( this, target);

        if (hasCollision) {
            if (collidedObjects.contains(target.owner)) return;

            collidedObjects.add(target.owner);
            owner.collisionEnter(target.owner, type);
            return;
        }

        if (collidedObjects.remove(target.owner)) owner.collisionLeave(target.owner);
    }

    @Override
    public void drawDebug(ShapeRenderer renderer) {
        renderer.circle(originPosition.x + positionOffset.x, originPosition.y + positionOffset.y, radius);
    }


    @Override
    public Vector2 getPosition() {
        if (originPosition == null) return null;
        if (positionOffset == null) return null;

        return new Vector2(originPosition.x + positionOffset.x, originPosition.y + positionOffset.y);
    }

    @Override
    public void setPosition(float x, float y) {
        if (originPosition == null) return;

        originPosition.x = x;
        originPosition.y = y;
    }

    @Override
    public boolean hasCheckedCollision() {
        return collisionChecks > 0;
    }

    public float radius() {
        return radius;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CollisionCircle circle)) return false;
        return Float.compare(circle.radius, radius) == 0 && Objects.equals(owner, circle.owner) && Objects.equals(activeTypes, circle.activeTypes);
    }

    @Override
    public int hashCode() {
        return hashCode;
    }
}
