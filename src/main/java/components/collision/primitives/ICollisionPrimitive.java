package components.collision.primitives;

import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType;
import components.collision.ICollidable;
import view.DebugShape;

import java.util.Set;

/**
 * <p>ICollisionPrimitive is an interface for defining the basic structure of a collision primitive object.
 * A collision primitive is a geometric shape used for detecting collisions between game entities.</p>
 *
 * <p>The interface extends {@link DebugShape}, and provides methods for managing the active and passive
 * collision types, the owning entity, and the position of the primitive. Additionally, it includes a method
 * for checking collisions against a {@link CollisionCircle} object.</p>
 */
public interface ICollisionPrimitive extends DebugShape {

    /**
     * @return the set of active {@link CollisionType}s for this collision primitive
     */
    Set<CollisionType> getActiveTypes();

    /**
     * @return the set of passive {@link CollisionType}s for this collision primitive, or null if none
     */
    default Set<CollisionType> getPassiveTypes() { return null; }

    /**
     * @return the {@link ICollidable} owner of this collision primitive
     */
    ICollidable getOwner();

    /**
     * Checks for a collision between this collision primitive and the specified {@link CollisionCircle}.
     *
     * @param target the target {@link CollisionCircle} to check for collision
     * @param type the {@link CollisionType.Type} of the collision to check
     */
    void checkCollision(CollisionCircle target, CollisionType.Type type);

    /**
     * @return the position of this collision primitive as a {@link Vector2}
     */
    Vector2 getPosition();

    /**
     * Sets the position of this collision primitive.
     *
     * @param x the x-coordinate of the new position
     * @param y the y-coordinate of the new position
     */
    void setPosition(float x, float y);

    /**
     * @return true if the collision check has already been performed for this collision primitive, false otherwise
     */
    boolean hasCheckedCollision();
}
