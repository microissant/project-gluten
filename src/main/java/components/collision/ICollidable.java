package components.collision;

import components.collision.primitives.CollisionCircle;
import components.health.Damage;
import components.health.HealthManager;
import components.health.IDamageable;
import components.physics.Physics;
import components.physics.PositionedBody;
import item.InteractAgent;

public interface ICollidable extends PositionedBody {

    /**
     * Collision detected with a target. Only applied once per collision
     *
     * @param target object collided with
     * @param type the target's matching collision type
     */
    void collisionEnter(ICollidable target, CollisionType.Type type);

    /**
     * Called when a collision ends between this object and a target.
     *
     * @param target The ICollidable object that was previously collided with.
     */
    default void collisionLeave(ICollidable target) {}

    /**
     * @return health manager which control the health of the object
     */
    default HealthManager getHealthComponent() { return null; }

    /**
     * Returns the IDamageable instance associated with the object. The
     * damageable object has a health value and can receive damage.
     *
     * @return An IDamageable instance, or null if the object doesn't have one.
     */
    default IDamageable getDamageable() { return null; }

    /**
     * @return the physics component controlling the movement of the object
     */
    default Physics getPhysicsComponent() { return null; }

    /**
     * Returns the InteractAgent associated with the object. The InteractAgent
     * can interact with interactable and tradable objects.
     *
     * @return An InteractAgent instance, or null if the object doesn't have one.
     */
    default InteractAgent getInteractAgent() { return null; }

    /**
     * @return collision circle which is used for object collision detection
     */
    CollisionCircle getCollisionPrimitive();

    /**
     * Returns the Damage taken by the object during a collision.
     *
     * @return A Damage instance representing the damage taken, or null if the object doesn't take damage.
     */
    default Damage damageTaken() { return null; }

}
