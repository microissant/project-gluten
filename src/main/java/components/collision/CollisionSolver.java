package components.collision;

import components.collision.primitives.CollisionCircle;

public class CollisionSolver {

    /**
     * Check if two circles overlap by checking if the distance
     * between them is less than their combined radius's
     *
     * @param circle1 first circle
     * @param circle2 second circle
     * @return true if the two circles overlap
     */
    public static boolean checkCollision(CollisionCircle circle1, CollisionCircle circle2) {
        if (!canCheckCollision(circle1, circle2)) return false;

        return circle1.getPosition().dst2(circle2.getPosition()) <= Math.pow(circle1.radius() + circle2.radius(), 2);
    }

    private static boolean canCheckCollision(CollisionCircle circle1, CollisionCircle circle2) {
        if (circle1 == null) return false;
        if (circle2 == null) return false;
        if (circle1.getPosition() == null) return false;
        return circle2.getPosition() != null;
    }
}
