package components.collision;

import app.global.Statistics;
import com.badlogic.gdx.math.Vector2;
import components.collision.primitives.CollisionCircle;
import components.collision.primitives.ICollisionPrimitive;
import components.collision.trigger.ITrigger;
import model.IUpdatable;
import model.event.EventBus;
import model.event.Subscribe;
import model.event.events.collision.*;
import util.UpdatableList;
import view.DebugShape;

import java.util.*;

/**
 * Manages collision detection between ICollisionPrimitives in the game world.
 * The CollisionManager uses a space partitioning technique to optimize
 * collision detection, and it supports adding/removing primitives and triggers.
 */
public class CollisionManager implements IUpdatable {

    private final List<CollisionCircle> toBeAdded;
    private final List<CollisionCircle> toBeRemoved;

    private final HashMap<CollisionCircle, Vector2> collisionCircles;
    private final SpacePartition spacePartition;
    private final UpdatableList<ITrigger> triggers;
    private long debugCollisionChecksCount = 0;

    public CollisionManager(int row, int col, float cellSize, EventBus eventBus) {
        if (cellSize < 0) {
            throw new IllegalArgumentException("cellSize cannot be negative");
        }

        spacePartition = new SpacePartition(row, col, cellSize);
        collisionCircles = new HashMap<>();
        triggers = new UpdatableList<>();

        toBeAdded = new LinkedList<>();
        toBeRemoved = new LinkedList<>();

        if (eventBus == null) return;
        eventBus.register(this);
    }

    /**
     * Adds a CollisionCircle to the collision system,
     * inserting it into the space partition and storing its position.
     *
     * @param circle The CollisionCircle to be added to the collision system.
     */
    public void addCollisionCircle(CollisionCircle circle) {
        if (circle == null) return;
        if (circle.getPosition() == null) return;

        spacePartition.insert(circle);
        collisionCircles.put(circle, new Vector2(circle.getPosition()));
    }

    /**
     * Adds a list of CollisionCircles to the collision system,
     * inserting each circle into the space partition and storing their positions.
     *
     * @param collisionCircles A list of CollisionCircles to be added to the collision system.
     */
    public void addCollisionCircle(List<CollisionCircle> collisionCircles) {
        for (CollisionCircle collisionCircle: collisionCircles) {
            addCollisionCircle(collisionCircle);
        }
    }

    /**
     * Removes a CollisionCircle from the collision system,
     * deleting it from the space partition and the stored positions.
     *
     * @param circle The CollisionCircle to be removed from the collision system.
     */
    public void removeCollisionCircle(CollisionCircle circle) {
        if (circle == null) return;
        if (!collisionCircles.containsKey(circle)) return;

        collisionCircles.remove(circle);
        spacePartition.remove(circle);
    }

    /**
     * Removes a list of CollisionCircles from the collision system,
     * deleting each circle from the space partition and the stored positions.
     *
     * @param circles A list of CollisionCircles to be removed from the collision system.
     */
    public void removeCollisionCircle(List<CollisionCircle> circles) {
        for (CollisionCircle circle: circles) {
            removeCollisionCircle(circle);
        }
    }

    /**
     * Updates the Collision Manager by performing collision checks for each CollisionCircle and CollisionRay.
     * This method:
     * <ol>
     *   <li>Adds and removes CollisionCircles from the toBeAdded and toBeRemoved queues.</li>
     *   <li>Updates the space partition for any moved CollisionCircles.</li>
     *   <li>Checks for collisions between CollisionCircles and other shapes in their range.</li>
     * </ol>
     *
     * @param deltaTime The time elapsed since the last update, in seconds. Currently not used in the method.
     */
    @Override
    public void update(double deltaTime) {
        debugCollisionChecksCount = 0;

        addCollisionCircle(toBeAdded);
        removeCollisionCircle(toBeRemoved);

        toBeAdded.clear();
        toBeRemoved.clear();

        long timeStart = System.nanoTime();

        for (Map.Entry<CollisionCircle, Vector2> entry : collisionCircles.entrySet()) {
            CollisionCircle circle = entry.getKey();
            Vector2 previousPosition = entry.getValue();

            // try to update space partition if the circle has moved since last update
            if (!previousPosition.equals(circle.getPosition())) {
                spacePartition.update(circle, previousPosition);
                previousPosition.x = circle.getPosition().x;
                previousPosition.y = circle.getPosition().y;
            }

            // Check for collision
            checkCollisions(circle, spacePartition.queryRange(circle));
        }

        long elapsedTime = System.nanoTime() - timeStart;

        triggers.update(deltaTime);

        // Update debug statistics
        Statistics.addCollisionChecksCount(debugCollisionChecksCount);
        Statistics.setCollisionTimeNS(elapsedTime);
    }

    /**
     * Checks for collisions between a given ICollisionPrimitive
     * and a Set of CollisionCircle targets.
     * If a collision is detected, the corresponding owners
     * are notified of the collision along with the matching collision type.
     *
     * @param primitive The ICollisionPrimitive to check for collisions (either a CollisionCircle or CollisionRay).
     * @param targets   A Set of CollisionCircle objects to check for collisions against the given primitive.
     */
    private void checkCollisions(ICollisionPrimitive primitive, Set<CollisionCircle> targets) {
        for (CollisionCircle target: targets) {

            // Check if the circle and target has matching collision types
            CollisionType.Type matchingType = findMatchingType(primitive.getActiveTypes(), target.getPassiveTypes());
            if (matchingType == null) continue;

            // Check if the two circles collide
            primitive.checkCollision(target, matchingType);
            debugCollisionChecksCount ++;
        }
    }

    /**
     * Finds the first matching collision type between two sets of CollisionTypes.
     * If no matching type is found, or if either set is empty or null, this method returns null.
     *
     * @param setA A Set of CollisionType objects to be compared.
     * @param setB A Set of CollisionType objects to be compared.
     * @return The first matching CollisionType.Type found in both sets, or null if no match is found.
     */
    private CollisionType.Type findMatchingType(Set<CollisionType> setA, Set<CollisionType> setB) {
        if (setA == null) return null;
        if (setB == null) return null;
        if (setA.isEmpty() || setB.isEmpty()) return null;

        Set<CollisionType> intersection = new HashSet<>(setA);
        intersection.retainAll(setB);
        if (intersection.isEmpty()) return null;
        return intersection.iterator().next().type();
    }


    @Subscribe
    public void addCollisionCirclesEvent(AddCollisionCirclesEvent event) {
        if (event == null) return;
        if (event.circles() == null) return;

        for (CollisionCircle circle: event.circles()) {
            primitiveToBeAdded(circle);
        }
    }

    @Subscribe
    public void addCollisionCircleEvent(AddCollisionCircleEvent event) {
        if (event == null) return;

        primitiveToBeAdded(event.circle());
    }

    private void primitiveToBeAdded(CollisionCircle circle) {
        if (circle == null) return;

        toBeAdded.add(circle);
    }

    @Subscribe
    public void removePrimitiveEvent(RemoveCollisionCircleEvent event) {
        if (event == null) return;
        if (event.circle() == null) return;

        toBeRemoved.add(event.circle());
    }

    @Subscribe
    public void addTrigger(SpawnTriggerEvent event) {
        if (event == null) return;
        if (event.trigger() == null) return;

        triggers.add(event.trigger());
        toBeAdded.add(event.trigger().getCollisionPrimitive());
    }

    @Subscribe
    public void removeTrigger(RemoveTriggerEvent event) {
        if (event == null) return;
        if (event.trigger() == null) return;

        triggers.remove(event.trigger());
        toBeRemoved.add(event.trigger().getCollisionPrimitive());
    }

    /**
     * Clears all the CollisionCircles from the collision system,
     * removing them from the space partition, stored positions, and triggers.
     */
    public void clear() {
        for (CollisionCircle circle: collisionCircles.keySet()) {
            spacePartition.remove(circle);
        }
        collisionCircles.clear();
        triggers.clear();
    }

    /**
     * Returns a Set of DebugShape objects representing the CollisionCircles
     * currently in the collision system.
     *
     * @return A Set of DebugShape objects representing the CollisionCircles.
     */
    public Set<DebugShape> getCollisionCircles() {
        return new HashSet<>(collisionCircles.keySet());
    }
}
