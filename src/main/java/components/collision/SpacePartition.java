package components.collision;

import com.badlogic.gdx.math.Vector2;
import components.collision.primitives.CollisionCircle;
import grid.Grid;

import java.util.*;

/**
 * SpacePartition class extends Grid and provides functionality for
 * efficient collision detection using spatial partitioning.
 *
 * <p>
 *     By placing collision circles in smaller partitions, the
 *     number of collision checks is vastly improved. This is because
 *     each partition has n^2 collision checks. If you have 100
 *     collision objects in one partition, 10000 checks has to be
 *     performed. However, if the space is divide into smaller
 *     sections, the number of checks can be reduced by as much as
 *     10-100 times. Even more if the objects are all spreed apart.
 * </p>
 *
 * <p>
 *     The world space is divided into a uniform grid. Each cell
 *     in the grid has a list of collision primitives that overlap
 *     the cell. When querying with a primitive, it will iterate
 *     through each cell the primitives bounding box overlap.
 *     Every shape that these cells contains, is collected
 *     in a set. This set contains all primitives where a potential
 *     collision occurs.
 * </p>
 */
public class SpacePartition extends Grid<List<CollisionCircle>> {
    private final float cellSize;
    private final HashSet<CollisionCircle> queryResult;

    public SpacePartition(int rows, int columns, float cellSize) {
        super(rows, columns);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                this.set(i, j, new ArrayList<>());
            }
        }

        this.cellSize = cellSize;
        queryResult = new HashSet<>();
    }

    /**
     * Inserts a CollisionCircle into the grid space partition
     * based on the specified BoundingBoxCells.
     *
     * <p>
     *     This method iterates through the grid cells corresponding
     *     to the provided bounding box, adding the circle to each.
     * </p>
     *
     * <p>
     *     If the bounding box is out of bounds, the method returns
     *     without performing any insertion.
     * </p>
     *
     * @param circle          The CollisionCircle to be inserted into the grid space partition.
     * @param boundingBoxCells The BoundingBoxCells representing the grid cells the circle should occupy.
     */
    private void insert(CollisionCircle circle, BoundingBoxCells boundingBoxCells) {
        if (!boundingBoxCells.inBounds) return;

        for (int row = boundingBoxCells.minRow; row <= boundingBoxCells.maxRow; row++) {
            for (int col = boundingBoxCells.minCol; col <= boundingBoxCells.maxCol; col++) {
                if (coordinateIsOnGrid(row, col)) {
                    get(row, col).add(circle);
                }
            }
        }
    }

    /**
     * Insert a single collision circle into the space
     * partition grid. The circle will be inserted in
     * the cells within the outer bounding box of the
     * circle.
     *
     * @param circle to be added to the space partition
     */
    public void insert(CollisionCircle circle) {
        if (circle == null) return;

        BoundingBoxCells boundingBoxCells = getBoundingBoxCells(circle);
        insert(circle, boundingBoxCells);
    }

    /**
     * Insert a collection of collision circles into
     * the space partition grid. Each circle is inserted
     * in the grid cells their outer bounding box overlap
     *
     * @param circles collection of circles to be added to the space partition
     */
    public void insert(Collection<CollisionCircle> circles) {
        if (circles == null) return;
        for (CollisionCircle circle: circles) {
            insert(circle);
        }
    }

    /**
     * Removes a CollisionCircle from the grid space partition
     * based on the specified BoundingBoxCells.
     * This method iterates through the grid cells corresponding
     * to the provided bounding box, removing the circle from each.
     *
     * @param circle          The CollisionCircle to be removed from the grid space partition.
     * @param boundingBoxCells The BoundingBoxCells representing the grid cells the circle occupies.
     */
    private void remove(CollisionCircle circle, BoundingBoxCells boundingBoxCells) {
        for (int row = boundingBoxCells.minRow; row <= boundingBoxCells.maxRow; row++) {
            for (int col = boundingBoxCells.minCol; col <= boundingBoxCells.maxCol; col++) {
                if (coordinateIsOnGrid(row, col)) {
                    get(row, col).remove(circle);
                }
            }
        }
    }

    /**
     * Removes a CollisionCircle from the grid space partition
     * based on its current position.
     * This method retrieves the BoundingBoxCells corresponding
     * to the circle's position and removes the circle from
     * the grid space partition.
     *
     * @param circle The CollisionCircle to be removed from the grid space partition.
     *               If the circle is null, the method returns without performing any removal.
     */
    public void remove(CollisionCircle circle) {
        if (circle == null) return;

        BoundingBoxCells boundingBoxCells = getBoundingBoxCells(circle);
        remove(circle, boundingBoxCells);
    }

    /**
     * Updates the grid space partition to reflect the change
     * in position of a CollisionCircle.
     *
     * <p>
     *     If the circle's new bounding box falls within the same
     *     grid cells as its old position, no update occurs.
     * </p>
     *
     * <p>
     *     Otherwise, the circle is removed from the grid cells
     *     corresponding to its old position and inserted into the
     *     grid cells corresponding to its new position.
     * </p>
     *
     *
     * @param circle      The CollisionCircle whose position may have been updated.
     * @param oldPosition The previous position of the circle, before its update.
     */
    public void update(CollisionCircle circle, Vector2 oldPosition) {
        BoundingBoxCells oldBoundingBoxCells = getBoundingBoxCells(oldPosition, circle.radius());
        BoundingBoxCells newBoundingBoxCells = getBoundingBoxCells(circle);

        if (oldBoundingBoxCells.equals(newBoundingBoxCells)) return;

        remove(circle, oldBoundingBoxCells);
        insert(circle, newBoundingBoxCells);
    }

    /**
     * Queries the grid space partition for CollisionCircle
     * objects that may potentially collide with the specified
     * circle.
     * This method iterates through the grid cells that overlap
     * with the bounding box of the input circle.
     *
     * @param circle The CollisionCircle for which potential collisions should be queried.
     * @return A Set of CollisionCircle objects that may potentially collide with the specified circle.
     *         If the circle or its center position are null, an empty Set is returned.
     */
    @SuppressWarnings("ForLoopReplaceableByForEach")
    public Set<CollisionCircle> queryRange(CollisionCircle circle) {
        if (circle == null) return new HashSet<>();
        if (circle.getPosition() == null) return new HashSet<>();

        queryResult.clear();
        BoundingBoxCells boundingBoxCells = getBoundingBoxCells(circle);
        for (int row = boundingBoxCells.minRow; row <= boundingBoxCells.maxRow; row++) {
            for (int col = boundingBoxCells.minCol; col <= boundingBoxCells.maxCol; col++) {
                if (coordinateIsOnGrid(row, col)) {
                    List<CollisionCircle> list = get(row, col);
                    int listSize = list.size();
                    for (int i = 0; i < listSize; i++) {
                        CollisionCircle target = list.get(i);
                        if (target == circle) continue;
                        queryResult.add(target);
                    }
                }
            }
        }
        return queryResult;
    }

    /**
     * Clears the content of each cell in the grid.
     * All CollisionCircle objects are removed from the grid space partition.
     */
    public void clear() {
        for (int row = 0; row < numRows(); row++) {
            for (int col = 0; col < numCols(); col++) {
                get(row, col).clear();
            }
        }
    }

    private BoundingBoxCells getBoundingBoxCells(Vector2 position, float radius) {
        int minCellRow = (int) ((position.y - radius) / cellSize);
        int minCellCol = (int) ((position.x - radius) / cellSize);
        int maxCellRow = (int) ((position.y + radius) / cellSize);
        int maxCellCol = (int) ((position.x + radius) / cellSize);
        boolean inBounds = maxCellRow >= 0 || maxCellCol >= 0 || minCellRow >= rows || maxCellCol >= columns;

        minCellRow = Math.max(minCellRow, 0);
        minCellCol = Math.max(minCellCol, 0);
        maxCellRow = Math.min(maxCellRow, rows);
        maxCellCol = Math.min(maxCellCol, columns);

        return new BoundingBoxCells(minCellRow, minCellCol, maxCellRow, maxCellCol, inBounds);
    }

    private BoundingBoxCells getBoundingBoxCells(CollisionCircle circle) {
        return getBoundingBoxCells(circle.getPosition(), circle.radius());
    }
    
    private record BoundingBoxCells(int minRow, int minCol, int maxRow, int maxCol, boolean inBounds) { }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SpacePartition that)) return false;
        if (Float.compare(that.cellSize, cellSize) != 0) return false;

        if (((SpacePartition) o).rows != rows) return false;
        if (((SpacePartition) o).columns != columns) return false;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (!get(i, j).equals(((SpacePartition) o).get(i, j))) return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = 17; // Prime number to start the calculation
        result = 31 * result + Float.floatToIntBits(cellSize); // Include cellSize in the hash code
        result = 31 * result + rows; // Include rows in the hash code
        result = 31 * result + columns; // Include columns in the hash code

        // Include the hash code for each cell in the grid
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                result = 31 * result + (get(i, j) != null ? get(i, j).hashCode() : 0);
            }
        }

        return result;
    }
}

