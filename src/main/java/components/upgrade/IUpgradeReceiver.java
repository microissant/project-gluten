package components.upgrade;

import components.upgrade.behavior.IUpgradableBehavior;
import components.upgrade.upgrades.currency.IUpgradableCurrency;
import components.upgrade.upgrades.health.IUpgradableHealth;
import components.upgrade.upgrades.physics.IUpgradablePhysics;
import components.upgrade.upgrades.weapons.IUpgradableWeaponManager;

/**
 * Interface for objects that stores upgradable objects
 */
public interface IUpgradeReceiver {

    /**
     * Get IUpgradableHealth object which can be upgraded
     * with various properties such as speed
     *
     * @return the upgradable health object, or {@code null} if the
     * upgradableHealth is not set
     */
    IUpgradableHealth getUpgradableHealth();

    /**
     * Get an IUpgradablePhysics object which can be upgraded
     * witch various properties such as speed
     *
     * @return the upgradable physics object, or {@code null} if the
     * upgradablePhysics is not set
     */
    IUpgradablePhysics getUpgradablePhysics();

    IUpgradableCurrency getUpgradableCurrency();

    IUpgradableWeaponManager getUpgradableWeaponManager();

    IUpgradableBehavior getUpgradableBehavior();
}
