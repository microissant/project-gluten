package components.upgrade.behavior;

import components.upgrade.behavior.behaviors.IBehavior;
import entities.Entity;
import model.IUpdatable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The BehaviorCollection class is an implementation of the IUpgradableBehavior interface
 * that manages a collection of behaviors for a game entity.
 * <p>
 * This class provides methods to add, remove, and execute behaviors for an entity. It
 * stores the behaviors in a map, where each behavior is associated with a unique BehaviorID.
 * This allows for a modular and flexible approach to implementing entity upgrades and
 * enhancements.
 * </p>
 *
 * <p>
 * Example usage:
 * 1. Create an instance of BehaviorCollection, passing the entity to the constructor.
 * 2. Add behaviors to the collection using the addBehavior method.
 * 3. Execute a behavior when needed, using the execute method.
 * </p>
 */
public class BehaviorCollection implements IUpgradableBehavior, IUpdatable {

    private final Map<BehaviorID, List<IBehavior>> behaviors;
    private final Entity entity;

    /**
     * Constructs a new BehaviorCollection for the specified entity.
     *
     * @param entity The entity whose behaviors are managed by this collection.
     */
    public BehaviorCollection(Entity entity) {
        behaviors = new HashMap<>();
        this.entity = entity;
    }

    /**
     * Adds a behavior to the collection, associating it with the specified BehaviorID.
     *
     * <p>
     * If the BehaviorID is not already in the collection, a new list of behaviors is
     * created and added to the map.
     * </p>
     *
     * @param behaviorID The unique identifier for the behavior.
     * @param behavior The behavior to be added to the collection.
     */
    @Override
    public void addBehavior(BehaviorID behaviorID, IBehavior behavior) {
        if (!behaviors.containsKey(behaviorID)) behaviors.put(behaviorID, new ArrayList<>());

        behaviors.get(behaviorID).add(behavior);
    }

    /**
     * Removes a behavior from the collection, given its associated BehaviorID.
     *
     * If the BehaviorID is not present in the collection, the method returns without
     * performing any action.
     *
     * @param behaviorID The unique identifier for the behavior.
     * @param behavior The behavior to be removed from the collection.
     */
    @Override
    public void removeBehavior(BehaviorID behaviorID, IBehavior behavior) {
        if (!behaviors.containsKey(behaviorID)) return;

        behaviors.get(behaviorID).remove(behavior);
    }

    /**
     * Executes all the behaviors associated with the specified BehaviorID.
     *
     * If the BehaviorID is not present in the collection, the method returns without
     * performing any action.
     *
     * @param behaviorID The unique identifier for the behavior(s) to be executed.
     */
    public void execute(BehaviorID behaviorID) {
        if (!behaviors.containsKey(behaviorID)) return;

        for (IBehavior behavior: behaviors.get(behaviorID)) {
            behavior.execute(entity);
        }
    }

    public void update(double deltaTime) {
        for (BehaviorID behaviorID: behaviors.keySet()) {
            for (IBehavior behavior: behaviors.get(behaviorID)) {
                behavior.update(deltaTime);
            }
        }
    }

}
