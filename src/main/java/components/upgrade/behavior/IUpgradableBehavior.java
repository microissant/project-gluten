package components.upgrade.behavior;

import components.upgrade.IUpgradable;
import components.upgrade.behavior.behaviors.IBehavior;

/**
 * The IUpgradableBehavior interface defines the methods required for managing
 * behaviors in an upgradable entity. This interface extends the IUpgradable
 * interface, which represents a general upgrade system.
 *
 * <p>
 * Classes implementing this interface should provide methods to add, remove,
 * and execute behaviors. The behaviors are associated with a unique BehaviorID,
 * allowing for a modular and flexible approach to implementing entity upgrades
 * and enhancements.
 * </p>
 *
 * Example usage:
 * 1. Create a class implementing IUpgradableBehavior, such as BehaviorCollection.
 * 2. Use the addBehavior and removeBehavior methods to manage behaviors for an entity.
 * 3. Execute behaviors as needed, depending on the implementation.
 */
public interface IUpgradableBehavior extends IUpgradable {

    /**
     * Adds a behavior to the upgradable entity, associating it with the specified
     * BehaviorID.
     *
     * @param behaviorID The unique identifier for the behavior.
     * @param behavior The behavior to be added to the upgradable entity.
     */
    void addBehavior(BehaviorID behaviorID, IBehavior behavior);

    /**
     * Removes a behavior from the upgradable entity, given its associated BehaviorID.
     *
     * @param behaviorID The unique identifier for the behavior.
     * @param behavior The behavior to be removed from the upgradable entity.
     */
    void removeBehavior(BehaviorID behaviorID, IBehavior behavior);

}
