package components.upgrade.behavior;

/**
 * The BehaviorID enum defines a set of unique identifiers for behaviors that can
 * be added to an entity in the game.
 *
 * <p>
 * Each identifier corresponds to a specific behavior, such as dashing. The identifiers
 * are used to manage behaviors in a BehaviorCollection, allowing for easy addition,
 * removal, and execution of behaviors.
 * </p>
 *
 * <p>
 * To add a new behavior to the system, create a new enum value in this list, and
 * implement the corresponding behavior class that extends the IBehavior interface.
 * </p>
 *
 * Example usage:
 * 1. Define a new BehaviorID for a custom behavior.
 * 2. Implement a class for the custom behavior, extending IBehavior.
 * 3. Add the custom behavior to a BehaviorCollection using the addBehavior method.
 */
public enum BehaviorID {

    DASH,
    HIT,

}
