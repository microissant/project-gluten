package components.upgrade.behavior.behaviors;

import components.collision.CollisionType;
import components.weapon.magazine.Magazine;
import components.weapon.projectile.ProjectileTypes;
import components.weapon.projectile.emitter.IProjectileEmitter;
import components.weapon.projectile.emitter.StandardEmitter;
import entities.Entity;
import model.event.EventBus;

/**
 * The FireRocketsInDirection class is an implementation of the IBehavior interface.
 * This behavior fires rockets in the direction of the specified entity when executed.
 *
 * <p>
 * This class is designed to work with a behavior collection that manages the addition,
 * removal, and execution of behaviors for an entity. This allows for a modular and
 * flexible approach to implementing entity upgrades and enhancements.
 * </p>
 *
 * Example usage:
 * 1. Create an instance of FireRocketsInDirection, passing the EventBus to the constructor.
 * 2. Add the instance to an entity's behavior collection.
 * 3. Execute the behavior when needed, using the behavior collection's execute method.
 */
public class FireRocketsInDirection implements IBehavior {

    private final EventBus eventBus;

    /**
     * Constructs a new FireRocketsInDirection behavior with the specified EventBus.
     *
     * @param eventBus The EventBus to be used for firing projectiles.
     */
    public FireRocketsInDirection(EventBus eventBus) {
        this.eventBus = eventBus;
    }

    /**
     * Executes the behavior by firing rockets in the direction of the specified entity.
     *
     * <p>
     * This method creates a projectile emitter, sets up a magazine with the desired
     * number of rockets, and emits the projectiles in the entity's direction.
     * The projectiles are assigned a collision type of ENEMY.
     * </p>
     *
     * @param entity The entity from which the rockets will be fired.
     */
    @Override
    public void execute(Entity entity) {
        if (eventBus == null) return;
        IProjectileEmitter emitter = new StandardEmitter(CollisionType.Type.ENEMY, eventBus);
        emitter.emitProjectiles(new Magazine(10, 10, null), entity.getDirection().scl(-1), entity.getPosition(), ProjectileTypes.Rocket,1);
    }
}