package components.upgrade.behavior.behaviors;

import entities.Entity;
import model.IUpdatable;

/**
 * The IBehavior interface represents a behavior that can be executed on a game entity.
 * This interface should be implemented by various behavior classes to provide specific
 * functionality to entities, such as firing projectiles or increasing speed.
 *
 * <p>
 * This interface is designed to be used with a behavior collection class that manages
 * the addition, removal, and execution of behaviors for an entity. This allows for
 * a modular and flexible approach to implementing entity upgrades and enhancements.
 * </p>
 *
 * Example usage:
 * 1. Create a class that implements IBehavior, providing the desired functionality.
 * 2. Add the behavior to an entity's behavior collection.
 * 3. Execute the behavior when needed, using the behavior collection's execute method.
 */
public interface IBehavior extends IUpdatable {

    /**
     * Executes the behavior on the specified entity.
     *
     * <p>
     * This method should be implemented to provide the desired functionality for the
     * behavior. The implementation may modify the entity's properties or interact with
     * other game components.
     * </p>
     *
     * @param entity The entity on which the behavior is to be executed.
     */
    void execute(Entity entity);
}