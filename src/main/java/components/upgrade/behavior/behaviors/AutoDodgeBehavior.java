package components.upgrade.behavior.behaviors;

import com.badlogic.gdx.math.Vector2;

import entities.Entity;
import model.event.EventBus;
import model.event.events.player.DodgeInDirectionEvent;
import util.CountdownTimer;

/**
 * The AutoDodgeBehavior class is an implementation of the IBehavior interface.
 * This behavior applies a dodge in a specified direction when executed.
 *
 * <p>
 * This class is designed to work with a behavior collection that manages the addition,
 * removal, and execution of behaviors for an entity. This allows for a modular and
 * flexible approach to implementing entity upgrades and enhancements.
 * </p>
 *
 * Example usage:
 * 1. Create an instance of AutoDodgeBehavior, passing the EventBus to the constructor.
 * 2. Add the instance to an entity's behavior collection.
 * 3. Execute the behavior when needed, using the behavior collection's execute method.
 */
public class AutoDodgeBehavior implements IBehavior {

    private final EventBus eventBus;
    private final CountdownTimer timer;
    private boolean canDodge = true;

    /**
     * Constructs a new AutoDodgeBehavior with the specified EventBus and cooldown time.
     * @param eventBus The EventBus to be used for dodging.
     * @param cooldownTime The cooldown time between automatic dodges.
     */
    public AutoDodgeBehavior(EventBus eventBus, float cooldownTime) {
        this.eventBus = eventBus;
        timer = new CountdownTimer(cooldownTime, () -> canDodge = true);
    }

    /**
     * Executes the behavior by applying a dodge in the specified direction.
     *
     * <p>
     * This method applies a dodge in the specified direction, and resets the cooldown timer.
     * </p>
     *
     * @param entity The entity that will perform the dodge.
     */
    @Override
    public void execute(Entity entity) {
        if (eventBus == null) return;
        if (entity == null) return;
        if (!canDodge) return;

        int healthAfterDamage = entity.getHealthComponent().getCurrentHealth() - entity.damageTaken().getDamageValue();
        if (healthAfterDamage > 0) return;
        Vector2 direction = new Vector2(1, -3);
        eventBus.post(new DodgeInDirectionEvent(direction));

        canDodge = false;
        timer.restart();
    }

    /**
     * Updates the behavior's internal timer.
     *
     * @param deltaTime The time since the last frame.
     */
    public void update(double deltaTime) {
        timer.update(deltaTime);
    }
}