package components.upgrade.upgrades;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import components.upgrade.IUpgradeReceiver;
import media.MediaManager;
import media.TextureID;
import model.IUpdatable;

/**
 * Common interface for all upgrades, defining their base type
 */
public interface IUpgrade extends IUpdatable {

    /**
     * <p>Apply the effects on the upgrade to the upgradable object</p>
     *
     * <p>If upgrade has already been applied to a upgradable object, return false</p>
     *
     * @param upgradeReceiver manager holding the upgradable objects
     */
    boolean apply(IUpgradeReceiver upgradeReceiver);

    /**
     * <p>Revert the upgrade if the upgrade has previously been applied</p>
     *
     * <p>Do nothing if the upgrade has already been reverted or has
     * never been applied</p>
     */
    void revert();

    /**
     *
     * @return true if the upgrade has been applied to an upgradable object
     */
    boolean isApplied();

    /**
     * @return true if the upgrade should only be applied once, false if it can
     * be applied multiple times
     */
    default boolean applyOnlyOnce() { return true; }

    /**
     *
     * @return title of the upgrade
     */
    String getTitle();

    /**
     * @return description of the upgrade
     */
    String getDescription();

    /**
     * @return image representing the upgrade. Is displayed on purchase menu and upgrade-hud
     */
    default TextureRegion getIcon() {
        return MediaManager.getTexture(TextureID.ICE_CREAM).getKeyFrame(0);
    }

    /**
     * Time to live determines how long a upgrade should be applied to the entity.
     *
     * @return a positive number that indicates how long it should last. If set to -1, it is permanent.
     */
    default float getTimeToLive() { return -1.0f;}

    /**
     * @return remaining time to live before it should be removed.
     */
    default float getTimeRemaining() { return Float.MAX_VALUE; }
}
