package components.upgrade.upgrades.weapons;

import components.upgrade.IUpgradable;

public interface IUpgradableWeaponManager extends IUpgradable {

    void enableUnlimitedAmmo();

    void disableUnlimitedAmmo();



}
