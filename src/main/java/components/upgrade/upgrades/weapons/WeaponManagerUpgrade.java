package components.upgrade.upgrades.weapons;

import components.upgrade.IUpgradeReceiver;
import components.upgrade.upgrades.Upgrade;

public abstract class WeaponManagerUpgrade extends Upgrade<IUpgradableWeaponManager> {

    @Override
    protected IUpgradableWeaponManager getUpgradable(IUpgradeReceiver upgradableHolder) {
        if (upgradableHolder == null) return null;
        return upgradableHolder.getUpgradableWeaponManager();
    }


}
