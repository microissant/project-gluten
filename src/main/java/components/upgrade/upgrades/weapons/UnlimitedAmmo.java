package components.upgrade.upgrades.weapons;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import components.upgrade.IUpgradeReceiver;
import media.MediaManager;
import media.TextureID;

public class UnlimitedAmmo extends WeaponManagerUpgrade {

    public UnlimitedAmmo(float timeToLive) {
        this.timeToLive = timeToLive;
    }

    @Override
    public boolean apply(IUpgradeReceiver upgradeReceiver) {
        if (cantBeApplied(upgradeReceiver)) return false;
        target.enableUnlimitedAmmo();
        return true;
    }

    @Override
    public void revert() {
        if (!isApplied()) return;
        target.disableUnlimitedAmmo();
        target = null;
    }

    @Override
    public TextureRegion getIcon() {
        return MediaManager.getTexture(TextureID.ITEM_ICONS).getKeyFrame(3);
    }

    @Override
    public String getTitle() {
        return "Gun rhymes with fun";
    }

    @Override
    public String getDescription() {
        return "Unlimited ammunition";
    }
}
