package components.upgrade.upgrades;

import components.upgrade.IUpgradable;
import components.upgrade.IUpgradeReceiver;

public abstract class Upgrade <E extends IUpgradable> implements IUpgrade {

    protected float timeLived;
    protected float timeToLive;
    protected E target;

    public Upgrade() {
        timeLived = 0;
        this.timeToLive = -1;
    }

    @Override
    public boolean isApplied() {
        return target != null;
    }

    @Override
    public void update(double deltaTime) {
        timeLived += deltaTime;
    }

    /**
     * <p>Check if the upgrade can be applied to the upgradable object.</p>
     *
     * <p>Can not be applied if the upgradable is null or the upgrade is already applied</p>
     *
     * @param upgradableHolder object holding all upgradable objects
     * @return true if upgrade can be applied, false otherwise
     */
    protected boolean cantBeApplied(IUpgradeReceiver upgradableHolder) {
        if (isApplied() && applyOnlyOnce()) return true;
        target = getUpgradable(upgradableHolder);
        return target == null;
    }

    /**
     * <p>Retrieve the correct upgradable object if it is available</p>
     *
     * @param upgradableHolder object holding all upgradable objects
     * @return upgradable object, or {@code null} if upgradableHolder or upgradable is null
     */
    protected abstract E getUpgradable(IUpgradeReceiver upgradableHolder);

    @Override
    public float getTimeToLive() {
        return timeToLive;
    }

    @Override
    public float getTimeRemaining() {
        return timeToLive - timeLived;
    }
}
