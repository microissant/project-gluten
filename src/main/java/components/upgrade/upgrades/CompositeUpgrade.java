package components.upgrade.upgrades;

import components.upgrade.IUpgradeReceiver;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for combining multiple individual and/or different upgrade types in one bigger upgrade
 */
public class CompositeUpgrade implements IUpgrade {

    private final List<IUpgrade> upgrades;
    private final String title;
    private final String description;

    public CompositeUpgrade(List<IUpgrade> upgrades, String title, String description) {
        if (upgrades == null) upgrades = new ArrayList<>();
        this.upgrades = upgrades;
        this.title = title;
        this.description = description;
    }

    @Override
    public boolean apply(IUpgradeReceiver upgradeReceiver) {
        if (isApplied()) return false;

        for (IUpgrade upgrade: upgrades) {
            upgrade.apply(upgradeReceiver);
        }
        return true;
    }

    @Override
    public void revert() {
        for (IUpgrade upgrade: upgrades) {
            upgrade.revert();
        }
    }

    @Override
    public boolean isApplied() {
        boolean hasBeenApplied = false;
        for (IUpgrade upgrade: upgrades) {
            hasBeenApplied |= upgrade.isApplied();
        }
        return hasBeenApplied;
    }

    @Override
    public String getTitle() {
        if (title == null) return "Composite upgrade";
        return title;
    }

    @Override
    public String getDescription() {
        if (description == null) return "Composite upgrade with multiple upgrades";
        return description;
    }

}
