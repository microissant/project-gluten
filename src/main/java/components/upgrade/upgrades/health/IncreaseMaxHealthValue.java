package components.upgrade.upgrades.health;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import components.upgrade.IUpgradeReceiver;
import media.MediaManager;
import media.TextureID;

import java.util.Objects;

public class IncreaseMaxHealthValue extends HealthUpgrade {
    private final int deltaMaxHealth;

    public IncreaseMaxHealthValue(int deltaMaxHealth) {
        this.deltaMaxHealth = deltaMaxHealth;
    }

    @Override
    public boolean apply(IUpgradeReceiver upgradeReceiver) {
        if (cantBeApplied(upgradeReceiver)) return false;

        target.increaseMaximumHealth(deltaMaxHealth);
        return true;
    }

    @Override
    public void revert() {
        if (target == null) return;
        target.decreaseMaximumHealth(deltaMaxHealth);
        target = null;
    }

    @Override
    public String getTitle() {
        return "Iron skin";
    }

    @Override
    public String getDescription() {
        return "Increase maximum health by " + deltaMaxHealth;
    }

    @Override
    public TextureRegion getIcon() {
        return MediaManager.getTexture(TextureID.ITEM_ICONS).getKeyFrame(5);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IncreaseMaxHealthValue that)) return false;
        if (!super.equals(o)) return false;
        return deltaMaxHealth == that.deltaMaxHealth;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), deltaMaxHealth);
    }
}
