package components.upgrade.upgrades.health;

import components.upgrade.IUpgradeReceiver;
import components.upgrade.upgrades.Upgrade;

public abstract class HealthUpgrade extends Upgrade<IUpgradableHealth> {

    @Override
    protected IUpgradableHealth getUpgradable(IUpgradeReceiver upgradableHolder) {
        if (upgradableHolder == null) return null;
        return upgradableHolder.getUpgradableHealth();
    }
}
