package components.upgrade.upgrades.health;

import components.health.Damage;
import components.health.IHealable;
import components.upgrade.IUpgradable;

public interface IUpgradableHealth extends IUpgradable, IHealable {

    /**
     * <p>Increase the maximum health by the input amount.</p>
     *
     * <p>Increasing the maximum health does not affect the current health value</p>
     *
     * <p>Value must be positive, or it will not be applied.
     * Instead, to increase maximum health, try `increaseMaximumHealth`.</p>
     *
     * @param deltaValue amount to decrease maximum health by.
     */
    void increaseMaximumHealth(int deltaValue);

    /**
     * <p>Decrease the maximum health by the input amount.</p>
     *
     * <p>If maximum health is decreased below the current health,
     * current health will be adjusted to the new maximum health.</p>
     *
     * <p>Value must be positive, or it will not be applied.
     * Instead, to increase maximum health, try `increaseMaximumHealth`.</p>
     *
     * @param deltaValue amount to decrease maximum health by.
     */
    void decreaseMaximumHealth(int deltaValue);

    /**
     * <p>Enable immunity against damage.</p>
     *
     * <p>When immune, no damage will be applied to object</p>
     */
    void enableImmunity();


    /**
     * <p>Disable immunity against damage.</p>
     *
     * <p>When immune, no damage will be applied to object</p>
     */
    void disableImmunity();

    /**
     * Apply damage to the object.
     *
     * @param damage assigned to apply the damage
     */
    int applyDamage(Damage damage);

}
