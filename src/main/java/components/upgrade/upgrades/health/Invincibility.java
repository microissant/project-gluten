package components.upgrade.upgrades.health;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import components.upgrade.IUpgradeReceiver;
import media.MediaManager;
import media.TextureID;

public class Invincibility extends HealthUpgrade {

    public Invincibility() {
        this(-1);
    }

    public Invincibility(float timeToLive) {
        this.timeToLive = timeToLive;
    }

    @Override
    public boolean apply(IUpgradeReceiver upgradeReceiver) {
        if (cantBeApplied(upgradeReceiver)) return false;

        target.enableImmunity();
        return true;
    }

    @Override
    public void revert() {
        if (!isApplied()) return;
        target.disableImmunity();
        target = null;
    }

    @Override
    public String getTitle() {
        return "Invincibility";
    }

    @Override
    public String getDescription() {
        return "Turn invincible, unable to take damage.";
    }

    @Override
    public TextureRegion getIcon() {
        return MediaManager.getTexture(TextureID.ITEM_ICONS).getKeyFrame(8);
    }
}
