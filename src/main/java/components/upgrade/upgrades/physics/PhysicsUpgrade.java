package components.upgrade.upgrades.physics;

import components.upgrade.IUpgradeReceiver;
import components.upgrade.upgrades.Upgrade;

public abstract class PhysicsUpgrade extends Upgrade<IUpgradablePhysics> {

    @Override
    protected IUpgradablePhysics getUpgradable(IUpgradeReceiver upgradableHolder) {
        if (upgradableHolder == null) return null;
        return upgradableHolder.getUpgradablePhysics();
    }

}
