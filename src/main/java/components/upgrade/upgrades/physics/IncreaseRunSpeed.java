package components.upgrade.upgrades.physics;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import components.upgrade.IUpgradeReceiver;
import media.MediaManager;
import media.TextureID;

import java.util.Objects;

public class IncreaseRunSpeed extends PhysicsUpgrade {

    private final float deltaSpeed;

    public IncreaseRunSpeed(float deltaSpeed) {
        this.deltaSpeed = (deltaSpeed > 0) ? deltaSpeed : 0;
    }

    @Override
    public boolean apply(IUpgradeReceiver upgradeReceiver) {
        if (cantBeApplied(upgradeReceiver)) return false;

        target.increaseMaxSpeed(deltaSpeed);
        return true;
    }

    @Override
    public void revert() {
        if (!isApplied()) return;
        target.decreaseMaxSpeed(deltaSpeed);
        target = null;
    }

    @Override
    public String getTitle() {
        return "Speedy delivery";
    }

    @Override
    public String getDescription() {
        return "Increase maximum movement speed by " + deltaSpeed + ".";
    }

    @Override
    public TextureRegion getIcon() {
        return MediaManager.getTexture(TextureID.ITEM_ICONS).getKeyFrame(18);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IncreaseRunSpeed that)) return false;
        return Float.compare(that.deltaSpeed, deltaSpeed) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(deltaSpeed);
    }
}
