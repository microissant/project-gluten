package components.upgrade.upgrades.physics;

import components.upgrade.IUpgradable;

public interface IUpgradablePhysics extends IUpgradable {

    /**
     * <p>Increase the maximum movement velocity, limiting the maximum speed
     * the object can travel</p>
     *
     * <p>If deltaMaxSpeed is negative, value will be set to 0. To decrease speed,
     * try the decreaseMaxSpeed method</p>
     *
     * @param deltaMaxSpeed change in speed. Must be a positive value
     */
    void increaseMaxSpeed(float deltaMaxSpeed);


    /**
     * <p>Decrease the maximum movement velocity, limiting the maximum speed
     * the object can travel</p>
     *
     * <p>If deltaMaxSpeed is negative, value will be set to 0. To increase speed,
     * try the increaseMaxSpeed method</p>
     *
     * @param deltaMaxSpeed change in speed. Must be a positive value
     */
    void decreaseMaxSpeed(float deltaMaxSpeed);


}
