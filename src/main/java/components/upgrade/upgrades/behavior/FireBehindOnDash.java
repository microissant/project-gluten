package components.upgrade.upgrades.behavior;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import components.upgrade.IUpgradeReceiver;
import components.upgrade.behavior.BehaviorID;
import components.upgrade.behavior.behaviors.FireRocketsInDirection;
import components.upgrade.behavior.behaviors.IBehavior;
import media.MediaManager;
import media.TextureID;
import model.event.EventBus;

public class FireBehindOnDash extends BehaviorUpgrade {

    final IBehavior behavior;

    public FireBehindOnDash(EventBus eventBus) {
        behavior = new FireRocketsInDirection(eventBus);
    }

    @Override
    public boolean apply(IUpgradeReceiver upgradeReceiver) {
        if (cantBeApplied(upgradeReceiver)) return false;

        target.addBehavior(BehaviorID.DASH, behavior);

        return true;
    }
    


    @Override
    public TextureRegion getIcon() {
        return MediaManager.getTexture(TextureID.ITEM_ICONS).getKeyFrame(16);
    }

    @Override
    public void revert() {
        if (target == null) return;
        target.removeBehavior(BehaviorID.DASH, behavior);
        target = null;
    }

    @Override
    public String getTitle() {
        return "Cool guy";
    }

    @Override
    public String getDescription() {
        return "Fire a rocket behind you when you dash";
    }
}
