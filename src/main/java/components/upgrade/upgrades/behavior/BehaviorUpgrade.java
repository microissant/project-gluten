package components.upgrade.upgrades.behavior;

import components.upgrade.IUpgradeReceiver;
import components.upgrade.behavior.IUpgradableBehavior;
import components.upgrade.upgrades.Upgrade;

public abstract class BehaviorUpgrade extends Upgrade<IUpgradableBehavior> {

    @Override
    protected IUpgradableBehavior getUpgradable(IUpgradeReceiver upgradableHolder) {
        if (upgradableHolder == null) return null;
        return upgradableHolder.getUpgradableBehavior();
    }
}
