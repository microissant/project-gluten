package components.upgrade.upgrades.behavior;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import components.upgrade.IUpgradeReceiver;
import components.upgrade.behavior.BehaviorID;
import components.upgrade.behavior.behaviors.AutoDodgeBehavior;
import components.upgrade.behavior.behaviors.IBehavior;
import media.MediaManager;
import media.TextureID;
import model.event.EventBus;

/**
 * Upgrade that adds an automatic dodge behavior to the target
 */
public class AutoDodgeUpgrade extends BehaviorUpgrade {

    private final IBehavior behavior;
    private final float cooldownTime = 10.0f;

    /**
     * Creates a new AutoDodgeUpgrade with a default cooldown time.
     * 
     * @param eventBus The event bus to use for the behavior.
     */
    public AutoDodgeUpgrade(EventBus eventBus) {
        behavior = new AutoDodgeBehavior(eventBus, cooldownTime);
    }

    /**
     * Creates a new AutoDodgeUpgrade with a behavior lifetime.
     * 
     * @param eventBus  The event bus to use for the behavior.
     * @param timeToLive The time to live of the behavior upgrade.
     */
    public AutoDodgeUpgrade(EventBus eventBus, float timeToLive) {
        behavior = new AutoDodgeBehavior(eventBus, cooldownTime);
        this.timeToLive = timeToLive;
    }

    @Override
    public boolean apply(IUpgradeReceiver upgradeReceiver) {
        if (cantBeApplied(upgradeReceiver))
            return false;

        target.addBehavior(BehaviorID.HIT, behavior);

        return true;
    }

    @Override
    public void revert() {
        if (target == null) return;
        target.removeBehavior(BehaviorID.HIT, behavior);
        target = null;
    }


    @Override
    public TextureRegion getIcon() {
        return MediaManager.getTexture(TextureID.ITEM_ICONS).getKeyFrame(7);
    }


    @Override
    public String getTitle() {
        return "Can't touch this";
    }

    @Override
    public String getDescription() {
        return "Automatically dodge upon receiving fatal damage. Cooldown: " + String.format("%.1f", cooldownTime)
                + "s";
    }
}
