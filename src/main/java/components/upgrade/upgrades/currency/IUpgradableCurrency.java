package components.upgrade.upgrades.currency;

import components.upgrade.IUpgradable;

public interface IUpgradableCurrency extends IUpgradable {


    /**
     * <p>
     *     Add a percentage multiplier to the money gained when depositing.
     * </p>
     *
     * <p>
     *     The total number of coins deposited is multiplied with a
     *     percentage value, called gainBuff.  This percentage value
     *     is by default 100.
     * </p>
     *
     * <p>
     *     When adding a percentage value to the gainBuff, the value
     *     is added to the current gainBuff: <br>
     *     gainBuff = previousGainBuff - decrease <br>
     *     where increase = added percentage value
     * </p>
     *
     * <p>
     *     <p>
     *         <b>How the gain buff is applied</b>
     *     </p>
     *     Let gainBuff = 150.0 (the total increased gains),
     *     balance = 10, and deposit = 10 </br>
     *
     *     The final balance is then: <br>
     *     balance + deposit * (gainBuff / 100) = <br>
     *     10 + 10 * 1.50 = 25
     * </p>
     *
     * @param deltaPercentValue how much more money is gained
     */
    void increaseGains(float deltaPercentValue);

    /**
     * <p>
     *     Add a negative percentage multiplier to the money gained
     *     when depositing. Lower-limit of gainBuff is 0 (i.e 0%)
     * </p>
     *
     * <p>
     *     The total number of coins deposited is multiplied with a
     *     percentage value, called gainBuff.  This percentage value
     *     is by default 100.
     * </p>
     *
     * <p>
     *     When adding a percentage value to the gainBuff, the value
     *     is added to the current gainBuff: <br>
     *     gainBuff = previousGainBuff - decrease <br>
     *     where increase = added percentage value
     * </p>
     *
     * <p>
     *     <p>
     *         <b>How the gain buff is applied</b>
     *     </p>
     *     Let gainBuff = 150.0 (the total increased gains),
     *     balance = 10, and deposit = 10 </br>
     *
     *     The final balance is then: <br>
     *     balance + deposit * (gainBuff / 100) = <br>
     *     10 + 10 * 1.50 = 25
     * </p>
     *
     * @param deltaPercentValue how many percent the gains is reduced by
     */
    void decreaseGains(float deltaPercentValue);

    /**
     * Set the gainBuff to 100, it's default value
     */
    void resetGainBuff();

}
