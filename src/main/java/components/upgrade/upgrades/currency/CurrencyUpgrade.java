package components.upgrade.upgrades.currency;

import components.upgrade.IUpgradeReceiver;
import components.upgrade.upgrades.Upgrade;

public abstract class CurrencyUpgrade extends Upgrade<IUpgradableCurrency> {

    @Override
    protected IUpgradableCurrency getUpgradable(IUpgradeReceiver upgradableHolder) {
        if (upgradableHolder == null) return null;
        return upgradableHolder.getUpgradableCurrency();
    }
}
