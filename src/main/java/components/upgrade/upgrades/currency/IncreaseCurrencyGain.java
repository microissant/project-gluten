package components.upgrade.upgrades.currency;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import components.upgrade.IUpgradeReceiver;
import media.MediaManager;
import media.TextureID;

public class IncreaseCurrencyGain extends CurrencyUpgrade {

    private final float increaseValue;

    public IncreaseCurrencyGain(float value, float timeToLive) {
        this(value);
        super.timeToLive = timeToLive;
    }

    public IncreaseCurrencyGain(float value) {
        increaseValue = value;
    }

    @Override
    public boolean apply(IUpgradeReceiver upgradeReceiver) {
        if (cantBeApplied(upgradeReceiver)) return false;

        target.increaseGains(increaseValue);

        return true;
    }

    @Override
    public void revert() {
        if (target == null) return;
        target.decreaseGains(increaseValue);
        target = null;
    }

    @Override
    public String getTitle() {
        return "Time is money";
    }

    @Override
    public String getDescription() {
        return "Increase coins earned by: " + increaseValue + "%";
    }


    @Override
    public TextureRegion getIcon() {
        return MediaManager.getTexture(TextureID.ITEM_ICONS).getKeyFrame(0);
    }

}
