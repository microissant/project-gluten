package components.upgrade;

import components.upgrade.upgrades.IUpgrade;

public interface IUpgradableEntity {

    /**
     * Add permanent upgrade to the entity
     *
     * @param upgrade upgrade to be added
     * @return true if successfully applied the upgrade
     */
    boolean addUpgrade(IUpgrade upgrade);

    /**
     * Remove a specific upgrade from the currently active and permanent upgrades
     *
     * @param upgrade to be removed
     */
    void removeUpgrade(IUpgrade upgrade);

    /**
     * Remove all currently active upgrades.
     * Includes both permanent and temporary upgrades.
     */
    void removeAllUpgrades();
}
