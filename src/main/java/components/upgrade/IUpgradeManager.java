package components.upgrade;

import components.upgrade.behavior.IUpgradableBehavior;
import components.upgrade.upgrades.IUpgrade;

/**
 * Upgrade manager handles all the upgrades that is applied and applies the upgrade to the correct
 * upgradable object
 */
public interface IUpgradeManager extends Iterable<IUpgrade> {

    /**
     * <p>Add a permanent upgrade and apply its effect to the upgradable object</p>
     *
     * <p>Permanent upgrades can only be removed through explisitly calling the "removeUpgrade" method</p>
     *
     * @param upgrade to be applied to the upgradable object
     * @return true if upgrade successfully has been applied, otherwise false
     */
    boolean addUpgrade(IUpgrade upgrade);

    /**
     * <p>Remove a permanent upgrade if it is active</p>
     *
     * @param upgrade to be removed
     */
    void removeUpgrade(IUpgrade upgrade);

    /**
     * <p>Update the timer of all temporary effect that are only active a certain time</p>
     *
     * <p>The upgrades with a depleated timer are removed. Their effects are also reversed on the
     * upgraded </p>
     * @param deltaTime time since last update (in seconds)
     */
    void update(double deltaTime);

    /**
     * Remove all permanent and temporary upgrades
     */
    void removeAll();

    /**
     * @return the total number of permanent upgrades currently active
     */
    int permanentUpgradesCount();

    void setUpgradableBehavior(IUpgradableBehavior upgradableBehavior);
}
