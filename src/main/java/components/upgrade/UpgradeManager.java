package components.upgrade;

import components.upgrade.behavior.IUpgradableBehavior;
import components.upgrade.upgrades.IUpgrade;
import components.upgrade.upgrades.currency.IUpgradableCurrency;
import components.upgrade.upgrades.health.IUpgradableHealth;
import components.upgrade.upgrades.physics.IUpgradablePhysics;
import components.upgrade.upgrades.weapons.IUpgradableWeaponManager;
import model.event.EventBus;
import model.event.events.upgrades.UpgradeRemovedEvent;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * <p>Class for managing all upgrades.</p>
 *
 * <p>Can handle both permanent and temporary upgrades</p>
 *
 * <p>Must be updated each time unit for temporary upgrades to be reversed correctly</p>
 */
public class UpgradeManager implements IUpgradeManager, IUpgradeReceiver {

    private final List<IUpgrade> permanentUpgrades;
    private final List<IUpgrade> temporaryUpgrades;
    private final List<IUpgrade> toBeRemovedTemporary;

    private IUpgradableHealth upgradableHealth;
    private IUpgradablePhysics upgradablePhysics;
    private IUpgradableWeaponManager upgradableWeaponManager;
    private IUpgradableCurrency upgradableCurrency;
    private IUpgradableBehavior upgradableBehavior;
    private final EventBus eventBus;

    public UpgradeManager(
            IUpgradableHealth upgradableHealth,
            IUpgradablePhysics upgradablePhysics,
            IUpgradableWeaponManager upgradableWeaponManager,
            IUpgradableCurrency upgradableCurrency,
            EventBus eventBus) {
        this.upgradableHealth = upgradableHealth;
        this.upgradablePhysics = upgradablePhysics;
        this.upgradableWeaponManager = upgradableWeaponManager;
        this.upgradableCurrency = upgradableCurrency;

        this.eventBus = eventBus;

        permanentUpgrades = new LinkedList<>();
        temporaryUpgrades = new LinkedList<>();
        toBeRemovedTemporary = new ArrayList<>();
    }

    public UpgradeManager(EventBus eventBus) {
        this(null, null, null, null, eventBus);
    }


    @Override
    public boolean addUpgrade(IUpgrade upgrade) {
        if (upgrade == null) return false;
        if (!upgrade.apply(this)) return false;

        if (upgrade.getTimeToLive() < 0) {
            permanentUpgrades.add(upgrade);
        } else {
            temporaryUpgrades.add(upgrade);
        }
        return true;
    }

    @Override
    public void removeUpgrade(IUpgrade upgrade) {
        if (upgrade == null) return;
        upgrade.revert();
        permanentUpgrades.remove(upgrade);
    }

    @Override
    public void update(double deltaTime) {
        for (IUpgrade upgrade: toBeRemovedTemporary) {
            upgrade.revert();
            if (eventBus == null) continue;
            eventBus.post(new UpgradeRemovedEvent(upgrade));
        }
        temporaryUpgrades.removeAll(toBeRemovedTemporary);

        for (IUpgrade upgrade: temporaryUpgrades) {
            upgrade.update(deltaTime);
            if (upgrade.getTimeRemaining() <= 0) {
                toBeRemovedTemporary.add(upgrade);
            }
        }
    }

    @Override
    public void removeAll() {
        permanentUpgrades.clear();
        temporaryUpgrades.clear();
    }

    @Override
    public int permanentUpgradesCount() {
        return permanentUpgrades.size();
    }


    @Override
    public IUpgradableHealth getUpgradableHealth() {
        return upgradableHealth;
    }

    @Override
    public IUpgradablePhysics getUpgradablePhysics() {
        return upgradablePhysics;
    }

    @Override
    public IUpgradableCurrency getUpgradableCurrency() {
        return upgradableCurrency;
    }

    @Override
    public IUpgradableWeaponManager getUpgradableWeaponManager() {
        return upgradableWeaponManager;
    }

    @Override
    public IUpgradableBehavior getUpgradableBehavior() {
        return upgradableBehavior;
    }

    public void setUpgradableHealth(IUpgradableHealth upgradableHealth) {
        this.upgradableHealth = upgradableHealth;
    }

    public void setUpgradablePhysics(IUpgradablePhysics upgradablePhysics) {
        this.upgradablePhysics = upgradablePhysics;
    }

    public void setUpgradableCurrency(IUpgradableCurrency upgradableCurrency) {
        this.upgradableCurrency = upgradableCurrency;
    }

    public void setUpgradableWeaponManager(IUpgradableWeaponManager upgradableWeaponManager) {
        this.upgradableWeaponManager = upgradableWeaponManager;
    }

    @Override
    public void setUpgradableBehavior(IUpgradableBehavior upgradableBehavior) {
        this.upgradableBehavior = upgradableBehavior;
    }

    @Override
    public Iterator<IUpgrade> iterator() {
        return new Iterator<>() {
            private int index = 0;
            private boolean inPermanentList = true;

            @Override
            public boolean hasNext() {
                if (inPermanentList) {
                    return index < permanentUpgrades.size() || !temporaryUpgrades.isEmpty();
                } else {
                    return index < temporaryUpgrades.size();
                }
            }

            @Override
            public IUpgrade next() {
                IUpgrade upgrade;

                if (inPermanentList) {
                    if (index < permanentUpgrades.size()) {
                        upgrade = permanentUpgrades.get(index);
                    } else {
                        index = 0;
                        inPermanentList = false;
                        upgrade = temporaryUpgrades.get(index);
                    }
                } else {
                    upgrade = temporaryUpgrades.get(index);
                }

                index++;
                return upgrade;
            }
        };
    }
}
