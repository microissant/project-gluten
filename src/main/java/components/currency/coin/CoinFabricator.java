package components.currency.coin;

/**
 * Class for creating and manipulating coins.
 *
 * <p>
 *     In addition to creating coins, it supports basic
 *     arithmetic operations with coins such as addition and subtraction.
 * </p>
 */
public class CoinFabricator {


    /**
     * Generate a new coin with 0 in value
     *
     * @return a coin with 0 in value
     */
    public Coin generateCoin(){
        return generateCoin(0);
    }

    /**
     * <p>
     *     Generate a new coin with a given value. If the value
     *     is a negative number, the returning coin will have 0
     *     in value
     * </p>
     *
     * @param value desired value of a new coin to be generated
     * @return A coin with the desired value, or if the value is
     * negative, a coin with 0 value.
     */
    public Coin generateCoin(int value){
        if (value < 0) return new Coin(0);
        return new Coin(value);
    }

    /**
     * <p>
     *     Generate a new coin where the value of the first last coin
     *     is subtracted from the value of the first coin.
     * </p>
     * <p>
     *     NewCoin value = CoinA value - CoinB value
     * </p>
     *
     * @param a first coin
     * @param b second coin
     * @return a coin with the value from coinA - CoinB
     */
    public Coin subtractCoins(Coin a, Coin b) {
        if (a == null && b == null) return generateCoin();
        if (a == null) return b;
        if (b == null) return a;

        return new Coin(a.getValue() - b.getValue());
    }

    /**
     * <p>
     *     Generate a new coin where the value of the two coins
     *     are added together
     * </p>
     * <p>
     *     NewCoin value = CoinA value + CoinB value
     * </p>
     *
     * @param a first coin
     * @param b second coin
     * @return a coin with the value from coinA + CoinB
     */
    public Coin addCoins(Coin a, Coin b) {
        if (a == null && b == null) return generateCoin();
        if (a == null) return b;
        if (b == null) return a;

        return new Coin(a.getValue() + b.getValue());
    }

    /**
     * <p>
     *     Generate a new coin where the value of the first last coin
     *     is subtracted from the value of the first coin.
     * </p>
     * <p>
     *     NewCoin value = CoinA value - value
     * </p>
     *
     * @param a first coin
     * @param value second coin
     * @return a coin with the value from coinA + value
     */
    public Coin addValueToCoin(Coin a, int value) {
        if (a == null) return null;
        if (value < 0) return a;

        return new Coin(a.getValue() + value);
    }
}
