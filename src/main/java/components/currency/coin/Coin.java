package components.currency.coin;

import java.util.Objects;

/**
 * Class for currency. Containing a value
 */
public class Coin {

    private final int value;
    private boolean isDepleted;

    public Coin(int value) {
        this.value = Math.max(value, 0);
        isDepleted = false;
    }

    Coin() {
        this(0);
    }

    /**
     * @return value of the coin
     */
    public int getValue() {
        return value;
    }

    /**
     * @return true if the coin has been marked depleted.
     */
    public boolean isDepleted() {
        return isDepleted;
    }

    /**
     * <p>
     *     Mark the coin as depleted
     * </p>
     *
     * <p>
     *     If a coin is depleted, it may be rejected by various
     *     currency actions.
     * </p>
     * Depleted coins can be used
     */
    public void setDepleted() {
        isDepleted = true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Coin coin)) return false;
        return value == coin.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
