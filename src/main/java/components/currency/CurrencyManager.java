package components.currency;

import components.currency.coin.Coin;
import components.currency.coin.CoinFabricator;
import components.upgrade.upgrades.currency.IUpgradableCurrency;

/**
 * <p>
 *     Class for controlling and storing coins. Support depositing
 *     and withdrawal of coins.
 * </p>
 *
 * <p>
 *     Only accepts withdrawal when there is sufficient funds in
 *     the balance, and the withdrawal amount is non-negative.
 * </p>
 *
 * <p>
 *     When depositing coins, the total value added to the balance
 *     is multiplied by a gain buff percentage, which can be
 *     adjusted to give extra or less coins upon depositing.
 * </p>
 *
 */
public class CurrencyManager implements IUpgradableCurrency, ITrader {

    private Coin balance;
    private final CoinFabricator coinFabricator;
    private float gainBuff;

    public CurrencyManager() {
        coinFabricator = new CoinFabricator();
        balance = coinFabricator.generateCoin();
        gainBuff = 100.0f;
    }

    /**
     * <p>
     *     Withdrawal coins from the balance of the {@code CurrencyManager}
     * </p>
     *
     * <p>
     *     Before a withdrawal happens, a validity check is done. This check
     *     ensures that the withdrawal value is non-negative, and that the
     *     balance has sufficient funds.
     * </p>
     *
     * @param value desired value to withdraw
     * @return a coin with the value withdrew, or a coin with 0 value if
     * insufficient balance
     */
    public Coin withdrawal(int value) {
        if (!isValidWithdrawal(value)) return coinFabricator.generateCoin();

        Coin coin = coinFabricator.generateCoin(value);
        balance = coinFabricator.subtractCoins(balance, coin);
        return coin;
    }

    private boolean isValidWithdrawal(int amount) {
        if (amount < 0) return false;
        return balance.getValue() >= amount;
    }

    /**
     * Deposit the coin in the {@code CurrencyManager} balance.
     *
     * <p>
     *     Before completing the deposit, it will check if the coin
     *     has been depleted. Depleted coins are not accepted.
     *     When a coin is accepted, it will be marked as depleted.
     * </p>
     *
     * <p>
     *     The deposited coin's value is multiplied by the gain buff.
     *     The resulting balance is calculated by the following forumal:
     *
     *     <p>
     *         new balance = old balance + (coin value * (gainBuff / 100))
     *     </p>
     * </p>
     *
     * @param coin to be added to the balance
     */
    @Override
    public void deposit(Coin coin) {
        if (coin == null) return;
        if (coin.isDepleted()) return;

        int toBeAdded = Math.round(coin.getValue() * (gainBuff / 100));
        balance = coinFabricator.addValueToCoin(balance, toBeAdded);
        coin.setDepleted();
    }

    @Override
    public void increaseGains(float deltaPercentValue) {
        if (deltaPercentValue < 0) return;

        gainBuff += deltaPercentValue;
    }

    @Override
    public void decreaseGains(float deltaPercentValue) {
        if (deltaPercentValue < 0) return;

        gainBuff -= deltaPercentValue;
        gainBuff = Math.max(gainBuff, 0);
    }

    /**
     * @return the total balance, all current coins
     */
    public Coin getBalance() {
        return balance;
    }

    /**
     * <p>
     *     The gainBuff determines how much extra coins
     *     are to be added when depositing coins in the
     *     {@code CurrencyManager}.
     * </p>
     *
     * @return the percentage value determining extra coins added
     * to the balance when depositing coins.
     */
    public float getGainBuff() {
        return gainBuff;
    }

    @Override
    public void resetGainBuff() {
        gainBuff = 100;
    }

    @Override
    public boolean canPurchase(Coin coin) {
        return isValidWithdrawal(coin.getValue());
    }

    @Override
    public void purchase(Coin coin) {
        withdrawal(coin.getValue());
    }
}
