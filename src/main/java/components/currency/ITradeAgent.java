package components.currency;

public interface ITradeAgent {

    default ITrader getTrader() { return null; }
}
