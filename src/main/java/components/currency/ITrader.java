package components.currency;

import components.currency.coin.Coin;

public interface ITrader {

    /**
     * Validate that the trader has sufficient funds to complete transaction.
     * Do nothing if the coin is null.
     *
     * @param coin required coins for purchase
     * @return true if the trader has sufficient funds, false otherwise
     */
    boolean canPurchase(Coin coin);

    /**
     * Perform the purchase, only if the trader has sufficient funds.
     * Do nothing if coin is null.
     *
     * @param coin required coins for purchase
     */
    void purchase(Coin coin);

    /**
     * Deposit money into the balance of the trader. The balance determines if
     * purchases can be made. Do nothing if the coin is null.
     *
     * @param coin value to be deposited
     */
    void deposit(Coin coin);
}
