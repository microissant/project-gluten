package components.health;

public class Damage {

    private final int damageValue;

    public Damage(int damageValue) {
        if (damageValue < 0) damageValue = 0;
        this.damageValue = damageValue;
    }

    public Damage() {
        this(1);
    }

    public Damage(Damage damage) {
        this(damage.damageValue);
    }

    /**
     * Subtract damage value to the given health input.
     * @param health start health
     * @return health after damage is applied
     */
    public int applyDamage(int health) {
        return health - damageValue;
    }

    /**
     * @return Amount of damage applied when using applyDamage
     */
    public int getDamageValue() {
        return damageValue;
    }

}
