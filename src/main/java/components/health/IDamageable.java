package components.health;

public interface IDamageable extends Killable {

    /**
     * @return the current health value of the object
     */
    int getCurrentHealth();

    /**
     * @return the maximum health value of the object
     */
    int getMaximumHealth();

    /**
     * Heal the object with a given healer
     *
     * @param healer assigned to apply the healing
     */
    void applyHeal(Healer healer);

    /**
     * Apply damage to the object.
     *
     * @param damage assigned to apply the damage
     */
    void applyDamage(Damage damage);
}
