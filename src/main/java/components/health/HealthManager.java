package components.health;

import components.upgrade.upgrades.health.IUpgradableHealth;

public class HealthManager implements IUpgradableHealth, IHealable {

    private int maximumHealth;
    private int currentHealth;

    private boolean isImmuneToDamage = false;

    public HealthManager() {
        this(1);
    }

    public HealthManager(int health) {
        if (health < 0) {
            currentHealth = 0;
            maximumHealth = 1;
        } else {
            maximumHealth = health;
            currentHealth = health;
        }
    }

    /**
     * @return current health of the health manager
     */
    public int getCurrentHealth() {
        return currentHealth;
    }

    /**
     * @return the limit on how much health the health manager can have.
     */
    public int getMaximumHealth() {
        return maximumHealth;
    }

    /**
     * Reduce health with the given damage.
     * If damage has negative value it is not applied.
     * Health will not be reduced lower than 0.
     *
     * @param damage damage to be applied to health
     * @return the total damage applied
     */
    public int applyDamage(Damage damage) {
        if (isImmuneToDamage) return 0;
        if (damage.getDamageValue() < 0) return 0;

        int oldHealth = currentHealth;
        currentHealth = damage.applyDamage(currentHealth);
        if (currentHealth < 0) currentHealth = 0;
        return oldHealth - currentHealth;
    }

    /**
     * @return True if health == 0, false otherwise.
     */
    public boolean isDead() {
        return currentHealth == 0;
    }

    @Override
    public void increaseMaximumHealth(int value) {
        if (value < 0) return;
        maximumHealth += value;
    }

    @Override
    public void decreaseMaximumHealth(int value) {
        if (value < 0) return;

        maximumHealth -= value;
        if (maximumHealth < 1) maximumHealth = 1;
        if (maximumHealth < currentHealth) currentHealth = maximumHealth;
    }

    @Override
    public void heal(Healer healer) {
        if (healer == null) return;
        if (isDead()) return;
        if (healer.getHealValue() < 1) return;

        currentHealth = healer.applyHeal(currentHealth);
        if (currentHealth > maximumHealth) currentHealth = maximumHealth;
    }

    /**
     * <p>Resurrect from death.</p>
     *
     * <p>If dead, set current health to 1</p>
     * <p>No effect if already alive</p>
     */
    public void resurrect() {
        if (isDead()) currentHealth = 1;
    }

    @Override
    public void enableImmunity() {
        isImmuneToDamage = true;
    }

    @Override
    public void disableImmunity() {
        isImmuneToDamage = false;
    }

    /**
     * @return true if all damage applied is ignored.
     */
    public boolean isImmuneToDamage() {
        return isImmuneToDamage;
    }

}
