package components.health;

public interface Killable {

    /**
     * @return true if object is dead. False if alive.
     */
    boolean isDead();
}
