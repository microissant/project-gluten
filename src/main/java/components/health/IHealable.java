package components.health;

public interface IHealable {

    /**
     * <p>Increase current health by the healing amount.</p>
     *
     * <p>Capped at maximum health.</p>
     *
     * <p>If already dead, healing will have no effect. Try `resurrect` first.</p>
     *
     * @param healer responsible for applying heal
     */
    void heal(Healer healer);

}
