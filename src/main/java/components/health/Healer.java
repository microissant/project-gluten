package components.health;

public class Healer {

    private final int healValue;

    public Healer() {
        this(1);
    }

    public Healer(int value) {
        healValue = Math.max(value, 0);
    }

    /**
     * Apply its healing value to the given input health
     * @param health to be healed
     * @return final health after heal
     */
    public int applyHeal(int health) {
        return health + healValue;
    }

    /**
     * @return the amount of healing the healer applies upon heal
     */
    public int getHealValue() {
        return healValue;
    }

}
