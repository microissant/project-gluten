package util;

import model.IUpdatable;

import java.util.LinkedList;
import java.util.List;

/**
 * List-class for handling continuous updating of a collection of
 * objects which can be set to be added and removed during iteration
 *
 * @param <E> type of the updatable object
 */
public class UpdatableList<E extends IUpdatable> implements IUpdatable {

    private final List<E> current;
    private final List<E> toBeRemoved;
    private final List<E> toBeAdded;

    /**
     * Constructor for UpdatableList.
     */
    public UpdatableList() {
        current = new LinkedList<>();
        toBeRemoved = new LinkedList<>();
        toBeAdded = new LinkedList<>();
    }

    /**
     * Updates all entities in the list.
     *
     * @param deltaTime time since last update.
     */
    @Override
    public void update(double deltaTime) {
        current.addAll(toBeAdded);
        updateEntities(deltaTime);
        current.removeAll(toBeRemoved);
        clearToBe();
    }

    /**
     * Adds an entity to the list.
     *
     * @param entity entity to add. Must not be null.
     */
    public void add(E entity) {
        if (entity == null) return;
        toBeAdded.add(entity);
    }

    /**
     * Removes an entity from the list.
     *
     * @param entity entity to remove. Must not be null.
     */
    public void remove(E entity) {
        if (entity == null) return;
        toBeRemoved.add(entity);
    }

    private void updateEntities(double deltaTime) {
        for (E entity: current) {
            entity.update(deltaTime);
        }
    }

    /**
     * Returns a copy of the current list with all entities that are scheduled for addition added.
     *
     * @return copy of the current list with all entities that are scheduled for addition added. Never null.
     */
    public List<E> getList() {
        List<E> list = new LinkedList<>(current);
        list.addAll(toBeAdded);

        return list;
    }

    /**
     * Clears all entities that are scheduled for removal or addition.
     */
    public void clearToBe() {
        toBeRemoved.clear();
        toBeAdded.clear();
    }

    /**
     * Clears all entities from the list and all entities that are scheduled for removal or addition.
     */
    public void clear() {
        current.clear();
        toBeAdded.clear();
        toBeRemoved.clear();
    }
}
