package util;

@FunctionalInterface
public interface OnTriggerEvent {

    /**
     * Execute an observed method
     */
    void execute();
}