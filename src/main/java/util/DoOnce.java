package util;

/**
 * A utility class that ensures a given operation is executed only once until reset.
 */
public class DoOnce {

    private boolean hasExecuted;
    private final OnTriggerEvent onComplete, onReset;

    /**
     * Constructs a new DoOnce instance with the specified onComplete listener.
     *
     * @param onComplete the listener to be called when the operation is executed
     */
    public DoOnce(OnTriggerEvent onComplete) {
        this(onComplete, null);
    }

    public DoOnce(OnTriggerEvent onComplete, OnTriggerEvent onReset) {
        this.onComplete = onComplete;
        this.onReset = onReset;
        hasExecuted = false;
    }

    /**
     * Executes the operation if it has not been executed before.
     * If the operation has already been executed, does nothing.
     */
    public void execute() {
        if (hasExecuted) return;
        onComplete.execute();
        hasExecuted = true;
    }

    /**
     * Resets the execution state, allowing the operation to be executed again.
     */
    public void reset() {
        if (!hasExecuted) return;

        hasExecuted = false;

        if (onReset == null) return;
        onReset.execute();
    }

    /**
     * Checks if the operation has been executed.
     *
     * @return true if the operation has been executed, false otherwise
     */
    public boolean hasExecuted() {
        return hasExecuted;
    }
}
