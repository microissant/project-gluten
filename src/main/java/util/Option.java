package util;

public class Option<E> {

    private final E defaultOption;
    private E currentOption;

    public Option(E defaultOption) {
        this.defaultOption = defaultOption;
        reset();
    }

    /**
     * Reset current selected option to its default setting
     */
    public void reset() {
        currentOption = defaultOption;
    }

    /**
     * @return the default setting
     */
    public E getDefaultOption() {
        return defaultOption;
    }

    /**
     * @return the current setting
     */
    public E getCurrentOption() {
        return currentOption;
    }

    /**
     * @param currentOption new current option to be applied
     */
    public void setCurrentOption(E currentOption) {
        this.currentOption = currentOption;
    }
}
