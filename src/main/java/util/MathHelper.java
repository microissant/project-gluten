package util;

import com.badlogic.gdx.math.Vector2;

/**contains static methods for math operations */
public class MathHelper {
    




    /** Treats vectors a and b as complex numbers and calculates the product.
     *  This product has two useful properties:
     * <ul>
     *  <li>angle(result) = angle(a) + angle(b)
     *  <li>dist(result) = dist(a) * dist(b)
     * <ul/>
     *  Order does not matter: a * b  = b * a.
     * @param a the first complex number
     * @param b the second complex number
     * @return the result
     */
    public static Vector2 complexProduct(Vector2 a, Vector2 b){
        Vector2 result = new Vector2();
        complexProduct(a,b,result);
        return result;
    }
    /** computes the complex product of a and b and sets the result vector to the result. Useful for preallocation.
     * see {@linkplain #complexProduct(Vector2, Vector2) complex Product} for the definition.
     * @param a the first vector
     * @param b the second vector
     * @param returnVector the result vector
     */
    public static void complexProduct(Vector2 a, Vector2 b, Vector2 returnVector){
        returnVector.set(a.x * b.x - a.y * b.y, a.x * b.y + b.x * a.y);
    }


    /**
     * converts a linear time parameter to a more smooth version in order to ease in and out.
     *
     * @param t a value between 0 and 1 that determines how close the animation is to finishing.
     * @return a smooth version of t that starts slow, then accelerates and then slows down at the end.
     */
    public static float smoothStep(float t){
        t = Math.min(Math.max(0,t),1);
        return t * t * t * (10 + t * (6 * t - 15)); 
    }


}
