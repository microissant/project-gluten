package util;

import model.IUpdatable;

/**
 * A countdown timer that implements IUpdatable and can be used to track time intervals.
 */
public class CountdownTimer implements IUpdatable {

    private float currentTime;
    private float maxTime;
    private boolean active;
    private final OnTriggerEvent onComplete;

    private final boolean isLooping;

    /**
     * Constructs a new CountdownTimer with the specified maxTime and onComplete method.
     *
     * @param maxTime The maximum time for the countdown in seconds.
     * @param onComplete The OnTimerCompleteListener that contains the method to call when the timer is complete.
     */
    public CountdownTimer(float maxTime, OnTriggerEvent onComplete) {
        this(maxTime, onComplete, false);
    }

    public CountdownTimer(float maxTime, OnTriggerEvent onComplete, boolean loop) {
        this.maxTime = maxTime;
        currentTime = 0;
        active = false;
        this.onComplete = onComplete;
        isLooping = loop;
    }



    /**
     * Updates the countdown timer with the given deltaTime.
     *
     * @param deltaTime The time elapsed since the last update call, in seconds.
     */
    @Override
    public void update(double deltaTime) {
        if (!active) return;

        currentTime += deltaTime;

        if (isComplete()) {
            onComplete.execute();
            active = false;
            if (isLooping) restart();
        }
    }

    /**
     * Checks if the timer has reached or exceeded its maxTime.
     *
     * @return true if the timer is complete, false otherwise.
     */
    public boolean isComplete() {
        return currentTime >= maxTime;
    }

    /**
     * Restarts the timer, resetting the currentTime and setting it to active.
     */
    public void restart() {
        currentTime = 0;
        active = true;
    }

    /**
     * Resets the timer, setting the currentTime back to 0 and setting it inactive.
     */
    public void reset() {
        currentTime = 0;
        active = false;
    }

    /**
     * Stops the timer, setting it to inactive.
     */
    public void stop() {
        active = false;
    }

    /**
     * Starts the timer, setting it to active.
     */
    public void start() {
        active = true;
    }

    /**
     * Checks if the timer has started.
     *
     * @return true if the timer has started, false otherwise.
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Returns the current elapsed time of the timer.
     *
     * @return The current elapsed time in seconds.
     */
    public float getCurrentTime() {
        return currentTime;
    }

    /**
     * Update the maximum time with a new value. Maximum time determines how long the timer
     * is before the observed method is triggered
     *
     * @param time new time in seconds
     */
    public void setTime(float time) {
        maxTime = time;
    }
}
