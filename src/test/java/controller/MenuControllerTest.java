package controller;

import com.badlogic.gdx.Input;
import model.event.EventBus;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

/**
 This class is used to test the {@link MenuController} class methods.
 */
class MenuControllerTest {

    // The mock event bus used for testing
    EventBus eventBus = mock(EventBus.class);

    // The MenuController instance used for testing
   MenuController controller = new MenuController(eventBus);


    /**
     * Tests the {@link MenuController#keyDown(int)} method with the SPACE key.
     * It should return false.
     */
    @Test
    void keyDown() {
        boolean result = controller.keyDown(Input.Keys.SPACE);
        assertFalse(result);
    }

    /**
     * Tests the {@link MenuController#keyUp(int)} method with the SPACE key.
     * It should return false.
     */
    @Test
    void keyUp() {
        boolean result = controller.keyUp(Input.Keys.SPACE);
        assertFalse(result);
    }


    /**
     * Tests the {@link MenuController#keyTyped(char)} method with the 'a' character.
     * It should return false.
     */
    @Test
    void keyTyped() {
        boolean result = controller.keyTyped('a');
        assertFalse(result);
    }


    /**
     * Tests the {@link MenuController#touchDown(int, int, int, int)} method with the LEFT button.
     * It should return false.
     */
    @Test
    void touchDown() {
        boolean  result = controller.touchDown(0, 0, 0, Input.Buttons.LEFT);
        assertFalse(result);
    }

    /**
     * Tests the {@link MenuController#touchUp(int, int, int, int)} method with the LEFT button.
     * It should return false.
     */
    @Test
    void touchUp() {
        boolean result = controller.touchUp(0, 0, 0, Input.Buttons.LEFT);
        assertFalse(result);
    }


    /**
     * Tests the {@link MenuController#touchDragged(int, int, int)} method.
     * It should return false.
     */
    @Test
    void touchDragged() {
        boolean result = controller.touchDragged(0, 0, 0);
        assertFalse(result);
    }

    /**
     * Tests the {@link MenuController#mouseMoved(int, int)} method.
     * It should return false.
     */
    @Test
    void mouseMoved() {
        boolean result = controller.mouseMoved(0, 0);
        assertFalse(result);

    }

    /**
     * Tests the {@link MenuController#(int, int)} method.
     * It should return false.
     */
    @Test
    void scrolled() {
        boolean  result = controller.scrolled(0, 0);
        assertFalse(result);
    }
}