package controller;

import app.Globals;
import com.badlogic.gdx.Input;
import gdx.MockInputWrapper;
import model.event.EventBus;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class GameControllerTest {
    EventBus eventBus = mock(EventBus.class);
    MockInputWrapper input = mock(MockInputWrapper.class);
    GameController controller = new GameController(eventBus);


    /**
     Tests the handleInput method of the GameController class by mocking input behavior and ensuring that the method
     is called without any errors.
     */
    @Test
    void handleInputTest() {

        MockInputWrapper mockInput = mock(MockInputWrapper.class);

        when(mockInput.isButtonPressed(Input.Buttons.LEFT)).thenReturn(true);
        EventBus eventBus = mock(EventBus.class);
        MockInputWrapper input = mock(MockInputWrapper.class);
        GameController controller = new GameController(eventBus);

        // Mock input behavior
        when(input.isButtonPressed(anyInt())).thenReturn(true);
        when(input.isButtonPressed(Input.Buttons.LEFT)).thenReturn(true);

        controller.handleInput();
    }

    /**
     Tests the keyDown method of the GameController class by passing in a keyboard input and ensuring that the method
     returns true.
     */
    @Test
    void keyDown() {
        boolean result = controller.keyDown(Input.Keys.SPACE);
        assertTrue(result);
    }


    /**
     Tests the keyUp method of the GameController class by passing in a keyboard input and ensuring that the method
     returns true.
     */
    @Test
    void keyUp() {
        boolean result = controller.keyUp(Input.Keys.SPACE);
        assertTrue(result);
    }


    /**
     Tests the keyTyped method of the GameController class by passing in a keyboard input and ensuring that the method
     returns true.
     */
    @Test
    void keyTyped() {
        boolean result = controller.keyTyped('a');
        assertTrue(result);
    }

    /**
     Tests the touchDown method of the GameController class by passing in touch input and ensuring that the method
     returns true.
     */
    @Test
    void touchDown() {
        boolean  result = controller.touchDown(0, 0, 0, Input.Buttons.LEFT);
        assertTrue(result);
    }


    /**
     Tests the touchUp method of the GameController class by passing in touch input and ensuring that the method
     returns true.
     */
    @Test
    void touchUp() {
        boolean result = controller.touchUp(0, 0, 0, Input.Buttons.LEFT);
        assertTrue(result);
    }

    /**
     Tests the touchDragged method of the GameController class by passing in touch input and ensuring that the method
     returns true.
     */
    @Test
    void touchDragged() {
        boolean result = controller.touchDragged(0, 0, 0);
        assertTrue(result);
    }

    /**
     Tests the mouseMoved method of the GameController class by passing in mouse input and ensuring that the method
     returns true and sets the currentAimMode to Mouse.
     */
    @Test
    void mouseMoved() {
        boolean result = controller.mouseMoved(0, 0);
        assertTrue(result);
        assertEquals(Globals.AimMode.Mouse, Globals.currentAimMode);
    }

    @Test
    void scrolled() {
        boolean  result = controller.scrolled(0, 0);
        assertTrue(result);
    }

    /**
     Tests the scrolled method of the GameController class by passing in scroll input and ensuring that the method
     returns true.
     */
    @Test
    void disable() {
        controller.disable();
        assertFalse(controller.isEnabled);
    }

    /**
     Tests the disable method of the GameController class by calling the method and ensuring that isEnabled is false.
     */
    @Test
    void enable() {
        controller.enable();
        assertTrue(controller.isEnabled);
    }
}
