package view.ui.widgets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import gdx.GdxTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


/**

 The ControllerLayoutTest class tests the functionality of the ControllerLayout class.
 */
class ControllerLayoutTest extends GdxTest {

    private ControllerLayout controllerLayout;

    /**

     Initializes the skin and the controllerLayout instance before each test case.
     */
    @BeforeEach
    public void setup() {
        Skin skin = new Skin(Gdx.files.internal("src/main/resources/packed/ui/uiskin.json"));

        controllerLayout = new ControllerLayout(skin);
    }

    /**
     Tests if the children count of the ControllerLayout is as expected.
     */
    @Test
    public void testCorrectChildrenCount() {

        assertFalse(controllerLayout.getChildren().isEmpty());

        assertEquals(2, controllerLayout.getChildren().size);
        assertTrue(controllerLayout.getChildren().get(0) instanceof Label);
        assertTrue(controllerLayout.getChildren().get(1) instanceof Table);
    }

}