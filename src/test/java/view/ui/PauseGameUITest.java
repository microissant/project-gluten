package view.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import gdx.GdxTest;
import model.event.EventBus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**

 The PauseGameUI class represents the user interface for pausing the game.
 It contains four buttons: resume, restart, settings and exit.
 This class extends the GdxTest class for testing purposes.
 */

class PauseGameUITest extends GdxTest {

    private PauseGameUI pauseGameUI;

    @Mock
    private Stage stage;

    @Mock
    private EventBus eventBus;

    /**
     * Sets up the skin, stage and event bus before each test.
     */
    @BeforeEach
    public void setup() {
        Skin skin = new Skin(Gdx.files.internal("src/main/resources/packed/ui/uiskin.json"));
        stage = mock(Stage.class);
        eventBus = mock(EventBus.class);

        pauseGameUI = new PauseGameUI(skin, stage, eventBus);
    }

    /**
     * Tests if the correct number of menu buttons are displayed.
     */
    @Test
    public void testCorrectMenuButtonsCount() {
        assertEquals(4, pauseGameUI.getChildren().size);
    }

    /**
     * Tests if all buttons have at least one listener and that it is a ClickListener.
     */
    @Test
    public void testAllButtonsHasListeners() {

        for (Actor actor: pauseGameUI.getChildren()) {
            
            // Skip if not text-button
            if (!(actor instanceof TextButton)) continue;

            // Check that its not empty
            assertFalse(actor.getListeners().isEmpty());

            // Iterate through each listener to check if it
            // has a ClickListener
            EventListener clickListener = null;
            for (EventListener listener: actor.getListeners()) {
                if (listener instanceof ClickListener) {
                    clickListener = listener;
                }
            }
            assertNotNull(clickListener);
        }
    }

    /**
     * Tests if starting the animation adds an action to all buttons.
     */
    @Test
    public void testStartAnimationAddsAction() {
        // Check that is has no action prior to starting animation
        for (Actor actor: pauseGameUI.getChildren()) {
            assertFalse(actor.hasActions());
        }

        pauseGameUI.startAnimations();

        // Check that action has been applied
        for (Actor actor: pauseGameUI.getChildren()) {
            assertTrue(actor.hasActions());
        }
    }

    /**
     * Tests if the stage is disposed after disposing the PauseGameUI instance.
     */
    @Test
    public void testDispose() {
        verify(stage, never()).dispose();

        pauseGameUI.dispose();

        verify(stage, times(1)).dispose();
    }
}
