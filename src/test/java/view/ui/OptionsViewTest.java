package view.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.Viewport;
import controller.MenuController;
import gdx.GdxTest;
import model.event.EventBus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;
import view.screens.Gluten;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class OptionsViewTest extends GdxTest {


    private OptionsView optionsView;

    @Mock
    private Stage stage;

    @Mock
    private Gluten gluten;

    @Mock
    private MenuController menuController;

    @Mock
    private Viewport viewport;

    @Mock
    private EventBus eventBus;

    private List<Actor> menuActors;

    /**
     * Sets up the test environment for each test by mocking objects needed and
     * adding all actors that is added in the constructor of OptionsView to a list.
     */
    @BeforeEach
    public void setup() {
        Skin skin = new Skin(Gdx.files.internal("src/main/resources/packed/ui/uiskin.json"));
        stage = mock(Stage.class);
        viewport = mock(Viewport.class);
        when(stage.getViewport()).thenReturn(viewport);
        eventBus = mock(EventBus.class);
        gluten = mock(Gluten.class);
        menuController = mock(MenuController.class);

        // Add all actors that is added in the constructor of optionsView to a list
        menuActors = new ArrayList<>();
        doAnswer((Answer<Void>) invocation -> {
            Actor actor = invocation.getArgument(0);
            menuActors.add(actor);
            return null;
        }).when(stage).addActor(any(Actor.class));

        optionsView = new OptionsView(gluten, menuController, skin, stage, eventBus);
    }

    /**
     * Tests that the correct starting mode is set.
     */
    @Test
    public void testCorrectStartingMode() {
        assertEquals(OptionsView.OptionsMode.DISPLAY, optionsView.getCurrentOptionsMode());
    }

    /**
     * Tests that resizing updates the stage.
     */
    @Test
    public void testResizeUpdatesStage() {
        verify(viewport, never()).update(anyInt(), anyInt(), anyBoolean());

        optionsView.resize(100, 100);

        verify(viewport, times(1)).update(100, 100, true);
    }

    /**
     * Tests that dispose is called correctly.
     */
    @Test
    public void testDispose() {
        verify(stage, never()).dispose();

        optionsView.dispose();

        verify(stage, times(1)).dispose();
    }

    /**
     * Tests that rendering is done correctly.
     */
    @Test
    public void testRender() {
        verify(stage, never()).act(anyFloat());
        verify(stage, never()).draw();

        optionsView.render(0.5f);

        verify(stage, times(1)).act(anyFloat());
        verify(stage, times(1)).draw();
    }

    @Test
    public void show() {
        assertDoesNotThrow(() -> optionsView.show());
    }

    /**
     * Tests that the main table size is correct.
     */
    @Test
    public void testMainTableSize() {
        assertEquals(1, menuActors.size());
        assertTrue(menuActors.get(0) instanceof Table);

        assertEquals(3, ((Table) menuActors.get(0)).getChildren().size);
    }
}