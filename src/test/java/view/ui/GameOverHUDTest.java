package view.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import gdx.GdxTest;
import model.event.EventBus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

/**

 This class contains JUnit tests for the {@link GameOverHUD} class.
 It extends {@link GdxTest} to provide a {@link Stage} instance for testing.
 Tests include checking that the correct number of menu buttons are displayed,
 and that starting the animation adds an action to each actor.
 */
class GameOverHUDTest extends GdxTest {

    private GameOverHUD gameOverHUD;

    @Mock
    private Stage stage;

    @Mock
    private EventBus eventBus;

    /**
     * Sets up a new instance of the {@link GameOverHUD} class for each test.
     */
    @BeforeEach
    public void setup() {
        Skin skin = new Skin(Gdx.files.internal("src/main/resources/packed/ui/uiskin.json"));
        stage = mock(Stage.class);
        eventBus = mock(EventBus.class);

        gameOverHUD = new GameOverHUD(skin, stage, eventBus);
    }

    /**
     * Tests that the correct number of menu buttons are displayed on the {@link GameOverHUD}.
     */
    @Test
    public void testCorrectMenuButtonsCount() {
        assertEquals(1, gameOverHUD.getChildren().size);
        assertTrue(gameOverHUD.getChildren().get(0) instanceof Table);
    }


    @Test
    public void testStartAnimationAddsAction() {
        // Check that is has no action prior to starting animation
        for (Actor actor: gameOverHUD.getChildren()) {
            assertFalse(actor.hasActions());
        }

        gameOverHUD.startAnimations();

        // Check that action has been applied
        for (Actor actor: gameOverHUD.getChildren()) {
            assertTrue(actor.hasActions());
        }
    }

}