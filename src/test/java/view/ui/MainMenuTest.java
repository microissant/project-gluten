package view.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import controller.MenuController;
import gdx.GdxTest;
import model.event.EventBus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;
import view.screens.Gluten;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class MainMenuTest extends GdxTest {

    private MainMenu mainMenu;

    @Mock
    private InputProcessor controller;

    @Mock
    private Stage stage;

    @Mock
    private Skin skin;

    @Mock
    private EventBus eventBus;

    private List<Actor> actors;

    @BeforeEach
    public void setup() {
        controller = mock(MenuController.class);
        skin = new Skin(Gdx.files.internal("src/main/resources/packed/ui/uiskin.json"));
        stage = mock(Stage.class);
        eventBus = mock(EventBus.class);
        Gluten gluten = mock(Gluten.class);

        actors = new ArrayList<>();
        doAnswer((Answer<Void>) invocation -> {
            Actor actor = invocation.getArgument(0);
            actors.add(actor);
            return null;
        }).when(stage).addActor(any(Actor.class));

        mainMenu = new MainMenu(gluten, controller, stage, skin, eventBus);
    }


    /**

     Tests whether the dispose() method correctly disposes of the stage.
     */
    @Test
    public void testDispose() {
        verify(stage, never()).dispose();

        mainMenu.dispose();

        verify(stage, times(1)).dispose();
    }

    /**

     Tests whether the render() method correctly calls the act() and draw() methods of the stage.
     */
    @Test
    public void testRender() {
        verify(stage, never()).act(anyFloat());
        verify(stage, never()).draw();

        mainMenu.render(0.5f);

        verify(stage, times(1)).act(anyFloat());
        verify(stage, times(1)).draw();
    }

    /**

     Tests whether the correct number of actors are added to the stage and whether they are of the correct type.
     */
    @Test
    public void testActorsCount() {
        assertEquals(2, actors.size());
        assertTrue(actors.get(0) instanceof Group);
        assertTrue(actors.get(1) instanceof Table);
    }

    /**

     Tests whether the show() method does not throw any exceptions.
     */
    @Test
    public void show() {
        assertDoesNotThrow(() -> mainMenu.show());
    }

}