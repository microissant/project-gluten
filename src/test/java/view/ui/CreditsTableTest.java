package view.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import gdx.GdxTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CreditsTableTest extends GdxTest {

    private CreditsTable creditsTable;


    /**
     * Sets up the test environment before each test case is executed.
     */
    @BeforeEach
    public void setup() {
        Skin skin = new Skin(Gdx.files.internal("src/main/resources/packed/ui/uiskin.json"));

        creditsTable = new CreditsTable(skin);
    }

    /**
     * Tests if the table has the correct number of children.
     */
    @Test
    public void testCorrectChildrenCount() {
        assertFalse(creditsTable.getChildren().isEmpty());
        assertEquals(1, creditsTable.getChildren().size);
        assertTrue(creditsTable.getChildren().get(0) instanceof Table);

        Table content = (Table) creditsTable.getChildren().get(0);
        assertFalse(content.getChildren().isEmpty());
    }







}