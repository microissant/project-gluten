package view.ui;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import org.junit.jupiter.api.BeforeEach;
import view.ScreenTest;
import view.screens.Gluten;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;

class CreditsScreenTest extends ScreenTest {

    /**

     A test class for the {@link CreditsScreen} class.
     This class extends {@link ScreenTest}, which provides common test methods for testing
     any LibGDX screen.
     The test methods included in this class verify that the screen can be shown, rendered,
     and disposed of without throwing any exceptions. It also checks if the expected actors
     are present in the stage and if the stage is updated on resize.
     */

    @BeforeEach
    public void setup() {
        super.setup();

        expectedActors = new ArrayList<>(List.of(
                Table.class,
                Table.class
        ));

        Gluten gluten = mock(Gluten.class);
        screen = new CreditsScreen(gluten, skin, stage);
    }
}