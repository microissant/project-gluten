package view.ui.hud;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import gdx.GdxTest;
import model.event.EventBus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import view.ui.hud.widgets.ResourceWidgetStack;
import view.ui.hud.widgets.TimeCounter;
import view.ui.hud.widgets.WeaponWidget;
import view.ui.hud.widgets.upgrades.UpgradeBar;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


/**

 This class tests the {@link GameHud} class which is responsible for displaying the game's heads-up display (HUD).
 */
class GameHudTest extends GdxTest {


    private GameHud gameHud;

    @Mock
    private EventBus eventBus;

    @Mock
    private Stage stage;

    /**
     * Sets up the necessary objects for the test methods to execute.
     */
    @BeforeEach
    public void setup() {
        eventBus = mock(EventBus.class);
        stage = mock(Stage.class);
        Skin skin = new Skin(Gdx.files.internal("src/main/resources/packed/ui/uiskin.json"));

        gameHud = new GameHud(skin, eventBus, stage);
    }


    /**
     * Tests that the correct number of children are added to the {@link GameHud} object during its construction.
     */
    @Test
    public void testCorrectChildrenCount() {
        assertFalse(gameHud.getChildren().isEmpty());

        assertEquals(4, gameHud.getChildren().size);
        assertTrue(gameHud.getChildren().get(0) instanceof ResourceWidgetStack);
        assertTrue(gameHud.getChildren().get(1) instanceof UpgradeBar);
        assertTrue(gameHud.getChildren().get(2) instanceof TimeCounter);
        assertTrue(gameHud.getChildren().get(3) instanceof WeaponWidget);
    }

    /**
     * Tests that the {@link GameHud} object can be updated with new information.
     */
    @Test
    public void testUpdate() {
        HUDInformation hudInformation = mock(HUDInformation.class);

        gameHud.update(1, hudInformation);

        verify(hudInformation, times(1)).getCurrentHealth();
        verify(hudInformation, times(1)).getMaxHealth();
        verify(hudInformation, times(1)).getCoins();
        verify(hudInformation, times(1)).getTimeElapsed();
        verify(hudInformation, times(1)).getAmmoCounter();
        verify(hudInformation, times(1)).getInteractableTarget();
    }




}