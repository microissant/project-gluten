package view.ui.hud.widgets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import gdx.GdxTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;

class DebugHUDViewTest extends GdxTest {

    private DebugHUDView debugViewHUD;


    @BeforeEach
    public void setup() {
        Skin skin = new Skin(Gdx.files.internal("src/main/resources/packed/ui/uiskin.json"));
        debugViewHUD = new DebugHUDView(skin);
    }

    @Test
    public void testNotEmpty() {
        assertFalse(debugViewHUD.getChildren().isEmpty());
    }

    @Test
    public void testUpdate() {
        assertDoesNotThrow(() -> debugViewHUD.update(1));
    }

}