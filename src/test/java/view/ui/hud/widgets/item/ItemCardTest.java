package view.ui.hud.widgets.item;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import gdx.GdxTest;
import item.loot.ILoot;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

class ItemCardTest extends GdxTest {

    private ItemCard itemCard;

    @Mock
    private ILoot loot;


    @BeforeEach
    public void setup() {
        Skin skin = new Skin(Gdx.files.internal("src/main/resources/packed/ui/uiskin.json"));
        loot = mock(ILoot.class);

        itemCard = new ItemCard(loot, skin);
    }

    @Test
    public void testCorrectChildren() {
        assertEquals(2, itemCard.getChildren().size);
        assertTrue(itemCard.getChildren().get(0) instanceof Table);
    }
}