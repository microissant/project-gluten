package view.ui.hud.widgets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import gdx.GdxTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TooltipTest extends GdxTest {


    private Tooltip tooltip;

    private String title, description;



    @BeforeEach
    public void setup() {
        Skin skin = new Skin(Gdx.files.internal("src/main/resources/packed/ui/uiskin.json"));
        title = "This is the title";
        description = "this is the description";

        tooltip = new Tooltip(title, description, skin);
    }

    @Test
    public void testCorrectChildren() {
        assertEquals(1, tooltip.getChildren().size);
        assertTrue(tooltip.getChildren().get(0) instanceof Table);

        Table content = (Table) tooltip.getChildren().get(0);
        assertEquals(2, content.getChildren().size);

        assertTrue(content.getChildren().get(0) instanceof Label);
        assertTrue(content.getChildren().get(1) instanceof Label);
    }

    @Test
    public void testCorrectTitleAndDescription() {
        Table content = (Table) tooltip.getChildren().get(0);
        Label titleLabel = (Label) content.getChildren().get(0);
        Label descLabel = (Label) content.getChildren().get(1);

        assertEquals(title, titleLabel.getText().toString());
        assertEquals(description, descLabel.getText().toString());
    }

}