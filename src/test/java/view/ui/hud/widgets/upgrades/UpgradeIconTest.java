package view.ui.hud.widgets.upgrades;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import components.upgrade.upgrades.IUpgrade;
import gdx.GdxTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

class UpgradeIconTest extends GdxTest {


    private UpgradeIcon upgradeIcon;

    @Mock
    private IUpgrade upgrade;


    @BeforeEach
    public void setup() {
        Skin skin = new Skin(Gdx.files.internal("src/main/resources/packed/ui/uiskin.json"));
        upgrade = mock(IUpgrade.class);

        upgradeIcon = new UpgradeIcon(upgrade, skin);
    }

    @Test
    public void testCorrectChildren() {
        assertEquals(3, upgradeIcon.getChildren().size);
        assertTrue(upgradeIcon.getChildren().get(0) instanceof Image);
        assertTrue(upgradeIcon.getChildren().get(1) instanceof Label);
    }

}