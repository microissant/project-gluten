package view.ui.hud;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import gdx.GdxTest;
import model.event.EventBus;
import model.event.events.ui.InitializeHUDEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import view.screens.GameView;

import static org.mockito.Mockito.*;

/**

 The HUDTest class is used to test the functionality of the HUD class.
 It extends the GdxTest class, which provides necessary LibGDX mocking and setup.
 */
public class HUDTest extends GdxTest {

    @Mock
    private EventBus eventBus;
    @Mock
    private Stage stage;
    @Mock
    private GameView gameView;

    private HUD hud;

    /**
     * This method sets up the necessary mocks and objects before each test case.
     */
    @BeforeEach
    public void setUp() {
        super.setUp();

        eventBus = mock(EventBus.class);
        stage = mock(Stage.class);
        Skin skin = new Skin(Gdx.files.internal("src/main/resources/packed/ui/uiskin.json"));
        SpriteBatch spriteBatch = mock(SpriteBatch.class);

        hud = new HUD(eventBus, skin, stage, gameView, spriteBatch);
    }


    /**
     * This test method tests whether the initialize method properly posts
     * the InitializeHUDEvent to the EventBus.
     */
    @Test
    public void testInitialize() {
        // Call the method to test
        hud.initialize();

        // Verify that the InitializeHUDEvent was posted to the EventBus
        verify(eventBus, times(2)).post(any(InitializeHUDEvent.class));
    }

}
