package view.screens;

import app.global.RenderOptions;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import controller.GameController;
import entities.particles.Particle;
import entities.player.PlayerCharacter;
import entities.player.PlayerManager;
import gdx.GdxTest;
import grid.WorldGrid;
import model.Model;
import model.event.EventBus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import view.ui.hud.HUDInformation;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class GameViewTest extends GdxTest {

    private Gluten gluten;
    private Model model;
    private GameController controller;
    private Skin skin;
    private Stage stage;
    private SpriteBatch spriteBatch;
    private ShapeRenderer shapeRenderer;
    private EventBus eventBus;
    private GameView gameView;

    @BeforeEach
    public void setup() {
        super.setUp();

        gluten = mock(Gluten.class);
        model = mock(Model.class);
        controller = mock(GameController.class);
        skin = new Skin(Gdx.files.internal("src/main/resources/packed/ui/uiskin.json"));
        stage = mock(Stage.class);
        spriteBatch = mock(SpriteBatch.class);
        shapeRenderer = mock(ShapeRenderer.class);
        eventBus = mock(EventBus.class);

        PlayerManager playerManager = mock(PlayerManager.class);
        PlayerCharacter character = mock(PlayerCharacter.class);
        when(character.getPosition()).thenReturn(new Vector2());
        when(playerManager.getPlayerCharacter()).thenReturn(character);
        when(model.getPlayer()).thenReturn(playerManager);

        Array<Particle> particles = new Array<>();
        particles.add(mock(Particle.class));
        when(model.getParticles()).thenReturn(particles);

        HUDInformation hudInformation = mock(HUDInformation.class);
        when(model.getHUDInformation()).thenReturn(hudInformation);

        WorldGrid worldGrid = mock(WorldGrid.class);
        TiledMap tiledMap = mock(TiledMap.class);
        MapLayers mapLayers = mock(MapLayers.class);
        MapLayer mapLayer = mock(MapLayer.class);
        when(mapLayers.get(any())).thenReturn(mapLayer);
        when(tiledMap.getLayers()).thenReturn(mapLayers);
        when(worldGrid.getTiledMap()).thenReturn(tiledMap);
        when(model.getWorldGrid()).thenReturn(worldGrid);

        gameView = new GameView(gluten, model, controller, skin, stage, spriteBatch, shapeRenderer, eventBus);
    }

    @Test
    public void testRenderGetInfoFromModel() {
        gameView.render(1);

        verify(model, times(2)).getPlayer();
        verify(model, times(1)).getEnemies();
        verify(model, times(1)).getParticles();
        verify(model, times(1)).getWorldGrid();
        verify(model, times(1)).getVariousEntities();
    }

    @Test
    public void testDrawDebugShapesFromModel() {
        RenderOptions.enableDrawDebug.setCurrentOption(true);
        RenderOptions.enableDebugCollision.setCurrentOption(true);
        RenderOptions.enableDebugPath.setCurrentOption(true);

        gameView.render(1);

        verify(model, times(1)).getDebugShapes();
    }


}