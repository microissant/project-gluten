package view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.Viewport;
import gdx.GdxTest;
import model.event.EventBus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public abstract class ScreenTest extends GdxTest {

    protected Screen screen;

    @Mock
    protected Stage stage;

    @Mock
    protected EventBus eventBus;

    protected List<Class<?>> expectedActors;

    protected List<Actor> actors;

    protected Skin skin;

    @Mock
    protected Viewport viewport;

    /**
            * Sets up the mocked stage, viewport, skin, and event bus before each test.
            * Also sets up a mock for adding actors to the stage and records the actors added for testing.
 */

    @BeforeEach
    public void setup() {

        skin = new Skin(Gdx.files.internal("src/main/resources/packed/ui/uiskin.json"));
        stage = mock(Stage.class);
        viewport = mock(Viewport.class);
        when(stage.getViewport()).thenReturn(viewport);

        eventBus = mock(EventBus.class);

        actors = new ArrayList<>();
        doAnswer((Answer<Void>) invocation -> {
            Actor actor = invocation.getArgument(0);
            actors.add(actor);
            return null;
        }).when(stage).addActor(any(Actor.class));
    }


    /**
     * Tests that the screen's show() method can be called without throwing an exception.
     */
    @Test
    public void show() {
        assertDoesNotThrow(() -> screen.show());
    }


    /**
     * Tests that the stage's act() and draw() methods are called once when the screen's render() method is called.
     */
    @Test
    public void testRender() {
        verify(stage, never()).act(anyFloat());
        verify(stage, never()).draw();

        screen.render(0.5f);

        verify(stage, times(1)).act(anyFloat());
        verify(stage, times(1)).draw();
    }

    /**
     * Tests that the stage's dispose() method is called once when the screen's dispose() method is called.
     */
    @Test
    public void testDispose() {
        verify(stage, never()).dispose();

        screen.dispose();

        verify(stage, times(1)).dispose();
    }


    /**
     * Tests that the number of actors added to the stage matches the expected number of actors, and that each added
     * actor is of a class that is assignable from the expected class for that actor.
     */
    @Test
    public void testActorsCount() {
        assertEquals(expectedActors.size(), actors.size());

        for (int i = 0; i < actors.size(); i++) {
            assertTrue(expectedActors.get(i).isAssignableFrom(actors.get(i).getClass()));
        }
    }

    /**
     * Tests that the viewport's update() method is called once when the screen's resize() method is called.
     */
    @Test
    public void testResizeUpdatesStage() {
        screen.resize(100, 100);

        verify(viewport, times(1)).update(100, 100, true);
    }




}
