package entities.enemies.state.behaviour;

import entities.enemy.state.behaviour.Idle;
import media.DrawProperties;
import media.TextureID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**Tests for the state Idle */

public class IdleTest {
    private final TextureID textureID = TextureID.COOKIE_ENEMY_IDLE;
    private DrawProperties drawProperties;
    private Idle idle;

    @BeforeEach
    public void setUp(){
        drawProperties  = mock(DrawProperties.class);
        idle = new Idle(drawProperties, textureID);
    }

    @Test
    public void testConstructorNullValues() {
        assertDoesNotThrow(() -> new Idle(drawProperties, null));
        assertDoesNotThrow(() -> new Idle(null, textureID));
        assertDoesNotThrow(() -> new Idle(null, null));
    }
    
    /**setAnimation() from Idle´s execute() method should only be called when the boolean hasExecuted is false.
     *This test checks that unless you call reset(), setAnimation is only run once no matter how many times you call execute() 
     */
    @Test
    public void executeAndReset(){
        idle.execute(1);
        verify(drawProperties, times(1)).setAnimation(any());

        for (int i = 0; i < 101; i++){
            idle.execute(1);
        }
        verify(drawProperties, times(1)).setAnimation(any());
        
        idle.reset();
        idle.execute(1);
        verify(drawProperties, times(2)).setAnimation(any());
    }
    
}
