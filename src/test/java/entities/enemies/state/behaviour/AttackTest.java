package entities.enemies.state.behaviour;

import com.badlogic.gdx.math.Vector2;
import components.weapon.WeaponManager;
import entities.enemy.state.behaviour.Attack;
import media.DrawProperties;
import media.TextureID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;

//Test class for the behaviour Attack.

class AttackTest {

    private Attack attack;
    private Vector2 aim;
    private WeaponManager weaponManager;
    private DrawProperties drawProperties;
    private TextureID textureID;

    /**
     * Sets up mock dependencies and creates a new Attack instance before each test.
     */
    @BeforeEach
    void setUp() {
        aim = Mockito.mock(Vector2.class);
        weaponManager = Mockito.mock(WeaponManager.class);
        drawProperties = Mockito.mock(DrawProperties.class);
        textureID = TextureID.COOKIE_ENEMY_IDLE; //The exact textureID is not important

        attack = new Attack(aim, weaponManager, drawProperties, textureID);
    }

    /**
     * Tests if NullPointerException is thrown when creating an Attack instance with null parameters.
     */
    @Test
    void testConstructorNullPointer() {
        assertThrows(NullPointerException.class, () -> new Attack(null, weaponManager, drawProperties, textureID));
        assertThrows(NullPointerException.class, () -> new Attack(aim, null, drawProperties, textureID));
    }

    /**
     * Tests resetting the attack state.
     */
    @Test
    void testReset() {
        attack.reset();
        assertFalse(attack.isComplete());
    }

    /**
     * Tests executing the attack and checking if it is complete.
     */
    @Test
    void testExecuteAndIsComplete() throws Exception {
        Mockito.when(weaponManager.shotsFiredPrimaryWeapon()).thenReturn(0, 1);
        attack.execute(1.0);
        assertTrue(attack.isComplete());
    }
}