package entities.enemies.state.behaviour.iceCreamBoss;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.badlogic.gdx.math.Vector2;

import components.physics.Physics;
import components.weapon.WeaponManager;
import entities.enemy.IEnemy;
import entities.enemy.state.behaviour.iceCreamBoss.BossShoot;
import media.DrawProperties;
import media.TextureID;

public class BossShootTest {
    private Vector2 aim;
    private WeaponManager weaponManager;
    private TextureID textureID;
    private DrawProperties drawProperties;
    private IEnemy owner;
    private BossShoot bossShoot;
    private Physics physics;
    private Vector2 vector;

    @BeforeEach
    public void setUp(){
        aim = mock(Vector2.class);
        weaponManager = mock(WeaponManager.class);
        textureID = TextureID.ICECREAMBOSS_SPIN; //The specific textureID is not important for this test
        drawProperties = mock(DrawProperties.class);
        owner = mock(IEnemy.class);
        physics = mock(Physics.class);
        vector = mock(Vector2.class);
        
        doNothing().when(drawProperties).setAnimation(textureID);
        doNothing().when(drawProperties).setAnimationSpeed(1f);
        when(owner.getPhysicsComponent()).thenReturn(physics);
        doNothing().when(physics).setLinearMovement(vector, 0);
        
        bossShoot = new BossShoot(aim, owner, weaponManager, drawProperties, textureID);
    }
    
    /**Test that the constructor throws exception when aim, owner, weaponManager or/and drawProperties is null and that it does not
     * throw anything for textureID
     * */
    @Test
    public void testConstructorNullValues(){
        assertThrows(NullPointerException.class, () -> new BossShoot(null, owner, weaponManager, drawProperties, textureID));
        assertThrows(NullPointerException.class, () -> new BossShoot(aim, owner, null, drawProperties, textureID));
        assertThrows(NullPointerException.class, () -> new BossShoot(aim, null, weaponManager, drawProperties, textureID));
        assertThrows(NullPointerException.class, () -> new BossShoot(aim, owner, weaponManager, null, textureID));
        assertThrows(NullPointerException.class, () -> new BossShoot(null, owner, null, drawProperties, textureID));
        assertThrows(NullPointerException.class, () -> new BossShoot(aim, null, null, drawProperties, textureID));
        assertThrows(NullPointerException.class, () -> new BossShoot(null, null, weaponManager, drawProperties, textureID));
        assertThrows(NullPointerException.class, () -> new BossShoot(null, null, null, drawProperties, textureID));
        assertThrows(NullPointerException.class, () -> new BossShoot(null, null, null, null, textureID));
        
        assertDoesNotThrow(() -> new BossShoot(aim, owner, weaponManager, drawProperties, null));
        
    }
    
    /**Checks that the weapon is fired the correct amount of times according to the amount
     * of shots and that it charges correctly.
     */
    @Test
    public void executeTest(){   
        //Set the amount of time until a shot is allowed
        when(drawProperties.getAnimationDuration()).thenReturn(10f);
        
        //The weapon should not fire given 0 delta time as input
        when(aim.set(vector)).thenReturn(vector);
        bossShoot.execute(0f);
        verify(weaponManager, never()).fireWeapon();
        verify(weaponManager, never()).update(2f);

        //Weapon fires when input > timeUntilShoot
        bossShoot.execute(11f);
        verify(weaponManager, times(1)).fireWeapon();
        verify(weaponManager, times(2)).update(1.0d);
        //bossShoot.reset();
    }

    @Test
    public void resetTest(){
        bossShoot.reset();
        verify(drawProperties, times(2)).getAnimationDuration();
    }



    
}
