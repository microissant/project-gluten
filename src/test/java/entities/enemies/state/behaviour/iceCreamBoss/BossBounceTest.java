package entities.enemies.state.behaviour.iceCreamBoss;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.badlogic.gdx.math.Vector2;

import components.physics.Physics;
import entities.enemy.IEnemy;
import entities.enemy.state.behaviour.iceCreamBoss.BossBounce;
import media.DrawProperties;
import media.TextureID;

public class BossBounceTest {
    private Vector2 aim;
    private TextureID textureID;
    private DrawProperties drawProperties;
    private IEnemy owner;
    private Physics physics;
    private Vector2 vector;
    private BossBounce bossBounce;


    @BeforeEach
    public void setUp(){
        aim = mock(Vector2.class);
        drawProperties = mock(DrawProperties.class);
        textureID = TextureID.ICECREAMBOSS_IDLE; //The specific textureID does not matter for this test
        owner = mock(IEnemy.class);
        physics = mock(Physics.class);
        vector = mock(Vector2.class);
        
        when(owner.getPhysicsComponent()).thenReturn(physics);
        doNothing().when(physics).setLinearMovement(aim, 0);

        bossBounce = new BossBounce(aim, owner, drawProperties, textureID);
    }

    /**Test that the constructor throws exception when aim, owner or/and drawProperties is null and that it does not
     * throw anything for textureID. The constructor calls reset() wich uses
     * owner.getPhysicsComponent().setLinearMovement(aim, speed).
     */
    @Test
    public void testConstructorNullValues(){

        when(owner.getPhysicsComponent()).thenReturn(physics);
        doNothing().when(physics).setLinearMovement(aim, 0);
        
        assertThrows(NullPointerException.class, () -> new BossBounce(null, owner, drawProperties, textureID));
        assertThrows(NullPointerException.class, () -> new BossBounce(aim, null, drawProperties, textureID));
        assertThrows(NullPointerException.class, () -> new BossBounce(aim, owner, null, textureID));
        assertThrows(NullPointerException.class, () -> new BossBounce(null, null, drawProperties, textureID));
        assertThrows(NullPointerException.class, () -> new BossBounce(aim, null, null, textureID));
        assertThrows(NullPointerException.class, () -> new BossBounce(null, owner, null, textureID));
        assertThrows(NullPointerException.class, () -> new BossBounce(null, null, null, textureID));
        
        assertDoesNotThrow(() -> new BossBounce(aim, owner, drawProperties, null));
    }

    /**Test that all the necessary methods are called */
    @Test
    public void executeTest(){
        
        when(owner.getPhysicsComponent()).thenReturn(physics);
        when(physics.getVelocity()).thenReturn(vector);
        
        bossBounce.execute(1f);
        verify(drawProperties, times(1)).setAnimation(textureID);
        verify(vector, times(1)).angleDeg();
    }

    /**Test that the return value is not null */
    @Test
    public void isCompleteTest(){
        assertNotNull(bossBounce.isComplete());
    }

}
