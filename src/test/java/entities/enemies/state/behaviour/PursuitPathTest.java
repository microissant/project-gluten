package entities.enemies.state.behaviour;

import com.badlogic.gdx.math.Vector2;
import components.physics.Physics;
import entities.enemy.state.behaviour.PursuitPath;
import entities.navigation.Navigator;
import media.DrawProperties;
import media.TextureID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**Tests for  */

public class PursuitPathTest {
    private Navigator navigator;
    private Physics physics;
    private Vector2 targetPosition;
    private DrawProperties drawProperties;
    private PursuitPath pursuitPath;
    private LinkedList path;
    private final TextureID activePursuitID = TextureID.COOKIE_ENEMY_WALK; //The exact textureID does not matter in this test
    private final TextureID stationaryPursuitID = TextureID.COOKIE_ENEMY_IDLE;

    @BeforeEach
    public void setUp(){
        navigator = mock(Navigator.class);
        physics = mock(Physics.class);
        targetPosition = mock(Vector2.class);
        drawProperties = mock(DrawProperties.class);
        path = mock(LinkedList.class);
        pursuitPath = new PursuitPath(navigator, physics, drawProperties, activePursuitID, stationaryPursuitID);
    }

    /**Tests that the constructor does not throw exceptions when recieving null values */
    @Test
    public void testConstructorNullValues(){
        assertDoesNotThrow(() -> new PursuitPath(null, physics, drawProperties, stationaryPursuitID, activePursuitID));
        assertDoesNotThrow(() -> new PursuitPath(navigator, null, drawProperties, stationaryPursuitID, activePursuitID));
        assertDoesNotThrow(() -> new PursuitPath(navigator, physics, null, stationaryPursuitID, activePursuitID));
        assertDoesNotThrow(() -> new PursuitPath(navigator, physics, drawProperties, null, activePursuitID));
        assertDoesNotThrow(() -> new PursuitPath(navigator, physics, drawProperties, stationaryPursuitID, null));
        assertDoesNotThrow(() -> new PursuitPath(null, null, null, null, null));
    }

    @Test
    public void executeWhenPhysicsNull(){
        this.physics = null;
        assertDoesNotThrow(() -> pursuitPath.execute(1));}
    
    @Test
    public void executeWhenPathNull(){        
        when(navigator.getPath()).thenReturn(null);
        assertDoesNotThrow(() -> pursuitPath.execute(1));
        }
    
    @Test
    public void executeWhenTargetPositionNull(){        
        this.targetPosition= null;
        assertDoesNotThrow(() -> pursuitPath.execute(1));
    }
    
    /**Uses a mock path in order to check that getPath() is called once.
     * It also checks that the program does not chrash when the size of that path is 0
     * or when the distance between the target and the object is < 1
    */
    @CsvSource(value = { "0.0", "0.3", "-0.6", "-23.53" , "0.9999", "-52331"})
    @ParameterizedTest(name = "dst2: {0}")
    public void executeWhenPathSizeZero(float x){        
        
        when(navigator.getPath()).thenReturn(path);
        when(path.size()).thenReturn(0);
        when(targetPosition.dst2(any())).thenReturn(x);
    
        assertDoesNotThrow(() -> pursuitPath.execute(1));
        verify(navigator, times(1)).getPath();
        
    }
    
    /**Checks that the correct methods are called when the object is stationary and when it's not */
    @Test
    public void executeStationaryTest(){
        when(physics.isStationary()).thenReturn(true);
        pursuitPath.execute(1);
        verify(drawProperties,times(1)).getTextureID();
        verify(drawProperties,times(1)).setAnimation(stationaryPursuitID);
        
        when(physics.isStationary()).thenReturn(false);
        pursuitPath.execute(1);
        verify(drawProperties,times(2)).getTextureID();
        verify(drawProperties,times(1)).setAnimation(activePursuitID);
    }

    /**Verifies that physics.accelerate is called when execute() recieves no null values
     * and a path > 0
     */
    @Test
    public void executeBehaviourHelperAndPhysics(){
        
        when(navigator.getPath()).thenReturn(path);
        when(path.size()).thenReturn(10);
        when(path.pollLast()).thenReturn(mock(Vector2.class));
        when(physics.getPosition()).thenReturn(mock(Vector2.class));
        
        pursuitPath.execute(1);
        verify(navigator, times(1)).getPath();
        verify(path, times(1)).size();
        verify(physics, times(1)).accelerate(any());
    }
}
