package entities.enemies.state.behaviour;

import entities.enemy.state.behaviour.dash.DashHelper;
import entities.enemy.state.behaviour.dash.IceCubeDash;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.*;


/** Test class for the behaviour IceCubeDash */

public class IceCubeDashTest {

    private IceCubeDash iceCubeDash;
    private DashHelper dashHelper;

    @BeforeEach
    public void setUp(){

        dashHelper = mock(DashHelper.class);
        iceCubeDash = new IceCubeDash(dashHelper);
    }

    @Test
    public void testConstructorNullValues(){
        assertDoesNotThrow(() -> new IceCubeDash(null));
    }

    /**Checks that the owners setAngle() method is being called after getting
     * the angle in degrees of its vector (point)
     */
    @Test
    public void executeTest(){
        iceCubeDash.execute(1);
        
        verify(dashHelper, times(1)).setAngleToMovingDirection();
    }
    
    /**Checks that the endDash() method is being called after
     * verifying that the owners squared euclidean length is less that the endDashStrength
     */
    @Test
    public void isCompleteTest(){
        float speed = 0f;
        when(dashHelper.getCurrentSpeed2()).thenReturn(speed);
        
        iceCubeDash.isComplete();
        verify(dashHelper, times(1)).endDash();
    }
    
}
