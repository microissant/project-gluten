package entities.enemies.state.behaviour;

import entities.enemy.IEnemy;
import entities.enemy.state.behaviour.CherryBombExplosionBehaviour;
import media.DrawProperties;
import model.event.EventBus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

/** Test class for the behaviour CherryBombExplosion */

public class CherryBombExplosionTest {
    private IEnemy owner;
    private DrawProperties drawProperties;
    private EventBus eventBus;
    private CherryBombExplosionBehaviour cherryBombExplosion;

    @BeforeEach
    public void setUp(){
        owner = mock(IEnemy.class);
        drawProperties = mock(DrawProperties.class);
        eventBus = mock(EventBus.class);
        cherryBombExplosion = new CherryBombExplosionBehaviour(owner, drawProperties, eventBus);
    }

    /**Checks that an exception only is thrown when owner = null
     */
    @Test
    public void testConstructorNullValues(){
        assertThrows(NullPointerException.class,() -> new CherryBombExplosionBehaviour(null, drawProperties, eventBus));
        assertThrows(NullPointerException.class,() -> new CherryBombExplosionBehaviour(null, null, eventBus));
        assertThrows(NullPointerException.class,() -> new CherryBombExplosionBehaviour(null, null, null));
        assertDoesNotThrow(() -> new CherryBombExplosionBehaviour(owner, null, eventBus));
        assertDoesNotThrow(() -> new CherryBombExplosionBehaviour(owner, drawProperties, null));
        assertDoesNotThrow(() -> new CherryBombExplosionBehaviour(owner, null, null));        
    }

    @Test
    public void isCompleteTest(){
        assertFalse(cherryBombExplosion.isComplete());
    }
    
}
