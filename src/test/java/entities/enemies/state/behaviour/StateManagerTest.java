package entities.enemies.state.behaviour;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import entities.enemy.state.State;
import entities.enemy.state.StateDefinition;
import entities.enemy.state.StateManager;
import entities.enemy.state.behaviour.Idle;
import entities.enemy.state.condition.IStateBehaviourCondition;
import entities.enemy.state.condition.IdleStateCondition;
import media.DrawProperties;

/**Test class for StateManager */

public class StateManagerTest {
    private StateManager stateManager;
    private List<StateDefinition> stateDefinitionsList;
    private StateDefinition stateForList1;
    private StateDefinition stateForList2;
    private StateDefinition stateForList3;
    
    @BeforeEach
    public void setUp(){
        stateForList1 = mock(StateDefinition.class);
        stateForList2 = mock(StateDefinition.class);
        stateForList3 = mock(StateDefinition.class);
        this.stateDefinitionsList = new ArrayList<>();       
        this.stateDefinitionsList.add(stateForList1);
        this.stateDefinitionsList.add(stateForList2);
        this.stateDefinitionsList.add(stateForList3);
        stateManager = new StateManager(stateDefinitionsList);

    }
    
    /**Checks that the constructor does not throw an exception when recieving null */
    @Test
    public void testConstructorNullValues(){
        assertDoesNotThrow(() -> new StateManager(null));
        
    }

    /**Checks that the constructor works as expected when recieving a list of StateDefinitions */
    @Test
    public void testConstructorIterator(){        
        verify(stateForList1, times(1)).state();
        verify(stateForList2, times(1)).state();
        verify(stateForList3, times(1)).state();        
    }
    
    /**Checks that no exception is thrown when addStateDefinition recieves a list containing null */
    @Test
    public void addStateDefinitionNullListTest(){
        ArrayList<StateDefinition> nullList = new ArrayList<>();
        nullList.add(null);
        assertDoesNotThrow(() -> stateManager.addStateDefinition(nullList));
    }
    
    /**If .state() has been called 2 times, it means that the state was not registered before
     * 
    */
    @Test
    public void addNewStateDefinitionTest(){
        StateDefinition newStateDefinition = mock(StateDefinition.class);
        stateManager.addStateDefinition(newStateDefinition);
        
        verify(newStateDefinition, times(2)).state();
    }
    
    /**If .state() has been called 3 times, it means that the state was registered before:
     * once in this method and twice in the constructor 
    */
    @Test
    public void addExistingStateDefinitionTest(){
        stateManager.addStateDefinition(stateForList1);
        verify(stateForList1, times(3)).state();
    }
    
    /**Checks that the method does not throw an exception when recieving null */
    @Test
    public void removeStateDefinitionsNullTest(){
        assertDoesNotThrow(() -> stateManager.removeStateDefinition(null));
        
        //When the StateDefinitions gets sorted, the comparator will call on .state() for each one of them
        verify(stateForList1, times(1)).state();
        verify(stateForList2, times(1)).state();
        verify(stateForList3, times(1)).state();
        
    }
    
    /**Checks that the update method does not throw an exception when no StateDefinitions are recieved 
     * in the constructor
     */
    @Test
    public void updateEmptyStateDefinitions(){
        StateManager stateManager2 = new StateManager();
        assertDoesNotThrow(() -> stateManager2.update(1));
    }
    
    /**Checks that the update method works with a real StateDefinition and that getState()
     * returns the correct current state
     */
    @Test
    public void updateAndGetState(){
        StateManager stateManager2 = new StateManager();
        IStateBehaviourCondition behaviourCondition = new IdleStateCondition();
        DrawProperties drawProperties = mock(DrawProperties.class);
        StateDefinition realDefinition = new StateDefinition(State.IDLE, behaviourCondition, new Idle(drawProperties, null));
        
        stateManager2.addStateDefinition(realDefinition);
        assertDoesNotThrow(() -> stateManager2.update(1));
        assertEquals(State.IDLE, stateManager2.getState());
    }

    
    
    
}
