package entities.enemies.state.behaviour;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.badlogic.gdx.math.Vector2;
import components.physics.Physics;
import entities.enemy.state.behaviour.PursuitTarget;

/** Test class for the behaviour PursuitTarget */

public class PursuitTargetTest {
    private Vector2 targetDirection;
    private Physics physics;
    private PursuitTarget pursuitTarget;

    @BeforeEach
    public void setUp(){
        targetDirection = mock(Vector2.class);
        physics = mock(Physics.class);
        pursuitTarget = new PursuitTarget(targetDirection, physics);
    }

    @Test
    public void testConstructorNullValues(){
        assertDoesNotThrow(() -> new PursuitTarget(null, physics));
        assertDoesNotThrow(() -> new PursuitTarget(targetDirection, null));
        assertDoesNotThrow(() -> new PursuitTarget(null, null));
    }

    @Test
    public void executeTest(){
        pursuitTarget.execute(1);
        verify(physics, times(1)).accelerate(targetDirection);
    }
}
