package entities.enemies.state.behaviour;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import entities.enemy.IEnemy;
import entities.enemy.enemies.EvilCookie;
import entities.enemy.state.behaviour.Dead;
import model.event.EventBus;

/** Test class for the state Dead */

public class DeadTest {

    private EventBus eventBus;
    private IEnemy enemy;
    private Dead dead;

    @BeforeEach
    public void setUp(){
        eventBus = mock(EventBus.class);
        enemy = mock(EvilCookie.class);
        dead = new Dead(eventBus, false, enemy);
    }

    @Test
    public void testConstructorNullValues() {
        assertDoesNotThrow(() -> new Dead(eventBus, false, null));
        assertDoesNotThrow(() -> new Dead(null, false, enemy));
        assertDoesNotThrow(() -> new Dead(null, false, null));
    }

    @Test
    public void executeTest(){
        dead.execute(1);
        verify(eventBus, times(1)).post(any());
    }
    
}
