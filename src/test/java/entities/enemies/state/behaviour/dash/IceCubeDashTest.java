package entities.enemies.state.behaviour.dash;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import entities.enemy.state.behaviour.dash.DashHelper;
import entities.enemy.state.behaviour.dash.IceCubeDash;

public class IceCubeDashTest {
    private DashHelper dashHelper;
    private IceCubeDash iceCubeDash;

    @BeforeEach()
    public void setUp(){
        dashHelper = mock(DashHelper.class);
        iceCubeDash = new IceCubeDash(dashHelper);
    }

    /**Test that all the necessary methods are called */
    @Test
    public void execute(){
        iceCubeDash.execute(1);
        verify(dashHelper, times(1)).setAngleToMovingDirection();
    }

    /**Tests that isComplete() returns True when the dash ends and false otherwise */
    @Test
    public void isComplete(){
        when(dashHelper.getCurrentSpeed2()).thenReturn(0f);
        assertTrue(iceCubeDash.isComplete());
        verify(dashHelper, times(1)).getCurrentSpeed2();
        verify(dashHelper, times(1)).endDash();

        when(dashHelper.getCurrentSpeed2()).thenReturn(100000f);
        assertFalse(iceCubeDash.isComplete());

    }
    
}
