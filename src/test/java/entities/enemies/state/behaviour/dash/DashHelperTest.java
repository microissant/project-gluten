package entities.enemies.state.behaviour.dash;

import com.badlogic.gdx.math.Vector2;
import components.physics.Physics;
import entities.enemy.IEnemy;
import entities.enemy.state.behaviour.dash.DashHelper;
import media.DrawProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.*;

public class DashHelperTest {
    private IEnemy owner;
    private DashHelper dashHelper;
    private Physics physics;
    private Vector2 velocity;

    @BeforeEach
    public void setUp(){
        owner = mock(IEnemy.class);
        dashHelper = new DashHelper(owner);
        physics = mock(Physics.class);
        velocity = mock(Vector2.class);
    }

    /**Check that the constructor does not throw an exception when recieving null*/
    @Test
    public void testConstructorNullValues(){
        assertDoesNotThrow(() -> new DashHelper(null));
    }

    /**Test that all methods are called and that doDash() does not throw an exception
     * if dashDirection is null
    */
    @Test
    public void doDashTwoArgumentsTest(){
        assertDoesNotThrow(()-> dashHelper.doDash(null, 1f));

        Vector2 dashDirection = new Vector2();
        when(owner.getPhysicsComponent()).thenReturn(physics);
        dashHelper.doDash(dashDirection, 1f);
        verify(owner, times(1)).getPhysicsComponent();
        verify(physics, times(1)).disableInstantMovement();
        verify(physics, times(1)).enableFriction();
        verify(physics, times(1)).setFriction(80f);
        verify(physics, times(1)).setAccelerationForce(1.0f);
        verify(physics, times(1)).accelerate(dashDirection);
    }

    /**Check that the method does not throw an exception when recieving null*/
    @Test
    public void doDashThreeArguments(){
        assertDoesNotThrow(() -> dashHelper.doDash(null, null, 1f));
    }

    /**Test that all the necessary methods are called */
    @Test
    public void getPositionTest(){
        dashHelper.getPosition();
        verify(owner, times(1)).getPosition();
    }

    /**Test that all the necessary methods are called */
    @Test
    public void getCurrentSpeed2Test(){
        when(owner.getPhysicsComponent()).thenReturn(physics);
        when(physics.getVelocity()).thenReturn(velocity);
        dashHelper.getCurrentSpeed2();
        verify(velocity, times(1)).len2();
    }

    /**Test that all the necessary methods are called */
    @Test
    public void setAngleToMovingDirection(){
        when(owner.getPhysicsComponent()).thenReturn(physics);
        when(physics.getVelocity()).thenReturn(velocity);
        when(owner.getProperties()).thenReturn(mock(DrawProperties.class));

        dashHelper.setAngleToMovingDirection();
        verify(velocity, times(1)).angleDeg();
    }


}
