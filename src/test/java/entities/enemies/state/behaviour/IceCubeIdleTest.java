package entities.enemies.state.behaviour;

import com.badlogic.gdx.math.Vector2;
import entities.enemy.state.behaviour.IceCubeIdle;
import entities.enemy.state.behaviour.dash.DashHelper;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

/** Test class for the behaviour IceCubeIdle */

public class IceCubeIdleTest {
    private final DashHelper dashHelper = mock(DashHelper.class);
    private final IceCubeIdle iceCubeIdle = new IceCubeIdle(dashHelper);

    @Test
    public void testConstructorNullValues(){
        assertDoesNotThrow(() -> new IceCubeIdle(null));
    }
    
    /**IceCubeIdle has a private variable timeUntilRandomDash that dictates how many time units
     * will have to pass until the ice cube can perform a dash. This number is randomly set and
     * has a bound of 2f.
     * This test checks that the iceCube does not dash when 0 delta time units have passed
     * and that it always dashes after two units.
     * Reset should adjust timeUntilRandomDash back to a random number <= 2 
     */
    @Test
    public void executeResetTest(){
        
        iceCubeIdle.execute(0);
        verify(dashHelper, never()).doDash(any(Vector2.class),eq(iceCubeIdle.dashStrength));   
        iceCubeIdle.execute(2);
        verify(dashHelper, times(1)).doDash(any(Vector2.class),eq(iceCubeIdle.dashStrength));

        iceCubeIdle.reset();
        
        iceCubeIdle.execute(0);
        verify(dashHelper, times(1)).doDash(any(Vector2.class),eq(iceCubeIdle.dashStrength));
        iceCubeIdle.execute(2);
        verify(dashHelper, times(2)).doDash(any(Vector2.class),eq(iceCubeIdle.dashStrength));

    }
}
