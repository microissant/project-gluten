package entities.enemies.state.behaviour;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.badlogic.gdx.math.Vector2;
import entities.enemy.IEnemy;
import entities.enemy.state.behaviour.CherryDeath;
import model.event.EventBus;

/** Test class for the behaviour CherryDeath */

public class CherryDeathTest {
    private EventBus eventBus;
    private IEnemy enemy;
    private Vector2 position;
    private CherryDeath cherryDeath;

    @BeforeEach
    public void setUp(){
        eventBus = mock(EventBus.class);
        enemy = mock(IEnemy.class);
        position = mock(Vector2.class);
    }

    @Test
    public void testConstructorNullValues(){
        assertDoesNotThrow(()->new CherryDeath(null, true, enemy, position));
        assertDoesNotThrow(()->new CherryDeath(eventBus, true, null, position));
        assertDoesNotThrow(()->new CherryDeath(eventBus, true, enemy, null));
        assertDoesNotThrow(()->new CherryDeath(null, true, null, position));
        assertDoesNotThrow(()->new CherryDeath(eventBus, true, null, null));
        assertDoesNotThrow(()->new CherryDeath(null, true, enemy, null));
        assertDoesNotThrow(()->new CherryDeath(null, true, null, null));
    }
    
    /**Checks that eventBus does not post any new event if the cherry is already dead */
    @Test
    public void executeWhenDeadTest(){
        cherryDeath = new CherryDeath(eventBus, true, enemy, position);
        cherryDeath.execute(1);
        verify(eventBus, never()).post(any());
    }
    
    /**Checks that eventBus posts two new events if the cherry is alive */
    @Test
    public void executeWhenAliveTest(){
        cherryDeath = new CherryDeath(eventBus, false, enemy, position);
        cherryDeath.execute(1);
        verify(eventBus, times(2)).post(any());
    }
}
