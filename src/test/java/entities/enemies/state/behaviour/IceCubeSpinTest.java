package entities.enemies.state.behaviour;

import com.badlogic.gdx.math.Vector2;
import components.physics.Physics;
import entities.enemy.enemies.IceCube;
import entities.enemy.state.behaviour.IceCubeSpin;
import entities.enemy.state.behaviour.dash.DashHelper;
import media.DrawProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/** Test class for the behaviour IceCubeSpin */

public class IceCubeSpinTest {
    private IceCube owner;
    private Vector2 targetPosition;
    private IceCubeSpin iceCubeSpin;
    private DashHelper dashHelper;

    @BeforeEach
    public void setUp(){
        owner = mock(IceCube.class);
        targetPosition = mock(Vector2.class);
        dashHelper = mock(DashHelper.class);
        iceCubeSpin = new IceCubeSpin(dashHelper,targetPosition);
    }

    @Test
    public void testConstructorNullValues(){
        assertDoesNotThrow(() -> new IceCubeSpin(dashHelper,null));
        assertDoesNotThrow(() -> new IceCubeSpin(null,targetPosition));
        assertDoesNotThrow(() -> new IceCubeSpin(null, null));
    }

    /**Spin is complete when spintime amount of delta time has passed.
     * isComplete() should not return true until then.
     */
    @Test
    public void isCompleteTest(){
        when(owner.getPhysicsComponent()).thenReturn(mock(Physics.class));
        when(owner.getProperties()).thenReturn(mock(DrawProperties.class));
        when(owner.getPosition()).thenReturn(mock(Vector2.class));        
        assertFalse(iceCubeSpin.isComplete());    
        iceCubeSpin.execute(IceCubeSpin.spinTime);
        assertTrue(iceCubeSpin.isComplete());
    }
    
    //TODO: fix this javadoc
    /** Checks that the ice cube dashes when spintime amount of delta time has passed 
     * and that it sets the new angle after.
     * It then resets timeLeft to spintime amount and makes sure that no dash is being executed
     * if delta time remains unchanged. 
     * The angle should update regardless
    */
    @Test
    public void executeTest(){

        iceCubeSpin.execute(IceCubeSpin.spinTime);
        verify(dashHelper, times(1)).doDash(dashHelper.getPosition(),targetPosition, IceCubeSpin.dashStrength);
        iceCubeSpin.reset();

        for (int i=0; i <100; i++){
            iceCubeSpin.execute(0);        
        }
        verify(dashHelper, times(1)).doDash(dashHelper.getPosition(),targetPosition, IceCubeSpin.dashStrength);
    }
    
}
