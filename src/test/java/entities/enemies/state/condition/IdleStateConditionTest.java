package entities.enemies.state.condition;

import entities.enemy.state.condition.IdleStateCondition;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**Tests for condition IdleState */

public class IdleStateConditionTest {
    private final IdleStateCondition idleStateCondition = new IdleStateCondition();

    /**Check That this method does not return a null value*/
    @Test
    public void stateConditionMetTest(){
        assertNotNull(idleStateCondition.stateConditionMet());
    }

    /**Check That this method does not return a null value*/
    @Test
    public void getStateTest(){
        assertNotNull(idleStateCondition.getState());
    }
}
