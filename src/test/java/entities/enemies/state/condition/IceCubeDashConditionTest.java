package entities.enemies.state.condition;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import entities.enemy.state.behaviour.dash.DashHelper;
import entities.enemy.state.condition.IceCubeDashCondition;

/**Tests for condition IceCubeDashCondition */

public class IceCubeDashConditionTest {
    private DashHelper dashHelper;
    private IceCubeDashCondition iceCubeDashCondition;

    @BeforeEach
    public void setUp(){
        dashHelper = mock(DashHelper.class);
        iceCubeDashCondition = new IceCubeDashCondition(dashHelper);
    }
    
    /**Check that the constructor does not throw an exception when recieving null*/
    @Test
    public void testConstructorNullValues(){
        assertDoesNotThrow(() -> new IceCubeDashCondition(null));
    }

    /**Check that this method does not return a null value
    */
    @Test
    public void stateConditionMetTest(){
        assertNotNull(iceCubeDashCondition.stateConditionMet());
    }
    
    /**Check that this method does not return a null value
    */
    @Test
    public void getStateTest(){
        assertNotNull(iceCubeDashCondition.getState());
    }
}
