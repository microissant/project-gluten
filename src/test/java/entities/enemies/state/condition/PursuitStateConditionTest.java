package entities.enemies.state.condition;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.badlogic.gdx.math.Vector2;
import entities.enemy.state.IStatefulEntity;
import entities.enemy.state.condition.PursuitStateCondition;
import grid.WorldGrid;

/**Tests for the condition PursuitStateCondition */

public class PursuitStateConditionTest {
    private Vector2 vectorTowardTarget;
    private Vector2 entityPosition;
    private Vector2 targetPosition;
    private IStatefulEntity owner;
    private WorldGrid worldGrid;
    private PursuitStateCondition pursuitStateCondition;

    @BeforeEach
    public void setUp(){
        vectorTowardTarget = mock(Vector2.class);
        entityPosition = mock(Vector2.class);
        targetPosition = mock(Vector2.class);
        owner = mock(IStatefulEntity.class);
        worldGrid = mock(WorldGrid.class);
        pursuitStateCondition = new PursuitStateCondition(owner, entityPosition, targetPosition, vectorTowardTarget, 0, worldGrid);
    }

    /**Check that the constructor does not throw an exception when recieving null*/
    @Test
    public void testConstructorNullValues(){
        assertDoesNotThrow(() -> new PursuitStateCondition(null, entityPosition, targetPosition, vectorTowardTarget, 0, worldGrid));
        assertDoesNotThrow(() -> new PursuitStateCondition(owner, null, targetPosition, vectorTowardTarget, 0, worldGrid));
        assertDoesNotThrow(() -> new PursuitStateCondition(owner, entityPosition, null, vectorTowardTarget, 0, worldGrid));
        assertDoesNotThrow(() -> new PursuitStateCondition(owner, entityPosition, targetPosition, null, 0, worldGrid));
        assertDoesNotThrow(() -> new PursuitStateCondition(owner, entityPosition, targetPosition, vectorTowardTarget, 0, null));
        assertDoesNotThrow(() -> new PursuitStateCondition(null, null, null, null, 0, null));
    }

    /**Check that this method does not return a null value
    */
    @Test
    public void stateConditionMetTest(){
        assertNotNull(pursuitStateCondition.stateConditionMet());
    }
    
    /**Check that this method does not return a null value
    */
    @Test
    public void getStateTest(){
        assertNotNull(pursuitStateCondition.getState());
    }
}
