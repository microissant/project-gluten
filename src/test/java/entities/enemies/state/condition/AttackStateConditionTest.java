package entities.enemies.state.condition;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.badlogic.gdx.math.Vector2;
import entities.Entity;
import entities.enemy.state.IStatefulEntity;
import entities.enemy.state.condition.AttackStateCondition;
import grid.WorldGrid;
import grid.TileData.TileRuleset;

/**Tests for condition AttackStateCondition */

public class AttackStateConditionTest {
    private Vector2 targetPosition;
    private IStatefulEntity owner;
    private Vector2 ownerPosition;
    private TileRuleset ruleset;
    private WorldGrid worldGrid;
    private AttackStateCondition attackStateCondition;

    @BeforeEach
    public void setUp(){
        targetPosition = new Vector2(10,10);
        owner = mock(IStatefulEntity.class);
        ownerPosition = mock(Vector2.class);
        ruleset = TileRuleset.WALKING; //The exact Tileruleset does not matter for this test
        worldGrid = mock(WorldGrid.class);
        attackStateCondition = new AttackStateCondition(owner, targetPosition, ownerPosition, ruleset, worldGrid);
    }

    /**Check that the constructor throws an IllegalArgumentException when either owner,
     * targetPosition or ownerPosition is null.
     * The test also checks that the constructor does not throw an exception if ruleset or worldGrid is null
     */
    @Test
    public void testConstructorNullValues(){
        assertThrows(IllegalArgumentException.class, () -> new AttackStateCondition(null, targetPosition, ownerPosition, ruleset, worldGrid));
        assertThrows(IllegalArgumentException.class, () -> new AttackStateCondition(owner, null, ownerPosition, ruleset, worldGrid));
        assertThrows(IllegalArgumentException.class, () -> new AttackStateCondition(owner, targetPosition, null, ruleset, worldGrid));
        assertThrows(IllegalArgumentException.class, () -> new AttackStateCondition(null, null, ownerPosition, ruleset, worldGrid));
        assertThrows(IllegalArgumentException.class, () -> new AttackStateCondition(owner, null, null, ruleset, worldGrid));
        assertThrows(IllegalArgumentException.class, () -> new AttackStateCondition(null, targetPosition, null, ruleset, worldGrid));
        assertThrows(IllegalArgumentException.class, () -> new AttackStateCondition(null, null, null, ruleset, worldGrid));
        
        assertDoesNotThrow(() -> new AttackStateCondition(owner, targetPosition, ownerPosition, null, worldGrid));
        assertDoesNotThrow(() -> new AttackStateCondition(owner, targetPosition, ownerPosition, ruleset, null));
        assertDoesNotThrow(() -> new AttackStateCondition(owner, targetPosition, ownerPosition, null, null));    
    }

    /**Check that the method returns false given the right conditions */
    @Test
    public void stateConditionMetFalseTest(){
        //returns false if owner does not have a target
        Entity target = mock(Entity.class);
        when(owner.hasTarget()).thenReturn(false);
        assertFalse(attackStateCondition.stateConditionMet());
        
        //returns false if target is dead
        reset(owner);
        when(owner.hasTarget()).thenReturn(true);
        when(owner.getTarget()).thenReturn(target);
        when(target.isDead()).thenReturn(true);
        assertFalse(attackStateCondition.stateConditionMet());
        
        //returns false if the target it out of attacking range
        //targetPosition is purposely set to be far away in the constructor
        reset(target);
        when(target.isDead()).thenReturn(false);        
        when(owner.getAttackRange()).thenReturn(0f);
        assertFalse(attackStateCondition.stateConditionMet());
    }

    /**Check that this method does not return a null value
    */
    @Test
    public void getStateTest(){
        assertNotNull(attackStateCondition.getState());
    }
}
