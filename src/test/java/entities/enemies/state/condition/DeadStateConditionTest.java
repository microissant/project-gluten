package entities.enemies.state.condition;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import components.health.Killable;
import entities.enemy.state.condition.DeadStateCondition;

/**Tests for the condition DeadStateCondition */

public class DeadStateConditionTest {
    private Killable killable;
    private DeadStateCondition deadStateCondition;

    @BeforeEach
    public void setUp(){
        killable = mock(Killable.class);
        deadStateCondition = new DeadStateCondition(killable);
    }

    /**Check that the constructor does not throw an exception when recieving null*/
    @Test
    public void testConstructorNullValues(){
        assertDoesNotThrow(() -> new DeadStateCondition(null));
    }

    /**Check that this method does not return a null value
    */
    @Test
    public void stateConditionMetTest(){
        assertNotNull(deadStateCondition.stateConditionMet());
    }
    
    /**Check that this method does not return a null value
    */
    @Test
    public void getStateTest(){
        assertNotNull(deadStateCondition.getState());
    }
}
