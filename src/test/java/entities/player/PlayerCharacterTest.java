package entities.player;

import grid.TileData;
import grid.WorldGrid;
import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType;
import components.collision.ICollidable;
import components.collision.primitives.CollisionCircle;
import components.currency.ITrader;
import components.health.Damage;
import components.health.Healer;
import components.health.HealthManager;
import components.health.IHealable;
import components.physics.Physics;
import components.upgrade.upgrades.IUpgrade;
import components.upgrade.upgrades.Upgrade;
import components.weapon.Weapon;
import components.weapon.WeaponManager;
import item.Interactable;
import media.DrawProperties;
import model.event.EventBus;
import model.event.events.input.InputDodge;
import model.event.events.input.InputInteract;
import model.event.events.input.InputMove;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mock;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class PlayerCharacterTest {
    private PlayerCharacter playerCharacter;

    @Mock
    private PlayerManager playerManager;

    @Mock
    private Physics physics;

    @Mock
    EventBus eventBus;

    @BeforeEach
    public void setUp() {
        playerManager = mock(PlayerManager.class);
        physics = mock(Physics.class);
        eventBus = mock(EventBus.class);
        playerCharacter = new PlayerCharacter(physics, playerManager, eventBus);

    }


    @CsvSource(value = {"1.0,1.0", "2.5, -1.3", "0.34, 0.0", "-0.253, -23.53", "0, 0", "-1000, -52331"})
    @ParameterizedTest(name = "Position x: {0} y: {1}")
    public void spawnPlayerPosition(float x, float y) {
        PlayerCharacter pc = new PlayerCharacter(new Physics(new Vector2(x, y)), null, null);

        assertEquals(new Vector2(x, y), pc.getPosition());
    }

    /**

     Tests that pushing the player character changes its position.
     */
    @Test
    public void pushChangesPosition() {
        PlayerCharacter pc = new PlayerCharacter(new Physics(new Vector2(0, 0)), null, null);

        pc.push(new Vector2(500, 0));
        pc.update(1);

        assertNotEquals(new Vector2(0, 0), pc.getPosition());
    }

    /**

     Tests that moving the player character in a given direction changes its position correctly.

     @param x the x-component of the direction to move the player character in

     @param y the y-component of the direction to move the player character in
     */
    @CsvSource(value = {"1.0,1.0", "2.5, -1.3", "0.34, 0.0", "-0.253, -23.53", "0, 0", "-1000, -52331"})
    @ParameterizedTest(name = "direction x: {0} y: {1}")
    public void moveCharacter(float x, float y) {
        PlayerManager pm = new PlayerManager(new Vector2(0, 0), 3);
        PlayerCharacter pc = pm.getPlayerCharacter();

        Vector2 dir = new Vector2(x, y);

        double dt = 0.0166666667;
        Vector2 prev = new Vector2(pc.getPosition().x, pc.getPosition().y);
        for (int i = 0; i < 10000; i++) {
            pc.move(dir);
            pc.update(dt);

            checkIfPositionChangedCorrectly(dir.x, prev.x, pc.getPosition().x);
            checkIfPositionChangedCorrectly(dir.y, prev.y, pc.getPosition().y);

            prev = new Vector2(pc.getPosition().x, pc.getPosition().y);
        }
    }

    private void checkIfPositionChangedCorrectly(float dir, float prev, float updated) {
        if (dir == 0) assertEquals(prev, updated);
        else if (dir > 0) assertTrue(updated > prev);
        else assertTrue(updated < prev);
    }


    @Test
    public void testSetInteractTarget() {

        // create a mock Interactable object
        Interactable interactable = mock(Interactable.class);

        // call the setInteractTarget method with the mock object
        playerCharacter.setInteractTarget(interactable);

        // check if the interactableTarget field was set correctly
        assertEquals(interactable, playerCharacter.getInteractTarget());
    }

    @Test
    public void testRemoveInteractTarget() {
        Interactable target = mock(Interactable.class);

        playerCharacter.setInteractTarget(target);
        assertNotNull(playerCharacter.getInteractTarget());

        playerCharacter.removeInteractTarget();
        // if there is  a interact target
        assertNull(playerCharacter.getInteractTarget());
    }

    @Test
    public void testGetInteractAgent() {

        assertEquals(playerCharacter, playerCharacter.getInteractAgent());
    }

/**

 Tests the addition of an upgrade to the player manager's list of upgrades.
 It creates a new player manager with a player character and mocks an upgrade.
 Then, it calls the addUpgrade() method on the player manager and asserts that
 the addUpgrade() method returns true, indicating that the upgrade was successfully added.
 */
    @Test
    void testAddUpgrade() {
        EventBus eventBus = mock(EventBus.class);
        PlayerManager playerManager = new PlayerManager(new Vector2(1, 3), 4, eventBus);
        PlayerCharacter playerCharacter = new PlayerCharacter(new Physics(), playerManager, eventBus);
        IUpgrade upgrade = mock(Upgrade.class);

        when(playerCharacter.addUpgrade(upgrade)).thenReturn(true);
        playerManager.addUpgrade(upgrade);
    }

    @Test
    public void testNormalUpdate() {
        // create a mock Physics object with zero velocity
        EventBus eventBus = mock(EventBus.class);
        PlayerManager playerManager = new PlayerManager(new Vector2(1, 3), 4, eventBus);

        // create a  PlayerCharacter with the  Physics object
        PlayerCharacter player = new PlayerCharacter(new Physics(), playerManager, eventBus);

        // call the update method
        player.update(1.0);


        // check that the last safe position has been updated
        assertEquals(new Vector2(0, 0), player.getPosition());
    }

    @Test
    public void testIsDodging() {
        // create a mock Physics object with a velocity of (1, 1)
        EventBus eventBus = mock(EventBus.class);
        PlayerManager playerManager = new PlayerManager(new Vector2(1, 3), 4, eventBus);
        // create a PlayerCharacter with the mock Physics object
        PlayerCharacter player = new PlayerCharacter(new Physics(), playerManager, eventBus);

        Physics physics = new Physics();
        player.setPosition(new Vector2(1, 1));
        assertEquals(new Vector2(1, 1), player.getPosition());


        // create a PlayerCharacter with the mock Physics object
        player = new PlayerCharacter(physics, playerManager, eventBus);

        // set up the player's dodge state

        player.move(new Vector2(1, 0));


        // create a mock WorldGrid object that returns true for TileData.TileProperty.HOLE at the player's position
        WorldGrid worldGrid = mock(WorldGrid.class);
        Vector2 holePosition = new Vector2(5, 5);
        physics.setPosition(holePosition);
        //when(player.tryDodge(e)).thenReturn(holePosition);
        when(worldGrid.checkForTilePropertyAtPixel(holePosition.x, holePosition.y, TileData.TileProperty.HOLE.val))
                .thenReturn(true);

        // call the update method
        player.update(1.0);

        // assert that the dodge was ended and the falling state was set
        assertFalse(player.isDodging());

    }


    @Test
    public void testGetMaximumHealth() {
        HealthManager healthManager = mock(HealthManager.class);
        when(playerManager.getHealthManager()).thenReturn(healthManager);
        when(healthManager.getMaximumHealth()).thenReturn(1);

        int max = playerCharacter.getMaximumHealth();

        assertEquals(1, max);
        verify(playerManager.getHealthManager()).getMaximumHealth();

    }

    @Test
    public void testSetWorldGrid() {

        TileData tileData = new TileData(TileData.TileType.EMPTY);
        WorldGrid worldGrid= new WorldGrid(4,5,10, tileData);
        PlayerCharacter player = new PlayerCharacter(new Physics(), playerManager, eventBus);

        player.setWorldGrid(worldGrid);

        // check if equals.
        assertEquals(worldGrid, player.getPhysicsComponent().getWorldGrid());
    }

    @Test
    public void testGetDamageableReturnsThis() {
        assertEquals(playerCharacter, playerCharacter.getDamageable());
    }

    @Test
    public void testGetDirectionReturnNomalizedVelocity() {
        Vector2 velocity = new Vector2(1, 3);
        Vector2 expectedDirection = new Vector2(velocity).nor();
        when(physics.getVelocity()).thenReturn(velocity);

        assertEquals(expectedDirection, playerCharacter.getDirection());
    }

    @Test
    public void testUnregisterFromEventBus() {
        playerCharacter.unregisterFromEventBus();

        verify(eventBus, times(1)).unregister(playerCharacter);
    }

    @Test
    public void getPositionTest() {
        Vector2 position = mock(Vector2.class);
        playerCharacter.getPosition();

    }

    @Test
    public void testTryDodge() {

        InputDodge inputDodge = new InputDodge();
        when(physics.getVelocity()).thenReturn(new Vector2());

        playerCharacter.tryDodge(inputDodge);

        verify(physics, times(1)).getVelocity();
    }

    @Test
    public void testInteractEvent() {

        InputInteract event = new InputInteract();
        Interactable interactable = mock(Interactable.class);

        playerCharacter.setInteractTarget(interactable);
        playerCharacter.interactEvent(event);
        // verify that interactable be called when the methode run.
        verify(interactable, times(1)).onInteract(playerCharacter);
    }

    @Test

    public void testInteractNullEvent() {
        Interactable interactable = mock(Interactable.class);

        playerCharacter.setInteractTarget(interactable);
        playerCharacter.interactEvent(null);

        //verify that interactable never called event is null
        verify(interactable, never()).onInteract(playerCharacter);

    }

    @Test
    public void testInteractEventWhenNullInteractable() {
        InputInteract event = new InputInteract();
        Interactable interactable = mock(Interactable.class);

        playerCharacter.setInteractTarget(null);
        playerCharacter.interactEvent(event);

        //verify that interactable never called when it is null
        verify(interactable, never()).onInteract(playerCharacter);

    }

    @Test
    public void testInteractNullEventWhenNullInteractable() {

        Interactable interactable = mock(Interactable.class);

        playerCharacter.setInteractTarget(null);
        playerCharacter.interactEvent(null);
        //verify that interactable never called when both event and interactable are null.
        verify(interactable, never()).onInteract(playerCharacter);


    }


    @Test
    public void testPush() {


        Vector2 expectedDir = new Vector2(1, 0);


        playerCharacter.push(expectedDir);

        when(physics.getVelocity()).thenReturn(new Vector2());

        verify(physics).applyForce(expectedDir);
    }

    @Test
    public void testMove() {

        WeaponManager weaponManager = mock(WeaponManager.class);
        Weapon weapon = mock(Weapon.class);
        when(playerManager.getWeaponManager()).thenReturn(weaponManager);
        when(weaponManager.getPrimary()).thenReturn(weapon);
        when(playerManager.getHealthManager()).thenReturn(new HealthManager(1));


        Vector2 dir = new Vector2(1, 0);


        playerCharacter.move(dir);


        verify(physics).accelerate(eq(dir));
        verify(weapon).setMoveDirection(eq(dir));
    }

    @Test
    public void testMovePressed() {
        Vector2 direction = new Vector2(1, 0);
        InputMove inputMove = new InputMove(direction);

        when(playerManager.getHealthManager()).thenReturn(new HealthManager(1));
        when(playerManager.getWeaponManager()).thenReturn(mock(WeaponManager.class));


        playerCharacter.movePressed(inputMove);

        // verify that playerManger called once when the method getWeaponManager() is run.
        verify(playerManager, times(1)).getWeaponManager();

        // verify that playerManger called once when the method getPrimary() is run.
        verify(playerManager.getWeaponManager(), times(1)).getPrimary();


    }

    @Test
    public void testGetTrader() {

        ITrader trader = mock(ITrader.class);
        when(playerManager.getTrader()).thenReturn(trader);


        ITrader result = playerCharacter.getTrader();

       // assert that trade is the same playerCharacter.getTrader().
        assertEquals(trader, result);
    }


    @Test
    public void testCollisionEnter() {
        ICollidable target = mock(ICollidable.class);
        CollisionType.Type type = CollisionType.Type.ENEMY;
        when(playerManager.getPlayerCharacter()).thenReturn(playerCharacter);


        playerCharacter.collisionEnter(target, type);

    }


    @Test
    public void testGetHealthComponent() {

        HealthManager expectedHealthManager = mock(HealthManager.class);
        when(playerManager.getHealthManager()).thenReturn(expectedHealthManager);


        HealthManager actualHealthManager = playerCharacter.getHealthComponent();

        // Assert
        assertEquals(expectedHealthManager, actualHealthManager);
    }

    @Test
    public void testGetPhysicsComponent() {

        Physics result = playerCharacter.getPhysicsComponent();

        // Assert
        assertEquals(physics, result);
    }

    @Test
    public void testGetCollisionPrimitive() {

        CollisionCircle expectedCollisionCircle = mock(CollisionCircle.class);
        playerCharacter.getCollisionPrimitive();
        // verify(expectedCollisionCircle, times(1));
    }

    @Test
    public void testGetCurrentHealth() {
        HealthManager healthManager = mock(HealthManager.class);
        when(playerManager.getHealthManager()).thenReturn(healthManager);
        when(healthManager.getCurrentHealth()).thenReturn(50);

        int currentHealth = playerCharacter.getCurrentHealth();

        assertEquals(50, currentHealth);
        verify(playerManager, times(1)).getHealthManager();
        verify(healthManager, times(1)).getCurrentHealth();
    }

    @Test
    public void testApplyHeal() {
        // Create a mock healer
        Healer healer = mock(Healer.class);

        // Create a mock HealthManager and stub its heal() method
        HealthManager healthManager = mock(HealthManager.class);
        when(playerManager.getHealthManager()).thenReturn(healthManager);

        // Call the method being tested
        playerCharacter.applyHeal(healer);

        // Verify that the HealthManager's heal() method was called once with the correct argument
        verify(healthManager, times(1)).heal(healer);
    }


    @Test
    public void testApplyDamage() {

        // Mock a damage object with a damage value of 10
        Damage damage = mock(Damage.class);
        when(damage.getDamageValue()).thenReturn(10);
        when(playerManager.getHealthManager()).thenReturn(mock(HealthManager.class));
        // Call applyDamage() and verify that it was called on the mock HealthManager with the correct arguments
        playerCharacter.applyDamage(damage);
        //verify(damage, times(1));
    }

    @Test
    public void testGetHealable() {
        IHealable healable = mock(IHealable.class);
        when(playerManager.getHealable()).thenReturn(healable);

        IHealable actualHealable = playerCharacter.getHealable();

        assertEquals(healable, actualHealable);
    }

    @Test
    public void testAddAndRemoveUpgrade() {
        // Create a mock upgrade
        IUpgrade upgrade = mock(IUpgrade.class);

        // Add the upgrade to the player manager
        playerCharacter.addUpgrade(upgrade);

        // Call the removeUpgrade method to remove the upgrade
        playerCharacter.removeUpgrade(upgrade);


        // Verify that the upgrade was added successfully
        verify(playerManager, times(1)).removeUpgrade(upgrade);


        // Verify that the upgrade was removed successfully
        //assertFalse(playerCharacter.getUpgrades().contains(upgrade));
    }

    @Test
    public void testRemoveAllUpgrades() {
        // Create some upgrades
        IUpgrade upgrade1 = mock(IUpgrade.class);
        IUpgrade upgrade2 = mock(IUpgrade.class);

        // Add the upgrades to the player manager
        playerCharacter.addUpgrade(upgrade1);
        playerCharacter.addUpgrade(upgrade2);
        playerCharacter.removeAllUpgrades();

        verify(playerManager, times(1)).removeAllUpgrades();
    }


    @Test
    public void testGetProperties() {

        DrawProperties props = new DrawProperties();
        props.rotation = 0f;
        props.scaleX = 1f;
        props.scaleY = 1f;

        DrawProperties result = playerCharacter.getProperties();
        assertEquals(result.rotation, props.rotation, 0.001);
        assertEquals(result.scaleX, props.scaleX, 0.001);
        assertEquals(result.scaleY, props.scaleY, 0.001);
    }




}



