package entities.player;

import com.badlogic.gdx.math.Vector2;

import components.currency.CurrencyManager;
import components.currency.ITrader;
import components.health.Damage;
import components.physics.Physics;
import components.upgrade.upgrades.IUpgrade;
import components.upgrade.upgrades.Upgrade;
import item.loot.ILoot;
import model.event.events.input.InputPrimary;
import model.event.events.item.ObtainLootEvent;
import model.event.events.entities.PlayerDeathEvent;
import model.event.events.ui.InitializeHUDEvent;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import components.currency.coin.Coin;
import components.weapon.Weapon;
import model.event.EventBus;
import org.mockito.Mockito;

public class PlayerManagerTest {


    /**
     * Tests that a player is created with the correct position.
     *
     * @param x The x-coordinate of the player's position.
     * @param y The y-coordinate of the player's position.
     */
    @CsvSource(value = {"1.0,1.0", "2.5, -1.3", "0.34, 0.0", "-0.253, -23.53", "0, 0", "-1000, -52331"})
    @ParameterizedTest(name = "Position x: {0} y: {1}")
    public void createWithCorrectPosition(float x, float y) {
        PlayerManager player = new PlayerManager(new Vector2(x, y), 3);

        assertEquals(new Vector2(x, y), player.getPhysicsBody().getPosition());

    }


    private PlayerManager playerManager;
    private EventBus eventBus;
    protected CurrencyManager currencyManager;
    private Physics physics;


    /**
     * Initializes objects needed for each test method.
     */
    @BeforeEach
    void setUp()  {
        physics= new Physics();

        currencyManager = new CurrencyManager();
        Vector2 aimDirection = new Vector2(0, 0);
        eventBus = mock(EventBus.class);
        playerManager = new PlayerManager(aimDirection, 4, eventBus);

    }
    /**
     * Tests that the player has a healable component.
     */
    @Test
    void testGetHealable() {
        assertNotNull(playerManager.getHealable());
    }

    /**
     * Tests that the player has a physics body.
     */
    @Test
    void testGetPhysicsBody() {
        assertNotNull(playerManager.getPhysicsBody());
    }


    /**
     * Tests that the player has a player character component.
     */
    @Test
    void testGetPlayerCharacter() {
        assertNotNull(playerManager.getPlayerCharacter());
    }


    /**
     * Tests that the player has a weapon manager component.
     */
    @Test
    void testGetWeaponManager() {
        assertNotNull(playerManager.getWeaponManager());
    }

    /**
     * Tests that the player has the correct coin balance.
     */
    @Test
    void testGetCoinBalance() {
        assertEquals(playerManager.getCoinBalance(), new Coin(10));
    }

    /**
     * Tests that the player's attack method works correctly.
     */
    @Test
    void testAttack() {
        playerManager.attack();
        assertNull(playerManager.getWeaponManager().getPrimary());
    }

    /**
     * Tests that a weapon can be added to the player's weapon manager.
     */
    @Test
    void testAddWeapon() {
        Weapon weapon = mock(Weapon.class);
        playerManager.addWeapon(weapon);
        Weapon primeryWeapon = playerManager.getWeaponManager().getPrimary();
        assertEquals(weapon, primeryWeapon);
    }

    /**
     * Tests that an upgrade can be added to the player's upgrade manager.
     */
    @Test
    void testAddUpgrade() {
        IUpgrade upgrade = mock(Upgrade.class);
        when(playerManager.addUpgrade(upgrade)).thenReturn(true);
        playerManager.addUpgrade(upgrade);
    }

    /**
     * Tests that the player's event bus can be retrieved.
     */
    @Test
    void getEventBusTest() {
        eventBus.post(new InputPrimary(new Vector2(2, 1)));
        assertNotNull(playerManager.getEventBus());
    }


    /**
     * Tests that the player's update method works correctly.
     */
    @Test
    void testUpdate() {
        Damage damage = new Damage(100);
        playerManager.update(1);

        assertFalse(playerManager.getPlayerCharacter().isDead());

        playerManager.getHealthManager().applyDamage(damage);
        playerManager.update(1);
        assertTrue(playerManager.getPlayerCharacter().isDead());
        eventBus.post(new PlayerDeathEvent(playerManager));
        playerManager.update(2);
        assertTrue(playerManager.getPlayerCharacter().isDead());
    }

    @Test
    public void testObtainLoot() {
        // Setup
        ILoot loot = Mockito.mock(ILoot.class);
        PlayerManager playerManager = new PlayerManager(Mockito.mock(Vector2.class), 100, Mockito.mock(EventBus.class));

        // Test
        playerManager.obtainLoot(new ObtainLootEvent(loot));

        // Verify
        verify(loot).obtain(playerManager);
    }

    @Test
    public void testInitializeHUD() {
        // Opprett en mock for eventBus
        EventBus eventBus = Mockito.mock(EventBus.class);

        // Opprett en instans av PlayerManager
        PlayerManager playerManager = new PlayerManager(new Vector2(0, 0), 100, eventBus);

        // Opprett noen oppgraderinger og legg dem til i spillerens oppgraderingsbehandler
        IUpgrade upgrade1 = Mockito.mock(IUpgrade.class);
        IUpgrade upgrade2 = Mockito.mock(IUpgrade.class);
        playerManager.addUpgrade(upgrade1);
        playerManager.addUpgrade(upgrade2);

        // Opprett en ekte instans av InitializeHUDEvent
        InitializeHUDEvent event = new InitializeHUDEvent();

        // Kall metoden som skal testes
        playerManager.initializeHUD(event);


    }

    /**
     * Verifies that the returned object is an instance of {@link CurrencyManager}.
     */
    @Test
    public void testGetTrader() {
        ITrader trader = playerManager.getTrader();

        assertTrue(trader instanceof CurrencyManager);
    }

    @Test
    public void testUnregisterFromEventBus(){
        playerManager.unregisterFromEventBus();

        verify(eventBus, times(1)).unregister(playerManager);
    }


}





