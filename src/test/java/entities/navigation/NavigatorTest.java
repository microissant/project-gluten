package entities.navigation;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.badlogic.gdx.math.Vector2;

import grid.GridCoordinate;
import grid.WorldGrid;
import grid.factory.RoomFactory;
import grid.factory.RoomLayout;
import grid.TileData.TileRuleset;
import entities.navigation.pathfinder.Neighbours;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;

public class NavigatorTest {

    private static WorldGrid grid;
    private Navigator navigator;
    private Vector2 startPosition;

    @BeforeAll
    static void createGrid() {

        String roomString =
                "wwwwwwwwwww  www," + // 10
                "w              w," + // 9
                "w              w," + // 8
                "wwwwww         w," + // 7
                "                ," + // 6
                "      w w       ," + // 5
                "w hh           w," + // 4
                "w hh       ww  w," + // 3
                "w hh       ww  w," + // 2
                "w              w," + // 1
                "wwwwwwwwwwwwwwww, "; // 0

        RoomFactory roomFactory = new RoomFactory();
        RoomLayout layout = roomFactory.createLayout(roomString);
        grid = new WorldGrid(11, 16, 1, null);
        layout.paste(grid, new GridCoordinate(0, 0), null);
    }

    @Test
    public void testNeighbours() {
        Neighbours[] neighbours = Neighbours.allValues();
        assertEquals(8, neighbours.length);

        Neighbours north = Neighbours.NORTH;
        assertEquals(0, north.x);
        assertEquals(-1, north.y);
        assertEquals(2.0f, north.cost);
    }

    /**
     * Tests that the walking navigator can find a path to a target horizontal to
     * the
     * start position.
     */
    @Test
    public void testWalkingOpenHorizontalPath() {
        Vector2 startPosition = new Vector2(1.5f, 6.5f);
        Navigator navigator = new Navigator(startPosition);

        assertNull(navigator.getPath());

        navigator.setWorldGrid(grid, TileRuleset.WALKING);
        navigator.setTarget(new Vector2(7.5f, 6.5f));

        LinkedList<Vector2> path = navigator.getPath();
        assertNotNull(path);

        LinkedList<Vector2> checkPathList = new LinkedList<Vector2>();
        checkPathList.add(new Vector2(7.5f, 6.5f));
        checkPathList.add(new Vector2(6.5f, 6.5f));
        checkPathList.add(new Vector2(5.5f, 6.5f));
        checkPathList.add(new Vector2(4.5f, 6.5f));
        checkPathList.add(new Vector2(3.5f, 6.5f));
        checkPathList.add(new Vector2(2.5f, 6.5f));
        assertEquals(checkPathList, path);
    }

    /**
     * Tests that the walking navigator can find a path to a target diagonal to the
     * start position.
     */
    @Test
    public void testWalkingOpenDiagonalPath() {
        Vector2 startPosition = new Vector2(4.5f, 5.5f);
        Navigator navigator = new Navigator(startPosition);

        assertNull(navigator.getPath());

        navigator.setWorldGrid(grid, TileRuleset.WALKING);
        navigator.setTarget(new Vector2(7.5f, 8.5f));

        LinkedList<Vector2> path = navigator.getPath();
        assertNotNull(path);

        LinkedList<Vector2> checkPathList = new LinkedList<Vector2>();
        checkPathList.add(new Vector2(7.5f, 8.5f));
        checkPathList.add(new Vector2(6.5f, 7.5f));
        checkPathList.add(new Vector2(5.5f, 6.5f));
        assertEquals(checkPathList, path);
    }

    /**
     * Tests that the walking navigator can find a path around a hole in the grid.
     * 
     * The holes are at (2, 2), (3, 2), (2, 3), (3, 3), (2, 4), (3, 4).
     */

    @Test
    public void testWalkingHole() {
        Vector2 startPosition = new Vector2(1.5f, 3.5f);
        Navigator navigator = new Navigator(startPosition);

        assertNull(navigator.getPath());

        navigator.setWorldGrid(grid, TileRuleset.WALKING);
        navigator.setTarget(new Vector2(4.5f, 3.5f));

        LinkedList<Vector2> path = navigator.getPath();
        assertNotNull(path);

        LinkedList<Vector2> holeList = new LinkedList<Vector2>();
        holeList.add(new Vector2(2.5f, 2.5f));
        holeList.add(new Vector2(3.5f, 2.5f));
        holeList.add(new Vector2(2.5f, 3.5f));
        holeList.add(new Vector2(3.5f, 3.5f));
        holeList.add(new Vector2(2.5f, 4.5f));
        holeList.add(new Vector2(3.5f, 4.5f));

        for (Vector2 cell : holeList) {
            assertFalse(path.contains(cell));
        }

        LinkedList<Vector2> checkPathList = new LinkedList<Vector2>();
        checkPathList.add(new Vector2(4.5f, 3.5f));
        checkPathList.add(new Vector2(4.5f, 2.5f));
        checkPathList.add(new Vector2(3.5f, 1.5f));
        checkPathList.add(new Vector2(2.5f, 1.5f));
        checkPathList.add(new Vector2(1.5f, 2.5f));
        assertEquals(checkPathList, path);
    }

    /**
     * Tests that the walking navigator can find a path around a wall in the grid.
     * 
     * The wall is at (7, 0) -> (7, 5)
     */
    @Test
    public void testWalkingWall() {
        Vector2 startPosition = new Vector2(1.5f, 8.5f);
        Navigator navigator = new Navigator(startPosition);

        assertNull(navigator.getPath());

        navigator.setWorldGrid(grid, TileRuleset.WALKING);
        navigator.setTarget(new Vector2(0.5f, 6.5f));

        LinkedList<Vector2> path = navigator.getPath();
        assertNotNull(path);

        LinkedList<Vector2> checkPathList = new LinkedList<Vector2>();

        LinkedList<Vector2> wallList = new LinkedList<Vector2>();
        wallList.add(new Vector2(7.5f, 0.5f));
        wallList.add(new Vector2(7.5f, 1.5f));
        wallList.add(new Vector2(7.5f, 2.5f));
        wallList.add(new Vector2(7.5f, 3.5f));
        wallList.add(new Vector2(7.5f, 4.5f));
        wallList.add(new Vector2(7.5f, 5.5f));

        for (Vector2 cell : wallList) {
            assertFalse(path.contains(cell));
        }

        checkPathList.add(new Vector2(0.5f, 6.5f));
        checkPathList.add(new Vector2(1.5f, 6.5f));
        checkPathList.add(new Vector2(2.5f, 6.5f));
        checkPathList.add(new Vector2(3.5f, 6.5f));
        checkPathList.add(new Vector2(4.5f, 6.5f));
        checkPathList.add(new Vector2(5.5f, 6.5f));
        checkPathList.add(new Vector2(6.5f, 7.5f));
        checkPathList.add(new Vector2(5.5f, 8.5f));
        checkPathList.add(new Vector2(4.5f, 8.5f));
        checkPathList.add(new Vector2(3.5f, 8.5f));
        checkPathList.add(new Vector2(2.5f, 8.5f));
        assertEquals(checkPathList, path);
    }

    /**
     * Tests that the flying navigator can find a path over a hole in the grid.
     * 
     * The holes are at (2, 2), (3, 2), (2, 3), (3, 3), (2, 4), (3, 4).
     */

    @Test
    public void testFlyingHole() {
        Vector2 startPosition = new Vector2(1.5f, 3.5f);
        Navigator navigator = new Navigator(startPosition);

        assertNull(navigator.getPath());

        navigator.setWorldGrid(grid, TileRuleset.FLYING);
        navigator.setTarget(new Vector2(4.5f, 3.5f));

        LinkedList<Vector2> path = navigator.getPath();
        assertNotNull(path);

        LinkedList<Vector2> checkPathList = new LinkedList<Vector2>();
        checkPathList.add(new Vector2(4.5f, 3.5f));
        checkPathList.add(new Vector2(3.5f, 3.5f));
        checkPathList.add(new Vector2(2.5f, 3.5f));
        assertEquals(checkPathList, path);
    }

    /**
     * Tests that the flying navigator can find a path around a wall in the grid.
     * 
     * The wall is at (7, 0) -> (7, 5)
     */
    @Test
    public void testFlyingWall() {
        Vector2 startPosition = new Vector2(1.5f, 8.5f);
        Navigator navigator = new Navigator(startPosition);

        assertNull(navigator.getPath());

        navigator.setWorldGrid(grid, TileRuleset.FLYING);
        navigator.setTarget(new Vector2(0.5f, 6.5f));

        LinkedList<Vector2> path = navigator.getPath();
        assertNotNull(path);

        LinkedList<Vector2> checkPathList = new LinkedList<Vector2>();

        LinkedList<Vector2> wallList = new LinkedList<Vector2>();
        wallList.add(new Vector2(7.5f, 0.5f));
        wallList.add(new Vector2(7.5f, 1.5f));
        wallList.add(new Vector2(7.5f, 2.5f));
        wallList.add(new Vector2(7.5f, 3.5f));
        wallList.add(new Vector2(7.5f, 4.5f));
        wallList.add(new Vector2(7.5f, 5.5f));

        for (Vector2 cell : wallList) {
            assertFalse(path.contains(cell));
        }

        checkPathList.add(new Vector2(0.5f, 6.5f));
        checkPathList.add(new Vector2(1.5f, 6.5f));
        checkPathList.add(new Vector2(2.5f, 6.5f));
        checkPathList.add(new Vector2(3.5f, 6.5f));
        checkPathList.add(new Vector2(4.5f, 6.5f));
        checkPathList.add(new Vector2(5.5f, 6.5f));
        checkPathList.add(new Vector2(6.5f, 7.5f));
        checkPathList.add(new Vector2(5.5f, 8.5f));
        checkPathList.add(new Vector2(4.5f, 8.5f));
        checkPathList.add(new Vector2(3.5f, 8.5f));
        checkPathList.add(new Vector2(2.5f, 8.5f));
        assertEquals(checkPathList, path);
    }

    /**
     * Tests that the navigator only updates the path when the target position
     * changes cells to avoid unnecessary method calls.
     */
    @Test
    public void testUpdateTargetPosition() {
        startPosition = new Vector2(1.5f, 1.5f);
        navigator = new Navigator(startPosition);
        navigator.setWorldGrid(grid, TileRuleset.WALKING);
        Vector2 targetPosition = new Vector2(4.5f, 6.5f);
        navigator.setTarget(targetPosition);
        navigator.update(0.1);
        assertNotNull(navigator.getPath());
        LinkedList<Vector2> path = navigator.getPath();

        Vector2 sameTargetPosition = new Vector2(4.7f, 6.7f);
        navigator.setTarget(sameTargetPosition);
        navigator.update(0.5);
        LinkedList<Vector2> samePath = navigator.getPath();
        assertEquals(path, samePath);

        Vector2 newTargetPosition = new Vector2(7.5f, 6.5f);
        navigator.setTarget(newTargetPosition);
        navigator.update(2);
        LinkedList<Vector2> newPath = navigator.getPath();
        assertNotEquals(path, newPath);

        Vector2 newTargetPosition2 = new Vector2(11.5f, 10.5f);
        navigator.setTarget(newTargetPosition2);
        navigator.update(5);
        LinkedList<Vector2> newPath2 = navigator.getPath();
        assertNotEquals(path, newPath2);
    }
}
