package entities.inanimate;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.badlogic.gdx.math.Vector2;
import model.event.EventBus;

/**Tests for CloseDoorsTrigger */

public class CloseDoorsTriggerTest {
    private Vector2 position;
    private EventBus eventBus;
    private CloseDoorsTrigger closeDoorsTrigger;
    

    @BeforeEach
    public void setUp(){
        position = mock(Vector2.class);
        eventBus = mock(EventBus.class);
        closeDoorsTrigger = new CloseDoorsTrigger(eventBus, position);

        doNothing().when(eventBus).post(any());
        doNothing().when(eventBus).unregister(any());
    }

    /**
     * Verify that the constructor does not throw exceptions when recieving null values 
    */
    @Test
    public void constructorTest(){
        assertDoesNotThrow(() -> new CloseDoorsTrigger(null, position));
        assertDoesNotThrow(() -> new CloseDoorsTrigger(eventBus, null));
        assertDoesNotThrow(() -> new CloseDoorsTrigger(null, null));
    }

    
    @Test
    public void getCollisionPrimitiveTest(){
        assertNotNull(closeDoorsTrigger.getCollisionPrimitive());
    }
    
    @Test
    public void getPropertiesTest(){
        assertNull(closeDoorsTrigger.getProperties());
    }
    
    /**
     * isDead should return false unless the private method closeDoors() is called 
     */
    @Test
    public void isDeadTest(){
        assertFalse(closeDoorsTrigger.isDead());
    }
    
    /**
     * Check that getPosition() returns the same value as being used in setPosition() 
     */
    @Test
    public void getPositionTest(){
        closeDoorsTrigger.setPosition(position);
        assertEquals(position, closeDoorsTrigger.getPosition());
    }

    /**
     * Verify that unregister() has been called
     */
    @Test
    public void unregisterFromEventBus(){
        closeDoorsTrigger.unregisterFromEventBus();
        verify(eventBus, times(1)).unregister(any());
    }
    
}
