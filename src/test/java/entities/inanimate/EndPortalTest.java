package entities.inanimate;

import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType;
import components.collision.ICollidable;
import components.collision.primitives.CollisionCircle;
import model.event.EventBus;
import model.event.events.Event;
import model.event.events.audio.PlaySoundEvent;
import model.event.events.game.EndLevelEvent;
import model.event.events.game.GameCompleteEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.List;

public class EndPortalTest {

    @Mock
    private EventBus eventBus;

    @Captor
    private ArgumentCaptor<Event> eventCaptor;

    private EndPortal endPortal;
    private Vector2 position;

    private static class TestCollidable implements ICollidable {
        @Override
        public void collisionEnter(ICollidable target, CollisionType.Type type) {
        }

        @Override
        public CollisionCircle getCollisionPrimitive() {
            return null;
        }

        @Override
        public Vector2 getPosition() {
            return null;
        }

        @Override
        public void setPosition(Vector2 pos) {
        }
    }

    /**
     * Sets up the test environment. The EventCapture is used to capture events posted to the EventBus.
     * The EventBus is mocked to prevent the events from being posted to the actual EventBus.
     * The position is used to create the EndPortal.
     * The EndPortal is created in the test methods.
     * The TestCollidable is used to test the collisionEnter method.
     */
    @BeforeEach
    public void setUp() {
        position = new Vector2(100, 200);
        eventBus = mock(EventBus.class);
        eventCaptor = ArgumentCaptor.forClass(Event.class);
    }

    @Test
    public void testConstructor() {
        endPortal = new EndPortal(eventBus, position, false);
        assertEquals(position, endPortal.getPosition());
        assertNotNull(endPortal.getCollisionPrimitive());
        assertFalse(endPortal.isDead());
    }

    @Test
    public void testCollisionEnter_endLevelEvent() {
        endPortal = new EndPortal(eventBus, position, false);
        endPortal.collisionEnter(new TestCollidable(), CollisionType.Type.PLAYER);

        verify(eventBus, times(3)).post(eventCaptor.capture());
        List<Event> postedEvents = eventCaptor.getAllValues();
        assertTrue(postedEvents.get(0) instanceof EndLevelEvent);
        assertTrue(postedEvents.get(1) instanceof PlaySoundEvent);
        assertTrue(postedEvents.get(2) instanceof PlaySoundEvent);
    }

    /**
     * Test whether the collisionEnter method of EndPortal will post the expected events when the EndPortal collides with a player.
     * This test verifies that the method posts 3 events: a GameCompleteEvent, and two PlaySoundEvents.
     */
    @Test
    public void testCollisionEnter_gameCompleteEvent() {
        endPortal = new EndPortal(eventBus, position, true);
        endPortal.collisionEnter(new TestCollidable(), CollisionType.Type.PLAYER);

        verify(eventBus, times(3)).post(eventCaptor.capture());
        List<Event> postedEvents = eventCaptor.getAllValues();
        assertTrue(postedEvents.get(0) instanceof GameCompleteEvent);
        assertTrue(postedEvents.get(1) instanceof PlaySoundEvent);
        assertTrue(postedEvents.get(2) instanceof PlaySoundEvent);
    }

    /**
     * Test whether the setPosition method of EndPortal correctly sets the position of the EndPortal object.
     * This test creates a new EndPortal object, sets its position to a new position using the setPosition method, and then checks that the position of the EndPortal object matches the new position.
     */
    @Test
    public void testSetPosition() {
        endPortal = new EndPortal(eventBus, position, true);
        Vector2 newPosition = new Vector2(30, 40);
        endPortal.setPosition(newPosition);

        assertEquals(newPosition, endPortal.getPosition());
    }


    /**
     * Test whether the unregisterFromEventBus method of EndPortal correctly unregisters the EndPortal object from the event bus.
     * This test creates a new EndPortal object, calls the unregisterFromEventBus method, and then checks that the event bus has called the unregister method with the EndPortal object as its argument.
     */
    @Test
    public void testUnregisterFromEventBus() {
        endPortal = new EndPortal(eventBus, position, true);
        endPortal.unregisterFromEventBus();
        verify(eventBus).unregister(endPortal);
    }
}
