package entities.enemy;

import com.badlogic.gdx.math.Vector2;
import entities.enemy.enemies.GhostPepperEnemy;
import media.SoundID;
import model.event.EventBus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class GhostPepperEnemyTest {
    private Vector2 position;
    private EventBus eventBus;
    private boolean required;
    private GhostPepperEnemy ghostPepper;
    
    
    @BeforeEach
    public void setUp(){
        eventBus = new EventBus();
        position = new Vector2();
        ghostPepper = new GhostPepperEnemy(position, false, eventBus);
    }
    
    /*Check that the program does not crash when constructor recieves null values */
    @Test
    public void constructorTest(){
        assertDoesNotThrow(() -> new GhostPepperEnemy(null, required, eventBus));
        assertDoesNotThrow(() -> new GhostPepperEnemy(position, required, null));
        assertDoesNotThrow(() -> new GhostPepperEnemy(null, required, null));       
        assertEquals(ghostPepper.deathSoundID, SoundID.GHOST_DEATH);
    }

    /**
     * Test suite for verifying the behavior of the getProperties() method of the EvilCookie class.
     */
    @Test
    public void getPropertiesTest(){
        assertNotNull(ghostPepper.getProperties());
    }

    /**
     * Test suite for verifying the behavior of the getCollisionPrimitive() method of the EvilCookie class.
     */
    @Test
    public void getCollisionPrimitiveTest(){
        assertNotNull(ghostPepper.getCollisionPrimitive());
    }

    /**
     * Test suite for verifying the behavior of the getPursuitRange() method of the EvilCookie class.
     */
    @Test
    public void getPursuitRangeTest(){
        assertTrue(ghostPepper.getPursuitRange() > 0);
    }

    /**
     * Test suite for verifying the behavior of the getAttackRange() method of the EvilCookie class.
     */
    @Test
    public void getAttackRangeTest(){
        assertTrue(ghostPepper.getAttackRange() > 0);
    }
}