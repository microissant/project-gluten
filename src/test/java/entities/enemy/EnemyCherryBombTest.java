package entities.enemy;

import com.badlogic.gdx.math.Vector2;
import entities.enemy.enemies.EnemyCherryBomb;
import media.SoundID;
import model.event.EventBus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**Test class for the enemy CherryBomb */

public class EnemyCherryBombTest {
    private Vector2 position;
    private EventBus eventBus;
    private boolean required;
    private EnemyCherryBomb cherryBomb; 

    @BeforeEach
    public void setUp(){
        eventBus = new EventBus();
        position = new Vector2();
        cherryBomb = new EnemyCherryBomb(position, false, eventBus);
    }

    /*Check that the program does not crash when constructor recieves null values*/
    @Test
    public void constructorTest(){
        assertDoesNotThrow(() -> new EnemyCherryBomb(null, required, eventBus));
        assertDoesNotThrow(() -> new EnemyCherryBomb(position, required, null));
        assertDoesNotThrow(() -> new EnemyCherryBomb(null, required, null));       
        assertEquals(cherryBomb.deathSoundID, SoundID.CHERRY_DEATH);
    }


    /* Tests the getProperties method to ensure it returns a non-null value */
    @Test
    public void getPropertiesTest(){
        assertNotNull(cherryBomb.getProperties());
    }

    /* Tests the getCollisionPrimitive method to ensure it returns a non-null value.*/
    @Test
    public void getCollisionPrimitiveTest(){
        assertNotNull(cherryBomb.getCollisionPrimitive());
    }


    /* test the getPursuitRange method to ensure it returns a positive value. */
    @Test
    public void getPursuitRangeTest(){
        assertTrue(cherryBomb.getPursuitRange() > 0);
    }


    /* Tests the getAttackRange method to ensure it returns a positive value. */
    @Test
    public void getAttackRangeTest(){
        assertTrue(cherryBomb.getAttackRange() > 0);
}
}