package entities.enemy.enemies;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.badlogic.gdx.math.Vector2;

import grid.TileData;
import grid.WorldGrid;
import grid.GridCoordinate;
import grid.TileData.TileType;
import model.event.EventBus;
import model.event.events.entities.CreateCrateEvent;

public class CrateTest {

    private EventBus bus;
    private WorldGrid worldGrid;
    private Crate crate;


    @BeforeEach
    public void initiate(){
        bus = new EventBus();
        worldGrid = new WorldGrid(10, 10, 16, new TileData(TileType.EMPTY));
        crate = new Crate(new Vector2(8,8),bus);
        bus.register(worldGrid);
        bus.post(new CreateCrateEvent(crate));
        worldGrid.set(new GridCoordinate(0,0), new TileData(TileType.CRATE));
    }


    //make sure that the entity is tied to its worldgrid
    //when it is destroyed by an explosion, the worldGrid should remove the entity.
    @Test
    void testGridDestruction() {
        assertEquals(new TileData(TileType.CRATE),worldGrid.get(0,0));
        worldGrid.explodeTiles(new Vector2(4,4), 1);
        assertTrue(crate.isDead());
        assertEquals(new TileData(TileType.EMPTY),worldGrid.get(0,0));
    }

    //make sure that the crate is removed from the grid when the crate is destroyed by something that isnt an explosion
    @Test
    void testEntityDestruction(){
        assertEquals(new TileData(TileType.CRATE),worldGrid.get(0,0));
        crate.destroy();    
        crate.update(1f);
        assertEquals(new TileData(TileType.EMPTY),worldGrid.get(0,0));
    }
}
