package entities.enemy;

import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType.Type;
import components.collision.ICollidable;
import components.collision.primitives.CollisionCircle;
import components.health.Damage;
import components.health.Healer;
import components.weapon.Weapon;
import entities.enemy.state.StateDefinition;
import entities.navigation.Navigator;
import grid.WorldGrid;
import media.DrawProperties;
import model.event.EventBus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/*Tests for the abstract class Enemy */

public class EnemyTest {

    private MockEnemy enemy;
    private EventBus eventBus;
    private Vector2 position;
    private Damage damage;
    private Healer healer;
    private MockEnemy target;
    private Weapon weapon;
    
    @BeforeEach
    public void setUp(){
        eventBus = mock(EventBus.class);
        position = mock(Vector2.class);
        weapon = mock(Weapon.class);
        damage = new Damage();
        healer = new Healer();
        enemy = new MockEnemy(position, 3, false, eventBus, 1, 2, 0);
        target = new MockEnemy(new Vector2(0,0), 3, false, eventBus, 1, 2, 0);

    }

    /**
     Tests the constructor with null parameters.
     It asserts that the constructor doesn't throw any exception.
     */
    @Test
    public void constructorNullTest(){
        assertDoesNotThrow(() -> new MockEnemy(null, 3, false, this.eventBus, 1, 2, 0));
        assertDoesNotThrow(() -> new MockEnemy(this.position, 3, false, null, 1, 2, 0));
        assertDoesNotThrow(() -> new MockEnemy(null, 3, false, null, 1, 2, 0));
        assertDoesNotThrow(() -> new MockEnemy(null, 3, false, this.eventBus, 1, 2, 0));
    }

    /**
     Tests the position method of the MockEnemy class.
     It asserts that the initial position of the enemy is (0, 0) and then checks that the position is changed to (2, 1) after calling the setPosition method.
     */
    @Test
    public void positionTest(){
        assertEquals(0, enemy.getPosition().x);
        assertEquals(0, enemy.getPosition().y);
        enemy.setPosition(new Vector2(2,1));
        
        assertEquals(2, enemy.getPosition().x);
        assertEquals(1, enemy.getPosition().y);
    }


    /*
     Tests the isDead method of the MockEnemy class.
     It asserts that the initial value of isDead is false and after applying damage to the enemy, isDead is true.
     */
    @Test
    public void isDeadTest(){
        assertFalse(enemy.isDead());
        Damage damage1 = new Damage(3);
        enemy.applyDamage(damage1);
        assertTrue(enemy.isDead());
    }
    
    @Test
    public void requiredTargetTest(){
        assertFalse(enemy.isRequiredTarget());
        MockEnemy enemy2 = new MockEnemy(position, 3, true, eventBus, 1, 2, 0);
        assertTrue(enemy2.isRequiredTarget());
    }
    
    /*Testing that the enemy can be damaged and healed*/
    @Test
    public void healerDamageTest(){
        assertEquals(3, enemy.getCurrentHealth());
        enemy.applyDamage(damage);
        assertEquals(2, enemy.getCurrentHealth());
        enemy.applyDamage(damage);
        assertEquals(1, enemy.getCurrentHealth());
        enemy.applyHeal(healer);
        assertEquals(2, enemy.getCurrentHealth());
        enemy.applyHeal(healer);
        assertEquals(3, enemy.getCurrentHealth());        
    }

    /*Testing that combatActivate() is called when applyDamage() is called and that
     *disableCombat int turn is called after a certain amount of time.
     */
    @Test
    public void combatTest(){       
        enemy.applyDamage(damage);
        assertEquals(1, enemy.combatActivateCount);
        enemy.update(100);
        assertEquals(1, enemy.disableCombatCount);
    }

    /** Verifies that the getDamageable method returns the instance of the enemy */
    @Test
    public void getDamagebleTest(){
        assertEquals(this.enemy, enemy.getDamageable());
    }
    
    @Test
    public void hasTargetTest(){
        assertFalse(enemy.hasTarget());
        enemy.setTarget(target);
        assertTrue(enemy.hasTarget());
    }
    
    // @Test originToTarget x and y is altered in the weapon class and does not update

    @Test
    public void getPhysicsComponentTest(){
        assertNotNull(enemy.getPhysicsComponent());
    }
    
    @Test
    public void getHealthComponentTest(){
        assertNotNull(enemy.getHealthComponent());
    }
    
    @Test
    public void getTargetTest(){
        assertNull(enemy.getTarget());
    }

    @Test 
    public void addWeaponTest(){
        enemy.addWeapon(weapon);
        verify(weapon,times(1)).setOriginPosition(any());
    }

    @Test
    public void unregisterFromEventBusTest(){
        enemy.unregisterFromEventBus();
        verify(eventBus,times(1)).unregister(any());
    }
    
    /*Checks that no exception is thrown when addStateDefinitions recieves a
     *null value or a list containing null values as an argument*/
    @Test
    public void addStateDefinitionsTest(){
        ArrayList<StateDefinition> nullList = new ArrayList<>();
        assertDoesNotThrow(()-> enemy.addStateDefinitions(null));

        assertDoesNotThrow(()-> enemy.addStateDefinitions(nullList));
    }


    /*Mockito does not work well with abstract classes, so this is the enemy class used for testing*/

    private static class MockEnemy extends Enemy{

        protected int combatActivateCount = 0;
        protected int disableCombatCount = 0;
        //protected int updateTargetDirectionCount = 0;

        private MockEnemy(Vector2 position, int startHealth, boolean isRequiredTarget, EventBus eventBus, int lootMin,
                int lootMax, float lootDropChance) {
            super(position, startHealth, isRequiredTarget, eventBus, lootMin, lootMax, lootDropChance);
        }

        @Override
        protected void activateCombat(){
            super.activateCombat();
            combatActivateCount ++;
        }
        
        @Override
        protected void disableCombat(){
            super.disableCombat();
            disableCombatCount ++;
        }

        @Override
        public void collisionEnter(ICollidable target, Type type) {
        }

        @Override
        public CollisionCircle getCollisionPrimitive() {
            return null;
        }

        @Override
        public float getPursuitRange() {
            return 0;
        }

        @Override
        public float getAttackRange() {
            return 0;
        }

        @Override
        public DrawProperties getProperties() {
            return null;
        }

        @Override
        protected StateDefinition getPursuitPathState(Navigator navigator, WorldGrid worldGrid) {
            return null;
        }

    }
            
    }

