package entities.enemy;

import com.badlogic.gdx.math.Vector2;
import entities.enemy.enemies.IceCube;
import media.SoundID;
import model.event.EventBus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class IceCubeTest {
    private Vector2 position;
    private EventBus eventBus;
    private boolean required;
    private IceCube iceCube;
    
    
    @BeforeEach
    public void setUp(){
        eventBus = new EventBus();
        position = new Vector2();
        required = false;
        iceCube = new IceCube(position, false, eventBus);
    }
    
    /*Check that the program does not crash when constructor recieves null values */
    @Test
    public void constructorTest(){
        assertDoesNotThrow(() -> new IceCube(null, required, eventBus));
        assertDoesNotThrow(() -> new IceCube(position, required, null));
        assertDoesNotThrow(() -> new IceCube(null, required, null));       
        assertEquals(iceCube.deathSoundID, SoundID.CUBE_DEATH);
    }

    /**
     * Test suite for verifying the behavior of the getProperties() method of the EvilCookie class.
     */
    @Test
    public void getPropertiesTest(){
        assertNotNull(iceCube.getProperties());
    }


    /**
     * Test suite for verifying the behavior of the getCollisionPrimitive() method of the EvilCookie class.
     */
    @Test
    public void getCollisionPrimitiveTest(){
        assertNotNull(iceCube.getCollisionPrimitive());
    }


    /**
     * Test suite for verifying the behavior of the getAttackRange() method of the EvilCookie class.
     */
    @Test
    public void getPursuitRangeTest(){
        assertTrue(iceCube.getPursuitRange() > 0);
    }

    /**
     * Test suite for verifying the behavior of the getAttackRange() method of the EvilCookie class.
     */
    @Test
    public void getAttackRangeTest(){
        assertTrue(iceCube.getAttackRange() > 0);
    }
}