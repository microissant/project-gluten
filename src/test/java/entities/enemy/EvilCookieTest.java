package entities.enemy;

import com.badlogic.gdx.math.Vector2;
import entities.enemy.enemies.EvilCookie;
import media.SoundID;
import model.event.EventBus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class EvilCookieTest {
    private Vector2 position;
    private EventBus eventBus;
    private EvilCookie evilCookie;

    private final boolean required = false;


    @BeforeEach
    public void setUp(){
        eventBus = new EventBus();
        position = new Vector2();
        evilCookie = new EvilCookie(position, false, eventBus);
    }
    
    /*Check that the program does not crash when constructor recieves null values */
    @Test
    public void constructorTest(){
        assertDoesNotThrow(() -> new EvilCookie(null, required, eventBus));
        assertDoesNotThrow(() -> new EvilCookie(position, required, null));
        assertDoesNotThrow(() -> new EvilCookie(null, required, null));       
        assertEquals(evilCookie.deathSoundID, SoundID.COOKIE_DEATH);
    }

    /**
     * Test suite for verifying the behavior of the getProperties() method of the EvilCookie class.
     */
    @Test
    public void getPropertiesTest(){
        assertNotNull(evilCookie.getProperties());
    }


    /**
     * Test suite for verifying the behavior of the getCollisionPrimitive() method of the EvilCookie class.
     */
    @Test
    public void getCollisionPrimitiveTest(){
        assertNotNull(evilCookie.getCollisionPrimitive());
    }

    /**
     * Test suite for verifying the behavior of the getPursuitRange() method of the EvilCookie class.
     */
    @Test
    public void getPursuitRangeTest(){
        assertNotNull(evilCookie.getPursuitRange());
    }

    /**
     * Test suite for verifying the behavior of the getAttackRange() method of the EvilCookie class.
     */
    @Test
    public void getAttackRangeTest(){
        assertNotNull(evilCookie.getAttackRange());
    }
}
