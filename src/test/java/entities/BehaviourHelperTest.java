package entities;

import com.badlogic.gdx.math.Vector2;
import grid.GridCoordinate;
import grid.TileData;
import grid.WorldGrid;
import grid.factory.RoomFactory;
import grid.factory.RoomLayout;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;



/**
 It tests the line of sight check functionality with different scenarios.
 */
public class BehaviourHelperTest {
    private static WorldGrid grid;


    /**
     This method sets up the grid for testing by creating a room layout
     and pasting it onto the grid.
     */
    @BeforeAll
    static void createGrid(){
     
        String roomString = 
        "wwwwwwwwwww  www,"+
        "w              w,"+
        "w              w,"+
        "wwwwww         w,"+
        "                ,"+
        "                ,"+
        "w hh           w,"+
        "w hh       ww  w,"+
        "w hh       ww  w,"+
        "w              w,"+
        "wwwwwwwwwwwwwwww, ";

        RoomFactory roomFactory = new RoomFactory();
        RoomLayout layout = roomFactory.createLayout(roomString);
        grid = new WorldGrid(11, 16, 1, null);
        layout.paste(grid, new GridCoordinate(0, 0), null);

    }


    /**
     This test checks if there is a line of sight between the given start and end points.
     @param startx the x-coordinate of the starting point
     @param starty the y-coordinate of the starting point
     @param endx the x-coordinate of the ending point
     @param endy the y-coordinate of the ending point
     */
    //there should be a line of sight in all of these tests
    @CsvSource(value = {"4,6,13,4","8.1,4,11.9,9.1","1,9,14,9","14,9,14,1"})
    @ParameterizedTest(name = "from: {0},{1} to: {2} {3}")
    void testLOS(float startx, float starty, float endx, float endy) {
        int ruleset = TileData.TileRuleset.WALKING.val;
        Vector2 start = new Vector2(startx,starty);
        Vector2 end = new Vector2(endx,endy);
        assertTrue(BehaviourHelper.lineOfSightCheck(grid, start,end, ruleset));
        //reverse
        assertTrue(BehaviourHelper.lineOfSightCheck(grid, end,start, ruleset));
    }

    /**
     This test checks if there is a line of sight between the given start and end points.
     @param startx the x-coordinate of the starting point
     @param starty the y-coordinate of the starting point
     @param endx the x-coordinate of the ending point
     @param endy the y-coordinate of the ending point
     */
    //there should be no line of sight in all of these tests
    @CsvSource(value = {"1,4,6,2","7,2,3,8","8,7,3,7","5,9,5,4"})
    @ParameterizedTest(name = "from: {0} , {1} to: {2} , {3}")
    void testNoLOS(float startx, float starty, float endx, float endy) {
        int ruleset = TileData.TileRuleset.WALKING.val;
        Vector2 start = new Vector2(startx,starty);
        Vector2 end = new Vector2(endx,endy);
        assertFalse(BehaviourHelper.lineOfSightCheck(grid, start,end, ruleset));
        //reverse
        assertFalse(BehaviourHelper.lineOfSightCheck(grid, end,start, ruleset));
      

    }

    //test if the line of sight check behaves differently with different rulesets
    @Test 
    void testRuleSet(){    
        int ruleset = TileData.TileRuleset.WALKING.val;

        //stop at hole
        Vector2 start = new Vector2(grid.cellSize,3*grid.cellSize);
        Vector2 end = new Vector2(5*grid.cellSize,3*grid.cellSize);
        assertFalse(BehaviourHelper.lineOfSightCheck(grid, start,end, ruleset));
        //dont stop if flying
        ruleset = TileData.TileRuleset.FLYING.val;
        assertTrue(BehaviourHelper.lineOfSightCheck(grid, start,end, ruleset));
    }

}
