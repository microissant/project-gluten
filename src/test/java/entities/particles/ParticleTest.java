package entities.particles;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.badlogic.gdx.graphics.g3d.particles.ParticleShader.ParticleType;
import com.badlogic.gdx.math.Vector2;

import media.DrawProperties;
import model.event.EventBus;

public class ParticleTest {
    private DrawProperties drawProperties;
    private Vector2 position;
    private ParticleBehaviour behaviour;
    private ParticleTypes type;
    private EventBus eventBus;
    public boolean alive;
    public Particle particle;

    @BeforeEach
    public void setUp(){
        position = mock(Vector2.class);
        behaviour = mock(ParticleBehaviour.class);
        drawProperties = mock(DrawProperties.class);
        type = ParticleTypes.EXPLOSION_V1;
        eventBus = mock(EventBus.class);
        particle = new Particle();
    }

    /**Verify that isAlive returns false when a new particle object is created */
    @Test
    public void constructorTest(){
        assertFalse(particle.isAlive());
        
    }
    
    @Test
    public void getPropertiesTest(){
        assertNotNull(particle.getProperties());
    }
    
    @Test
    public void getPositionTest(){
        assertNotNull(particle.getPosition());
    }
    
    /**Verify that isAlive returns the correct bool */
    @Test
    public void isAlive(){
        assertFalse(particle.isAlive());
        
        particle.init(position, type, eventBus);
        assertTrue(particle.isAlive());
        
        particle.reset();
        assertFalse(particle.isAlive());

    }
}
