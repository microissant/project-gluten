package entities.particles;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;

import model.event.EventBus;

/**
 This class contains unit tests for the ExplosionParticleBehaviour .
 */
public class ExplosionParticleBehaviourTest {
    private ExplosionParticleBehaviour explosionParticleBehaviour = new ExplosionParticleBehaviour();

    /**Verify that EventBus posts two new events */
    @Test
    public void startTest(){
        EventBus eventBus = mock(EventBus.class);
        doNothing().when(eventBus).post(any());
        
        explosionParticleBehaviour.start(eventBus);
        verify(eventBus, times(2)).post(any());
    }
    
}
