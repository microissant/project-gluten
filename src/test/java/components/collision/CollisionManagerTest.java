package components.collision;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import components.collision.primitives.CollisionCircle;
import components.collision.trigger.ITrigger;
import components.weapon.projectile.IProjectile;
import entities.enemy.IEnemy;
import model.event.EventBus;
import model.event.events.collision.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


/**
 * This class tests the functionality of the CollisionManager.
 * It checks if the collision detection between different collision primitives is working as intended.
 */
public class CollisionManagerTest {

    CollisionManager collisionManager;

    @Mock
    IProjectile projectile1, projectile2, projectile3;

    @Mock
    CollisionCircle circle1, circle2, circle3, projectileCircle1, projectileCircle2, projectileCircle3;

    @Mock
    CollisionCircle enemyCircle1, enemyCircle2, enemyCircle3, triggerCircle1, triggerCircle2;

    @Mock
    IEnemy enemy1, enemy2, enemy3;

    @Mock
    ITrigger trigger1, trigger2;


    @BeforeEach
    public void setup() {
        collisionManager = new CollisionManager(10, 10, 10, null);

        circle1 = mock(CollisionCircle.class);
        circle2 = mock(CollisionCircle.class);
        circle3 = mock(CollisionCircle.class);
        projectileCircle1 = mock(CollisionCircle.class);
        projectileCircle2 = mock(CollisionCircle.class);
        projectileCircle3 = mock(CollisionCircle.class);
        enemyCircle1 = mock(CollisionCircle.class);
        enemyCircle2 = mock(CollisionCircle.class);
        enemyCircle3 = mock(CollisionCircle.class);
        triggerCircle1 = mock(CollisionCircle.class);
        triggerCircle2 = mock(CollisionCircle.class);

        projectile1 = mock(IProjectile.class);
        projectile2 = mock(IProjectile.class);
        projectile3 = mock(IProjectile.class);

        enemy1 = mock(IEnemy.class);
        enemy2 = mock(IEnemy.class);
        enemy3 = mock(IEnemy.class);

        trigger1 = mock(ITrigger.class);
        trigger2 = mock(ITrigger.class);

        when(circle1.getPosition()).thenReturn(new Vector2());
        when(circle2.getPosition()).thenReturn(new Vector2());
        when(circle3.getPosition()).thenReturn(new Vector2());
        when(projectileCircle1.getPosition()).thenReturn(new Vector2());
        when(projectileCircle2.getPosition()).thenReturn(new Vector2());
        when(projectileCircle3.getPosition()).thenReturn(new Vector2());
        when(enemyCircle1.getPosition()).thenReturn(new Vector2());
        when(enemyCircle2.getPosition()).thenReturn(new Vector2());
        when(enemyCircle3.getPosition()).thenReturn(new Vector2());
        when(triggerCircle1.getPosition()).thenReturn(new Vector2());
        when(triggerCircle2.getPosition()).thenReturn(new Vector2());

        when(projectile1.getCollisionPrimitive()).thenReturn(projectileCircle1);
        when(projectile2.getCollisionPrimitive()).thenReturn(projectileCircle2);
        when(projectile3.getCollisionPrimitive()).thenReturn(projectileCircle3);

        when(enemy1.getCollisionPrimitive()).thenReturn(enemyCircle1);
        when(enemy2.getCollisionPrimitive()).thenReturn(enemyCircle2);
        when(enemy3.getCollisionPrimitive()).thenReturn(enemyCircle3);

        when(trigger1.getCollisionPrimitive()).thenReturn(triggerCircle1);
        when(trigger2.getCollisionPrimitive()).thenReturn(triggerCircle2);
    }

    @Test
    public void testConstructor() {
        assertThrows(IllegalArgumentException.class, () -> new CollisionManager(5, 0, -50, null));
        assertThrows(IllegalArgumentException.class, () -> new CollisionManager(-5, 0, -50, null));
        assertThrows(IllegalArgumentException.class, () -> new CollisionManager(10, 10, -50, new EventBus()));
        assertDoesNotThrow(() -> new CollisionManager(10, 10, 16, null));
    }


    /**
     * Tests the collision detection between two circles.
     */
    @Test
    public void checkCircleCollision() {
        CollisionEntity collisionEntity1 = new CollisionEntity();
        CollisionCircle circle1 = new CollisionCircle(
                new Vector2(25, 25),
                new Vector2(),
                10,
                collisionEntity1,
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.PROJECTILE)
                )),
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.PROJECTILE)
                ))
        );

        CollisionEntity collisionEntity2 = new CollisionEntity();
        CollisionCircle circle2 = new CollisionCircle(
                new Vector2(40, 25),
                new Vector2(),
                10,
                collisionEntity2,
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.PROJECTILE)
                )),
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.PROJECTILE)
                ))
        );

        collisionManager.addCollisionCircle(circle1);
        collisionManager.addCollisionCircle(circle2);
        collisionManager.update(1.0d/60.0d);

        assertTrue(collisionEntity1.hasCollided);
        assertEquals(1, collisionEntity1.targets.size());
        assertEquals(collisionEntity2, collisionEntity1.targets.get(0));

        assertTrue(collisionEntity2.hasCollided);
        assertEquals(1, collisionEntity2.targets.size());
        assertEquals(collisionEntity1, collisionEntity2.targets.get(0));
    }

    @Test
    public void updateChangesCollision() {
        CollisionEntity collisionEntity1 = new CollisionEntity();
        CollisionCircle circle1 = new CollisionCircle(
                new Vector2(25, 25),
                new Vector2(),
                5,
                collisionEntity1,
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.PROJECTILE)
                )),
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.PROJECTILE)
                ))
        );

        CollisionEntity collisionEntity2 = new CollisionEntity();
        CollisionCircle circle2 = new CollisionCircle(
                new Vector2(40, 25),
                new Vector2(),
                5,
                collisionEntity2,
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.PROJECTILE)
                )),
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.PROJECTILE)
                ))
        );

        collisionManager.addCollisionCircle(circle1);
        collisionManager.addCollisionCircle(circle2);
        collisionManager.update(1.0d/60.0d);

        assertFalse(collisionEntity1.hasCollided);
        assertEquals(0, collisionEntity1.targets.size());

        assertFalse(collisionEntity2.hasCollided);
        assertEquals(0, collisionEntity2.targets.size());

        circle2.setPosition(40, 25);
        collisionManager.update(1.0d/60.0d);

        assertFalse(collisionEntity1.hasCollided);
        assertEquals(0, collisionEntity1.targets.size());

        assertFalse(collisionEntity2.hasCollided);
        assertEquals(0, collisionEntity2.targets.size());


        circle2.setPosition(30, 25);
        collisionManager.update(1.0d/60.0d);

        assertTrue(collisionEntity1.hasCollided);
        assertEquals(1, collisionEntity1.targets.size());
        assertEquals(collisionEntity2, collisionEntity1.targets.get(0));

        assertTrue(collisionEntity2.hasCollided);
        assertEquals(1, collisionEntity2.targets.size());
        assertEquals(collisionEntity1, collisionEntity2.targets.get(0));
    }


    @Test
    public void onlyDetectActiveAndPassiveTypes() {

        CollisionEntity collisionEntity1 = new CollisionEntity();
        CollisionCircle circle1 = new CollisionCircle(
                new Vector2(25, 25),
                new Vector2(),
                15,
                collisionEntity1,
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.PROJECTILE)
                )),
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.PROJECTILE)
                ))
        );

        CollisionEntity collisionEntity2 = new CollisionEntity();
        CollisionCircle circle2 = new CollisionCircle(
                new Vector2(40, 25),
                new Vector2(),
                15,
                collisionEntity2,
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.PROJECTILE)
                )),
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.PROJECTILE)
                ))
        );

        CollisionEntity collisionEntity3 = new CollisionEntity();
        CollisionCircle circle3 = new CollisionCircle(
                new Vector2(30, 25),
                new Vector2(),
                5,
                collisionEntity3,
                new HashSet<>(),
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.PROJECTILE)
                ))
        );


        collisionManager.addCollisionCircle(circle1);
        collisionManager.addCollisionCircle(circle2);
        collisionManager.addCollisionCircle(circle3);

        collisionManager.update(1.0d/60.0d);

        assertTrue(collisionEntity1.hasCollided);
        assertEquals(2, collisionEntity1.targets.size());
        assertTrue(collisionEntity1.targets.contains(collisionEntity2));
        assertTrue(collisionEntity1.targets.contains(collisionEntity3));

        assertTrue(collisionEntity2.hasCollided);
        assertEquals(2, collisionEntity2.targets.size());
        assertTrue(collisionEntity2.targets.contains(collisionEntity1));
        assertTrue(collisionEntity2.targets.contains(collisionEntity3));

        assertFalse(collisionEntity3.hasCollided);
        assertTrue(collisionEntity3.targets.isEmpty());
    }

    @Test
    public void onlyDetectSameType() {

        CollisionEntity collisionEntity1 = new CollisionEntity();
        CollisionCircle circle1 = new CollisionCircle(
                new Vector2(25, 25),
                new Vector2(),
                15,
                collisionEntity1,
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.PROJECTILE)
                )),
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.PROJECTILE)
                ))
        );

        CollisionEntity collisionEntity2 = new CollisionEntity();
        CollisionCircle circle2 = new CollisionCircle(
                new Vector2(40, 25),
                new Vector2(),
                15,
                collisionEntity2,
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.PROJECTILE)
                )),
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.PROJECTILE)
                ))
        );

        CollisionEntity collisionEntity3 = new CollisionEntity();
        CollisionCircle circle3 = new CollisionCircle(
                new Vector2(30, 25),
                new Vector2(),
                5,
                collisionEntity3,
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.PLAYER)
                )),
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.PLAYER)
                ))
        );

        CollisionEntity collisionEntity4 = new CollisionEntity();
        CollisionCircle circle4 = new CollisionCircle(
                new Vector2(32, 30),
                new Vector2(),
                5,
                collisionEntity4,
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.PLAYER)
                )),
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.PLAYER)
                ))
        );

        CollisionEntity collisionEntity5 = new CollisionEntity();
        CollisionCircle circle5 = new CollisionCircle(
                new Vector2(28, 28),
                new Vector2(),
                5,
                collisionEntity5,
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.ENEMY)
                )),
                new HashSet<>(Set.of(
                        new CollisionType(CollisionType.Type.ENEMY)
                ))
        );

        collisionManager.addCollisionCircle(circle1);
        collisionManager.addCollisionCircle(circle2);
        collisionManager.addCollisionCircle(circle3);
        collisionManager.addCollisionCircle(circle4);
        collisionManager.addCollisionCircle(circle5);

        collisionManager.update(1.0d/60.0d);

        assertTrue(collisionEntity1.hasCollided);
        assertEquals(1, collisionEntity1.targets.size());
        assertTrue(collisionEntity1.targets.contains(collisionEntity2));

        assertTrue(collisionEntity2.hasCollided);
        assertEquals(1, collisionEntity1.targets.size());
        assertTrue(collisionEntity2.targets.contains(collisionEntity1));

        assertTrue(collisionEntity3.hasCollided);
        assertEquals(1, collisionEntity3.targets.size());
        assertTrue(collisionEntity3.targets.contains(collisionEntity4));

        assertTrue(collisionEntity4.hasCollided);
        assertEquals(1, collisionEntity4.targets.size());
        assertTrue(collisionEntity4.targets.contains(collisionEntity3));

        assertFalse(collisionEntity5.hasCollided);
        assertTrue(collisionEntity5.targets.isEmpty());
    }

    @Test
    public void testRemoveCollisionCircle() {
        collisionManager.addCollisionCircle(circle1);
        assertTrue(collisionManager.getCollisionCircles().contains(circle1));

        collisionManager.removeCollisionCircle(circle1);
        assertTrue(collisionManager.getCollisionCircles().isEmpty());
    }

    @Test
    public void testRemoveCollisionCircleNotAdded() {
        assertDoesNotThrow(() -> collisionManager.removeCollisionCircle(circle1));
    }

    @Test
    public void testRemoveMultipleCircles() {
        List<CollisionCircle> circles = new ArrayList<>(Arrays.asList(circle1, circle2));

        collisionManager.addCollisionCircle(circles);
        assertEquals(2, collisionManager.getCollisionCircles().size());

        collisionManager.removeCollisionCircle(circles);
        assertTrue(collisionManager.getCollisionCircles().isEmpty());
    }

    @Test
    public void testClearRemovesAllCircles() {
        // Add circles and triggers
        collisionManager.addCollisionCircle(circle1);
        collisionManager.addCollisionCircle(circle2);
        collisionManager.addCollisionCircle(circle3);
        collisionManager.update(0.016);

        assertEquals(3, collisionManager.getCollisionCircles().size());

        // Clear should remove all
        collisionManager.clear();
        assertTrue(collisionManager.getCollisionCircles().isEmpty());
    }


    @Test
    public void testAddCollisionCircleEvent() {

        collisionManager.addCollisionCircleEvent(new AddCollisionCircleEvent(circle1));
        collisionManager.update(0.016);

        assertEquals(1, collisionManager.getCollisionCircles().size());
    }


    @Test
    public void testAddArrayWithCollisionCircleEvent() {
        Array<CollisionCircle> circles = new Array<>();
        circles.add(circle1);
        circles.add(circle2);
        circles.add(circle3);

        collisionManager.addCollisionCirclesEvent(new AddCollisionCirclesEvent(circles));
        collisionManager.update(0.016);

        assertEquals(3, collisionManager.getCollisionCircles().size());
    }

    @Test
    public void testAddArrayWithCollisionCircleEventWithNothing() {
        Array<CollisionCircle> circles = new Array<>();

        collisionManager.addCollisionCirclesEvent(new AddCollisionCirclesEvent(circles));
        collisionManager.update(0.016);

        assertTrue(collisionManager.getCollisionCircles().isEmpty());
    }

    @Test
    public void testAddArrayWithCollisionCircleEventNull() {
        collisionManager.addCollisionCirclesEvent(new AddCollisionCirclesEvent(null));
        collisionManager.update(0.016);

        assertTrue(collisionManager.getCollisionCircles().isEmpty());
    }

    @Test
    public void testAddArrayWithCollisionCircleEventWithMultipleNulls() {
        Array<CollisionCircle> circles = new Array<>();
        circles.add(null);
        circles.add(null);
        circles.add(null);
        circles.add(null);

        collisionManager.addCollisionCirclesEvent(new AddCollisionCirclesEvent(circles));
        collisionManager.update(0.016);

        assertTrue(collisionManager.getCollisionCircles().isEmpty());
    }

    @Test
    public void testAddProjectilesWithNull() {
        assertDoesNotThrow(() -> collisionManager.addCollisionCircleEvent(null));
        assertDoesNotThrow(() -> collisionManager.addCollisionCircleEvent(new AddCollisionCircleEvent(null)));
    }

    @Test
    public void testProjectileDeathRemovesCollision() {
        collisionManager.addCollisionCircleEvent(new AddCollisionCircleEvent(circle1));
        collisionManager.update(0.016f);

        assertEquals(1, collisionManager.getCollisionCircles().size());

        collisionManager.removePrimitiveEvent(new RemoveCollisionCircleEvent(circle1));
        collisionManager.update(0.016f);

        assertTrue(collisionManager.getCollisionCircles().isEmpty());
    }

    @Test
    public void testProjectileDeathEventWhenNotAdded() {
        collisionManager.removePrimitiveEvent(new RemoveCollisionCircleEvent(circle1));
        collisionManager.update(0.016f);

        assertTrue(collisionManager.getCollisionCircles().isEmpty());
    }

    @Test
    public void testProjectileDeathEventNullValues() {
        assertDoesNotThrow(() -> collisionManager.removePrimitiveEvent(new RemoveCollisionCircleEvent(null)));
        assertDoesNotThrow(() -> collisionManager.removePrimitiveEvent(null));
    }

    @Test
    public void testAddTriggers() {

        ITrigger trigger = mock(ITrigger.class);
        CollisionCircle circle = mock(CollisionCircle.class);
        when(circle.getPosition()).thenReturn(new Vector2());
        when(trigger.getCollisionPrimitive()).thenReturn(circle);

        collisionManager.addTrigger(new SpawnTriggerEvent(trigger));
        collisionManager.update(0.016);

        assertEquals(1, collisionManager.getCollisionCircles().size());
    }


    @Test
    public void testAddTriggerWithNoPosition() {
        ITrigger trigger = mock(ITrigger.class);
        CollisionCircle circle = mock(CollisionCircle.class);
        when(trigger.getCollisionPrimitive()).thenReturn(circle);

        collisionManager.addTrigger(new SpawnTriggerEvent(trigger));
        collisionManager.update(0.016);

        assertTrue(collisionManager.getCollisionCircles().isEmpty());
    }

    @Test
    public void testAddNullTrigger() {
        assertDoesNotThrow(() -> collisionManager.addTrigger(null));
        assertDoesNotThrow(() -> collisionManager.addTrigger(new SpawnTriggerEvent(null)));
    }

    @Test
    public void testRemoveTrigger() {
        ITrigger trigger = mock(ITrigger.class);
        CollisionCircle circle = mock(CollisionCircle.class);
        when(circle.getPosition()).thenReturn(new Vector2());
        when(trigger.getCollisionPrimitive()).thenReturn(circle);

        // Add trigger to manager
        collisionManager.addTrigger(new SpawnTriggerEvent(trigger));
        collisionManager.update(0.016);
        assertEquals(1, collisionManager.getCollisionCircles().size());

        // remove trigger from manager
        collisionManager.removeTrigger(new RemoveTriggerEvent(trigger));
        collisionManager.update(0.016);
        assertTrue(collisionManager.getCollisionCircles().isEmpty());
    }

    @Test
    public void testRemoveNullTrigger() {
        assertDoesNotThrow(() -> collisionManager.addTrigger(null));
        assertDoesNotThrow(() -> collisionManager.addTrigger(new SpawnTriggerEvent(null)));
    }

    private static class CollisionEntity implements ICollidable {

        boolean hasCollided = false;

        final List<ICollidable> targets = new ArrayList<>();

        @Override
        public void collisionEnter(ICollidable target, CollisionType.Type type) {
            hasCollided = true;
            if (target == null) return;
            this.targets.add(target);
        }

        @Override
        public CollisionCircle getCollisionPrimitive() {
            return null;
        }

        @Override
        public Vector2 getPosition() {
            return null;
        }

        @Override
        public void setPosition(Vector2 pos) {

        }
    }
}
