package components.collision.trigger.damage;

import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType;
import components.collision.ICollidable;
import components.collision.primitives.CollisionCircle;
import components.health.Damage;
import components.health.IDamageable;
import components.physics.Physics;
import model.event.EventBus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class DamageRadiusTest {

    private DamageRadius damageRadius;
    private Vector2 originPosition;
    private IFalloffType falloffType;

    @BeforeEach
    void setUp() {
        originPosition = new Vector2(0, 0);
        EventBus eventBus = mock(EventBus.class);
        falloffType = mock(IFalloffType.class);
        damageRadius = new DamageRadius(
                originPosition,
                10,
                new Damage(50),
                10,
                falloffType,
                new CollisionType(CollisionType.Type.ENEMY),
                eventBus
        );
    }

    @Test
    void testGetPosition() {
        Vector2 position = damageRadius.getPosition();
        assertEquals(originPosition, position);
    }

    @Test
    void testSetPosition() {
        Vector2 newPosition = new Vector2(5, 5);
        damageRadius.setPosition(newPosition);
        assertEquals(newPosition, damageRadius.getPosition());
    }

    /**
     * Test the collisionEnter() method of DamageRadius when target has all components.
     * Should apply force on physics and damage target.
     */
    @Test
    void testCollisionEnter() {
        ICollidable target = mock(ICollidable.class);
        Vector2 targetPosition = new Vector2(5, 5);
        Physics physicsComponent = mock(Physics.class);
        IDamageable damageable = mock(IDamageable.class);

        when(target.getPosition()).thenReturn(targetPosition);
        when(target.getPhysicsComponent()).thenReturn(physicsComponent);
        when(target.getDamageable()).thenReturn(damageable);
        when(falloffType.calculateFalloff(anyFloat())).thenReturn(1f);

        damageRadius.collisionEnter(target, CollisionType.Type.ENEMY);

        float expectedForce = 10f; // The force defined in the setUp method.
        Vector2 expectedDirection = new Vector2(targetPosition).sub(originPosition).setLength(expectedForce);
        verify(physicsComponent, times(1)).applyForce(eq(expectedDirection));

        int expectedDamageValue = 50; // The damage value defined in the setUp method.
        verify(damageable, times(1)).applyDamage(argThat(argument -> argument.getDamageValue() == expectedDamageValue));
    }

    /**
     * Test the collisionEnter() method of DamageRadius when target has no health components.
     * Should not throw any exceptions or damage target.
     */
    @Test
    public void testCollisionEnterNoDamageable() {
        ICollidable target = mock(ICollidable.class);
        Vector2 targetPosition = new Vector2(5, 5);
        Physics physicsComponent = mock(Physics.class);

        when(target.getPosition()).thenReturn(targetPosition);
        when(target.getPhysicsComponent()).thenReturn(physicsComponent);
        when(target.getDamageable()).thenReturn(null);
        when(falloffType.calculateFalloff(anyFloat())).thenReturn(1f);

        assertDoesNotThrow(() -> damageRadius.collisionEnter(target, CollisionType.Type.ENEMY));

        float expectedForce = 10f; // The force defined in the setUp method.
        Vector2 expectedDirection = new Vector2(targetPosition).sub(originPosition).setLength(expectedForce);
        verify(physicsComponent, times(1)).applyForce(eq(expectedDirection));
    }

    /**
     * Test the collisionEnter() method of DamageRadius when target has no components.
     * Should not throw any exceptions or apply any effect on physics.
     */
    @Test
    public void testCollisionEnterNoPhysicsComponent() {
        ICollidable target = mock(ICollidable.class);
        Vector2 targetPosition = new Vector2(5, 5);
        IDamageable damageable = mock(IDamageable.class);

        when(target.getPosition()).thenReturn(targetPosition);
        when(target.getDamageable()).thenReturn(damageable);
        when(falloffType.calculateFalloff(anyFloat())).thenReturn(1f);

        assertDoesNotThrow(() -> damageRadius.collisionEnter(target, CollisionType.Type.ENEMY));

        int expectedDamageValue = 50; // The damage value defined in the setUp method.
        verify(damageable, times(1)).applyDamage(argThat(argument -> argument.getDamageValue() == expectedDamageValue));
    }

    /**
     * Test the collisionEnter() method of DamageRadius when target has no components.
     * Should not throw any exceptions and do nothing to target.
     */
    @Test
    void testCollisionEnterNoComponents() {
        ICollidable target = mock(ICollidable.class);
        Vector2 targetPosition = new Vector2(5, 5);

        when(target.getPosition()).thenReturn(targetPosition);
        when(target.getPhysicsComponent()).thenReturn(null);
        when(target.getHealthComponent()).thenReturn(null);

        assertDoesNotThrow(() -> damageRadius.collisionEnter(target, CollisionType.Type.ENEMY));

        verify(target, times(1)).getPhysicsComponent();
        verify(target, times(1)).getDamageable();
    }

    @Test
    public void testCorrectCollisionPrimitive() {
        CollisionCircle primitive = new CollisionCircle(
                null, null,
                10, damageRadius,
                new HashSet<>(Set.of(new CollisionType(CollisionType.Type.ENEMY))), new HashSet<>());

        assertEquals(primitive, damageRadius.getCollisionPrimitive());
    }
}
