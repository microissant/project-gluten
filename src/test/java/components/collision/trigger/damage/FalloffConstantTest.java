package components.collision.trigger.damage;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FalloffConstantTest {

    @Test
    void testCalculateFalloff() {
        FalloffConstant falloffNone = new FalloffConstant();

        float falloffValue = falloffNone.calculateFalloff(0);
        assertEquals(1, falloffValue);

        falloffValue = falloffNone.calculateFalloff(0.5f);
        assertEquals(1, falloffValue);

        falloffValue = falloffNone.calculateFalloff(1);
        assertEquals(1, falloffValue);

        falloffValue = falloffNone.calculateFalloff(2);
        assertEquals(1, falloffValue);
    }
}
