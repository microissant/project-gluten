package components.collision;

import com.badlogic.gdx.math.Vector2;
import components.collision.primitives.CollisionCircle;
import grid.CoordinateItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class SpacePartitionTest {

    SpacePartition spacePartition;
    final int rows = 10;
    final int cols = 10;
    final float cellSize = 10;

    @BeforeEach
    public void setup() {
        spacePartition = new SpacePartition(rows, cols, cellSize);
    }


    @Test
    public void insertNull() {
        spacePartition.insert((CollisionCircle) null);
        spacePartition.insert((List<CollisionCircle>) null);

        for (CoordinateItem<List<CollisionCircle>> cell: spacePartition) {
            assertEquals(new ArrayList<>(), cell.item());
        }
    }

    @Test
    public void insertCorrect() {
        CollisionCircle circle = new CollisionCircle(new Vector2(25, 25), new Vector2(), 10, null, null, null);
        spacePartition.insert(circle);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (i < 1 || i > 3 || j < 1 || j > 3) {
                    assertEquals(new ArrayList<>(), spacePartition.get(0, 0));
                } else {
                    assertEquals(circle, spacePartition.get(i,j).get(0));
                }
            }
        }
    }

    @Test
    public void insertCirclePartiallyOutOfBounds() {
        CollisionCircle circle = new CollisionCircle(new Vector2(0, 0), new Vector2(), 10, null, null, null);
        spacePartition.insert(circle);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (i <= 1 && j <= 1) {
                    assertEquals(circle, spacePartition.get(i,j).get(0));
                } else {
                    assertEquals(new ArrayList<>(), spacePartition.get(i, j));
                }
            }
        }
    }

    @Test
    public void insertCircleEntirelyOutOfBounds() {
        CollisionCircle circle1 = new CollisionCircle(new Vector2(-cellSize*2, -cellSize*2), new Vector2(), cellSize, null, null, null);
        CollisionCircle circle2 = new CollisionCircle(new Vector2(rows*cellSize*2, cols*cellSize*2), new Vector2(), cellSize, null, null, null);

        spacePartition.insert(circle1);
        spacePartition.insert(circle2);

        for (CoordinateItem<List<CollisionCircle>> cell: spacePartition) {
            assertEquals(new ArrayList<>(), cell.item());
        }
    }

    @Test
    public void insertListOfCircles() {
        CollisionCircle circle1 = new CollisionCircle(new Vector2(50, 50), new Vector2(), cellSize, null, null, null);
        CollisionCircle circle2 = new CollisionCircle(new Vector2(55, 55), new Vector2(), cellSize, null, null, null);
        CollisionCircle circle3 = new CollisionCircle(new Vector2(90, 90), new Vector2(), cellSize, null, null, null);

        List<CollisionCircle> list = new ArrayList<>();
        list.add(circle1);
        list.add(circle2);
        list.add(circle3);
        spacePartition.insert(list);


        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (i >= 8 && j >= 8) {
                    assertEquals(1, spacePartition.get(i, j).size());
                    assertEquals(circle3, spacePartition.get(i, j).get(0));
                }
                else if (i >= 4 && i <= 6 && j >= 4 && j <= 6) {
                    assertEquals(2, spacePartition.get(i, j).size());
                    assertEquals(circle1, spacePartition.get(i, j).get(0));
                    assertEquals(circle2, spacePartition.get(i, j).get(1));
                }
                else {
                    assertEquals(new ArrayList<>(), spacePartition.get(i, j));
                }
            }
        }
    }

    @Test
    public void insertListOfNull() {
        List<CollisionCircle> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(null);
        }
        spacePartition.insert(list);

        for (CoordinateItem<List<CollisionCircle>> cell: spacePartition) {
            assertEquals(new ArrayList<>(), cell.item());
        }
    }

    @Test
    public void removeNull() {
        SpacePartition spacePartition1 = new SpacePartition(rows, cols, cellSize);

        assertEquals(spacePartition1, spacePartition);
        spacePartition.remove(null);
        assertEquals(spacePartition1, spacePartition);
    }

    @Test
    public void removeCircleNotInserted() {
        CollisionCircle circle = new CollisionCircle(new Vector2(), new Vector2(), 5, null, null, null);
        SpacePartition spacePartition1 = new SpacePartition(rows, cols, cellSize);

        assertEquals(spacePartition1, spacePartition);
        spacePartition.remove(circle);
        assertEquals(spacePartition1, spacePartition);
    }

    @Test
    public void removeCorrectly() {
        SpacePartition spacePartition1 = new SpacePartition(rows, cols, cellSize);
        CollisionCircle circle = new CollisionCircle(new Vector2(25, 25), new Vector2(), 10, null, null, null);
        spacePartition.insert(circle);
        assertNotEquals(spacePartition1, spacePartition);
        spacePartition.remove(circle);
        assertEquals(spacePartition1, spacePartition);
    }

    @Test
    public void updateWhenMovedChangesCells() {
        CollisionCircle circle = new CollisionCircle(new Vector2(25, 25), new Vector2(), 10, null, null, null);
        spacePartition.insert(circle);

        // Correct cells before move
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (i < 1 || i > 3 || j < 1 || j > 3) {
                    assertEquals(new ArrayList<>(), spacePartition.get(0, 0));
                } else {
                    assertEquals(1, spacePartition.get(i, j).size());
                    assertEquals(circle, spacePartition.get(i, j).get(0));
                }
            }
        }

        circle.setPosition(50, 50);
        spacePartition.update(circle, new Vector2(25, 25));

        // Correct cells after move and update
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (i >= 4 && i <= 6 && j >= 4 && j <= 6) {
                    assertEquals(1, spacePartition.get(i, j).size());
                    assertEquals(circle, spacePartition.get(i, j).get(0));
                } else {
                    assertEquals(new ArrayList<>(), spacePartition.get(i, j));
                }
            }
        }
    }

    @Test
    public void noCellChangeWhenMoveToLittle() {
        CollisionCircle circle = new CollisionCircle(new Vector2(25, 25), new Vector2(), 10, null, null, null);
        spacePartition.insert(circle);

        // Correct cells before move
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (i < 1 || i > 3 || j < 1 || j > 3) {
                    assertEquals(new ArrayList<>(), spacePartition.get(0, 0));
                } else {
                    assertEquals(1, spacePartition.get(i, j).size());
                    assertEquals(circle, spacePartition.get(i, j).get(0));
                }
            }
        }

        circle.getPosition().x = 26;
        circle.getPosition().y = 26;
        spacePartition.update(circle, new Vector2(25, 25));

        // Correct cells after move and update
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (i < 1 || i > 3 || j < 1 || j > 3) {
                    assertEquals(new ArrayList<>(), spacePartition.get(0, 0));
                } else {
                    assertEquals(1, spacePartition.get(i, j).size());
                    assertEquals(circle, spacePartition.get(i, j).get(0));
                }
            }
        }
    }

    @Test
    public void queryRangeEmptyGrid() {
        assertEquals(new HashSet<>(), spacePartition.queryRange(new CollisionCircle(new Vector2(), new Vector2(), 5, null, null, null)));
    }

    @Test
    public void queryNull() {
        assertEquals(new HashSet<>(), spacePartition.queryRange(null));
        assertEquals(new HashSet<>(), spacePartition.queryRange(new CollisionCircle(null, new Vector2(), 5, null, null, null)));
    }

    @Test
    public void queryRangeAllNeighbours() {
        CollisionCircle circle1 = new CollisionCircle(new Vector2(25, 25), new Vector2(), 10.1f, null, null, null);
        CollisionCircle circle2 = new CollisionCircle(new Vector2(35, 35), new Vector2(), 10.2f, null, null, null);
        CollisionCircle circle3 = new CollisionCircle(new Vector2(15, 45), new Vector2(), 10.3f, null, null, null);
        CollisionCircle circle4 = new CollisionCircle(new Vector2(60, 50), new Vector2(), 9.9f, null, null, null);
        List<CollisionCircle> list = new ArrayList<>(Arrays.asList(
                circle1, circle2, circle3, circle4
        ));
        spacePartition.insert(list);

        Set<CollisionCircle> queryResult = spacePartition.queryRange(circle1);

        assertEquals(2, queryResult.size());
        assertTrue(queryResult.contains(circle2));
        assertTrue(queryResult.contains(circle3));
        assertFalse(queryResult.contains(circle1));
        assertFalse(queryResult.contains(circle4));

        queryResult = spacePartition.queryRange(circle2);
        assertEquals(2, queryResult.size());
        assertTrue(queryResult.contains(circle1));
        assertTrue(queryResult.contains(circle3));
        assertFalse(queryResult.contains(circle2));
        assertFalse(queryResult.contains(circle4));


        queryResult = spacePartition.queryRange(circle3);
        assertEquals(2, queryResult.size());
        assertTrue(queryResult.contains(circle1));
        assertTrue(queryResult.contains(circle2));
        assertFalse(queryResult.contains(circle3));
        assertFalse(queryResult.contains(circle4));

        queryResult = spacePartition.queryRange(circle4);
        assertEquals(0, queryResult.size());
        assertFalse(queryResult.contains(circle4));
        assertFalse(queryResult.contains(circle1));
        assertFalse(queryResult.contains(circle2));
        assertFalse(queryResult.contains(circle3));
    }

    @Test
    public void queryRangeBigCircleInBoundFromFarAway() {
        CollisionCircle circle1 = new CollisionCircle(new Vector2(25, 25), new Vector2(), 10, null, null, null);
        CollisionCircle circle2 = new CollisionCircle(new Vector2(60, 50), new Vector2(), 50, null, null, null);
        spacePartition.insert(circle1);
        spacePartition.insert(circle2);

        Set<CollisionCircle> queryResult = spacePartition.queryRange(circle1);
        assertEquals(1, queryResult.size());
        assertTrue(queryResult.contains(circle2));
        assertFalse(queryResult.contains(circle1));

        queryResult = spacePartition.queryRange(circle2);
        assertTrue(queryResult.contains(circle1));
        assertFalse(queryResult.contains(circle2));
    }

    @Test
    public void queryRangeNoShapesInRange() {
        CollisionCircle circle1 = new CollisionCircle(new Vector2(30, 15), new Vector2(), 10, null, null, null);
        CollisionCircle circle2 = new CollisionCircle(new Vector2(85, 10), new Vector2(), 10, null, null, null);
        CollisionCircle circle3 = new CollisionCircle(new Vector2(5, 45), new Vector2(), 10, null, null, null);
        CollisionCircle circle4 = new CollisionCircle(new Vector2(55, 45), new Vector2(), 10, null, null, null);
        CollisionCircle circle5 = new CollisionCircle(new Vector2(95, 45), new Vector2(), 10, null, null, null);
        CollisionCircle circle6 = new CollisionCircle(new Vector2(30, 70), new Vector2(), 10, null, null, null);
        CollisionCircle circle7 = new CollisionCircle(new Vector2(85, 85), new Vector2(), 10, null, null, null);

        List<CollisionCircle> list = new ArrayList<>(Arrays.asList(
                circle1, circle2, circle3, circle4, circle5, circle6, circle7
        ));
        spacePartition.insert(list);

        for (CollisionCircle circle: list) {
            Set<CollisionCircle> queryResult = spacePartition.queryRange(circle);

            assertTrue(queryResult.isEmpty());
        }
    }

    @Test
    public void clearEmpty() {
        SpacePartition spacePartition1 = new SpacePartition(rows, cols, cellSize);
        assertEquals(spacePartition1, spacePartition);
        spacePartition.clear();
        assertEquals(spacePartition1, spacePartition);
    }

    @Test
    public void clearAll() {
        CollisionCircle circle1 = new CollisionCircle(new Vector2(30, 15), new Vector2(), 10, null, null, null);
        CollisionCircle circle2 = new CollisionCircle(new Vector2(85, 10), new Vector2(), 10, null, null, null);
        CollisionCircle circle3 = new CollisionCircle(new Vector2(5, 45), new Vector2(), 10, null, null, null);
        CollisionCircle circle4 = new CollisionCircle(new Vector2(55, 45), new Vector2(), 10, null, null, null);
        CollisionCircle circle5 = new CollisionCircle(new Vector2(95, 45), new Vector2(), 10, null, null, null);
        CollisionCircle circle6 = new CollisionCircle(new Vector2(30, 70), new Vector2(), 10, null, null, null);
        CollisionCircle circle7 = new CollisionCircle(new Vector2(85, 85), new Vector2(), 10, null, null, null);
        List<CollisionCircle> list = new ArrayList<>(Arrays.asList(
                circle1, circle2, circle3, circle4, circle5, circle6, circle7
        ));
        spacePartition.insert(list);

        SpacePartition spacePartition1 = new SpacePartition(rows, cols, cellSize);
        assertNotEquals(spacePartition1, spacePartition);
        spacePartition.clear();
        assertEquals(spacePartition1, spacePartition);
    }
}
