package components.collision.event;

import components.collision.CollisionType;
import components.collision.ICollidable;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.mockito.Mockito.*;

class CollisionDefinitionsEventTest {

    @Test
    void testTriggerCollisionEvent() {
        // Create mock collision events
        ICollisionEvent selfEvent = mock(ICollisionEvent.class);
        ICollisionEvent targetEvent = mock(ICollisionEvent.class);

        // Create maps with collision events
        HashMap<CollisionType.Type, ICollisionEvent> selfCollisionEvents = new HashMap<>();
        selfCollisionEvents.put(CollisionType.Type.ENEMY, selfEvent);

        HashMap<CollisionType.Type, ICollisionEvent> targetCollisionEvents = new HashMap<>();
        targetCollisionEvents.put(CollisionType.Type.ENEMY, targetEvent);

        // Create CollisionDefinitionsEvent
        CollisionDefinitionsEvent collisionDefinitionsEvent = new CollisionDefinitionsEvent(selfCollisionEvents, targetCollisionEvents);

        // Create mock collidable objects
        ICollidable eventOrigin = mock(ICollidable.class);
        ICollidable target = mock(ICollidable.class);

        // Trigger collision event
        collisionDefinitionsEvent.triggerCollisionEvent(eventOrigin, target, CollisionType.Type.ENEMY);

        // Verify that each event's execute method is called once with the corresponding target
        verify(selfEvent, times(1)).execute(eventOrigin);
        verify(targetEvent, times(1)).execute(target);
    }

    @Test
    void testTriggerCollisionEvent_NoEventForType() {
        // Create empty maps
        HashMap<CollisionType.Type, ICollisionEvent> selfCollisionEvents = new HashMap<>();
        HashMap<CollisionType.Type, ICollisionEvent> targetCollisionEvents = new HashMap<>();

        // Create CollisionDefinitionsEvent
        CollisionDefinitionsEvent collisionDefinitionsEvent = new CollisionDefinitionsEvent(selfCollisionEvents, targetCollisionEvents);

        // Create mock collidable objects
        ICollidable eventOrigin = mock(ICollidable.class);
        ICollidable target = mock(ICollidable.class);

        // Trigger collision event
        collisionDefinitionsEvent.triggerCollisionEvent(eventOrigin, target, CollisionType.Type.ENEMY);

        // Verify that no interactions occurred
        verifyNoInteractions(eventOrigin);
        verifyNoInteractions(target);
    }
}
