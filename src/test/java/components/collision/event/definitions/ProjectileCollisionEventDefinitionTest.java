package components.collision.event.definitions;

import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType;
import components.collision.ICollidable;
import components.health.Damage;
import components.health.IDamageable;
import components.physics.Physics;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

class ProjectileCollisionEventDefinitionTest {

    @Test
    void testTriggerCollisionEvent() {
        // Create mock damages and impact force
        Damage selfDamage = mock(Damage.class);
        Damage targetDamage = mock(Damage.class);
        Vector2 impactForce = new Vector2(10, 10);

        IDamageable originDamageble = mock(IDamageable.class);
        IDamageable targetDamagable = mock(IDamageable.class);
        Physics targetPhysics = mock(Physics.class);


        // Create ProjectileCollisionEventDefinition
        ProjectileCollisionEventDefinition projectileCollisionEventDefinition = new ProjectileCollisionEventDefinition(
                CollisionType.Type.ENEMY, selfDamage, targetDamage, impactForce);

        // Create mock collidable objects
        ICollidable eventOrigin = mock(ICollidable.class);
        when(eventOrigin.getDamageable()).thenReturn(originDamageble);

        ICollidable target = mock(ICollidable.class);
        when(target.getDamageable()).thenReturn(targetDamagable);
        when(target.getPhysicsComponent()).thenReturn(targetPhysics);

        // Trigger collision event
        projectileCollisionEventDefinition.triggerCollisionEvent(eventOrigin, target, CollisionType.Type.ENEMY);

        // Verify that the getDamageable methods are called
        verify(eventOrigin, times(1)).getDamageable();
        verify(originDamageble, times(1)).applyDamage(selfDamage);

        verify(target, times(1)).getDamageable();
        verify(targetDamagable, times(1)).applyDamage(targetDamage);
        verify(target, times(1)).getPhysicsComponent();
        verify(targetPhysics, times(1)).applyForce(impactForce);
    }

    @Test
    void testTriggerCollisionEvent_NoImpactForce() {
        // Create mock damages and impact force
        Damage selfDamage = mock(Damage.class);
        Damage targetDamage = mock(Damage.class);

        IDamageable originDamageble = mock(IDamageable.class);
        IDamageable targetDamagable = mock(IDamageable.class);
        Physics targetPhysics = mock(Physics.class);


        // Create ProjectileCollisionEventDefinition
        ProjectileCollisionEventDefinition projectileCollisionEventDefinition = new ProjectileCollisionEventDefinition(
                CollisionType.Type.ENEMY, selfDamage, targetDamage);

        // Create mock collidable objects
        ICollidable eventOrigin = mock(ICollidable.class);
        when(eventOrigin.getDamageable()).thenReturn(originDamageble);

        ICollidable target = mock(ICollidable.class);
        when(target.getDamageable()).thenReturn(targetDamagable);
        when(target.getPhysicsComponent()).thenReturn(targetPhysics);

        // Trigger collision event
        projectileCollisionEventDefinition.triggerCollisionEvent(eventOrigin, target, CollisionType.Type.ENEMY);

        // Verify that the getDamageable methods are called
        verify(eventOrigin, times(1)).getDamageable();
        verify(originDamageble, times(1)).applyDamage(selfDamage);

        verify(target, times(1)).getDamageable();
        verify(targetDamagable, times(1)).applyDamage(targetDamage);
        verify(target, times(1)).getPhysicsComponent();
        verify(targetPhysics, times(1)).applyForce(new Vector2());
    }
}
