package components.collision.event.events;

import com.badlogic.gdx.math.Vector2;
import components.collision.ICollidable;
import components.physics.Physics;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

class CollisionPushEventTest {

    @Test
    void testExecute() {
        Vector2 pushForce = new Vector2(10, 10);
        CollisionPushEvent pushEvent = new CollisionPushEvent(pushForce);

        ICollidable target = mock(ICollidable.class);
        Physics physicsComponent = mock(Physics.class);

        when(target.getPhysicsComponent()).thenReturn(physicsComponent);

        pushEvent.execute(target);

        verify(physicsComponent, times(1)).applyForce(pushForce);
    }

    @Test
    void testExecuteNoPhysicsComponent() {
        Vector2 pushForce = new Vector2(10, 10);
        CollisionPushEvent pushEvent = new CollisionPushEvent(pushForce);

        ICollidable target = mock(ICollidable.class);

        when(target.getPhysicsComponent()).thenReturn(null);

        pushEvent.execute(target);

        verify(target, times(1)).getPhysicsComponent();
        verifyNoMoreInteractions(target);
    }

    @Test
    void testExecuteNullTarget() {
        Vector2 pushForce = new Vector2(10, 10);
        CollisionPushEvent pushEvent = new CollisionPushEvent(pushForce);

        pushEvent.execute(null);

        // No exceptions should be thrown when the target is null.
    }

    @Test
    void testExecuteNullPushForce() {
        CollisionPushEvent pushEvent = new CollisionPushEvent(null);

        ICollidable target = mock(ICollidable.class);

        pushEvent.execute(target);

        verify(target, times(0)).getPhysicsComponent();
        verifyNoMoreInteractions(target);
    }
}
