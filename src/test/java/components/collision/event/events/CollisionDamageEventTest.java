package components.collision.event.events;

import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType;
import components.collision.ICollidable;
import components.collision.primitives.CollisionCircle;
import components.health.Damage;
import components.health.Healer;
import components.health.IDamageable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CollisionDamageEventTest {

    private CollisionDamageEvent damageEvent;

    @BeforeEach
    public void setUp() {
        Damage testDamage = new Damage(10);
        damageEvent = new CollisionDamageEvent(testDamage);
    }

    @Test
    public void executeAppliesDamageWhenTargetHasHealthManager() {
        // Mock collidable with health manager
        MockCollidableWithHealthManager target = new MockCollidableWithHealthManager(100);

        // Execute the event
        damageEvent.execute(target);

        // Check if the damage was applied
        assertEquals(90, target.getHealth());
    }

    @Test
    public void executeDoesNothingWhenTargetIsNull() {
        // Execute the event with a null target
        damageEvent.execute(null);

        // No exception should be thrown, test passes if it reaches this point
    }

    @Test
    public void executeDoesNothingWhenTargetHasNoHealthManager() {
        // Mock collidable without health manager
        ICollidable target = new MockCollidableWithoutHealthManager();

        // Execute the event
        damageEvent.execute(target);

        // No exception should be thrown, test passes if it reaches this point
    }

    // Mock classes for testing purposes
    private static class MockCollidableWithHealthManager implements ICollidable, IDamageable {
        private int health;

        MockCollidableWithHealthManager(int health) {
            this.health = health;
        }

        @Override
        public void collisionEnter(ICollidable target, CollisionType.Type type) {

        }

        @Override
        public IDamageable getDamageable() {
            return this;
        }

        @Override
        public CollisionCircle getCollisionPrimitive() {
            return null;
        }

        @Override
        public int getCurrentHealth() {
            return 0;
        }

        @Override
        public int getMaximumHealth() {
            return 0;
        }

        @Override
        public void applyHeal(Healer healer) {

        }

        @Override
        public void applyDamage(Damage damage) {
            health -= damage.getDamageValue();
        }

        public int getHealth() {
            return health;
        }

        @Override
        public boolean isDead() {
            return false;
        }

        @Override
        public Vector2 getPosition() {
            return null;
        }

        @Override
        public void setPosition(Vector2 pos) {

        }
    }

    private static class MockCollidableWithoutHealthManager implements ICollidable {
        @Override
        public void collisionEnter(ICollidable target, CollisionType.Type type) {

        }

        @Override
        public CollisionCircle getCollisionPrimitive() {
            return null;
        }

        @Override
        public Vector2 getPosition() {
            return null;
        }

        @Override
        public void setPosition(Vector2 pos) {

        }
    }
}
