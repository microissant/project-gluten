package components.collision.event.events;

import components.collision.ICollidable;
import components.collision.event.ICollisionEvent;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

class CompositeCollisionEventTest {

    @Test
    void testExecute() {
        // Create mock collision events
        ICollisionEvent event1 = mock(ICollisionEvent.class);
        ICollisionEvent event2 = mock(ICollisionEvent.class);
        ICollisionEvent event3 = mock(ICollisionEvent.class);

        List<ICollisionEvent> eventList = Arrays.asList(event1, event2, event3);

        // Create CompositeCollisionEvent
        CompositeCollisionEvent compositeEvent = new CompositeCollisionEvent(eventList);

        // Create mock target
        ICollidable target = mock(ICollidable.class);

        // Execute composite event
        compositeEvent.execute(target);

        // Verify that each event's execute method is called once with the target
        verify(event1, times(1)).execute(target);
        verify(event2, times(1)).execute(target);
        verify(event3, times(1)).execute(target);
    }
}
