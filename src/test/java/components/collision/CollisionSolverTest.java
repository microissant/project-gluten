package components.collision;

import com.badlogic.gdx.math.Vector2;
import components.collision.primitives.CollisionCircle;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CollisionSolverTest {


    @CsvSource(value = { "2.38,1.72", "-3.56, 7.7", "-2.1, -0.48", "-1.9, -2.72" })
    @ParameterizedTest(name = "Velocity x: {0} y: {1}")
    public void circleCollisionFalseValues(float x, float y) {

        CollisionCircle circle1 = new CollisionCircle(new Vector2(), new Vector2(), 1, null, null, null);
        CollisionCircle circle2 = new CollisionCircle(new Vector2(x,y), new Vector2(), 1, null, null, null);

        assertFalse(CollisionSolver.checkCollision(circle1, circle2));
    }

    @CsvSource(value = { "0, 0", "0.52, 0.3", "0.54, 1.6", "0, 2", "-0.78, 0.5", "-0.3, -0.28", "1, -1"})
    @ParameterizedTest(name = "circle2 x: {0} y: {1}")
    public void circleCollisionTrueValues(float x, float y) {

        CollisionCircle circle1 = new CollisionCircle(new Vector2(), new Vector2(), 1, null, null, null);
        CollisionCircle circle2 = new CollisionCircle(new Vector2(x,y), new Vector2(), 1, null, null, null);

        assertTrue(CollisionSolver.checkCollision(circle1, circle2));
    }

    @Test
    public void circleCollisionNullValues() {
        CollisionCircle circle1 = new CollisionCircle(new Vector2(), new Vector2(), 1, null, null, null);
        CollisionCircle circle2 = new CollisionCircle(null, new Vector2(), 1, null, null, null);

        assertFalse(CollisionSolver.checkCollision(circle1, null));
        assertFalse(CollisionSolver.checkCollision(circle1, circle2));
        assertFalse(CollisionSolver.checkCollision(null, null));
        assertFalse(CollisionSolver.checkCollision(null, circle2));
        assertFalse(CollisionSolver.checkCollision(circle2, circle2));
    }

    @Test
    public void rayCircleCollisionNullValues() {
        CollisionCircle circle1 = new CollisionCircle(new Vector2(), new Vector2(), 1, null, null, null);
        CollisionCircle circle3 = new CollisionCircle(null, new Vector2(), 1, null, null, null);


        assertFalse(CollisionSolver.checkCollision(circle1, null));
        assertFalse(CollisionSolver.checkCollision(circle1, circle3));
        assertFalse(CollisionSolver.checkCollision(null, null));
        assertFalse(CollisionSolver.checkCollision(null, circle3));
        assertFalse(CollisionSolver.checkCollision(circle3, circle3));
    }

}
