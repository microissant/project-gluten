package components.weapon.magazine;

import components.weapon.projectile.emitter.IProjectileEmitter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class MagazineTest {

    private Magazine magazine;

    @Mock
    IProjectileEmitter projectileEmitter;

    @BeforeEach
    public void setup() {
        magazine = new Magazine(10, 10, null);

        projectileEmitter = mock(IProjectileEmitter.class);

        when(projectileEmitter.maxAmmoCost()).thenReturn(3);
        when(projectileEmitter.minAmmoCost()).thenReturn(1);
    }

    /**
     * Tests the Magazine constructor with 0 ammo.
     * Verifies that the current ammo is set to 0 and the max ammo is set correctly.
     */
    @Test
    public void testConstructorAmmo() {

        magazine = new Magazine(0, 10, null);

        assertEquals(0, magazine.currentAmmo());
        assertEquals(10, magazine.maxAmmo());
    }

    /**
     * Tests the Magazine constructor with negative ammo values.
     * Verifies that the current and max ammo are set correctly.
     */
    @Test
    public void testConstructorMoreCurrentThenMax() {
        magazine = new Magazine(10, 5, null);

        assertEquals(5, magazine.currentAmmo());
        assertEquals(5, magazine.maxAmmo());
    }

    /**
     * Tests the Magazine constructor with negative ammo values.
     * Verifies that the current and max ammo are set correctly.
     */
    @Test
    public void testConstructorNegativeAmmo() {
        // negative current, positive max
        magazine = new Magazine(-5, 10, null);
        assertEquals(0, magazine.currentAmmo());
        assertEquals(10, magazine.maxAmmo());

        // positive current, negative max
        magazine = new Magazine(10, -5, null);
        assertEquals(0, magazine.currentAmmo());
        assertEquals(0, magazine.maxAmmo());

        // Both current and max negative
        magazine = new Magazine(-10, -6, null);
        assertEquals(0, magazine.currentAmmo());
        assertEquals(0, magazine.currentAmmo());
    }

    /**
     * Tests the Magazine's sufficientAmmunition method when there is enough ammo for max cost.
     * Verifies that the method returns true and that the correct number of ammo is retrieved.
     */
    @Test
    public void testSufficientAmmoForMaxCost() {
        assertTrue(magazine.sufficientAmmunition(projectileEmitter));
        assertEquals(3, magazine.retrieveAmmo(projectileEmitter));
    }

    /**
     * Tests the Magazine's sufficientAmmunition method when there is not enough ammo for max cost but enough for min cost.
     * Verifies that the method returns true and that the correct number of ammo is retrieved.
     */
    @Test
    public void testSufficientAmmoForMinButNotMax() {
        magazine = new Magazine(2, 5, null);


        assertTrue(magazine.sufficientAmmunition(projectileEmitter));
        assertEquals(2, magazine.retrieveAmmo(projectileEmitter));
    }


    /**
     * Tests the Magazine's sufficientAmmunition method when there is not enough ammo for either max or min cost.
     * Verifies that the method returns false and that 0 ammo is retrieved.
     */
    @Test
    public void testInsufficientAmmo() {
        magazine = new Magazine(0, 10, null);

        assertFalse(magazine.sufficientAmmunition(projectileEmitter));
        assertEquals(0, magazine.retrieveAmmo(projectileEmitter));
    }

    /**
     * Tests the Magazine's addBullets method when there is room for more ammo.
     * Verifies that the ammo is added and that the current and max ammo values are correct.
     */
    @Test
    public void testAddAmmoWhenNeeded() {
        magazine = new Magazine(1,5, null);

        magazine.addBullets(3);

        assertEquals(4, magazine.currentAmmo());
        assertEquals(5, magazine.maxAmmo());
    }

    /**
     * Tests the Magazine's addBullets method when there is no room for more ammo.
     * Verifies that the ammo is not added and that the current and max ammo values are correct.
     */
    @Test
    public void testAddAmmoWhenFull() {
        magazine = new Magazine(5,5, null);

        magazine.addBullets(3);

        assertEquals(5, magazine.currentAmmo());
        assertEquals(5, magazine.maxAmmo());
    }


    /**
     * Tests the Magazine's addBullets method when adding a negative number of bullets.
     * Verifies that no ammo is added and that the current and max ammo values are correct.
     */

    @Test
    public void testAddNegativeAmmo() {
        magazine = new Magazine(5, 5, null);

        magazine.addBullets(-3);

        assertEquals(5, magazine.currentAmmo());
        assertEquals(5, magazine.maxAmmo());
    }
}