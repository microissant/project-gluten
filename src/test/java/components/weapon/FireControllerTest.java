package components.weapon;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.badlogic.gdx.math.Vector2;
import components.weapon.FireController.State;
import components.weapon.magazine.IMagazine;
import components.weapon.projectile.ProjectileTypes;
import components.weapon.projectile.emitter.IProjectileEmitter;

/**
 * Tests for the class FireController
 */

public class FireControllerTest {

    private IMagazine magazine;
    private IProjectileEmitter projectileEmitter;
    private IWeapon ownerWeapon;
    private ProjectileTypes projectileType;
    private FireControllerDetails details;
    private FireController fireController;
    private FireControllerDetails details2;
    private FireController fireController2;

    @BeforeEach
    public void setUp(){
        magazine = mock(IMagazine.class);
        projectileEmitter = mock(IProjectileEmitter.class);
        ownerWeapon = mock(IWeapon.class);
        projectileType = ProjectileTypes.TOPPING;
        
        //Create one FireController where automaticRelease == true
        details = new FireControllerDetails(1f, 1f, true, true, true, 0.1f, 3, 1f);
        fireController = new FireController(details, projectileEmitter, magazine, ownerWeapon, projectileType);
        
        //Create one FireController where automaticRelease == false
        details2 = new FireControllerDetails(2f, 1f, false, true, true, 0.1f, 3, 1f);
        fireController2 = new FireController(details2, projectileEmitter, magazine, ownerWeapon, projectileType);
    }

    /**
     * Checks that the constructor only throws an exception when details == null and not for
     * other null values.
    */
    @Test
    public void constructorNullValuesTest(){
        assertThrows(NullPointerException.class, () -> new FireController(null, projectileEmitter, magazine, ownerWeapon, projectileType));
        
        assertDoesNotThrow(() -> new FireController(details, null, magazine, ownerWeapon, projectileType));
        assertDoesNotThrow(() -> new FireController(details, projectileEmitter, null, ownerWeapon, projectileType));
        assertDoesNotThrow(() -> new FireController(details, projectileEmitter, magazine, null, projectileType));
        assertDoesNotThrow(() -> new FireController(details, null, magazine, ownerWeapon, null));
        assertDoesNotThrow(() -> new FireController(details, projectileEmitter, null, ownerWeapon, null));
        assertDoesNotThrow(() -> new FireController(details, projectileEmitter, magazine, null, null));
        assertDoesNotThrow(() -> new FireController(details, null, magazine, null, projectileType));
        assertDoesNotThrow(() -> new FireController(details, null, null, ownerWeapon, projectileType));
        assertDoesNotThrow(() -> new FireController(details, null, null, null, projectileType));
        assertDoesNotThrow(() -> new FireController(details, null, null, null, null));
    }
    
    /**
     * Checks that getCurrentState returns a state and that the states update in the correct order
     * when AutomaticRelease == true
    */
    @Test
    public void changeStateWithAutomaticReleaseTest(){
        assertEquals(State.IDLE, fireController.getCurrentState());
        
        fireController.activate();
        assertEquals(State.CHARGE, fireController.getCurrentState());
        
        fireController.activate();        
        fireController.update(10);
        assertEquals(State.BURST, fireController.getCurrentState());
        
        fireController.update(4);
        assertEquals(State.DISCHARGE, fireController.getCurrentState());
        
        fireController.update(4);
        assertEquals(State.IDLE, fireController.getCurrentState());
        
    }
    
    /**
     * Checks that getCurrentState returns a state and that the states update in the correct 
     * order when AutomaticRelease == false 
    */
    @Test
    public void changeStateWithoutAutomaticReleaseTest(){
        assertEquals(State.IDLE, fireController2.getCurrentState());
        
        fireController2.activate();
        assertEquals(State.CHARGE, fireController2.getCurrentState());
       
        fireController2.activate();        
        fireController2.update(10);
        assertEquals(State.CHARGE, fireController2.getCurrentState());
        
        fireController2.update(4);
        assertEquals(State.CHARGE, fireController2.getCurrentState());
        
        fireController2.reset();

        fireController2.update(4);
        assertEquals(State.DISCHARGE, fireController2.getCurrentState());
        
    }

    /**
     *Checks that the enum contains the correct states.
     */
    @Test
    public void testEnumValues() {
        assertEquals(5, State.values().length);
        assertArrayEquals(new State[]{State.IDLE, State.CHARGE, State.BURST, State.BETWEENFIRE, State.DISCHARGE}, State.values());
    }

    /**
     * Checks that the enums valueOf() method returns the correct value
     */
    @Test
    public void testEnumBehavior() {
        assertEquals(State.IDLE, State.valueOf("IDLE"));
        assertEquals(State.CHARGE, State.valueOf("CHARGE"));
        assertEquals(State.BURST, State.valueOf("BURST"));
        assertEquals(State.BETWEENFIRE, State.valueOf("BETWEENFIRE"));
        assertEquals(State.DISCHARGE, State.valueOf("DISCHARGE"));
    }

    @Test
    public void getChargeFactor(){
        assertNotNull(fireController.getChargeFactor());
    }


    /**
     * Checks that the correct methods are called within fire() when calling update() 
    */
    @Test
    public void updateFireTest(){
        //Set up mock objects and use them
        Vector2 direction = mock(Vector2.class);
        Vector2 originPosition = mock(Vector2.class);
        fireController.setAimDirection(direction);
        fireController.setOriginPosition(originPosition);
        
        //Dictate the behaviour of certain methods in order to verify that they have been called
        when(projectileEmitter.emitProjectiles(magazine, direction, originPosition, projectileType, 1f)).thenReturn(2);
        doNothing().when(ownerWeapon).countShots(anyInt());
        when(magazine.sufficientAmmunition(any())).thenReturn(true);
        
        //Activate the correct sequence of methods in order to change state to BURST.
        //fire() will not be called otherwise
        fireController.activate();       
        fireController.update(10);
        fireController.update(4);
        
        //The methods will be called 3 times each, equaling the burst number given in the constructor in setUp()
        verify(projectileEmitter, times(3)).emitProjectiles(magazine, direction, originPosition, projectileType, 1f);
        verify(ownerWeapon, times(3)).countShots(anyInt());
        verify(magazine, times(3)).sufficientAmmunition(any());
    }

    /**
     * Tests that chargeFactor returns the correct value when automaticRelease == true.
     * stateElapsedTime is seemingly always 0
     */
    @Test
    public void chargeFactorWithAutomaticReleaseTest(){
        //State is IDLE
        assertEquals(0, fireController.getChargeFactor());
        
        //Changes state to CHARGE
        fireController.activate();
        fireController.update(50);

        assertEquals(0, fireController.getChargeFactor());
        
        //Changes state to BURST
        fireController.activate();        
        fireController.update(10);
        assertEquals(0, fireController.getChargeFactor());
        
        //Changes state to DISCHARGE
        fireController.update(10);
        assertEquals(0, fireController.getChargeFactor());
    }
    
    /**
     * Tests that chargeFactor returns the correct value when automaticRelease == true.
     * Chargefactor is here set to 2f in the constructor.
     * getChargeFactor will then perform deltatime/chargefactor when state == CHARGE
     */
    @Test
    public void chargeFactorWithoutAutomaticReleaseTest(){
        //State is IDLE
        assertEquals(0, fireController2.getChargeFactor());
        
        //Changes state to CHARGE
        fireController2.activate();
        assertEquals(0, fireController2.getChargeFactor());
                
        fireController2.update(10);
        assertEquals(5, fireController2.getChargeFactor());
        
        // (10/2)+(10/2)
        fireController2.update(10);
        assertEquals(10, fireController2.getChargeFactor());
               
        //Changes state to DISCHARGE
        fireController2.reset();
        fireController2.update(10);
        assertEquals(0, fireController2.getChargeFactor());
    }

    /**
     * Tests that timeBetweenShotsFactor returns the correct value.
     * stateElapsedTime is seemingly always 0
     */
    @Test
    public void timeBetweenShotsFactorTest(){
        //State is IDLE
        assertEquals(0, fireController.getTimeBetweenShotsFactor());
        
        //Changes state to CHARGE
        fireController.activate();
        assertEquals(0, fireController.getTimeBetweenShotsFactor());
        
        //Changes state to BURST
        fireController.activate();        
        fireController.update(10);
        assertEquals(0, fireController.getTimeBetweenShotsFactor());
        
        //Changes state to DISCHARGE
        fireController.update(10);
        assertEquals(State.DISCHARGE, fireController.getCurrentState());
        assertEquals(0, fireController.getTimeBetweenShotsFactor());
    }
    
}
