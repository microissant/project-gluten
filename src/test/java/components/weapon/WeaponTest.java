package components.weapon;

import com.badlogic.gdx.math.Vector2;
import components.weapon.projectile.ProjectileTypes;
import components.weapon.projectile.emitter.IProjectileEmitter;
import media.SoundID;
import media.TextureID;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mock;

import static org.mockito.Mockito.mock;

class WeaponTest {

    @Mock
    private TestWeapon testWeapon;

    @Mock
    private Vector2 positionOffset, barrelOffset;

    @Mock
    FireControllerDetails fireControllerDetails;

    ProjectileTypes projectileTypes;

    @Mock
    private IProjectileEmitter projectileEmitter;




    /**

     Sets up the test environment before each test case.
     */

    @BeforeEach
    public void setup() {

        positionOffset = mock(Vector2.class);
        barrelOffset = mock(Vector2.class);
        int spawnAmmo = 10;
        int maxAmmo = 10;
        String name = "Test rifle";
        fireControllerDetails = mock(FireControllerDetails.class);
        TextureID textureID = TextureID.SHOT_GUN;
        float distToOwner = 2;
        SoundID soundID = SoundID.GUN_SHOT;
        projectileTypes = ProjectileTypes.BasicBullet;

        testWeapon = new TestWeapon(
                positionOffset,
                barrelOffset,
                spawnAmmo,
                maxAmmo,
                fireControllerDetails,
                name,
                projectileEmitter,
                textureID,
                distToOwner,
                soundID,
                projectileTypes
                );
    }





        /**
         The TestWeapon class is a test implementation of the Weapon class.
         */
    public static class TestWeapon extends Weapon {


        public TestWeapon(Vector2 positionOffset, Vector2 barrelOffset, int spawnAmmo, int maxAmmo, FireControllerDetails details, String name, IProjectileEmitter projectileEmitter, TextureID textureID, float distanceFromOwner, SoundID soundID, ProjectileTypes projectileType) {
            super(positionOffset, barrelOffset, spawnAmmo, maxAmmo, details, name, projectileEmitter, textureID, distanceFromOwner, soundID, projectileType);
        }




    }

}