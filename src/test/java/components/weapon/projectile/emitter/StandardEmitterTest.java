package components.weapon.projectile.emitter;

import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType;
import components.weapon.magazine.Magazine;
import components.weapon.projectile.ProjectileTypes;
import model.event.EventBus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class StandardEmitterTest {

    private StandardEmitter emitter;

    @Mock
    private Magazine magazine;

    @Mock
    private EventBus eventBus;

    private ProjectileTypes projectileTypes;

    @BeforeEach
    public void setup() {
        magazine = mock(Magazine.class);
        eventBus = mock(EventBus.class);
        CollisionType.Type collisionType = CollisionType.Type.ENEMY;
        projectileTypes = ProjectileTypes.BasicBullet;

        emitter = new StandardEmitter(collisionType, eventBus);
    }

    /**
     Tests if the minAmmoCost and maxAmmoCost methods return the expected ammo cost values.
     */
    @Test
    public void testCorrectAmmoCount() {
        assertEquals(1, emitter.minAmmoCost());
        assertEquals(1, emitter.maxAmmoCost());
    }

    /**
     Tests if the emitProjectiles method properly emits projectiles when the magazine has enough ammo.
     */
    @Test
    public void testEmitProjectilesWithAmmo() {
        when(magazine.currentAmmo()).thenReturn(10);
        when(magazine.retrieveAmmo(any())).thenReturn(10);
        emitter.emitProjectiles(magazine, new Vector2(), new Vector2(), projectileTypes,1);

        verify(eventBus, times(2)).post(any());
    }


    /**
     Tests if the emitProjectiles method does not emit projectiles when the magazine has no ammo.
     */
    @Test
    public void testEmitProjectilesWithoutAmmo() {
        when(magazine.retrieveAmmo(any())).thenReturn(0);
        emitter.emitProjectiles(magazine, new Vector2(), new Vector2(), projectileTypes,1);

        verify(eventBus, never()).post(any());
    }
}