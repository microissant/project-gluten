package components.weapon.projectile;

import com.badlogic.gdx.math.Vector2;
import components.collision.CollisionType;
import components.collision.ICollidable;
import components.health.Damage;
import components.health.Healer;
import model.event.EventBus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


class PoolableProjectileTest {


    private PoolableProjectile projectile;


    @Mock
    private EventBus eventBus;

    @Mock
    private Vector2 position, dir;

    @Mock
    private float speed;

    @Mock
    private CollisionType.Type collisionType;

    @Mock
    private ProjectileTypes projectileTypes;


    /**
     * Sets up the {@link PoolableProjectile} instance to be tested.
     */
    @BeforeEach
    public void setup() {
        eventBus = mock(EventBus.class);

        projectile = new PoolableProjectile(eventBus);

        position = mock(Vector2.class);
        dir = mock(Vector2.class);
        collisionType = CollisionType.Type.ENEMY;
        projectileTypes = ProjectileTypes.BasicBullet;
        speed = 10;

        projectile.init(position, dir, speed, collisionType, projectileTypes);
    }


    /**
     * Tests that objects are not null.
     */
    @Test
    public void testObjectsNotNull() {
        assertNotNull(projectile.getProperties());
        assertNotNull(projectile.getHealthComponent());
        assertNotNull(projectile.getCollisionPrimitive());
        assertNotNull(projectile.getPosition());
    }

    /**
     * Tests that a projectile kills itself after colliding with an object.
     */
    @Test
    public void testCollisionEnterKillsSelf() {
        ICollidable target = mock(ICollidable.class);
        assertFalse(projectile.isDead());
        projectile.collisionEnter(target, collisionType);
        assertTrue(projectile.isDead());
    }


    /**
     * Tests that the projectile unregisters itself from the event bus.
     */
    @Test
    public void testUnregisterFromBus() {
        verify(eventBus, never()).unregister(projectile);
        projectile.unregisterFromEventBus();
        verify(eventBus, times(1)).unregister(projectile);
    }


    /**
     * Tests that a projectile can be healed.
     */
    @Test
    public void testApplyHeal() {
        projectile.getHealthComponent().increaseMaximumHealth(10);

        assertEquals(1, projectile.getCurrentHealth());
        projectile.applyHeal(new Healer(5));
        assertEquals(6, projectile.getCurrentHealth());
    }

    /**
     * Tests that a projectile does not receive heal if the heal object passed is null.
     */
    @Test
    public void testApplyHealNull() {
        projectile.getHealthComponent().increaseMaximumHealth(10);
        int healthBefore = projectile.getCurrentHealth();

        assertDoesNotThrow(() -> projectile.applyHeal(null));
        assertEquals(healthBefore, projectile.getCurrentHealth());
    }

    /**
     * Tests that a projectile can receive damage.
     */
    @Test
    public void testApplyDamage() {
        projectile.getHealthComponent().increaseMaximumHealth(9);
        projectile.applyHeal(new Healer(10));

        assertEquals(10, projectile.getCurrentHealth());
        projectile.applyDamage(new Damage(2));
        assertEquals(8, projectile.getCurrentHealth());
        projectile.applyDamage(new Damage(1));
        assertEquals(7, projectile.getCurrentHealth());
        projectile.applyDamage(new Damage(6));
        assertEquals(1, projectile.getCurrentHealth());
    }

    /**
     Tests that applying a null damage to the projectile does not throw an exception and does not affect its current health.
     */
    @Test
    public void testApplyDamageNull() {
        projectile.getHealthComponent().increaseMaximumHealth(9);
        projectile.applyHeal(new Healer(10));

        int healthBefore = projectile.getCurrentHealth();

        assertDoesNotThrow(() -> projectile.applyDamage(null));
        assertEquals(healthBefore, projectile.getCurrentHealth());
    }

    /** checking that the damageable object of the projectile is itself.*/
    @Test
    public void testDamageableIsSelf() {
        assertEquals(projectile, projectile.getDamageable());
    }

    /**T ests that the PoolableProjectile is dead when its health reaches zero. */
    @Test
    public void testDeadWhenHealthIsZero() {
        int currentHealth = projectile.getCurrentHealth();
        projectile.applyDamage(new Damage(currentHealth));
        assertEquals(0, projectile.getCurrentHealth());
        assertTrue(projectile.isDead());
        assertFalse(projectile.isAlive());
    }

    /** Tests that the PoolableProjectile dies after colliding with the wall a certain number of times. */
    @Test
    public void testRemainingCollisions() {
        int collisonsStartCount = 3;

        projectile.setRemainingGridCollisions(collisonsStartCount);

        for (int i = 0; i < collisonsStartCount; i++) {
            assertFalse(projectile.isDead());
            projectile.hitWall();
        }
        assertTrue(projectile.isDead());
    }

    /** Tests that the PoolableProjectile dies after a certain amount of time has passed. */
    @Test
    public void testDiesAfterTime() {
        assertFalse(projectile.isDead());
        projectile.update(5);
        assertTrue(projectile.isDead());
    }
}