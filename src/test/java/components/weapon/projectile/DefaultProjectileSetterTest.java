package components.weapon.projectile;

import media.DrawProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.mockito.Mockito.*;

class DefaultProjectileSetterTest {

    private DefaultProjectileSetter setter;

    @Mock
    private PoolableProjectile projectile;

    /**
     * Sets up the test environment before each test method is run.
     * It creates a mock object of a PoolableProjectile and initializes the DefaultProjectileSetter object.
     */
    @BeforeEach
    public void setup() {
        setter = new DefaultProjectileSetter();
        projectile = mock(PoolableProjectile.class);

        when(projectile.getProperties()).thenReturn(new DrawProperties());
    }

    /**
     * Tests if the DefaultProjectileSetter sets the correct values to the properties of a projectile object.
     * It verifies that the setBoxCollision, setRuleset, setReaction, setRemainingGridCollisions, setDeathParticle,
     * setRotateTowardsDir, removeDamageRadius, setRotationSpeed, and getProperties methods of the projectile object
     * are called once.
     */
    @Test
    public void testSetCorrectValues() {

        setter.set(projectile, null);

        verify(projectile, times(1)).setBoxCollision(anyFloat(), anyFloat(), anyFloat(), anyFloat());
        verify(projectile, times(1)).setRuleset(any());
        verify(projectile, times(1)).setReaction(any());
        verify(projectile, times(1)).setRemainingGridCollisions(anyInt());
        verify(projectile, times(1)).setDeathParticle(any());
        verify(projectile, times(1)).setRotateTowardsDir(anyBoolean());
        verify(projectile, times(1)).removeDamageRadius();
        verify(projectile, times(1)).setRotationSpeed(anyFloat());
        verify(projectile, times(2)).getProperties();
    }
}