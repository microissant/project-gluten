package components.weapon;

import com.badlogic.gdx.math.Vector2;
import model.event.EventBus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class WeaponManagerTest {


    private WeaponManager weaponManager;

    @Mock
    private EventBus eventBus;
    @Mock
    Weapon weapon1, weapon2, weapon3;

    /**

     Initializes the object being tested and mock objects before each test.
     */
    @BeforeEach
    public void setup() {
        eventBus = mock(EventBus.class);
        weaponManager = new WeaponManager(new Vector2(), new Vector2(), eventBus);
        weapon1 = mock(Weapon.class);
        weapon2 = mock(Weapon.class);
        weapon3 = mock(Weapon.class);
    }


    /**
     Tests the addition of the first weapon to the WeaponManager.
     Expects the primary weapon to be set and the secondary weapon to be null.
     */
    @Test
    public void testAddFirstWeapon() {
        assertNull(weaponManager.getPrimary());
        assertNull(weaponManager.getSecondary());

        weaponManager.addWeapon(weapon1);

        assertEquals(weapon1, weaponManager.getPrimary());
        assertNull(weaponManager.getSecondary());
    }


    /**
     Tests the addition of multiple weapons to the WeaponManager.
     Expects the primary and secondary weapons to be set as per the order of addition.
     */
    @Test
    public void testAddMultipleWeaponsInSuccession() {
        assertNull(weaponManager.getPrimary());
        assertNull(weaponManager.getSecondary());

        weaponManager.addWeapon(weapon1);

        assertEquals(weapon1, weaponManager.getPrimary());
        assertNull(weaponManager.getSecondary());

        weaponManager.addWeapon(weapon2);

        assertEquals(weapon2, weaponManager.getPrimary());
        assertEquals(weapon1, weaponManager.getSecondary());

        weaponManager.addWeapon(weapon3);

        assertEquals(weapon3, weaponManager.getPrimary());
        assertEquals(weapon1, weaponManager.getSecondary());
    }


    /**
     Tests the swapping of weapons when no weapons are present.
     Expects the primary and secondary weapons to be null.
     */
    @Test
    public void testSwapWeaponsWhenNull() {
        assertNull(weaponManager.getPrimary());
        assertNull(weaponManager.getSecondary());

        weaponManager.swapWeapons();

        assertNull(weaponManager.getPrimary());
        assertNull(weaponManager.getSecondary());
    }


    /**
     * Test case that checks if weapons can be swapped when there is only one primary weapon.
     */
    @Test
    public void testSwapWithOnlyPrimaryWeapon() {
        weaponManager.addWeapon(weapon1);

        assertEquals(weapon1, weaponManager.getPrimary());
        assertNull(weaponManager.getSecondary());

        weaponManager.swapWeapons();

        assertNull(weaponManager.getSecondary());
        assertEquals(weapon1, weaponManager.getPrimary());
    }

    /**
     Tests the firing of the weapon when no weapon is present.
     Expects no exception to be thrown.
     */
    @Test
    public void testFireNullWeapon() {
        assertDoesNotThrow(() -> weaponManager.fireWeapon());
    }

    /**
     Tests the firing of the weapon when the primary weapon is present.
     Expects the primary weapon to be fired once.
     */
    @Test
    public void testFireWeapon() {

        weaponManager.addWeapon(weapon1);
        weaponManager.fireWeapon();

        verify(weapon1, times(1)).attack();
    }

    /**
     * Test case that checks if only the primary weapon can be fired when multiple weapons are present.
     */
    @Test
    public void testFireWeaponOnlyFiresPrimary() {
        weaponManager.addWeapon(weapon1);
        weaponManager.addWeapon(weapon2);

        weaponManager.fireWeapon();

        verify(weapon1, never()).attack();
        verify(weapon2, times(1)).attack();
    }

    /**
     Test case to verify if the correct count of shots fired by the primary weapon is returned.
     */
    @Test
    public void testShotsFiredPrimary() {
        int shotsFired = 23;
        when(weapon1.shotsFiredCount()).thenReturn(shotsFired);
        weaponManager.addWeapon(weapon1);

        assertEquals(shotsFired, weaponManager.shotsFiredPrimaryWeapon());
    }

    /**

     Test case to verify if zero is returned when only the secondary weapon has been fired.
     */
    @Test
    public void testShotsFiredWhenOnlySecondaryHasFired() {
        int shotsFired = 23;
        when(weapon1.shotsFiredCount()).thenReturn(shotsFired);
        weaponManager.addWeapon(weapon1);
        weaponManager.addWeapon(weapon2);

        assertEquals(0, weaponManager.shotsFiredPrimaryWeapon());
    }

    @Test
    public void testShotsFiredNoWeaponAdded() {
        assertEquals(0, weaponManager.shotsFiredPrimaryWeapon());
    }

    /**
     Test case to verify if zero is returned when no weapons have been added.
     */
    @Test
    public void testIsWeaponIdleNoWeaponAdded() {
        weaponManager.addWeapon(weapon1);
        when(weapon1.isIdle()).thenReturn(true);

        assertTrue(weaponManager.isWeaponIdle());

        when(weapon1.isIdle()).thenReturn(false);
        assertFalse(weaponManager.isWeaponIdle());
    }

    /**
     Test case to verify if the primary weapon is updated correctly.
     */
    @Test
    public void testUpdatesPrimaryWeapon() {
        weaponManager.addWeapon(weapon1);

        weaponManager.update(1);

        verify(weapon1, times(1)).update(1);
    }


    /**
     Test case to verify if only the primary weapon is updated when both primary and secondary weapons have been added.
     */
    @Test
    public void testUpdatesOnlyPrimaryWeapon() {
        weaponManager.addWeapon(weapon1);
        weaponManager.addWeapon(weapon2);

        weaponManager.update(1);

        verify(weapon2, times(1)).update(1);
        verify(weapon1, never()).update(1);
    }
}