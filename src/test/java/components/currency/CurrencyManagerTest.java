package components.currency;

import components.currency.coin.Coin;
import components.currency.coin.CoinFabricator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CurrencyManagerTest {

    CurrencyManager currencyManager;
    CoinFabricator coinFabricator;

    @BeforeEach
    public void setup() {
        currencyManager = new CurrencyManager();
        coinFabricator = new CoinFabricator();
    }

    @Test
    public void invalidWithdrawalAmount() {
        Coin expected = coinFabricator.generateCoin();

        for (int i = -100; i < 100; i++) {
            assertEquals(expected, currencyManager.withdrawal(i));
        }
    }

    @Test
    public void validWithdrawalAmount() {
        int expectedBalance = 10000;
        currencyManager.deposit(coinFabricator.generateCoin(expectedBalance));
        assertEquals(expectedBalance, currencyManager.getBalance().getValue());

        for (int i = 0; i < 100; i++) {
            assertEquals(coinFabricator.generateCoin(i), currencyManager.withdrawal(i));
            assertEquals(expectedBalance -= i, currencyManager.getBalance().getValue());
        }
    }

    @Test
    public void increaseGainsCorrectly() {
        int coinValue = 100;
        float percentageIncrease = 10;

        int expectedBalance = 0;
        for (int i = 1; i < 100; i++) {
            currencyManager.increaseGains(percentageIncrease);
            currencyManager.deposit(coinFabricator.generateCoin(coinValue));
            expectedBalance += coinValue * (1 + (percentageIncrease * i) / 100);
            assertEquals(100 + percentageIncrease * i, currencyManager.getGainBuff());
            assertEquals(expectedBalance, currencyManager.getBalance().getValue());
        }
    }

    @Test
    public void decreaseGainsCorrectly() {
        int coinValue = 100;
        float percentageDecrease = 10;

        int expectedBalance = 0;
        for (int i = 1; i < 10; i++) {
            currencyManager.decreaseGains(percentageDecrease);
            currencyManager.deposit(coinFabricator.generateCoin(coinValue));
            expectedBalance += coinValue * (1 - (percentageDecrease * i) / 100);
            assertEquals(100 - percentageDecrease * i, currencyManager.getGainBuff());
            assertEquals(expectedBalance, currencyManager.getBalance().getValue());
        }
    }

    @Test
    public void onlyAcceptCoinOnce() {
        Coin coin = coinFabricator.generateCoin(10);
        currencyManager.deposit(coin);
        int expectedBalance = currencyManager.getBalance().getValue();

        for (int i = 0; i < 100; i++) {
            currencyManager.deposit(coin);
            assertEquals(expectedBalance, currencyManager.getBalance().getValue());
        }
    }


    @Test
    public void decreaseIsLimited() {
        currencyManager.decreaseGains(50);
        assertEquals(50, currencyManager.getGainBuff());
        currencyManager.decreaseGains(25);
        assertEquals(25, currencyManager.getGainBuff());
        currencyManager.decreaseGains(100);
        assertEquals(0, currencyManager.getGainBuff());
    }

    @Test
    public void resetGainBuffCorrectly() {

        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < 100; j++) {
                currencyManager.increaseGains(i);
                currencyManager.decreaseGains(j);
                currencyManager.resetGainBuff();
                assertEquals(100, currencyManager.getGainBuff());
            }
        }

        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < 100; j++) {
                currencyManager.increaseGains(j);
                currencyManager.decreaseGains(i);
                currencyManager.resetGainBuff();
                assertEquals(100, currencyManager.getGainBuff());
            }
        }
    }
}
