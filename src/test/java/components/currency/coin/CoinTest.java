package components.currency.coin;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

public class CoinTest {

    Coin coin;

    @BeforeEach
    public void setup() {
        coin = new Coin();
    }


    @Test
    public void equalsCorrectly() {
        for (int i = -100; i < 100; i++) {
            for (int j = -100; j < 100; j++) {
                coin = new Coin(i);
                Coin coin1 = new Coin(j);

                if (coin.getValue() == coin1.getValue())
                    assertEquals(coin, coin1);
                else
                    assertNotEquals(coin, coin1);
            }
        }
    }

    @CsvSource(value = { "1", "2", "-1", "253", "3958432", "-53314", "532", "-8" })
    @ParameterizedTest(name = "Value of coin: {0}")
    public void constructorOnlyPositiveValues(int value) {
        coin = new Coin(value);

        int excepted = Math.max(value, 0);
        assertEquals(excepted, coin.getValue());
    }

    @Test
    public void depletedCoinCorrectness() {
        assertFalse(coin.isDepleted());
        coin.setDepleted();
        assertTrue(coin.isDepleted());
        coin.setDepleted();
        assertTrue(coin.isDepleted());
    }


}
