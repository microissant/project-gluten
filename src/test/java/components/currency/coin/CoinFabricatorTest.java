package components.currency.coin;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class CoinFabricatorTest {

    CoinFabricator fabricator;

    @BeforeEach
    public void setup() {
        fabricator = new CoinFabricator();
    }

    @Test
    public void generateCorrectCoins() {
        for (int i = -100; i < 100; i++) {
            assertEquals(Math.max(i, 0), fabricator.generateCoin(i).getValue());
        }
    }

    @Test
    public void subtractNullCoins() {
        Coin coinA = new Coin(5);
        Coin expectedNonNull = new Coin(5);
        Coin expectedNull = new Coin();

        assertEquals(expectedNull, fabricator.subtractCoins(null, null));
        assertEquals(expectedNull, fabricator.subtractCoins(null, null));
        assertEquals(expectedNonNull, fabricator.subtractCoins(coinA, null));
        assertEquals(expectedNonNull, fabricator.subtractCoins(coinA, null));
        assertEquals(expectedNonNull, fabricator.subtractCoins(null, coinA));
        assertEquals(expectedNonNull, fabricator.subtractCoins(null, coinA));
    }

    @CsvSource(value = { "8, 3, 5, 0", "100, 23, 77, 0", "5, 5, 0, 0", "0, 0, 0, 0", "-5, -5, 0, 0", })
    @ParameterizedTest(name = "subtraction a: {0} b: {1} c: {2} d: {3}")
    public void subtractCoinsCorrect(int a, int b, int c, int d) {
        Coin coinA = new Coin(a);
        Coin coinB = new Coin(b);
        Coin coinC = new Coin(c);
        Coin coinD = new Coin(d);

        assertEquals(coinC, fabricator.subtractCoins(coinA, coinB));
        assertEquals(coinD, fabricator.subtractCoins(coinB, coinA));
    }

    @Test
    public void addNegativeValueToCoinReturnOriginal() {
        Coin coin = new Coin(5);
        Coin expected = new Coin(5);

        for (int i = -100; i < 0; i++) {
            assertEquals(expected, fabricator.addValueToCoin(coin, i));
        }
    }

    @Test
    public void addValueToNullCoin() {
        Coin coin = null;

        for (int i = 0; i < 100; i++) {
            assertNull(fabricator.addValueToCoin(null, i));
        }
    }

    @Test
    public void addNullCoins() {
        Coin coinA = new Coin(5);
        Coin coinB = null;
        Coin coinC = null;
        Coin expectedNonNull = new Coin(5);
        Coin expectedNull = new Coin();

        assertEquals(expectedNull, fabricator.addCoins(null, null));
        assertEquals(expectedNull, fabricator.addCoins(null, null));
        assertEquals(expectedNonNull, fabricator.addCoins(coinA, null));
        assertEquals(expectedNonNull, fabricator.addCoins(coinA, null));
        assertEquals(expectedNonNull, fabricator.addCoins(null, coinA));
        assertEquals(expectedNonNull, fabricator.addCoins(null, coinA));
    }

    @CsvSource(value = { "8, 3, 11", "100, 23, 123", "5, 5, 10", "0, 0, 0", "-5, -5, 0", })
    @ParameterizedTest(name = "subtraction a: {0} b: {1} expected: {2}")
    public void addCoinsCorrectly(int a, int b, int expectedValue) {
        Coin coinA = new Coin(a);
        Coin coinB = new Coin(b);
        Coin expectedCoin = new Coin(expectedValue);

        assertEquals(expectedCoin, fabricator.addCoins(coinA, coinB));
        assertEquals(expectedCoin, fabricator.addCoins(coinB, coinA));
    }



}
