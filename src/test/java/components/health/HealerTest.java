package components.health;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HealerTest {

    /**
     * Tests that the Healer class can be created with positive values.
     */
    @Test
    public void createWithPositiveValue() {
        Healer healer = new Healer();
        assertEquals(1, healer.getHealValue());

        for (int i = 0; i < 1000; i++) {
            healer = new Healer(i);
            assertEquals(i, healer.getHealValue());
        }
    }

    /**
     * Tests that the Healer class cannot be created with negative values.
     */
    @Test
    public void createWithNegativeValues() {
        Healer healer;

        for (int i = 0; i > -1000; i--) {
            healer = new Healer(i);
            assertEquals(0, healer.getHealValue());
        }
    }

    /**
     * Tests that applying heal to a Healer object results in the correct value.
     */
    @Test
    public void applyingCorrect() {
        Healer healer = new Healer();
        Healer healer2 = new Healer(5);

        assertEquals(2, healer.applyHeal(1));
        assertEquals(15, healer2.applyHeal(10));
        assertEquals(-7, healer2.applyHeal(-12));
    }

}
