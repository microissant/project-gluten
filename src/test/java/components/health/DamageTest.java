package components.health;

//import components.health.Damage;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DamageTest {

    /**
     * Test the creation of a damage object with a positive value.
     */
    @Test
    public void createWithPositiveValue() {
        Damage dmg = new Damage();
        assertEquals(1, dmg.getDamageValue());

        for (int i = 0; i < 1000; i++) {
            dmg = new Damage(i);
            assertEquals(i, dmg.getDamageValue());
        }
    }

    /**
     * Test the creation of a damage object with a negative value.
     */
    @Test
    public void createWithNegativeValues() {
        Damage dmg;
        for (int i = 0; i > -1000; i--) {
            dmg = new Damage(i);
            assertEquals(0, dmg.getDamageValue());
        }
    }

    /**
     * Test applying damage to a damage object.
     */
    @Test
    public void applyingCorrect() {
        Damage dmg1 = new Damage();
        Damage dmg2 = new Damage(5);

        assertEquals(1, dmg1.applyDamage(2));
        assertEquals(45, dmg2.applyDamage(50));
        assertEquals(-13, dmg2.applyDamage(-8));
    }
}
