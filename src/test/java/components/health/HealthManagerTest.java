package components.health;

import components.health.Damage;
import components.health.Healer;
import components.health.HealthManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class HealthManagerTest {

    HealthManager hm;


    /**
     * Initialize the HealthManager object before each test case is run.
     */
    @BeforeEach
    public void setup() {
        hm = new HealthManager();
    }


    /**
     * Test if a character can be spawned with positive health.
     * The test creates HealthManager objects with health values ranging from 0 to 999,
     * and asserts that the current health of the character is equal to the health
     * value that was set.
     */
    @Test
    public void spawnWithPositiveHealth() {
        HealthManager hm = new HealthManager();
        assertEquals(1, hm.getCurrentHealth());

        for (int i = 0; i < 1000; i++) {
            hm = new HealthManager(i);
            assertEquals(i, hm.getCurrentHealth());
        }
    }

    /**
     * Test if a character can be spawned with negative health.
     *
     * The test creates HealthManager objects with health values ranging from 0 to -999,
     * and asserts that the current health of the character is always 0, regardless of the
     * health value that was set.
     */
    @Test
    public void spawnWithNegativeHealth() {
        HealthManager hm;
        for (int i = 0; i > -1000; i--) {
            hm = new HealthManager(i);
            assertEquals(0, hm.getCurrentHealth());
        }
    }

    /**
     * Test if a character can take damage until it dies.
     *
     * The test creates a HealthManager object with a starting health of 3, and applies
     * damage to the character until it dies. The test asserts that the character took
     * 3 points of damage before dying.
     */
    @Test
    public void simpleApplyDamageUntilDead() {
        int x = 3;
        HealthManager hm = new HealthManager(x);
        Damage dmg = new Damage();

        int count = 0;
        while (!hm.isDead()) {
            hm.applyDamage(dmg);
            count ++;
        }

        assertEquals(3, count);
    }


    /**
     * Test if a character can be spawned with the maximum possible health value.
     * The test creates HealthManager objects with health values ranging from -500 to 500,
     * and asserts that the maximum health value of the character is equal to the absolute
     * value of the health value that was set. If the health value is negative, the test
     * also asserts that the current health of the character is always 0.
     */
    @Test
    public void spawnMaximumHealthValue() {
        int expectedMax;
        int expectedCurrent;
        for (int i = -500; i <= 500; i++) {
            HealthManager hm = new HealthManager(i);

            if (i < 0) {
                expectedMax = 1;
                expectedCurrent = 0;
            }
            else {
                expectedMax = i;
                expectedCurrent = expectedMax;
            }

            assertEquals(expectedMax, hm.getMaximumHealth());
            assertEquals(expectedCurrent, hm.getCurrentHealth());
        }
    }

    @Test
    public void healthNeverBellowZero() {
        HealthManager hm = new HealthManager(10);
        Damage dmg = new Damage(100);

        assertFalse(hm.isDead());

        for (int i = 0; i < 100; i++) {
            hm.applyDamage(dmg);
            assertTrue(hm.isDead());
            assertEquals(0, hm.getCurrentHealth());
        }
    }

    @Test
    public void correctDamageApplied() {
        HealthManager hm = new HealthManager(50);
        Damage dmg = new Damage(15);

        assertEquals(15, hm.applyDamage(dmg));
        assertEquals(35, hm.getCurrentHealth());
        dmg = new Damage(30);
        assertEquals(30, hm.applyDamage(dmg));
        assertEquals(5, hm.getCurrentHealth());
        assertEquals(5, hm.applyDamage(dmg));
        assertEquals(0, hm.applyDamage(dmg));
    }

    @Test
    public void increaseMaximumHealth() {
        HealthManager hm = new HealthManager();
        hm.increaseMaximumHealth(1);
        assertEquals(2, hm.getMaximumHealth());
        assertEquals(1, hm.getCurrentHealth());

        hm.increaseMaximumHealth(3);
        assertEquals(5, hm.getMaximumHealth());
        assertEquals(1, hm.getCurrentHealth());

        for (int i = 0; i < 100; i++) {
            hm.increaseMaximumHealth(1);
        }
        assertEquals(105, hm.getMaximumHealth());
        assertEquals(1, hm.getCurrentHealth());
    }

    @Test
    public void decreaseMaximumHealthInLoop() {
        HealthManager hm = new HealthManager(10);

        int count = 0;
        while (hm.getMaximumHealth() > 1) {
            hm.decreaseMaximumHealth(1);
            count ++;
        }
        assertEquals(9, count);
    }

    @Test
    public void decreaseMaximumHealthUnderLimit() {
        HealthManager hm = new HealthManager(50);

        hm.decreaseMaximumHealth(10);
        assertEquals(40, hm.getCurrentHealth());
        assertEquals(40, hm.getMaximumHealth());

        for (int i = 0; i < 2; i++) {
            hm.decreaseMaximumHealth(40);
            assertEquals(1, hm.getCurrentHealth());
            assertEquals(1, hm.getMaximumHealth());
        }
    }

    @Test
    public void increaseHealthWithNegativeValues() {
        HealthManager hm = new HealthManager();

        for (int i = 0; i < 100; i++) {
            hm.increaseMaximumHealth(-i);
            assertEquals(1, hm.getMaximumHealth());
        }
    }

    @Test
    public void decreaseHealthWithNegativeValues() {
        HealthManager hm = new HealthManager(100);

        for (int i = 0; i < 100; i++) {
            hm.decreaseMaximumHealth(- i);
            assertEquals(100, hm.getMaximumHealth());
            assertEquals(100, hm.getCurrentHealth());
        }
    }

    @Test
    public void heal() {
        hm.increaseMaximumHealth(9);
        assertEquals(1, hm.getCurrentHealth());

        Healer healer = new Healer();
        hm.heal(healer);
        assertEquals(2, hm.getCurrentHealth());

        for (int i = 3; i < 11; i++) {
            hm.heal(healer);
            assertEquals(i, hm.getCurrentHealth());
        }

        assertEquals(hm.getMaximumHealth(), hm.getCurrentHealth());
    }

    @Test
    public void healAboveMaximum() {
        hm.increaseMaximumHealth(9);
        assertEquals(1, hm.getCurrentHealth());

        hm.heal(new Healer(5));
        assertEquals(6, hm.getCurrentHealth());

        hm.heal(new Healer(13));
        assertEquals(10, hm.getCurrentHealth());
        assertEquals(hm.getMaximumHealth(), hm.getCurrentHealth());
    }

    @Test
    public void healWhenDead() {
        hm = new HealthManager(0);
        assertTrue(hm.isDead());

        for (int i = 0; i < 100; i++) {
            hm.heal(new Healer(i));
            assertTrue(hm.isDead());
        }
    }

    @Test
    public void healWithNegativeValue() {
        hm.increaseMaximumHealth(9);
        assertEquals(1, hm.getCurrentHealth());

        for (int i = 0; i > -1000; i--) {
            hm.heal(new Healer(i));
            assertEquals(1, hm.getCurrentHealth());
        }
    }

    @Test
    public void resurrectWhenDead() {
        hm = new HealthManager(0);
        hm.increaseMaximumHealth(9);
        assertTrue(hm.isDead());

        hm.resurrect();
        assertFalse(hm.isDead());
        assertEquals(1, hm.getCurrentHealth());
    }

    @Test
    public void resurrectWhenAlive() {
        hm.increaseMaximumHealth(9);
        assertFalse(hm.isDead());

        hm.resurrect();
        assertFalse(hm.isDead());
        assertEquals(1, hm.getCurrentHealth());
    }

    @Test
    public void immuneToDamage() {
        hm = new HealthManager(10);
        Damage dmg = new Damage(5);

        assertEquals(10, hm.getCurrentHealth());
        hm.applyDamage(dmg);
        assertEquals(5, hm.getCurrentHealth());
        hm.enableImmunity();

        for (int i = 0; i < 100; i++) {
            dmg = new Damage(i);
            hm.applyDamage(dmg);
            assertEquals(5, hm.getCurrentHealth());
        }

        assertFalse(hm.isDead());
        hm.disableImmunity();
        dmg = new Damage(5);
        hm.applyDamage(dmg);
        assertTrue(hm.isDead());
    }
}
