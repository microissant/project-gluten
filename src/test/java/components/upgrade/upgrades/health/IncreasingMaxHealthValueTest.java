package components.upgrade.upgrades.health;

import components.health.HealthManager;
import components.upgrade.UpgradeManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class IncreasingMaxHealthValueTest {
    IncreaseMaxHealthValue increaseMaxHealthValue;
    HealthManager healthManager;
    UpgradeManager upgradeManager;
    final int baseHealth = 100;

    /**
     * Sets up the necessary objects and variables needed for the tests to run.
     */
    @BeforeEach
    public void setup() {
        healthManager = new HealthManager(baseHealth);
        upgradeManager = new UpgradeManager(null);
        upgradeManager.setUpgradableHealth(healthManager);
        increaseMaxHealthValue = new IncreaseMaxHealthValue(baseHealth);
    }

    /**
     * Tests that the title and description of the upgrade are not null or empty.
     */
    @Test
    public void titleAndDescriptionNotEmpty() {
        assertNotNull(increaseMaxHealthValue.getTitle());
        assertNotNull(increaseMaxHealthValue.getDescription());

        assertFalse(increaseMaxHealthValue.getTitle().isBlank());
        assertFalse(increaseMaxHealthValue.getDescription().isBlank());
    }

    /**
     * Tests that the apply method returns false if there is no upgradable container.
     */
    @Test
    public void applyWithNoUpgradableContainer() {
        assertFalse(increaseMaxHealthValue.apply(null));
    }

    /**
     * Tests that the apply method correctly increases the maximum health value of the {@link HealthManager}.
     */
    @Test
    public void applyCorrectMaxHealth() {
        int deltaValue = 5;
        increaseMaxHealthValue = new IncreaseMaxHealthValue(deltaValue);

        assertEquals(baseHealth, healthManager.getMaximumHealth());
        upgradeManager.addUpgrade(increaseMaxHealthValue);
        assertEquals(baseHealth + deltaValue, healthManager.getMaximumHealth());
    }

    /**
     * Tests that an upgrade can only be applied once to the {@link HealthManager}.
     */
    @Test
    public void applyUpgradeOnlyOnce() {
        int deltaValue = 5;
        increaseMaxHealthValue = new IncreaseMaxHealthValue(deltaValue);

        upgradeManager.addUpgrade(increaseMaxHealthValue);
        assertEquals(baseHealth + deltaValue, healthManager.getMaximumHealth());

        for (int i = 0; i < 100; i++) {
            upgradeManager.addUpgrade(increaseMaxHealthValue);
            assertEquals(baseHealth + deltaValue, healthManager.getMaximumHealth());
        }
    }

    /**
     * Tests that the revert method correctly restores the original maximum health value of the {@link HealthManager}.
     */
    @Test
    public void revertRestoresOriginalValue() {
        int deltaValue = 5;
        increaseMaxHealthValue = new IncreaseMaxHealthValue(deltaValue);

        upgradeManager.addUpgrade(increaseMaxHealthValue);
        assertEquals(baseHealth + deltaValue, healthManager.getMaximumHealth());
        increaseMaxHealthValue.revert();
        assertEquals(baseHealth, healthManager.getMaximumHealth());
    }

    /**
     * Tests that a revert can only be applied once to the {@link HealthManager}.
     */
    @Test
    public void revertOnlyOnce() {
        int deltaValue = 5;

        increaseMaxHealthValue = new IncreaseMaxHealthValue(deltaValue);

        upgradeManager.addUpgrade(increaseMaxHealthValue);
        assertEquals(baseHealth + deltaValue, healthManager.getMaximumHealth());

        for (int i = 0; i < 100; i++) {
            increaseMaxHealthValue.revert();
            assertEquals(baseHealth, healthManager.getMaximumHealth());
        }
    }


    @Test
    public void applyMultipleUpgrades() {
        List<Integer> data = new ArrayList<>(Arrays.asList
                (2, 0, 2, 5, 100, 1, 12)
        );
        float deltaSpeed = 0;

        assertEquals(baseHealth, healthManager.getMaximumHealth());
        for (Integer n: data) {
            for (int i = 0; i < 100; i++) {
                deltaSpeed += n;
                upgradeManager.addUpgrade(new IncreaseMaxHealthValue(n));
                assertEquals(baseHealth + deltaSpeed, healthManager.getMaximumHealth());
            }
        }
    }
}
