package components.upgrade.upgrades.health;

import components.health.HealthManager;
import components.upgrade.UpgradeManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class InvincibilityTest {


    Invincibility invincibility;
    HealthManager healthManager;
    UpgradeManager upgradeManager;


    /**
     * Initializes the necessary objects before each test.
     */
    @BeforeEach
    public void setup() {
        healthManager = new HealthManager();
        upgradeManager = new UpgradeManager(null);
        upgradeManager.setUpgradableHealth(healthManager);
        invincibility = new Invincibility();
    }


    /**
     * Tests that the title and description of the upgrade are not empty.
     */
    @Test
    public void titleAndDescriptionNotEmpty() {
        assertNotNull(invincibility.getTitle());
        assertNotNull(invincibility.getDescription());

        assertFalse(invincibility.getTitle().isBlank());
        assertFalse(invincibility.getDescription().isBlank());
    }

    /**
     * Tests that the upgrade cannot be applied if there is no upgradable container.
     */
    @Test
    void applyWithNoUpgradableContainer() {
        assertFalse(invincibility.apply(null));
    }

    /**
     * Tests that the upgrade makes the player immune to damage when applied.
     */
    @Test
    public void applyInvincibility() {
        assertFalse(healthManager.isImmuneToDamage());
        upgradeManager.addUpgrade(invincibility);
        assertTrue(healthManager.isImmuneToDamage());
    }

    /**
     * Tests that the upgrade can only be applied once.
     */
    @Test
    public void applyInvincibilityOnlyOnce() {
        assertFalse(healthManager.isImmuneToDamage());

        for (int i = 0; i < 100; i++) {
            upgradeManager.addUpgrade(invincibility);
            assertTrue(healthManager.isImmuneToDamage());
        }
    }

    /**
     * Tests that reverting the upgrade restores the original value.
     */
    @Test
    public void revertRestoresOriginalValue() {

        assertFalse(healthManager.isImmuneToDamage());
        upgradeManager.addUpgrade(invincibility);
        assertTrue(healthManager.isImmuneToDamage());

        for (int i = 0; i < 100; i++) {
            invincibility.revert();
            assertFalse(healthManager.isImmuneToDamage());
        }
    }

}
