package components.upgrade.upgrades.currency;

import components.currency.CurrencyManager;
import components.upgrade.UpgradeManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class IncreaseCurrencyGainTest {

    IncreaseCurrencyGain increaseCurrencyGain;
    CurrencyManager currencyManager;
    UpgradeManager upgradeManager;
    final float baseGainIncrease = 10.0f;
    float baseGainBuff;

    /**
     This method sets up the currency manager, upgrade manager, and the IncreaseCurrencyGain object before each test.
     */
    @BeforeEach
    public void setup() {
        currencyManager = new CurrencyManager();
        upgradeManager = new UpgradeManager(null);
        upgradeManager.setUpgradableCurrency(currencyManager);
        increaseCurrencyGain = new IncreaseCurrencyGain(baseGainIncrease);
        baseGainBuff = currencyManager.getGainBuff();
    }

    /**
     This test ensures that the title and description of the IncreaseCurrencyGain object are not empty.
     */
    @Test
    public void titleAndDescriptionNotEmpty() {
        assertNotNull(increaseCurrencyGain.getTitle());
        assertNotNull(increaseCurrencyGain.getDescription());

        assertFalse(increaseCurrencyGain.getTitle().isBlank());
        assertFalse(increaseCurrencyGain.getDescription().isBlank());
    }


    /**
     This test ensures that the apply method returns false when the upgradable container is null.
     */
    @Test
    public void applyWithNoUpgradableContainer() {
        assertFalse(increaseCurrencyGain.apply(null));
    }


    /**
     This test ensures that the apply method correctly applies the gain buff to the currency manager.
     */
    @Test
    public void applyCorrectGainBuff() {
        float deltaValue = 5.0f;
        increaseCurrencyGain = new IncreaseCurrencyGain(deltaValue);

        assertEquals(baseGainBuff, currencyManager.getGainBuff());
        upgradeManager.addUpgrade(increaseCurrencyGain);
        assertEquals(baseGainBuff + deltaValue, currencyManager.getGainBuff());
    }

    /**
     This test ensures that the apply method only applies the upgrade once.
     */
    @Test
    public void applyUpgradeOnlyOnce() {
        float deltaValue = 5.0f;
        increaseCurrencyGain = new IncreaseCurrencyGain(deltaValue);

        upgradeManager.addUpgrade(increaseCurrencyGain);
        assertEquals(baseGainBuff + deltaValue, currencyManager.getGainBuff());

        for (int i = 0; i < 100; i++) {
            upgradeManager.addUpgrade(increaseCurrencyGain);
            assertEquals(baseGainBuff + deltaValue, currencyManager.getGainBuff());
        }
    }

    /**
     This test ensures that the revert method restores the original value of the currency gain buff.
     */
    @Test
    public void revertRestoresOriginalValue() {
        int deltaValue = 5;
        increaseCurrencyGain = new IncreaseCurrencyGain(deltaValue);

        upgradeManager.addUpgrade(increaseCurrencyGain);
        assertEquals(baseGainBuff + deltaValue, currencyManager.getGainBuff());
        increaseCurrencyGain.revert();
        assertEquals(baseGainBuff, currencyManager.getGainBuff());
    }

    /**
     This test ensures that the revert method only reverts the upgrade once.
     */
    @Test
    public void revertOnlyOnce() {
        int deltaValue = 5;

        increaseCurrencyGain = new IncreaseCurrencyGain(deltaValue);

        upgradeManager.addUpgrade(increaseCurrencyGain);
        assertEquals(baseGainBuff + deltaValue, currencyManager.getGainBuff());

        for (int i = 0; i < 100; i++) {
            increaseCurrencyGain.revert();
            assertEquals(baseGainBuff, currencyManager.getGainBuff());
        }
    }

    @Test
    public void applyMultipleUpgrades() {
        List<Integer> data = new ArrayList<>(Arrays.asList
                (2, 0, 2, 5, 100, 1, 12)
        );
        float deltaSpeed = 0;

        assertEquals(baseGainBuff, currencyManager.getGainBuff());
        for (Integer n: data) {
            for (int i = 0; i < 100; i++) {
                deltaSpeed += n;
                upgradeManager.addUpgrade(new IncreaseCurrencyGain(n));
                assertEquals(baseGainBuff + deltaSpeed, currencyManager.getGainBuff());
            }
        }
    }
}
