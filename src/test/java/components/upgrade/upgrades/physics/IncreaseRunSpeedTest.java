package components.upgrade.upgrades.physics;

import components.physics.Physics;
import components.upgrade.UpgradeManager;
import model.event.EventBus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class IncreaseRunSpeedTest {

    IncreaseRunSpeed increaseRunSpeed;
    Physics physics;
    UpgradeManager upgradeManager;
    final float baseMaxSpeed = 10;
    final float deltaMaxSpeed = 5;

    /**
     * Set up the environment before each test.
     */
    @BeforeEach
    public void setup() {
        increaseRunSpeed = new IncreaseRunSpeed(deltaMaxSpeed);
        physics = new Physics();
        physics.setMaxSpeed(baseMaxSpeed);
        upgradeManager = new UpgradeManager(new EventBus());
        upgradeManager.setUpgradablePhysics(physics);
    }

    /**
     * Test the constructor with negative values.
     * Verifies that max speed isn't changed when adding the upgrade.
     */
    @Test
    public void negativeValueInConstructor() {
        increaseRunSpeed = new IncreaseRunSpeed(-1);
        assertEquals(baseMaxSpeed, physics.getMaxSpeed());
        upgradeManager.addUpgrade(increaseRunSpeed);
        assertEquals(baseMaxSpeed, physics.getMaxSpeed());
    }

    /**
     * Test that title and description are not null or empty strings.
     */
    @Test
    public void titleAndDescriptionNotEmpty() {
        assertNotNull(increaseRunSpeed.getTitle());
        assertNotNull(increaseRunSpeed.getDescription());

        assertFalse(increaseRunSpeed.getTitle().isBlank());
        assertFalse(increaseRunSpeed.getDescription().isBlank());
    }


    /**
     * Test the apply method when there's no upgradable container.
     * Verifies that the upgrade is not added.
     */
    @Test
    public void applyWithNoUpgradableContainer() {
        assertFalse(increaseRunSpeed.apply(null));
    }

    /**
     * Test that the upgrade correctly increases the maximum speed.
     */
    @Test
    public void applyCorrectMaxSpeed() {
        assertEquals(baseMaxSpeed, physics.getMaxSpeed());
        upgradeManager.addUpgrade(increaseRunSpeed);
        assertEquals(baseMaxSpeed + deltaMaxSpeed, physics.getMaxSpeed());
    }

    /**
     * Test that the upgrade can only be applied once.
     * Verifies that the maximum speed is increased only once.
     */
    @Test
    public void applyOnlyOnce() {
        assertEquals(baseMaxSpeed, physics.getMaxSpeed());

        for (int i = 0; i < 100; i++) {
            upgradeManager.addUpgrade(increaseRunSpeed);
            assertEquals(baseMaxSpeed + deltaMaxSpeed, physics.getMaxSpeed());
        }
    }

    /**
     * Test that the upgrade correctly reverts the maximum speed.
     * Verifies that the maximum speed is restored to its original value.
     */
    @Test
    public void revertRestoresOriginalSpeed() {
        assertEquals(baseMaxSpeed, physics.getMaxSpeed());
        upgradeManager.addUpgrade(increaseRunSpeed);
        assertEquals(baseMaxSpeed + deltaMaxSpeed, physics.getMaxSpeed());
        increaseRunSpeed.revert();
        assertEquals(baseMaxSpeed, physics.getMaxSpeed());
    }

    /**
     * Test that the upgrade can only be reverted once.
     * Verifies that the maximum speed is restored to its original value only once.
     */
    @Test
    public void revertOnlyOnce() {
        assertEquals(baseMaxSpeed, physics.getMaxSpeed());
        upgradeManager.addUpgrade(increaseRunSpeed);
        assertEquals(baseMaxSpeed + deltaMaxSpeed, physics.getMaxSpeed());
        for (int i = 0; i < 100; i++) {
            increaseRunSpeed.revert();
            assertEquals(baseMaxSpeed, physics.getMaxSpeed());
        }
    }

    @Test
    public void applyMultipleUpgrades() {
        List<Float> data = new ArrayList<>(Arrays.asList
                (2.0f,0.0f, 1.5f, 5.0f, 100.0f,0.5f, 12.5f)
        );
        float deltaSpeed = 0;

        assertEquals(baseMaxSpeed, physics.getMaxSpeed());
        for (Float n: data) {
            for (int i = 0; i < 100; i++) {
                deltaSpeed += n;
                upgradeManager.addUpgrade(new IncreaseRunSpeed(n));
                assertEquals(baseMaxSpeed + deltaSpeed, physics.getMaxSpeed());
            }
        }
    }



}
