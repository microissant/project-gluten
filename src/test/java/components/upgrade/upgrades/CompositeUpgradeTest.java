package components.upgrade.upgrades;

import components.health.HealthManager;
import components.physics.Physics;
import components.upgrade.UpgradeManager;
import components.upgrade.upgrades.health.IncreaseMaxHealthValue;
import components.upgrade.upgrades.health.Invincibility;
import components.upgrade.upgrades.physics.IncreaseRunSpeed;
import model.event.EventBus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class CompositeUpgradeTest {

    IUpgrade compositeUpgrade;

    Physics physics;
    HealthManager healthManager;
    UpgradeManager upgradeManager;

    final float baseMaxSpeed = 100;
    final int baseMaxHealth = 100;

    final float deltaMaxSpeed = 5;
    final int deltaMaxHealth = 2;


    /**
     * Sets up the test environment before each test method is run.
     */
    @BeforeEach
    public void setup() {
        physics = new Physics();
        physics.setMaxSpeed(baseMaxSpeed);
        healthManager = new HealthManager(baseMaxHealth);
        upgradeManager = new UpgradeManager(new EventBus());
        upgradeManager.setUpgradableHealth(healthManager);
        upgradeManager.setUpgradablePhysics(physics);
    }


    /**
     * Tests that a CompositeUpgrade instance with null arguments passed to the constructor
     * has non-blank title and description fields.
     */
    @Test
    public void nullConstructorArgument() {
        compositeUpgrade = new CompositeUpgrade(null, null, null);

        assertNotNull(compositeUpgrade.getTitle());
        assertNotNull(compositeUpgrade.getDescription());

        assertFalse(compositeUpgrade.getTitle().isBlank());
        assertFalse(compositeUpgrade.getDescription().isBlank());
    }

    /**
     * Tests that a CompositeUpgrade instance has its title and description fields set
     * correctly by the constructor.
     */
    @CsvSource(value = {
            "\"a valid title\", \"a valid description\"",
            "\"Second Title\",\"Second Description\" ",
            "\"Title\",\"Description\"",
            "\"Increase damage\",\"Weapon does 5% more damage\""

            })
    @ParameterizedTest(name = "title: {0} desc: {1}")
    public void correctTitleAndDescription(String title, String description) {
        compositeUpgrade = new CompositeUpgrade(null, title, description);
        assertEquals(title, compositeUpgrade.getTitle());
        assertEquals(description, compositeUpgrade.getDescription());
    }

    /**
     * Tests that a CompositeUpgrade instance containing two upgrades applied to it applies
     * them correctly, and that the revert method undoes the changes.
     */
    @Test
    public void applyAndRevertTwoUpgradesCorrectly() {
        List<IUpgrade> upgrades = new ArrayList<>(Arrays.asList(
                new IncreaseMaxHealthValue(deltaMaxHealth),
                new IncreaseRunSpeed(deltaMaxSpeed)
        ));

        assertEquals(baseMaxHealth, healthManager.getMaximumHealth());
        assertEquals(baseMaxSpeed, physics.getMaxSpeed());

        compositeUpgrade = new CompositeUpgrade(upgrades, null, null);
        upgradeManager.addUpgrade(compositeUpgrade);

        assertEquals(baseMaxHealth + deltaMaxHealth, healthManager.getMaximumHealth());
        assertEquals(baseMaxSpeed + deltaMaxSpeed, physics.getMaxSpeed());

        compositeUpgrade.revert();
        assertEquals(baseMaxHealth, healthManager.getMaximumHealth());
        assertEquals(baseMaxSpeed, physics.getMaxSpeed());
    }


    /**
     * Tests that a CompositeUpgrade instance containing two upgrades applied to it applies
     * them correctly, and that the revert method undoes the changes.
     */
    @Test
    public void applyAndRevertThreeUpgradesCorrectly() {
        List<IUpgrade> upgrades = new ArrayList<>(Arrays.asList(
                new IncreaseMaxHealthValue(deltaMaxHealth),
                new Invincibility(),
                new IncreaseRunSpeed(deltaMaxSpeed)
        ));
        compositeUpgrade = new CompositeUpgrade(upgrades, null, null);

        assertEquals(baseMaxHealth, healthManager.getMaximumHealth());
        assertFalse(healthManager.isImmuneToDamage());
        assertEquals(baseMaxSpeed, physics.getMaxSpeed());

        upgradeManager.addUpgrade(compositeUpgrade);

        assertEquals(baseMaxHealth + deltaMaxHealth, healthManager.getMaximumHealth());
        assertTrue(healthManager.isImmuneToDamage());
        assertEquals(baseMaxSpeed + deltaMaxSpeed, physics.getMaxSpeed());

        compositeUpgrade.revert();
        assertEquals(baseMaxHealth, healthManager.getMaximumHealth());
        assertFalse(healthManager.isImmuneToDamage());
        assertEquals(baseMaxSpeed, physics.getMaxSpeed());
    }




    /**
     Tests whether the {@link CompositeUpgrade} class reverts the applied upgrades correctly.
     Specifically, this test checks whether calling the {@link CompositeUpgrade#revert()} method once
     reverts all the applied upgrades only once.
     */
    @Test
    public void revertOnlyOnce() {
        List<IUpgrade> upgrades = new ArrayList<>(Arrays.asList(
                new IncreaseMaxHealthValue(deltaMaxHealth),
                new Invincibility(),
                new IncreaseRunSpeed(deltaMaxSpeed)
        ));
        compositeUpgrade = new CompositeUpgrade(upgrades, null, null);
        upgradeManager.addUpgrade(compositeUpgrade);

        assertEquals(baseMaxHealth + deltaMaxHealth, healthManager.getMaximumHealth());
        assertTrue(healthManager.isImmuneToDamage());
        assertEquals(baseMaxSpeed + deltaMaxSpeed, physics.getMaxSpeed());

        for (int i = 0; i < 100; i++) {
            compositeUpgrade.revert();
            assertEquals(baseMaxHealth, healthManager.getMaximumHealth());
            assertFalse(healthManager.isImmuneToDamage());
            assertEquals(baseMaxSpeed, physics.getMaxSpeed());
        }
    }

    /**
     Tests whether the {@link CompositeUpgrade} class applies upgrades correctly.
     Specifically, this test checks whether the {@link UpgradeManager#addUpgrade(IUpgrade)} method
     correctly adds an upgrade, and whether the {@link CompositeUpgrade#isApplied()} method returns
     the correct result.
     */
    @Test
    public void checkIsAppliedCorrectly() {
        List<IUpgrade> upgrades = new ArrayList<>();
        IUpgrade appliedUpgrade = new IncreaseMaxHealthValue(5);
        for (int i = 0; i < 100; i++) {
            upgrades.add(new IncreaseMaxHealthValue(i));
        }
        upgrades.add(appliedUpgrade);

        assertTrue(upgradeManager.addUpgrade(appliedUpgrade));
        assertFalse(upgradeManager.addUpgrade(new CompositeUpgrade(upgrades, null, null)));
        assertTrue(new CompositeUpgrade(upgrades, null, null).isApplied());
    }
}
