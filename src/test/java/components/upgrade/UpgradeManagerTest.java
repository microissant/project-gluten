package components.upgrade;

import components.health.HealthManager;
import components.physics.Physics;
import components.upgrade.upgrades.IUpgrade;
import components.upgrade.upgrades.health.IncreaseMaxHealthValue;
import model.event.EventBus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UpgradeManagerTest {


    HealthManager healthManager;
    Physics physics;
    UpgradeManager upgradeManager;

    /**
     * Set up the test environment by initializing the variables and creating an UpgradeManager object.
     */
    @BeforeEach
    public void setup() {
        healthManager = new HealthManager();
        physics = new Physics();
        upgradeManager = new UpgradeManager(new EventBus());
        upgradeManager.setUpgradableHealth(healthManager);
        upgradeManager.setUpgradablePhysics(physics);
    }

    /**
     * Test that adding an upgrade applies only non-active permanent upgrades.
     */
    @Test
    public void addUpgradeAppliesOnlyNonActivePermUpgrades() {
        IUpgrade maxHealthUpgrade = new IncreaseMaxHealthValue(5);

        assertEquals(0, upgradeManager.permanentUpgradesCount());
        assertTrue(upgradeManager.addUpgrade(maxHealthUpgrade));
        assertFalse(upgradeManager.addUpgrade(maxHealthUpgrade));
        assertTrue(upgradeManager.addUpgrade(new IncreaseMaxHealthValue(5)));
        assertEquals(2, upgradeManager.permanentUpgradesCount());
    }

    /**
     * Test that removing an upgrade correctly updates the upgrade count.
     */
    @Test
    public void removeUpgradeCorrectly() {
        assertEquals(0, upgradeManager.permanentUpgradesCount());
        upgradeManager.removeUpgrade(null);
        assertEquals(0, upgradeManager.permanentUpgradesCount());

        IUpgrade upgrade = new IncreaseMaxHealthValue(5);
        upgradeManager.addUpgrade(upgrade);
        assertEquals(1, upgradeManager.permanentUpgradesCount());
        upgradeManager.removeUpgrade(upgrade);
        assertEquals(0, upgradeManager.permanentUpgradesCount());
    }

    /**
     * Test that removing an upgrade and adding it again works correctly.
     */
    @Test
    public void removeAndAddAgain() {
        IUpgrade upgrade = new IncreaseMaxHealthValue(5);
        assertTrue(upgradeManager.addUpgrade(upgrade));

        assertFalse(upgradeManager.addUpgrade(upgrade));
        upgradeManager.removeUpgrade(upgrade);
        assertTrue(upgradeManager.addUpgrade(upgrade));
    }
}
