package components.physics;

import com.badlogic.gdx.math.Vector2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class PhysicsTest {

    Physics p;
    final double dtAt60FPS = 0.0166666666667;

    @BeforeEach
    public void setup() {
        p = new Physics();
        p.setMaxSpeed(100);
        p.setAccelerationForce(100);
        p.setFriction(100);
    }

    @Test
    public void createPhysicsPositionTestNoBreaking() {
        Physics p1 = new Physics(new Vector2(2.5f,6.3f));

        assertEquals(new Vector2(0,0), p.getPosition());
        assertEquals(new Vector2(2.5f,6.3f), p1.getPosition());
    }

    @CsvSource(value = { "1.0,1.0,", "2.5, -1.3", "0.34, 0.0", "-0.253, -23.53" })
    @ParameterizedTest(name = "Velocity x: {0} y: {1}")
    public void velocityUpdateTestNoBreaking(float x, float y) {
        p.disableFriction();
        p.setVelocity(new Vector2(x, y));
        updatePhysics(new Vector2(x, y));
    }

    @Test
    public void movementDisabledTestNoBreaking() {
        p.setVelocity(new Vector2(2.1f,23.5f));
        p.stopMovement();
        for (int i = 0; i < 5; i++) {
            p.update(1.0);
            assertEquals(new Vector2(0.0f,0.0f), p.getPosition());
        }
    }

    @Test
    public void flipMovementEnabledNoBreaking() {
        p.disableFriction();

        // Move once
        p.setVelocity(new Vector2(2.1f,23.5f));
        p.update(1.0);
        assertEquals(new Vector2(2.1f,23.5f), p.getPosition());

        // Disable and check that it does not update
        p.stopMovement();
        for (int i = 0; i < 5; i++) {
            p.update(1.0);
            assertEquals(new Vector2(2.1f,23.5f), p.getPosition());
        }

        // Start moving again and check that it moves
        p.setVelocity(new Vector2(1,1));
        updatePhysics(new Vector2(1, 1));
    }

    private void updatePhysics(Vector2 velocity) {
        double dt = 0.5;
        double startX = p.getPosition().x;
        double startY = p.getPosition().y;

        for (int i = 1; i < 11; i++) {
            p.update(dt);

            double targetX = startX + velocity.x * dt * i;
            double targetY = startY + velocity.y * dt * i;
            assertEquals(targetX, p.getPosition().x, 0.00001);
            assertEquals(targetY, p.getPosition().y, 0.00001);
        }
    }

    // TODO: remove or fix test

    private double calculateDeltaPos(float velocity, float acceleration, float dt) {
        return velocity * dt + 0.5 * acceleration * dt * dt;
    }


    @Test
    public void accelerateToMaxSpeed() {
        p.setMaxSpeed(100);
        p.setAccelerationForce(100);
        p.disableInstantMovement();

        for (int i = 0; i < 10000; i++) {
            p.accelerate(new Vector2(1, 0));
            p.update(dtAt60FPS);
        }

        assertEquals(100, p.getVelocity().x, 5);
        assertEquals(0, p.getVelocity().y, 5);
    }

    @Test
    public void setPositionTest() {
        p.setPosition(new Vector2(10, 10));
        assertEquals(new Vector2(10, 10), p.getPosition());
        assertEquals(new Vector2(0, 0), p.getVelocity());


        p.setPosition(new Vector2(100, -100));
        assertEquals(new Vector2(100, -100), p.getPosition());
        assertEquals(new Vector2(0, 0), p.getVelocity());
    }

    @Test
    public void breakingFromVelocity() {
        p.setFriction(1000);
        p.setMaxSpeed(100);
        p.enableScaledFriction();
        p.enableFriction();
        Vector2 velocity = new Vector2(100, 0);
        p.setVelocity(velocity);

        for (int i = 0; i < 60; i++) {
            p.update(dtAt60FPS);
        }

        assertEquals(new Vector2(0, 0), p.getVelocity());
        assertNotEquals(new Vector2(0, 0), p.getPosition());
    }

    @Test
    public void instantAcceleration() {
        p.enableInstantMovement();
        p.setMaxSpeed(50);

        p.accelerate(new Vector2(1,0));
        assertEquals(new Vector2(50, 0), p.getVelocity());
        p.update(0.000000000001);
        p.update(0.000000000001);
        assertEquals(new Vector2(0, 0), p.getVelocity());
    }

    @Test
    public void rotateVelocity() {
        p.setLinearMovement(new Vector2(1, 1), 5);
        p.rotateVelocity((float) Math.PI / 2);

        Vector2 expected = new Vector2(-1, 1).nor().scl(5);

        assertEquals(expected.x, p.getVelocity().x, 0.000001);
        assertEquals(expected.y, p.getVelocity().y, 0.000001);
    }

}
