package components.physics.reaction;

import com.badlogic.gdx.math.Vector2;
import components.physics.Physics;
import grid.WorldGrid;
import grid.collision.Collider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.mockito.invocation.InvocationOnMock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * Test class for {@link BounceReaction}.
 */
public class BounceReactionTest {
    private BounceReaction bounceReaction;
    private Physics physics;
    private WorldGrid grid;
    private Collider collider;

    @BeforeEach
    public void setUp() {
        bounceReaction = new BounceReaction();
        physics = Mockito.mock(Physics.class);
        grid = Mockito.mock(WorldGrid.class);
        collider = Mockito.mock(Collider.class);

        when(physics.getCollider()).thenReturn(collider);
    }

    /**
     * Tests that noReaction does not change the position of the physics object.
     */
    @Test
    public void testNoReaction() {
        Vector2 velocityDelta = new Vector2(3, 4);
        Vector2 initialPosition = new Vector2(1, 1);
        Vector2 expectedPosition = initialPosition.cpy().add(velocityDelta);

        when(physics.getPosition()).thenReturn(initialPosition);

        bounceReaction.noReaction(physics, velocityDelta);

        assertEquals(expectedPosition, initialPosition);
        verify(physics).getPosition();
        verifyNoMoreInteractions(physics);
    }

    /**
     * Tests that implementation returns false if no collision occurs.
     */
    @Test
    public void testImplementation_noCollision() {
        Vector2 velocityDelta = new Vector2(3, 4);
        Vector2 initialPosition = new Vector2(1, 1);
        Vector2 initialVelocity = new Vector2(3, 4);

        when(physics.getPosition()).thenReturn(initialPosition);
        when(physics.getVelocity()).thenReturn(initialVelocity);
        when(collider.collide(physics, velocityDelta, grid, 0, new Vector2())).thenReturn(false);

        boolean result = bounceReaction.implementation(physics, velocityDelta, grid, 0);

        assertFalse(result);
    }

    /**
     * Tests that implementation returns true if a collision occurs, and that the
     * position is updated.
     */
    @Test
    public void testImplementation_collision() {
        Vector2 velocityDelta = new Vector2(3, 4);
        Vector2 position = new Vector2(10, 10);
        Vector2 newPosition = new Vector2(13, 14);

        when(physics.getPosition()).thenReturn(position);
        when(collider.collide(eq(physics), eq(velocityDelta), eq(grid), eq(0), any(Vector2.class)))
                .thenAnswer(new Answer<Boolean>() {
                    @Override
                    public Boolean answer(InvocationOnMock invocation) {
                        Vector2 result = invocation.getArgument(4);
                        result.set(newPosition);
                        return true;
                    }
                });

        boolean hitWall = bounceReaction.implementation(physics, velocityDelta, grid, 0);

        assertTrue(hitWall);
        assertEquals(newPosition, position);
    }

    /**
     * Tests that implementation returns true if a collision occurs, and that the
     * position and velocity are updated.
     */
    @Test
    public void testImplementation_bounce() {
        Vector2 velocityDelta = new Vector2(3, 4);
        Vector2 initialPosition = new Vector2(10, 10);
        Vector2 initialVelocity = new Vector2(3, 4);
        Vector2 bouncedPosition = new Vector2(12, 13);
        Vector2 bouncedVelocity = new Vector2(-3, -4);

        when(physics.getPosition()).thenReturn(initialPosition);
        when(physics.getVelocity()).thenReturn(initialVelocity);

        when(collider.collide(eq(physics), eq(velocityDelta), eq(grid), eq(0), any(Vector2.class)))
                .thenAnswer(new Answer<Boolean>() {
                    @Override
                    public Boolean answer(InvocationOnMock invocation) {
                        Vector2 result = invocation.getArgument(4);
                        result.set(bouncedPosition);
                        return true;
                    }
                });

        boolean hitWall = bounceReaction.implementation(physics, velocityDelta, grid, 0);

        assertTrue(hitWall);
        assertEquals(bouncedPosition, initialPosition);
        assertEquals(bouncedVelocity, initialVelocity);
    }
}
