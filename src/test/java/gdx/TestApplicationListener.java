package gdx;

import com.badlogic.gdx.ApplicationListener;


 /*
  TestApplicationListener used for testing purposes.
 */
public class TestApplicationListener implements ApplicationListener {
    @Override
    public void create() {
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void render() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
    }
}
