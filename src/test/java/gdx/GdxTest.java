package gdx;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;
import com.badlogic.gdx.backends.headless.HeadlessNativesLoader;
import com.badlogic.gdx.graphics.GL20;
import media.MediaManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import static org.mockito.Mockito.mock;



/**
 The GdxTest class is an abstract class that provides setup and teardown methods for running unit tests that use the
 libGDX framework. Any test class that extends this class can use the setup and teardown methods to initialize and
 dispose of resources as necessary.
 <p>
 This class uses a headless version of the libGDX application to run tests, which allows the tests to run without the
 need for a graphical window or user input. It also sets up mock implementations of the GL20 and Graphics interfaces
 provided by libGDX to simulate rendering and graphics functions in a headless environment.
 */
public abstract class GdxTest {

    private HeadlessApplication headlessApplication;

    @BeforeEach
    public void setUp() {
        HeadlessApplicationConfiguration config = new HeadlessApplicationConfiguration();
        headlessApplication = new HeadlessApplication(new TestApplicationListener(), config);

        // Load headless natives
        HeadlessNativesLoader.load();

        // Set the GL20 instance to the MockGL20 implementation
        Gdx.gl = Gdx.gl20 = mock(GL20.class);
        Gdx.graphics = new MockGraphics();

        MediaManager.initResources();
    }

    @AfterEach
    public void tearDown() {
        headlessApplication.exit();
    }
}
