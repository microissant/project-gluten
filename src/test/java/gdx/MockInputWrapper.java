package gdx;

import com.badlogic.gdx.Gdx;


/*The MockInputWrapper class provides a wrapper around the input class in the libGDX library to mock user input for testing purposes.
 */
public class MockInputWrapper {
    public boolean isButtonPressed(int button) {
        return Gdx.input.isButtonPressed(button);
    }


}