package grid;

import com.badlogic.gdx.math.Vector2;
import grid.TileData.TileRuleset;
import grid.TileData.TileType;
import grid.factory.RoomFactory;
import grid.factory.RoomLayout;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class WorldGridTest {

    /**
     Tests the overwrite functionality of the WorldGrid class.
     Creates a new WorldGrid and pastes a RoomLayout into it.
     Verifies that the tiles within the RoomLayout have overwritten the corresponding tiles in the WorldGrid.
     */
    @Test
    void overwriteTest(){
        WorldGrid world = new WorldGrid(10, 10, 16, null);
        RoomFactory factory  = new RoomFactory();
        String s= "h wh, ";

        RoomLayout layout = factory.createLayout(s);
        layout.paste(world,new GridCoordinate(2, 1),null);
     
        TileData emptyType = new TileData(TileType.EMPTY);
        TileData wallType = new TileData(TileType.DESTRUCTIBLEWALL);
        TileData holeType = new TileData(TileType.HOLE);

  
        for(CoordinateItem<TileData> cItem : world){
            if(cItem.coordinate().row == 2 && cItem.coordinate().col>=1 && cItem.coordinate().col<=4){
                if(cItem.coordinate().col == 1)assertEquals(cItem.item(), holeType);
                else if(cItem.coordinate().col == 2)assertEquals(cItem.item(), emptyType);
                else if(cItem.coordinate().col == 3)assertEquals(cItem.item(), wallType);
                else assertEquals(cItem.item(), holeType);
                
            } else {
                //make sure that no other tiles have been affected
                assertNull(cItem.item());
            }
        }

    }

    /**
     Tests the getAtPixel functionality of the WorldGrid class.
     Creates a new WorldGrid and sets a tile at a specific pixel location.
     Verifies that the getAtPixel method returns the expected tile.
     */
    @Test 
    void pixelTest(){
        WorldGrid world = new WorldGrid(10, 10, 16, null);
        //1g + 8, 3g + 1
        assertNull(world.getAtPixel(24, 49));

        world.set(3,1,new TileData(TileType.DESTRUCTIBLEWALL));
        assertEquals(new TileData(TileType.DESTRUCTIBLEWALL),world.getAtPixel(24, 49));
    }

    /**
     Tests the checkForTilePropertyAtPixel functionality of the WorldGrid class.
     Creates a new WorldGrid and sets tiles at specific pixel locations with certain tile properties.
     Verifies that the checkForTilePropertyAtPixel method returns the expected results for those pixel locations and tile properties.
     */
    @Test 
    void propertyTest(){
        WorldGrid world = new WorldGrid(10, 10, 16, new TileData(TileType.DESTRUCTIBLEWALL));

        TileRuleset ruleset = TileRuleset.WALKING;
        assertTrue(world.checkForTilePropertyAtPixel(24, 49,ruleset));
        assertTrue(world.checkForTilePropertyAtPixel(24, 49,ruleset.val));

        world.set(3,1,new TileData(TileType.HOLE));
        assertTrue(world.checkForTilePropertyAtPixel(24, 49,ruleset));
        world.set(3,1,new TileData(TileType.EMPTY));
        assertFalse(world.checkForTilePropertyAtPixel(24, 49,ruleset));
        world.set(3,1,new TileData(TileType.HOLE));
        assertTrue(world.checkForTilePropertyAtPixel(24, 49,ruleset.val));
        world.set(3,1,new TileData(TileType.EMPTY));
        assertFalse(world.checkForTilePropertyAtPixel(24, 49,ruleset.val)); 
    }



    @Test 
    void explosionTest(){
        WorldGrid world = new WorldGrid(5, 5, 1, null);
        WorldGrid expectedWorld = new WorldGrid(5, 5, 1, null);
        String worldString = " www ,"+
                             "wwwIw,"+
                             "wwwww,"+
                             " Iww ,"+
                             "  w I,EvilCookie";
        String expectedString = " www ,"+
                                "ww Iw,"+
                                "w   w,"+
                                " I w ,"+
                                "  w I,EvilCookie";
        RoomFactory factory = new RoomFactory();
        RoomLayout worldLayout = factory.createLayout(worldString);
        RoomLayout expectedLayout = factory.createLayout(expectedString);
        worldLayout.paste(world, new GridCoordinate(0, 0), null);
        expectedLayout.paste(expectedWorld, new GridCoordinate(0,0), null);

        world.explodeTiles(new Vector2(2,2), 1);
        for(CoordinateItem<TileData> cItem : world){
            assertEquals(expectedWorld.get(cItem.coordinate()),cItem.item());

        }

    
    }

}
