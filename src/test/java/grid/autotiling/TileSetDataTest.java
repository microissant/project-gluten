package grid.autotiling;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import grid.TileData.TileType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;



/**

 A class for testing the TileSetData class.
 The testRead method tests if the object correctly parses a JSON string and checks that the TileSetData object
 correctly stores and retrieves the parsed data.
 The testWrite method tests if the input JSON string is equal to the output JSON string.
 Note: This test class assumes that the Json and JsonReader classes have already been tested and are working as expected.
 */


public class TileSetDataTest {

    /**
     Tests if the object correctly parses a JSON string and checks that the TileSetData object
     correctly stores and retrieves the parsed data.
     */
    @Test
    void testRead() {
        //check if the object correctly parses the json
        String jsonString ="{tileTypeData:{HOLE:[{complexity:FOURBIT,layer:1,offset:57},{complexity:ZEROBIT,layer:0,offset:0}]},alternatives:{4:[4,3,7]}}";
        Json json = new Json();
        JsonReader reader = new JsonReader();
        JsonValue jValue = reader.parse(jsonString);
        TileSetData tileSetData = new TileSetData();
        tileSetData.read(json,jValue);
        int[] alternatives = tileSetData.getAlternatives(4);

        //don't add anything
        assertNull(tileSetData.getAlternatives(3));
        
        int[] expectedAlternatives = new int[] {4,3,7};
        for(int i = 0; i < alternatives.length; i++ ){
            assertEquals(expectedAlternatives[i],alternatives[i]);
        }

        //make sure the values exist and have the correct amount of elements
        assertNotEquals(null, tileSetData.getTileInstructions(TileType.HOLE));
        assertEquals(2,tileSetData.getTileInstructions(TileType.HOLE).size());
        

    }



    /*Tests if the input JSON string is equal to the output JSON string.*/
    @Test
    void testWrite() {
        //check if the input json is equal to the output json
        Json json = new Json();
        String expected = "{tileTypeData:{HOLE:[{complexity:FOURBIT,layer:1,offset:57},{complexity:ZEROBIT,layer:0,offset:0}]},alternatives:{4:[4,3,7]}}";
        JsonReader reader = new JsonReader();
        JsonValue jValue = reader.parse(expected);
        TileSetData tileSetData= new TileSetData();
        tileSetData.read(json,jValue);
        assertEquals(expected,json.toJson(tileSetData));

    }
}
