package grid.autotiling;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import grid.autotiling.TileSetInstruction.TileComplexity;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 A test class for TileSetInstruction. It includes test cases for the read() and write() methods of TileSetInstruction.
 */
public class TileSetInstructionTest {


    /*Tests whether the TileSetInstruction object can correctly read a JSON string and populate its fields with the data.*/
    @Test
    void testRead() {
        //make sure that the object correctly reads the json
        String jsonString = "{\"complexity\": \"ZEROBIT\",\"layer\": 1,\"offset\": 0}";
        Json json = new Json();
        JsonReader reader = new JsonReader();
        JsonValue jValue = reader.parse(jsonString);

        TileSetInstruction instruction = new TileSetInstruction();
        instruction.read(json,jValue);
        assertEquals(TileSetInstruction.TileComplexity.ZEROBIT,instruction.complexity);
        assertEquals(1,instruction.layer);
        assertEquals(0,instruction.offset);
    }

    /*Tests whether the TileSetInstruction object can correctly write a JSON string based on its field values. */
    @Test
    void testWrite() {
        //make sure that the object correctly creates an accurate json string
        String jsonString = "{complexity:ZEROBIT,layer:1,offset:0}";
        Json json = new Json();
        TileSetInstruction instruction = new TileSetInstruction(TileComplexity.ZEROBIT,1,0);
        assertEquals(jsonString,json.toJson(instruction));
    }
}
