package grid.autotiling;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


/**
 The BitmaskTest class tests the functionality of the Bitmask class.
 It contains a test for the removeCornerBits method, which iterates through all possible
 bitmasks and checks if the corner bits are properly removed and not removed when they shouldn't be,
 and that no other bits have changed.
 */
public class BitmaskTest {
    @Test
    void testRemoveCornerBits() {

        //iterate through all possible bitmasks
        for(int bitmask =0; bitmask < (1<<8); bitmask++){

            int result = Bitmask.removeCornerBits(bitmask);
            //make sure that corner bits are removed
            assertTrue(!((Bitmask.getBitAt(bitmask, 1)==0) || (Bitmask.getBitAt(bitmask, 3)==0)) || (Bitmask.getBitAt(result, 0)==0));
            assertTrue(!((Bitmask.getBitAt(bitmask, 1)==0) || (Bitmask.getBitAt(bitmask, 4)==0)) || (Bitmask.getBitAt(result, 2)==0));
            assertTrue(!((Bitmask.getBitAt(bitmask, 3)==0) || (Bitmask.getBitAt(bitmask, 6)==0)) || (Bitmask.getBitAt(result, 5)==0));
            assertTrue(!((Bitmask.getBitAt(bitmask, 6)==0) || (Bitmask.getBitAt(bitmask, 4)==0)) || (Bitmask.getBitAt(result, 7)==0));
            
            //make sure that corner bits arent removed if they shouldn't be removed
            assertFalse((Bitmask.getBitAt(bitmask, 0)==1) && (Bitmask.getBitAt(result, 0)==0) && (Bitmask.getBitAt(result, 1)==1) && (Bitmask.getBitAt(result, 3)==1));
            assertFalse((Bitmask.getBitAt(bitmask, 2)==1) && (Bitmask.getBitAt(result, 2)==0) && (Bitmask.getBitAt(result, 1)==1) && (Bitmask.getBitAt(result, 4)==1));
            assertFalse((Bitmask.getBitAt(bitmask, 5)==1) && (Bitmask.getBitAt(result, 5)==0) && (Bitmask.getBitAt(result, 3)==1) && (Bitmask.getBitAt(result, 6)==1));
            assertFalse((Bitmask.getBitAt(bitmask, 7)==1) && (Bitmask.getBitAt(result, 7)==0) && (Bitmask.getBitAt(result, 6)==1) && (Bitmask.getBitAt(result, 4)==1));

            //make sure that no other bits have changed
            assertEquals(bitmask | (1+4+32+128), result | (1+4+32+128));

        }


    }
}
