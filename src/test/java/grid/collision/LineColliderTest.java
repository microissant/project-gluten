package grid.collision;

import com.badlogic.gdx.math.Vector2;
import components.physics.Physics;
import grid.GridCoordinate;
import grid.TileData;
import grid.WorldGrid;
import grid.factory.RoomFactory;
import grid.factory.RoomLayout;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LineColliderTest {
    private static WorldGrid grid;
    private static LineCollider collider;
    private static Physics physics;

    @BeforeAll
    static void createGrid(){
     
        String roomString = 
        "wwwwwwwwwww  www,"+
        "w              w,"+
        "w              w,"+
        "wwwwww         w,"+
        "                ,"+
        "      w w       ,"+
        "w hh           w,"+
        "w hh       ww  w,"+
        "w hh       ww  w,"+
        "w              w,"+
        "wwwwwwwwwwwwwwww, ";

        RoomFactory roomFactory = new RoomFactory();
        RoomLayout layout = roomFactory.createLayout(roomString);
        grid = new WorldGrid(11, 16, 1, null);
        layout.paste(grid, new GridCoordinate(0, 0), null);
        collider = new LineCollider();
        physics = new Physics();
    }


    //none of these should collide with a wall
    @CsvSource(value = {"6,7,7,0","12,9,0,-3","5,3,1,-1","9,2,-1,1","8,8,-1,0","11,5,0,1"})
    @ParameterizedTest(name = "pos: {0},{1} vel: {2} {3}")
    void testNoCollision(float posx, float posy, float vx, float vy) {
        
        physics.setPosition(new Vector2(posx,posy));
        physics.setVelocity(new Vector2(vx,vy));
        Vector2 result = new Vector2();
        collider.collide(physics,physics.getVelocity(), grid, TileData.TileRuleset.WALKING.val,result);
        assertEquals(posx + vx, result.x,0.1);
        assertEquals(posy + vy, result.y,0.1);


        result.sub(physics.getPosition());
        //check that none of the components of the velocity vector have increased in size
        assertTrue(Math.abs(physics.getVelocity().x)+0.1>Math.abs(result.x));
        assertTrue(Math.abs(physics.getVelocity().y)+0.1>Math.abs(result.y));
    }

    @CsvSource(value = {"5,8,0,-3","7,6,2,-2","10,2,3,0","2,1,0,2","8.5,7,-3,0"})
    @ParameterizedTest(name = "pos: {0},{1} vel: {2} {3}")
    public void testCollision(float posx, float posy, float vx, float vy){
        physics.setPosition(new Vector2(posx,posy));
        physics.setVelocity(new Vector2(vx,vy));
        Vector2 result = new Vector2();
        collider.collide(physics,physics.getVelocity(), grid, TileData.TileRuleset.WALKING.val,result);
        result.sub(physics.getPosition());

        //check that none of the components of the velocity vector have increased in size
        assertTrue(Math.abs(physics.getVelocity().x)+0.1>Math.abs(result.x));
        assertTrue(Math.abs(physics.getVelocity().y)+0.1>Math.abs(result.y));

        //make sure that the length of the velocity vector has decreased
        assertTrue(physics.getVelocity().len()>result.len());

    }
}
