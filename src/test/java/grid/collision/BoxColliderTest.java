package grid.collision;

import com.badlogic.gdx.math.Vector2;
import components.physics.Physics;
import grid.GridCoordinate;
import grid.TileData;
import grid.WorldGrid;
import grid.factory.RoomFactory;
import grid.factory.RoomLayout;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


/**
 The BoxCollider class is tested by verifying that the collider does not collide with a wall when the
 input position and velocity do not intersect with a wall. Additionally, the collider is tested by verifying
 that the velocity vector's components do not increase in size and the length of the velocity vector
 decreases after a collision with a wall.
 */
public class BoxColliderTest {
    private static WorldGrid grid;
    private static BoxCollider collider;
    private static Physics physics;


    @BeforeAll
    static void createGrid(){
     
        String roomString = 
        "wwwwwwwwwww  www,"+
        "w              w,"+
        "w              w,"+
        "wwwwww         w,"+
        "                ,"+
        "      w w       ,"+
        "w hh           w,"+
        "w hh       ww  w,"+
        "w hh       ww  w,"+
        "w              w,"+
        "wwwwwwwwwwwwwwww, ";

        RoomFactory roomFactory = new RoomFactory();
        RoomLayout layout = roomFactory.createLayout(roomString);
        grid = new WorldGrid(11, 16, 1, null);
        layout.paste(grid, new GridCoordinate(0, 0), null);
        collider = new BoxCollider();
        collider.setDimensions(1, 1, 1, 1);
        physics = new Physics();
    }


    //none of these should collide with a wall
    @CsvSource(value = {"7,7,5,0","12,8,0,-1","5,3,1,-1","9,2,-1,1","8,8,-1,0","11,5,0,1"})
    @ParameterizedTest(name = "pos: {0},{1} vel: {2} {3}")
    void testNoCollision(float posx, float posy, float vx, float vy) {
        
        physics.setPosition(new Vector2(posx,posy));
        physics.setVelocity(new Vector2(vx,vy));
        Vector2 result = new Vector2();
        collider.collide(physics,physics.getVelocity(), grid, TileData.TileRuleset.WALKING.val,result);
        assertEquals(posx + vx, result.x,0.1);
        assertEquals(posy + vy, result.y,0.1);


        result.sub(physics.getPosition());
        //check that none of the components of the velocity vector have increased in size
        assertTrue(Math.abs(physics.getVelocity().x)+0.1>Math.abs(result.x));
        assertTrue(Math.abs(physics.getVelocity().y)+0.1>Math.abs(result.y));
    }

    @CsvSource(value = {"7,7,0,-1","10,5,-1,0","9,7,0,-1","13,8,1,1","5,2,-1,1"})
    @ParameterizedTest(name = "pos: {0},{1} vel: {2} {3}")
    public void testCollision(float posx, float posy, float vx, float vy){
        physics.setPosition(new Vector2(posx,posy));
        physics.setVelocity(new Vector2(vx,vy));
        Vector2 result = new Vector2();
        collider.collide(physics,physics.getVelocity(), grid, TileData.TileRuleset.WALKING.val,result);
        result.sub(physics.getPosition());

        //check that none of the components of the velocity vector have increased in size
        assertTrue(Math.abs(physics.getVelocity().x)+0.1>Math.abs(result.x));
        assertTrue(Math.abs(physics.getVelocity().y)+0.1>Math.abs(result.y));

        //make sure that the length of the velocity vector has decreased
        assertTrue(physics.getVelocity().len()>result.len());

    }

}
