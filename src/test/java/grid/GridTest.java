package grid;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class GridTest {

    /**
     * Tests setting and getting values from the grid.
     * Initializes a Grid object, sets and gets the value of a cell and checks if it's correct.
     */
    @Test
    void setGetTest(){
        Grid<Integer> grid = new Grid<>(20,20,0);
        assertEquals(grid.get(2,2),0);
        grid.set(2,2,1);
        assertEquals(grid.get(2,2),1);
        assertEquals(grid.get(3,3),0);
    }

    /**
     * Tests throwing of errors in Grid.
     * Initializes a Grid object and throws IndexOutOfBoundsException when accessing cells out of bounds,
     * and throws IllegalArgumentException when creating an illegal grid.
     */
    @Test
    void testThrowErrors(){
        Grid<Integer> grid = new Grid<>(20,20,0);
        //accessing something outside
        assertThrows(IndexOutOfBoundsException.class, () -> grid.get(-1,0));
        assertThrows(IndexOutOfBoundsException.class, () -> grid.get(20,0));
        //creating an illegal grid
        assertThrows(IllegalArgumentException.class, () -> new Grid<>(0,0));
        assertThrows(IllegalArgumentException.class, () -> new Grid<>(-1,0));
    }

    /**
     * Tests boundary conditions of the grid.
     * Initializes a Grid object and checks if coordinates are on the grid or not.
     */
    @Test 
    void boundaryTest(){
        Grid<Integer> grid = new Grid<>(2,2,0);
        assertFalse(grid.coordinateIsOnGrid(2, 2));
        assertTrue(grid.coordinateIsOnGrid(1, 0));
        assertFalse(grid.coordinateIsOnGrid(-1, 1));
    }

    /**
     * Tests the number of rows and columns in the grid.
     * Initializes a Grid object and checks if the number of rows and columns is correct.
     */
    @Test
    void correctNumRows(){
        Grid<Integer> grid = new Grid<>(2,3,0);
        assertEquals(2, grid.numRows());
        assertEquals(3, grid.numCols());
        grid = new Grid<>(5,4,0);
        assertEquals(5, grid.numRows());
        assertEquals(4, grid.numCols());
    }
}
