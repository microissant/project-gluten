package grid;

import grid.TileData.TileType;
import grid.factory.RoomFactory;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RoomFactoryTest {


    /**
     Tests the RoomFactory's ability to correctly parse a string representation of a room layout.
     Creates a string representing a room layout, then verifies that the RoomFactory's createLayout method
     correctly creates an IGrid object that matches the expected tile types at each coordinate.
     */
    @Test
    void CorrectParseTest(){
        String[] box = new String[] {
            "wwww,",
            "w  w,",
            "w hw,",
            "ww w, ,"};
        String s = String.join("",box);
        RoomFactory factory  = new RoomFactory();

        IGrid<TileData> grid = factory.createLayout(s).getGrid();
        TileData wallType = new TileData(TileType.DESTRUCTIBLEWALL);
        TileData holeType = new TileData(TileType.HOLE);
        TileData emptyType = new TileData(TileType.EMPTY);

        //bottom row
        assertEquals(wallType, grid.get(new GridCoordinate(0, 0)));
        assertEquals(wallType, grid.get(new GridCoordinate(0, 1)));
        assertEquals(emptyType, grid.get(new GridCoordinate(0, 2)));
        assertEquals(wallType, grid.get(new GridCoordinate(0, 3)));
        //second row
        assertEquals(wallType, grid.get(new GridCoordinate(1, 0)));
        assertEquals(emptyType, grid.get(new GridCoordinate(1, 1)));
        assertEquals(holeType, grid.get(new GridCoordinate(1, 2)));
        assertEquals(wallType, grid.get(new GridCoordinate(1, 3)));
        //third row
        assertEquals(wallType, grid.get(new GridCoordinate(2, 0)));
        assertEquals(emptyType, grid.get(new GridCoordinate(2, 1)));
        assertEquals(emptyType, grid.get(new GridCoordinate(2, 2)));
        assertEquals(wallType, grid.get(new GridCoordinate(2, 3)));
        //fourth row
        assertEquals(wallType, grid.get(new GridCoordinate(3, 0)));
        assertEquals(wallType, grid.get(new GridCoordinate(3, 1)));
        assertEquals(wallType, grid.get(new GridCoordinate(3, 2)));
        assertEquals(wallType, grid.get(new GridCoordinate(3, 3)));



    }
}
