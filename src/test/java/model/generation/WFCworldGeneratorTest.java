package model.generation;

import grid.CoordinateItem;
import grid.Grid;
import grid.GridCoordinate;
import grid.WorldGrid;
import grid.autotiling.Bitmask;
import model.event.EventBus;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.LinkedList;
import java.util.Queue;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class WFCworldGeneratorTest {
    
    /**checks if every cell in the path grid is compatible with its neighbors */
    @Test
    void testGenerateWorld() {
        WFCworldGenerator worldGenerator = new WFCworldGenerator();
        Grid<PathBitmask> grid = worldGenerator.generatePaths(5, 17, null,null);

        PathBitmask empty = new PathBitmask(0, 0);
        PathBitmask last = empty;
        int[] RightCheck = new int[] {PathBitmask.TOPRIGHTCORNER,PathBitmask.RIGHTDOOR,PathBitmask.RIGHTWALL,PathBitmask.BOTTOMRIGHTCORNER};
        int[] leftCheck = new int[] {PathBitmask.TOPLEFTCORNER,PathBitmask.LEFTDOOR,PathBitmask.LEFTWALL,PathBitmask.BOTTOMLEFTCORNER};
        int[] TopCheck = new int[] {PathBitmask.TOPLEFTCORNER,PathBitmask.TOPDOOR,PathBitmask.TOPWALL,PathBitmask.TOPRIGHTCORNER};
        int[] BottomCheck = new int[] {PathBitmask.BOTTOMLEFTCORNER,PathBitmask.BOTTOMDOOR,PathBitmask.BOTTOMWALL,PathBitmask.BOTTOMRIGHTCORNER};
        

        //System.out.println(printGrid(grid));
        //check if the right side aligns 
        for(int r= 0; r < grid.numRows(); r++){
            for(int c= grid.numCols()-1; c >=0 ; c--){

                PathBitmask current = grid.get(r,c);
                for(int i =0; i < RightCheck.length; i++){
                    assertEquals(Bitmask.getBitAt(last.bits, leftCheck[i]) , Bitmask.getBitAt(current.bits, RightCheck[i]));
                }
                last = current;
            }
        }
        last = empty;
        //check if the left side aligns
        for(int r= 0; r < grid.numRows(); r++){
            for(int c= 0; c <grid.numCols(); c++){

                PathBitmask current = grid.get(r,c);
                for(int i =0; i < RightCheck.length; i++){
                    assertEquals(Bitmask.getBitAt(last.bits, RightCheck[i]) , Bitmask.getBitAt(current.bits, leftCheck[i]));
                }
                last = current;
            }
        }
       
        //check if the top side aligns
        for(int c= 0; c <grid.numCols(); c++){
            last = empty;
            for(int r= grid.numRows()-1; r >=0; r--){

                PathBitmask current = grid.get(r,c);
                for(int i =0; i < RightCheck.length; i++){
                    assertEquals(Bitmask.getBitAt(last.bits, BottomCheck[i]) , Bitmask.getBitAt(current.bits, TopCheck[i]));
                }
                last = current;
            }
        }
        
        //check if the bottom side aligns
        for(int c= 0; c <grid.numCols(); c++){
            last = empty;
            for(int r= 0; r < grid.numRows(); r++){

                PathBitmask current = grid.get(r,c);
                for(int i =0; i < RightCheck.length; i++){
                    assertEquals(Bitmask.getBitAt(last.bits, TopCheck[i]) , Bitmask.getBitAt(current.bits, BottomCheck[i]));
                }
                last = current;
            }
        }
    }

    /*checks if the method creates a path from the starting coordinate to the target coordinate* */
    @CsvSource(value = {"0,0,14,14","0,5,14,5","14,0,0,14"})
    @ParameterizedTest(name = "start = {0},{1},  target = {2},{3}")
    void testGenerateSafePath(int startRow, int startCol, int targetRow, int targetCol) {
        int rows = 15;
        int columns = 15;

        boolean flag =false;

        Grid<Boolean> searched = new Grid<>(rows, columns,false);
        Grid<PathBitmask> grid = new Grid<>(rows, columns);
        for(CoordinateItem<PathBitmask> cItem : grid){
            grid.set(cItem.coordinate(), new PathBitmask(0,0));
        }
        GridCoordinate start = new GridCoordinate(startRow, startCol);
        GridCoordinate target = new GridCoordinate(targetRow, targetCol);


        WFCworldGenerator generator = new WFCworldGenerator();
        generator.generateSafePath(grid, searched,start, target);
        searched = new Grid<>(rows, columns,false);

        Queue<GridCoordinate> toSearch = new LinkedList<>();
        
        toSearch.add(start);
        while(!toSearch.isEmpty()){
            GridCoordinate currentCoordinate = toSearch.poll();
            if(currentCoordinate.equals(target)){
                flag = true;
                break;
            }
            searched.set(currentCoordinate,true);
            PathBitmask bitmask = grid.get(currentCoordinate);
            GridCoordinate nextCoordinate;
            nextCoordinate = new GridCoordinate(currentCoordinate.row+1, currentCoordinate.col);
            if(Bitmask.getBitAt(bitmask.bits, PathBitmask.TOPDOOR)==1 && !searched.get(nextCoordinate)){
                toSearch.add(nextCoordinate);
            }
            nextCoordinate = new GridCoordinate(currentCoordinate.row, currentCoordinate.col+1);
            if(Bitmask.getBitAt(bitmask.bits, PathBitmask.RIGHTDOOR)==1 && !searched.get(nextCoordinate)){
                toSearch.add(nextCoordinate);
            }
            nextCoordinate = new GridCoordinate(currentCoordinate.row, currentCoordinate.col-1);
            if(Bitmask.getBitAt(bitmask.bits, PathBitmask.LEFTDOOR)==1 && !searched.get(nextCoordinate)){
                toSearch.add(nextCoordinate);
            }
            nextCoordinate = new GridCoordinate(currentCoordinate.row-1, currentCoordinate.col);
            if(Bitmask.getBitAt(bitmask.bits, PathBitmask.BOTTOMDOOR)==1 && !searched.get(nextCoordinate)){
                toSearch.add(nextCoordinate);
            }
        }
        
        assertTrue(flag);
    }
    

    /**checks if the produced worldgrid has the correct dimensions */
    @CsvSource(value = {"10,10","5,9","9,5","2,3"})
    @ParameterizedTest(name = "dimensions = {0},{1}")
    void dimensionSanityTest(int rows, int cols){
        WFCworldGenerator generator = new WFCworldGenerator();

        WorldGrid worldGrid = generator.generateWorld(rows, cols, new EventBus(), 16, new GridCoordinate(0,0)).worldGrid();

        assertEquals(RoomPlacer.roomWidth * cols + RoomPlacer.margin * 2 , worldGrid.numCols());
        assertEquals(RoomPlacer.roomHeight * rows + RoomPlacer.margin * 2 , worldGrid.numRows());
    }
}
