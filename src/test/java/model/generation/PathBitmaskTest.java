package model.generation;

import grid.autotiling.Bitmask;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 This class tests the functionality of the PathBitmask class.
 */
public class PathBitmaskTest {

    /**
     This method tests the determine() method of the PathBitmask class.
     It makes sure that the input is legal, and tests the method multiple times to account for randomness.
     It also checks that determined bits are unchanged, and that the result is legal and that all bits are determined.
     @param bits the bits to determine
     @param determined the determined bits
     */

    @CsvSource(value = { "1716,4040","1574,23"})
    @ParameterizedTest(name = "bits: {0}, determined: {1}")
    void testDetermine(int bits, int determined) {
        
        //make sure that the input is legal
        assertEquals(PathBitmask.fixIllegalState(bits), bits);

        //do it multiple times to account for randomness
        for(int i =0; i < 100; i++){
            PathBitmask bitmask = new PathBitmask(bits, determined);
            
            bitmask.determine(0.5f);
            //make sure that determined bits are unchanged
            assertEquals(bits & determined, bitmask.bits & determined);

            //make sure that the result is legal
            assertEquals(PathBitmask.fixIllegalState(bitmask.bits), bitmask.bits);
            //make sure that all bits are determined
            for(int j =0; j < PathBitmask.size; j++){
                assertEquals(1,Bitmask.getBitAt(bitmask.determinedBits, j));
            }
             
        }

    }

    /**
     This method tests the fixIllegalState() method of the PathBitmask class.
     It tests that the input is legal and returns the expected value.
     @param input the input to fix
     @param expected the expected output
     */
    @CsvSource(value = { "3797,3780","2313,0","1254,1254"})
    @ParameterizedTest(name = "input: {0} ")
    void testFixIllegalState(int input, int expected) {
        assertEquals(expected, PathBitmask.fixIllegalState(input));
    }

    /**
     Sets the horizontal determined bits of this bitmask using the given bitmask and direction.
     @param bits1,bits2,result,determined The other bitmask to use.
     @param dir The direction to use.
     */
    @CsvSource(value = { "2214,2170,4095,1,230","2214,2170,4095,-1,2455","2214,2170,4095,0,2214","2214,2170,0,-1,2214","2214,2170,0,1,2214","1262,48,4095,1,1254","48,1262,4095,-1,49"})
    @ParameterizedTest(name = "input: {0} ")
    void testSetHorizontaldetermined(int bits1, int bits2, int determined2,int dir, int result) {
        PathBitmask bitmask1 = new PathBitmask(bits1,0);
        PathBitmask bitmask2 = new PathBitmask(bits2,determined2);

        bitmask1.setHorizontalDetermined(bitmask2, dir);
        assertEquals(result, bitmask1.bits);

    }
    /**
     Sets the vertical determined bits of this bitmask using the given bitmask and direction.
     @param  bits1,bits2,result,determined The other bitmask to use.
     @param dir The direction to use.
     */
    @CsvSource(value = { "1262,1782,4095,1,1254"})
    @ParameterizedTest(name = "input: {0} ")
    void testSetVerticaldetermined(int bits1, int bits2, int determined2,int dir, int result) {
        PathBitmask bitmask1 = new PathBitmask(bits1,0);
        PathBitmask bitmask2 = new PathBitmask(bits2,determined2);

        bitmask1.setVerticalDetermined(bitmask2, dir);
        assertEquals(result, bitmask1.bits);

    }

    /**
     Sets the horizontal path bits of this bitmask using the given direction.
     @param dir The direction to use.
     */
    @CsvSource(value = { "1,128","0,0","-1,32"})
    @ParameterizedTest(name = "Direction: {0}")
    void testSetHorizontalPath(int dir, int result) {
        PathBitmask bitmask= new PathBitmask(0, 0);
        bitmask.setHorizontalPath(dir);
        assertEquals(result, bitmask.bits);
    }

    /**
     Sets the vertical path bits of this bitmask using the given direction.
     @param dir The direction to use.
     */
    @CsvSource(value = { "1,4","0,0","-1,1024"})
    @ParameterizedTest(name = "Direction: {0}")
    void testSetVerticalPath(int dir, int result) {
        PathBitmask bitmask= new PathBitmask(0, 0);
        bitmask.setHorizontalPath(dir);
    }

    /**
     Sets the diagonal determined bits of this bitmask using the given bitmask, x direction, and y direction.
     @param    bits1,bits2 bitmask to use.
     @param xdir The x direction to use.
     @param ydir The y direction to use.
     */
    @CsvSource(value = { "918,2534,4095,1,1,926","918,2534,3839,1,1,918","918,2534,0,1,1,918","918,2534,4095,-1,-1,662"})
    @ParameterizedTest(name = "input: {0} ")
    void testSetDiagonalDetermined(int bits1, int bits2, int determined2, int xdir, int ydir, int result) {
        PathBitmask bitmask1 = new PathBitmask(bits1,0);
        PathBitmask bitmask2 = new PathBitmask(bits2,determined2);

        bitmask1.setDiagonalDetermined(bitmask2, xdir,ydir);
        assertEquals(result, bitmask1.bits);
    }

    @CsvSource(value = { "918,0,0,4095,true,undetermined is compatible","918,4095,495,4095,false,determined is not compatible","918,4095,0,4095,false, non zero is not compatible with zero"})
    @ParameterizedTest(name = "{5} ")
    void testIsCompatible(int bits1,int determined1, int bits2, int determined2, boolean result, String name) {
       PathBitmask bitmask1 = new PathBitmask(bits1, determined1); 
       PathBitmask bitmask2 = new PathBitmask(bits2, determined2);

       assertEquals(result, bitmask1.isCompatible(bitmask2));
    }


    //checks if the string constructor produces the correct bitmasks
    //the strings are supposed to be backwards
    @CsvSource(value = {"000011110000,111100001111,240,3855","100011001000,000100110001,305,2248"})
    @ParameterizedTest(name = "{0},{1}")
    void testConstructor(String bits, String determinedBits, int resultBits, int resultDeterminedBits){
        PathBitmask bitmask = new PathBitmask(bits,determinedBits);

        assertEquals(resultBits, bitmask.bits);
        assertEquals(resultDeterminedBits, bitmask.determinedBits);

    }
}
