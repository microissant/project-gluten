package model;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import components.collision.CollisionManager;
import entities.Entity;
import entities.enemy.IEnemy;
import entities.particles.Particle;
import entities.player.PlayerManager;
import grid.WorldGrid;
import model.event.EventBus;
import model.event.events.DestructiveExplosionEvent;
import model.event.events.game.LoadNextLevelEvent;
import model.event.events.game.NewGameEvent;
import model.event.events.game.RestartLevelEvent;
import model.level.LevelManager;
import model.state.GameState;
import model.state.GameStateManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.lang.reflect.Field;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.*;

/**
 It contains methods for testing various functionality of the Model class.
 */

public class ModelTest {

    private Model model;

    @Mock
    private EventBus eventBus;

    @Mock
    private EntityManager entityManager;

    /**
     This method sets up the required mocks and the instance of the Model class to be used in the test methods.
     */
    @BeforeEach
    public void setUp() {
        eventBus = mock(EventBus.class);
        entityManager = mock(EntityManager.class);
        model = new Model(eventBus);
    }

    /**
     This method sets the value of a private field in an object using reflection.
     @param target the object whose field value is to be set
     @param fieldName the name of the field whose value is to be set
     @param value the new value of the field
     @throws NoSuchFieldException if the field with the given name does not exist in the object
     @throws IllegalAccessException if the field is inaccessible
     */
    private void setPrivateField(Object target, String fieldName, Object value) throws NoSuchFieldException, IllegalAccessException {
        Field field = target.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        field.set(target, value);
    }

    /**
     This method tests the newGameEvent() method of the Model class.
     @throws NoSuchFieldException if the field with the given name does not exist in the object
     @throws IllegalAccessException if the field is inaccessible
     */
    @Test
    public void testNewGameEvent() throws NoSuchFieldException, IllegalAccessException {
        NewGameEvent event = mock(NewGameEvent.class);
        LevelManager levelManager = mock(LevelManager.class);

        // Set the mocked LevelManager using reflection
        setPrivateField(model, "levelManager", levelManager);

        model.newGameEvent(event);

        // Verify the clear() and loadNextLevel() methods in the LevelManager are called
        verify(levelManager).clear();
        verify(levelManager).loadNextLevel();
    }


    /**

     This method tests the nextLevel() method of the Model class.
     @throws NoSuchFieldException if the field with the given name does not exist in the object
     @throws IllegalAccessException if the field is inaccessible
     */
    @Test
    public void testNextLevel() throws NoSuchFieldException, IllegalAccessException {
        LoadNextLevelEvent event = new LoadNextLevelEvent();
        LevelManager levelManager = mock(LevelManager.class);

        // Set the mocked LevelManager using reflection
        setPrivateField(model, "levelManager", levelManager);

        model.nextLevel(event);

        // Verify the loadNextLevel() method in the LevelManager is called
        verify(levelManager).loadNextLevel();
    }

    /**
     This method tests the getPlayer() method of the Model class.
     @throws NoSuchFieldException if the field with the given name does not exist in the object
     @throws IllegalAccessException if the field is inaccessible
     */
    @Test
    public void testGetPlayer() throws NoSuchFieldException, IllegalAccessException {
        entityManager = mock(EntityManager.class);
        when(entityManager.getPlayerManager()).thenReturn(mock(PlayerManager.class));

        setPrivateField(model, "entityManager", entityManager);

        PlayerManager playerManager = model.getPlayer();
        assertNotNull(playerManager);
    }

    /**
     This method tests the restartLevel() method of the Model class.
     @throws NoSuchFieldException if the field with the given name does not exist in the object
     @throws IllegalAccessException if the field is inaccessible
     */
    @Test
    public void testRestartLevel() throws NoSuchFieldException, IllegalAccessException {
        RestartLevelEvent event = new RestartLevelEvent();
        LevelManager levelManager = mock(LevelManager.class);

        // Set the mocked LevelManager using reflection
        setPrivateField(model, "levelManager", levelManager);

        model.restartLevel(event);

        // Verify the resetCurrentLevel() and loadNextLevel() methods in the LevelManager are called
        verify(levelManager).resetCurrentLevel();
        verify(levelManager).loadNextLevel();
    }

    /**
     Test the update() method when the game state is ACTIVE.
     Verifies that update methods are called on EntityManager, CollisionManager, and GameStateManager.
     @throws NoSuchFieldException if a field with the specified name is not found
     @throws IllegalAccessException if the underlying field is inaccessible
     */
    @Test
    public void testUpdate() throws NoSuchFieldException, IllegalAccessException {
        CollisionManager collisionManager = mock(CollisionManager.class);
        GameStateManager gameStateManager = mock(GameStateManager.class);

        // Set the mocked EntityManager, CollisionManager, and GameStateManager using reflection
        setPrivateField(model, "entityManager", entityManager);
        setPrivateField(model, "collisionManager", collisionManager);
        setPrivateField(model, "gameStateManager", gameStateManager);

        when(gameStateManager.getGameState()).thenReturn(GameState.ACTIVE);

        model.update(1.0);

        // Verify update methods are called on EntityManager, CollisionManager, and GameStateManager
        verify(entityManager).update(1.0);
        verify(collisionManager).update(1.0);
        verify(gameStateManager).update(1.0);
    }

    /**
     Test the update() method when the game state is not ACTIVE.
     Verifies that update methods are NOT called on EntityManager, CollisionManager, and GameStateManager.
     @throws NoSuchFieldException if a field with the specified name is not found
     @throws IllegalAccessException if the underlying field is inaccessible
     */
    @Test
    public void testUpdateNotActive() throws NoSuchFieldException, IllegalAccessException {
        CollisionManager collisionManager = mock(CollisionManager.class);
        GameStateManager gameStateManager = mock(GameStateManager.class);

        // Set the mocked EntityManager, CollisionManager, and GameStateManager using reflection
        setPrivateField(model, "entityManager", entityManager);
        setPrivateField(model, "collisionManager", collisionManager);
        setPrivateField(model, "gameStateManager", gameStateManager);

        when(gameStateManager.getGameState()).thenReturn(GameState.PAUSED);

        model.update(1.0);

        // Verify update methods are NOT called on EntityManager, CollisionManager, and GameStateManager
        verify(entityManager, never()).update(1.0);
        verify(collisionManager, never()).update(1.0);
        verify(gameStateManager, never()).update(1.0);
    }

    /**
     Test the getParticles() method.
     Verifies that the method returns the particles from the EntityManager.
     @throws NoSuchFieldException if a field with the specified name is not found
     @throws IllegalAccessException if the underlying field is inaccessible
     */
    @Test
    public void testGetParticles() throws NoSuchFieldException, IllegalAccessException {
        Array<Particle> particles = new Array<>();
        when(entityManager.getParticles()).thenReturn(particles);

        setPrivateField(model, "entityManager", entityManager);

        Array<Particle> result = model.getParticles();
        assertSame(particles, result);
    }

    /**
     Test the getWorldGrid() method.
     Verifies that the method returns the WorldGrid from the LevelManager.
     @throws NoSuchFieldException if a field with the specified name is not found
     @throws IllegalAccessException if the underlying field is inaccessible
     */
    @Test
    public void testGetWorldGrid() throws NoSuchFieldException, IllegalAccessException {
        LevelManager levelManager = mock(LevelManager.class);
        WorldGrid worldGrid = mock(WorldGrid.class);
        when(levelManager.getWorldGrid()).thenReturn(worldGrid);

        setPrivateField(model, "levelManager", levelManager);

        WorldGrid result = model.getWorldGrid();
        assertSame(worldGrid, result);
    }

    /**
     Tests the destructiveExplosion method of the Model class.
     Verifies that the corresponding method is called on the WorldGrid when a DestructiveExplosionEvent is triggered.
     @throws NoSuchFieldException if a field is not found
     @throws IllegalAccessException if a field cannot be accessed
     */
    @Test
    public void testDestructiveExplosion() throws NoSuchFieldException, IllegalAccessException {
        LevelManager levelManager = mock(LevelManager.class);
        WorldGrid worldGrid = mock(WorldGrid.class);
        when(levelManager.getWorldGrid()).thenReturn(worldGrid);

        setPrivateField(model, "levelManager", levelManager);

        Vector2 explosionPosition = new Vector2(5, 5);
        int explosionRadius = 2;

        DestructiveExplosionEvent event = new DestructiveExplosionEvent(explosionPosition, explosionRadius);
        model.destructiveExplosion(event);

        verify(worldGrid).explodeTiles(explosionPosition, explosionRadius);
    }

    /**
     Tests the getEnemies method of the Model class.
     Verifies that the list of enemies returned by the EntityManager is the same as the one returned by the Model.
     @throws NoSuchFieldException if a field is not found
     @throws IllegalAccessException if a field cannot be accessed
     */
    @Test
    public void testGetEnemies() throws NoSuchFieldException, IllegalAccessException {
        EntityManager entityManager = mock(EntityManager.class);
        List<IEnemy> enemies = mock(List.class);
        when(entityManager.getEnemies()).thenReturn(enemies);

        setPrivateField(model, "entityManager", entityManager);

        List<IEnemy> result = model.getEnemies();
        assertSame(enemies, result);
    }

    /**
     Tests the getVariousEntities method of the Model class.
     Verifies that the list of various entities returned by the EntityManager is the same as the one returned by the Model.
     @throws NoSuchFieldException if a field is not found
     @throws IllegalAccessException if a field cannot be accessed
     */
    @Test
    public void testGetVariousEntities() throws NoSuchFieldException, IllegalAccessException {
        EntityManager entityManager = mock(EntityManager.class);
        List<Entity> variousEntities = mock(List.class);
        when(entityManager.getVariousEntities()).thenReturn(variousEntities);

        setPrivateField(model, "entityManager", entityManager);

        List<Entity> result = model.getVariousEntities();
        assertSame(variousEntities, result);
    }

}
