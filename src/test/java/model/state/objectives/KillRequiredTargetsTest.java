package model.state.objectives;

import components.collision.CollisionType;
import components.collision.ICollidable;
import components.collision.primitives.CollisionCircle;
import components.health.Damage;
import entities.enemy.Enemy;
import entities.enemy.state.StateDefinition;
import entities.navigation.Navigator;
import grid.WorldGrid;
import media.DrawProperties;
import model.event.events.entities.SpawnEntityEvent;
import model.state.objective.KillRequiredTargets;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class KillRequiredTargetsTest {

    KillRequiredTargets killRequiredTargets;
    final double frameTime = 1.0/60.0;

    @BeforeEach
    public void setup() {
        killRequiredTargets = new KillRequiredTargets();
    }

    @Test
    public void correctProgression() {
        TestEnemy enemy1 = new TestEnemy(true);
        TestEnemy enemy2 = new TestEnemy(true);
        TestEnemy enemy3 = new TestEnemy(true);

        killRequiredTargets.registerEnemy(new SpawnEntityEvent<>(enemy1));
        killRequiredTargets.registerEnemy(new SpawnEntityEvent<>(enemy2));
        killRequiredTargets.registerEnemy(new SpawnEntityEvent<>(enemy3));

        killRequiredTargets.update(frameTime);
        assertEquals(0, killRequiredTargets.currentCount());
        assertEquals(3, killRequiredTargets.maxCount());
        assertEquals(0, killRequiredTargets.getProgression());

        enemy1.kill();
        killRequiredTargets.update(frameTime);
        assertEquals(1, killRequiredTargets.currentCount());
        assertEquals(0.33333, killRequiredTargets.getProgression(), 0.01);

        enemy2.kill();
        killRequiredTargets.update(frameTime);
        assertEquals(2, killRequiredTargets.currentCount());
        assertEquals(0.666666, killRequiredTargets.getProgression(), 0.01);

        enemy3.kill();
        killRequiredTargets.update(frameTime);
        assertEquals(3, killRequiredTargets.currentCount());
        assertEquals(1, killRequiredTargets.getProgression());

        assertTrue(killRequiredTargets.isComplete());
    }

    @Test
    public void titleAndDescriptionNonEmpty() {
        assertNotNull(killRequiredTargets.title());
        assertNotNull(killRequiredTargets.description());

        assertFalse(killRequiredTargets.title().isBlank());
        assertFalse(killRequiredTargets.description().isBlank());
    }



    private static class TestEnemy extends Enemy {

        protected TestEnemy(boolean isRequiredTarget) {
            super(null, 1, isRequiredTarget, null, 0, 1, 1);
            //super.deathSound = SoundID.COOKIE_DEATH;
        }

        public void kill() {
            healthManager.applyDamage(new Damage(healthManager.getCurrentHealth()));
        }

        @Override
        public void collisionEnter(ICollidable target, CollisionType.Type type) {

        }

        @Override
        public CollisionCircle getCollisionPrimitive() {
            return null;
        }

        @Override
        public DrawProperties getProperties() {
            return null;
        }

        @Override
        protected StateDefinition getPursuitPathState(Navigator navigator, WorldGrid worldGrid) {
            return null;
        }

        @Override
        public float getPursuitRange() {
            return 0;
        }

        @Override
        public float getAttackRange() {
            return 0;
        }
    }
}
