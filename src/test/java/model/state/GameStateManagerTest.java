package model.state;

import model.event.EventBus;
import model.level.LevelManager;
import model.state.objective.IObjective;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

public class GameStateManagerTest {

    private GameStateManager gameStateManager;

    @Mock
    private EventBus eventBus;
    @Mock
    private IObjective objective;
    final double frameTime = 1.0/60.0;

    @BeforeEach
    public void setup() {
        eventBus = mock(EventBus.class);
        objective = mock(IObjective.class);
        LevelManager levelManager = mock(LevelManager.class);

        gameStateManager = new GameStateManager(eventBus, levelManager);
    }

    @Test
    public void updateChangesElapsedTime() {
        assertEquals(0, gameStateManager.getStatistics().getTotalTimeElapsed());

        for (int i = 1; i < 61; i++) {
            gameStateManager.update(frameTime);
            assertEquals(frameTime * i, gameStateManager.getStatistics().getTotalTimeElapsed(), 0.0001);
        }
    }
}
