package model;

import components.collision.primitives.CollisionCircle;
import entities.enemy.IEnemy;
import entities.inanimate.CloseDoorsTrigger;
import entities.inanimate.EndPortal;
import entities.player.PlayerCharacter;
import entities.player.PlayerManager;
import grid.WorldGrid;
import item.InteractAgent;
import item.Item;
import item.chest.IChest;
import model.event.EventBus;
import model.event.events.EnemyDeathEvent;
import model.event.events.FireWeaponEvent;
import model.event.events.WorldGridCreatedEvent;
import model.event.events.collision.CreateDoorEvent;
import model.event.events.entities.*;
import model.event.events.item.ItemPickupEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**Test class for EntityManager */

public class EntityManagerTest {

    private WorldGrid worldGrid;
    private EventBus eventBus;
    private EntityManager entityManager;
    private CollisionCircle collisionCircle;
    
    @BeforeEach
    public void setUp(){
        eventBus = mock(EventBus.class);
        worldGrid = mock(WorldGrid.class);
        entityManager = new EntityManager(eventBus);
        collisionCircle = mock(CollisionCircle.class);
        doNothing().when(eventBus).post(any());
        
    }
    
    /**Checks that the constructor does not throw an exception when EventBus is null.
     * It also checks that eventBus.register() has been called once when EventBus is not null
     */
    @Test
    public void constructorTest(){
        assertDoesNotThrow(() -> new EntityManager(null));
        
        EntityManager entityManager = new EntityManager(eventBus);
        verify(eventBus, times(1)).register(entityManager);
    }
    
    //All test methods starting with "spawn" or "remove" perform the same routine

    /**
     * Checks that the method does not throw an exception when the event is null,
     * when the event contains null values or when entitys CollisionCircle is null.
     * Also verifies that eventBus.post() has been called once when no null values are provided
     */
    @Test
    public void spawnEnemyTest(){
        IEnemy enemy = mock(IEnemy.class);
        
        assertDoesNotThrow(() -> entityManager.spawnEnemy(null));
        assertDoesNotThrow(() -> entityManager.spawnEnemy(new SpawnEntityEvent<IEnemy>(null)));
        assertDoesNotThrow(() -> entityManager.spawnEnemy(new SpawnEntityEvent<IEnemy>(enemy)));
        
        when(enemy.getCollisionPrimitive()).thenReturn(collisionCircle);
        entityManager.spawnEnemy(new SpawnEntityEvent<IEnemy>(enemy));
        verify(eventBus, times(1)).post(any());       
    }

    /**

     Tests the removeEnemy method. It verifies that the method does not throw an exception when
     the event is null, when the event contains null values or when entitys CollisionCircle is null.
     It also verifies that eventBus.post() has been called once when no null values are provided.
     */
    @Test
    public void removeEnemyTest(){
        IEnemy enemy = mock(IEnemy.class);
        
        assertDoesNotThrow(() -> entityManager.removeEnemy(null));
        assertDoesNotThrow(() -> entityManager.removeEnemy(new EnemyDeathEvent(null)));
        assertDoesNotThrow(() -> entityManager.removeEnemy(new EnemyDeathEvent(enemy)));
        
        when(enemy.getCollisionPrimitive()).thenReturn(collisionCircle);
        entityManager.removeEnemy(new EnemyDeathEvent(enemy));
        verify(eventBus, times(1)).post(any());
    }


    /**

     Tests the removeItem method. It verifies that the method does not throw an exception when
     the event is null, when the event contains null values or when entitys CollisionCircle is null.
     It also verifies that eventBus.post() has been called once when no null values are provided.
     */
    @Test
    public void removeItemTest(){
        Item item = mock(Item.class);
        InteractAgent agent = mock(InteractAgent.class);
        
        assertDoesNotThrow(() -> entityManager.removeItem(null));
        assertDoesNotThrow(() -> entityManager.removeItem(new ItemPickupEvent(null, null)));  
        assertDoesNotThrow(() -> entityManager.removeItem(new ItemPickupEvent(item, agent)));
        
        when(item.getCollisionPrimitive()).thenReturn(collisionCircle);
        entityManager.removeItem(new ItemPickupEvent(item, agent));
        verify(eventBus, times(1)).post(any());
    }

    /**
     * Tests that the spawnPlayer method works correctly.
     * Checks that the event is successfully posted to the eventBus when the playerCharacter and collisionPrimitive are not null.
     */
    @Test
    public void spawnPlayerTest(){
        PlayerManager playerManager = mock(PlayerManager.class);
        PlayerCharacter playerCharacter = mock(PlayerCharacter.class);
        assertDoesNotThrow(() -> entityManager.spawnPlayer(null));
        
        when(playerManager.getPlayerCharacter()).thenReturn(playerCharacter);     
        assertDoesNotThrow(()-> entityManager.spawnPlayer(new SpawnPlayerEvent(playerManager)));
        
        when(playerCharacter.getCollisionPrimitive()).thenReturn(collisionCircle);
        entityManager.spawnPlayer(new SpawnPlayerEvent(playerManager));
        verify(eventBus, times(1)).post(any());
    }

    /**
     * Tests that the worldGridCreated method works correctly.
     */
    @Test
    public void worldGridCreatedTest(){
        assertDoesNotThrow(() -> entityManager.worldGridCreated(null));
        assertDoesNotThrow(() -> entityManager.worldGridCreated(new WorldGridCreatedEvent(null)));
    }


    /**
     * Tests that the spawnEndPortal method works correctly.
     * Checks that the event is successfully posted to the eventBus when the endPortal and collisionPrimitive are not null.
     */
    @Test
    public void spawnEndPortalTest(){
        EndPortal endPortal = mock(EndPortal.class);
        assertDoesNotThrow(() -> entityManager.spawnEndPortal(null));
        assertDoesNotThrow(() -> entityManager.spawnEndPortal(new SpawnEndPortalEvent(null)));
        assertDoesNotThrow(() -> entityManager.spawnEndPortal(new SpawnEndPortalEvent(endPortal)));
        
        when(endPortal.getCollisionPrimitive()).thenReturn(collisionCircle);
        entityManager.spawnEndPortal(new SpawnEndPortalEvent(endPortal));
        verify(eventBus, times(1)).post(any());
        
    }

    /**
     * Tests that the spawnDoorTrigger method works correctly.
     * Checks that the event is successfully posted to the eventBus when the closeDoorsTrigger and collisionPrimitive are not null.
     */
    @Test
    public void spawnDoorTriggerTest(){
        CloseDoorsTrigger closeDoorsTrigger = mock(CloseDoorsTrigger.class);
        assertDoesNotThrow(() -> entityManager.spawnDoorTrigger(null));
        assertDoesNotThrow(() -> entityManager.spawnDoorTrigger(new CreateDoorEvent(null)));
        assertDoesNotThrow(() -> entityManager.spawnDoorTrigger(new CreateDoorEvent(closeDoorsTrigger)));
        
        when(closeDoorsTrigger.getCollisionPrimitive()).thenReturn(collisionCircle);
        entityManager.spawnDoorTrigger(new CreateDoorEvent(closeDoorsTrigger));
        verify(eventBus, times(1)).post(any());
        
    }

    /**
     * Tests that the spawnChest method works correctly.
     * Checks that the event is successfully posted to the eventBus when the chest and collisionPrimitive are not null.
     */
    @Test
    public void spawnChestTest(){
        IChest chest = mock(IChest.class);
        assertDoesNotThrow(()->entityManager.spawnChest(null));
        assertDoesNotThrow(()->entityManager.spawnChest(new SpawnChestEvent(null)));
        assertDoesNotThrow(()->entityManager.spawnChest(new SpawnChestEvent(chest)));
        
        when(chest.getCollisionPrimitive()).thenReturn(collisionCircle);
        entityManager.spawnChest(new SpawnChestEvent(chest));
        verify(eventBus, times(1)).post(any());
        
    }

    /**
     * Tests that the spawnItem method works correctly.
     * Checks that the event is successfully posted to the eventBus when the item and collisionPrimitive are not null.
     */
    @Test
    public void spawnItemTest(){
        Item item = mock(Item.class);
        assertDoesNotThrow(() -> entityManager.spawnItem(null));
        assertDoesNotThrow(() -> entityManager.spawnItem(new SpawnItemEvent(null)));
        assertDoesNotThrow(() -> entityManager.spawnItem(new SpawnItemEvent(item)));
        
        when(item.getCollisionPrimitive()).thenReturn(collisionCircle);
        entityManager.spawnItem(new SpawnItemEvent(item));
        verify(eventBus, times(1)).post(any());       
    }
    
    
    @Test
    public void addProjectileFiredTest(){
        assertDoesNotThrow(() -> entityManager.addProjectileFired(null));
        FireWeaponEvent fireWeaponEvent = new FireWeaponEvent(null, null, null);
        
        assertDoesNotThrow(() -> entityManager.addProjectileFired(fireWeaponEvent));
    }
    
    /**Checks that getWorldGrid() returns the same worldGrid as used by setWorldGrid()
    */
    @Test
    public void getAndSetWorldGridTest(){
        assertDoesNotThrow(() -> entityManager.setWorldGrid(null));
        
        entityManager.setWorldGrid(worldGrid);
        assertEquals(worldGrid, entityManager.getWorldGrid());
        
    }
    /**
     Tests the creation of a particle using the EntityManager's createParticle() method.
     Ensures that no exceptions are thrown when null is passed as an argument to the method.
     */

    @Test
    public void createParticleTest(){
        assertDoesNotThrow(() -> entityManager.createParticle(null));
    }

    /**
     Tests the EntityManager's getParticles() method to ensure that it returns a non-null value.
     */
    @Test
    public void getParticlesTest(){
        assertNotNull(entityManager.getParticles());
    }

    /**
     Tests the EntityManager's getProjectiles() method to ensure that it returns a non-null value.
     */
    @Test
    public void getProjectilesTest(){
        assertNotNull(entityManager.getProjectiles());
    }

    /**
     Tests the EntityManager's getVariousEntities() method to ensure that it returns a non-null value.
     */
    @Test
    public void getVariousEntitiesTest(){
        assertNotNull(entityManager.getVariousEntities());
    }

    /**
     Tests the EntityManager's getItems() method to ensure that it returns a non-null value.
     */
    @Test
    public void getItemsTest(){
        assertNotNull(entityManager.getItems());
    }


    
}
