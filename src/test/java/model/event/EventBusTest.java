package model.event;

import model.event.events.Event;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

public class EventBusTest {

    EventBus eventBus;
    EventSubscriber subscriber;

    @BeforeEach
    public void setup() {
        eventBus = new EventBus();
        subscriber = new EventSubscriber();
    }


    /**
     * Tests that the event bus can correctly register and post events to subscribers.
     * It uses a CsvSource to send a list of strings to the subscriber and tests that the
     * subscriber has received the event.
     * @param string The string message to be sent as an event to the subscriber.
     */
    @CsvSource(value = { "\"This message is sent\"", "\"Today is a great day\"", "\"Evil cookies are bad\"" })
    @ParameterizedTest(name = "Sent string: {0}")
    public void registerAndPostCorrectly(String string) {
        assertFalse(subscriber.hasReceivedEvent);
        assertEquals("", subscriber.receivedMessage);

        eventBus.post(new StringEvent(string));

        assertTrue(subscriber.hasReceivedEvent);
        assertEquals(string, subscriber.receivedMessage);
    }

    /**
     * Tests that the event bus can correctly register and unregister subscribers.
     * It sends two string messages to the subscriber and tests that the subscriber has only
     * received the first message, after unregistering the subscriber, it sends another
     * string message and tests that the subscriber has still only received the first message.
     */
    @Test
    public void registerAndUnregisterCorrectly() {
        String first = "This is the first string sent";
        String last= "This is the last string sent";

        assertFalse(subscriber.hasReceivedEvent);
        assertEquals("", subscriber.receivedMessage);

        eventBus.post(new StringEvent(first));
        assertEquals(first, subscriber.receivedMessage);

        eventBus.unregister(subscriber);
        eventBus.post(new StringEvent(last));
        assertEquals(first, subscriber.receivedMessage);
    }


    /**
     * Tests that the event bus can correctly handle multiple events of the same type.
     * It sends a count event from 0 to 99 to the subscriber and checks that the counter
     * in the subscriber increments by each count event received.
     */
    @Test
    public void postMultipleTimesSameType() {
        int count = 0;
        for (int i = 0; i < 100; i++) {
            eventBus.post(new CountEvent(i));
            count += i;
            assertEquals(count, subscriber.counter);
        }
    }

    /**
     * Tests that the event bus can correctly handle multiple events of different types.
     * It sends a count event and a string event to the subscriber and tests that the
     * subscriber has received both events correctly.
     */
    @Test
    public void postMultipleTimesMultipleTypes() {
        int count = 5;
        String text = "this is the goal text";

        eventBus.post(new CountEvent(count));
        eventBus.post(new StringEvent(text));

        assertTrue(subscriber.hasReceivedEvent);
        assertEquals(count, subscriber.counter);
        assertEquals(text, subscriber.receivedMessage);
    }

    @Test
    public void postMultipleTimesMultiplyTypesExtra() {
        int count = 0;
        List<String> phrases = new ArrayList<>(Arrays.asList(
                "This is the first phrase",
                "Lorem ipsum",
                "Project gluten",
                "Evil cookie",
                "Chef",
                "Bergen city in Norway"
        ));
        Random random = new Random();
        String currentText;

        for (int i = 0; i < 100; i++) {
            count += i;
            currentText = phrases.get(random.nextInt(phrases.size()));
            eventBus.post(new CountEvent(i));
            eventBus.post(new StringEvent(currentText));
            assertEquals(count, subscriber.counter);
            assertEquals(currentText, subscriber.receivedMessage);
        }
    }


    /**
     * A subscriber that listens for events on the event bus and updates its state accordingly.
     */
    private class EventSubscriber {

        //Indicates whether the subscriber has received an event or not.//
        boolean hasReceivedEvent;

        //The sum of all count events received by the subscriber.//
        int counter;

        // The string message received by the subscriber.//
        String receivedMessage;

        /**
         * Constructs a new event subscriber and registers it with the event bus.
         */
        private EventSubscriber() {
            hasReceivedEvent = false;
            counter = 0;
            receivedMessage = "";
            eventBus.register(this);
        }

        /**
         * Handles string events received by the subscriber.
         * @param event the string event received by the subscriber
         */
        @Subscribe
        public void stringListener(StringEvent event) {
            receivedMessage = event.string;
            hasReceivedEvent = true;
        }


    /**
     * Handles count events received by the subscriber.
     * @param event the count event received by the subscriber
     */
        @Subscribe
        public void countListener(CountEvent event) {
            counter += event.count;
            hasReceivedEvent = true;
        }
    }


    /* An event that contains a string message.*/
    private record StringEvent(String string) implements Event { }

    /* An event that contains a count value. */
    private record CountEvent(int count) implements Event { }



}
