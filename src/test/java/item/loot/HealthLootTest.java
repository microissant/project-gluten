package item.loot;

import components.health.Healer;
import components.health.IHealable;
import entities.player.PlayerCharacter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

public class HealthLootTest {
    private ILooter mockLooter;
    private IHealable mockHealable;
    private Healer healer;
    private PlayerCharacter mockPlayer;

    @BeforeEach
    public void setUp() {
        mockLooter = Mockito.mock(ILooter.class);
        mockHealable = Mockito.mock(IHealable.class);
        mockPlayer = Mockito.mock(PlayerCharacter.class);
    }

    /**
     * Tests for the constructor with a single heal value
     */
    @Test
    public void testConstructorWithHealValue() {
        HealthLoot loot = new HealthLoot(7);
        when(mockPlayer.getHealable()).thenReturn(mockHealable);
        mockPlayer.getHealable().heal(healer);
        assertNotNull(loot);
    }

    /**
     * Tests for the obtain method
     */
    @Test
    public void testObtain() {
        HealthLoot loot = new HealthLoot(7);
        when(mockLooter.getHealable()).thenReturn(mockHealable);

        loot.obtain(mockLooter);

        verify(mockHealable).heal(any(Healer.class));
    }

    /**
     * Tests for the obtain method with null looter
     */
    @Test
    public void testObtainWithNullLooter() {
        HealthLoot loot = new HealthLoot(7);

        loot.obtain(null);

        verify(mockHealable, never()).heal(any(Healer.class));
    }

    /**
     * Tests for the obtain method with null healable
     */
    @Test
    public void testObtainWithNullHealable() {
        HealthLoot loot = new HealthLoot(7);
        when(mockLooter.getHealable()).thenReturn(null);

        loot.obtain(mockLooter);

        verify(mockHealable, never()).heal(any(Healer.class));
    }

    /**
     * Tests for the displayTitle method
     */
    @Test
    public void testDisplayTitle() {
        HealthLoot loot = new HealthLoot(7);

        String title = loot.displayTitle();

        assertEquals("+7 health", title);
    }

    /**
     * Tests for the displayDescription method
     */
    @Test
    public void testDisplayDescription() {
        HealthLoot loot = new HealthLoot(7);

        String description = loot.displayDescription();

        assertEquals(null, description);
    }

}
