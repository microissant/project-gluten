package item.chest;

import com.badlogic.gdx.math.Vector2;
import components.collision.ICollidable;
import components.currency.ITrader;
import item.InteractAgent;
import item.loot.ILoot;
import model.event.EventBus;
import model.event.events.collision.RemoveCollisionCircleEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


/**
 * Test class for the Chest class.
 */
public class ChestTest {

    private Chest chest;
    private EventBus eventBus;

    /**
     * Set up method executed before each test case.
     */
    @BeforeEach
    public void setUp() {
        Vector2 position = new Vector2(1, 1);
        LootTable lootTable = mock(LootTable.class);
        int lootCount = 3;
        int cost = 5;
        eventBus = mock(EventBus.class);

        chest = new Chest(position, lootTable, lootCount, cost, eventBus);
    }

    /**
     * Test that the getCollisionPrimitive() method returns a non-null value.
     */
    @Test
    public void testGetCollisionPrimitive() {
        assertNotNull(chest.getCollisionPrimitive());
    }

    /**
     * Test that the isDead() method returns false.
     */
    @Test
    public void testIsDead() {
        assertFalse(chest.isDead());
    }

    /**
     * Test that the getPosition() method returns the correct position.
     */
    @Test
    public void testGetPosition() {
        assertEquals(1, chest.getPosition().x, 0);
        assertEquals(1, chest.getPosition().y, 0);
    }

    /**
     * Test that the setPosition() method sets the position correctly.
     */
    @Test
    public void testSetPosition() {
        Vector2 newPos = new Vector2(3, 3);
        chest.setPosition(newPos);
        assertEquals(3, chest.getPosition().x, 0);
        assertEquals(3, chest.getPosition().y, 0);
    }

    /**
     * Test that the getCost() method returns the correct cost.
     */
    @Test
    public void testGetCost() {
        assertEquals(5, chest.getCost().getValue());
    }

    /**
     * Test that the isLocked() method returns true when the chest is locked.
     */
    @Test
    public void testIsLocked() {
        assertTrue(chest.isLocked());
    }

    /**
     * Test that the onInteract() method unlocks the chest and sets the cost to null.
     */
    @Test
    public void testOnInteractUnlock() {
        ITrader trader = mock(ITrader.class);
        when(trader.canPurchase(any())).thenReturn(true);

        InteractAgent agent = mock(InteractAgent.class);
        when(agent.getTrader()).thenReturn(trader);

        chest.onInteract(agent);

        assertFalse(chest.isLocked());
        assertNull(chest.getCost());
    }

    /**
     * Test that the onInteract() method unlocks the chest when the trader can afford the cost.
     */
    @Test
    public void testOnInteract() {
        InteractAgent agent = mock(InteractAgent.class);
        ITrader trader = mock(ITrader.class);
        when(agent.getTrader()).thenReturn(trader);
        when(trader.canPurchase(any())).thenReturn(true);

        chest.onInteract(agent);

        assertFalse(chest.isLocked());
    }

    /**
     * Test that the collisionEnter() method sets the interact target for the agent.
     */
    @Test
    public void testCollisionEnter() {
        ICollidable target = mock(ICollidable.class);
        InteractAgent agent = mock(InteractAgent.class);
        when(target.getInteractAgent()).thenReturn(agent);

        chest.collisionEnter(target, null);

        verify(agent).setInteractTarget(chest);
    }

    /**
     * Test that the collisionLeave() method removes the interact target for the agent.
     */
    @Test
    public void testCollisionLeave() {
        ICollidable target = mock(ICollidable.class);
        InteractAgent agent = mock(InteractAgent.class);
        when(target.getInteractAgent()).thenReturn(agent);

        chest.collisionLeave(target);

        verify(agent).removeInteractTarget();
    }

    /**
     * Test that the getItems() method returns null when the chest is locked.
     */
    @Test
    public void testGetItemsWhenLocked() {
        assertNull(chest.getItems());
    }

    /**
     * Test that the looted() method removes the chest from the event
     * bus and removes the interact target for the agent.
     */
    @Test
    public void testLooted() {
        InteractAgent agent = mock(InteractAgent.class);
        chest.onInteract(agent); // Unlock the chest

        chest.looted();

        verify(eventBus).post(any(RemoveCollisionCircleEvent.class));
        verify(agent).removeInteractTarget();
    }

    /**
     * Test that the interactPromptText() method returns the correct
     * prompt when the chest is locked.
     */
    @Test
    public void testInteractPromptTextWhenLocked() {
        assertEquals("Unlock chest", chest.interactPromptText());
    }

    /**
     * Test that the unregisterFromEventBus() method unregisters
     * the chest from the event bus.
     */
    @Test
    public void testUnregisterFromEventBus() {
        chest.unregisterFromEventBus();

        verify(eventBus).unregister(chest);
    }

    /**
     * Test for the getItems() method when the chest is unlocked and contains loot.
     * Expects the method to return a list of loot items that matches the expected list.
     */
    @Test
    public void testGetItemsWhenUnlocked() {
        InteractAgent agent = mock(InteractAgent.class);
        ITrader trader = mock(ITrader.class);
        when(agent.getTrader()).thenReturn(trader);
        when(trader.canPurchase(any())).thenReturn(true);

        List<ILoot> expectedItems = Arrays.asList(mock(ILoot.class), mock(ILoot.class), mock(ILoot.class));
        LootTable lootTable = mock(LootTable.class);
        when(lootTable.getItems(anyInt())).thenReturn(expectedItems);

        Chest chest = new Chest(new Vector2(1, 1), lootTable, 3, 5, mock(EventBus.class));
        chest.onInteract(agent); // unlock chest

        List<ILoot> actualItems = chest.getItems();

        assertEquals(expectedItems.size(), actualItems.size());
        assertTrue(actualItems.containsAll(expectedItems));
    }

    /**
     * Test for the getItems() method when the chest is unlocked but contains no loot.
     * Expects the method to return an empty list.
     */
    @Test
    public void testGetItemsWhenUnlockedAndNoLoot() {
        InteractAgent agent = mock(InteractAgent.class);
        ITrader trader = mock(ITrader.class);
        when(agent.getTrader()).thenReturn(trader);
        when(trader.canPurchase(any())).thenReturn(true);

        List<ILoot> expectedItems = Collections.emptyList();
        LootTable lootTable = mock(LootTable.class);
        when(lootTable.getItems(anyInt())).thenReturn(expectedItems);

        Chest chest = new Chest(new Vector2(1, 1), lootTable, 0, 5, mock(EventBus.class));
        chest.onInteract(agent); // unlock chest

        List<ILoot> actualItems = chest.getItems();

        assertTrue(actualItems.isEmpty());
        assertTrue(actualItems.containsAll(expectedItems));
    }

     /**
     * Test for the getItems() method when the trader cannot afford the chest cost.
     * Expects the method to return null.
     * */
    @Test
    public void testGetItemsWhenNotEnoughMoney() {
        InteractAgent agent = mock(InteractAgent.class);
        ITrader trader = mock(ITrader.class);
        when(agent.getTrader()).thenReturn(trader);
        when(trader.canPurchase(any())).thenReturn(false);

        List<ILoot> expectedItems = Collections.emptyList();
        LootTable lootTable = mock(LootTable.class);
        when(lootTable.getItems(anyInt())).thenReturn(expectedItems);

        Chest chest = new Chest(new Vector2(1, 1), lootTable, 3, 5, mock(EventBus.class));
        chest.onInteract(agent); // chest should not be unlocked

        List<ILoot> actualItems = chest.getItems();

        assertNull(actualItems);
    }


}