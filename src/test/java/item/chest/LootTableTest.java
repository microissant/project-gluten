package item.chest;

import item.loot.ILoot;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class LootTableTest {

    private LootTable lootTable;

    @BeforeEach
    void setUp() {
        HashMap<ILoot, Integer> table = new HashMap<>();
        table.put(mock(ILoot.class), 1);
        table.put(mock(ILoot.class), 2);
        table.put(mock(ILoot.class), 3);
        lootTable = new LootTable(table);
    }

    /**
     * Tests that {@link LootTable#getItems(int)} returns an
     * empty list when the table is empty.
     */
    @Test
    void testGetItemsWhenEmptyTable() {
        HashMap<ILoot, Integer> emptyTable = new HashMap<>();
        lootTable = new LootTable(emptyTable);
        List<ILoot> items = lootTable.getItems(2);
        assertEquals(0, items.size());
    }

    /**
     * Tests that {@link LootTable#getItems(int)} returns a list of the
     * specified number of items when the number of items is less
     * than the size of the table.
     */
    @Test
    void testGetItemsWhenNumberOfItemsLessThanTableSize() {
        List<ILoot> items = lootTable.getItems(1);
        assertEquals(1, items.size());
    }

    /**
     * Tests that {@link LootTable#getItems(int)} returns a list
     * of the size of the table when the number of items is greater
     * than the size of the table.
     */
    @Test
    void testGetItemsWhenNumberOfItemsGreaterThanTableSize() {
        List<ILoot> items = lootTable.getItems(10);
        assertEquals(3, items.size());
    }
}

