package util;

import com.badlogic.gdx.math.Vector2;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MathHelperTest {


    void assertComplexProductResult(Vector2 a, Vector2 b, Vector2 result) {
        //check to see if the angle is the sum of the angles
        assertEquals((a.angleDeg() + b.angleDeg() ) % 360, result.angleDeg(),0.1);
        //check to see if the distance is the product of the distances
        assertEquals(a.len() * b.len(), result.len(),0.1);
    }

    /**

     Asserts that the complex product of two complex numbers is correct.
     @param aReal the real part of the first complex number
     @param aImaginary the imaginary part of the first complex number
     @param bReal the real part of the second complex number
     @param bImaginary the imaginary part of the second complex number
     */
    @CsvSource(value = {"1,2,-3,2","-3,2,1,2","-3,-3,2,2","-5,-2,-7,-1"})
    @ParameterizedTest(name = "{0}+{1}i * {2}+{3}i")
    void testComplexProduct(float aReal, float aImaginary, float bReal, float bImaginary) {
        Vector2 result = new Vector2();
        Vector2 a = new Vector2(aReal, aImaginary);
        Vector2 b = new Vector2(bReal, bImaginary);
        result.set(MathHelper.complexProduct(a,b));
        assertComplexProductResult(a, b, result);
    }


    /**
     Tests the smoothStep function of the MathHelper class.
     @param input the input value for the smoothStep function
     */

    //make sure that the function doesnt return a value outside of the range [0,1]
    @CsvSource(value = {"0","0.3","0.9","1.0","2.0","-3.0"})
    @ParameterizedTest(name = "input: {0}")
    void testSmoothStep(float input) {

        float result =MathHelper.smoothStep( input); 
        assertTrue(result <= 1.0 );
        assertTrue(result >= 0.0 );
    }
}
