package util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for the DoOnce utility class.
 */
public class DoOnceTest {

    private DoOnce doOnce;
    private int operationCounter;

    @BeforeEach
    public void setup() {
        operationCounter = 0;
        doOnce = new DoOnce(() -> operationCounter++);
    }

    /**
     * Test that the operation is executed only once.
     */
    @Test
    public void testExecute() {
        doOnce.execute();
        doOnce.execute();
        assertEquals(1, operationCounter, "Operation should be executed only once");
    }

    /**
     * Test that the reset method allows the operation to be executed again.
     */
    @Test
    public void testReset() {
        doOnce.execute();
        doOnce.reset();
        doOnce.execute();
        assertEquals(2, operationCounter, "Operation should be executed twice after reset");
    }

    /**
     * Test the hasExecuted method.
     */
    @Test
    public void testHasExecuted() {
        assertFalse(doOnce.hasExecuted(), "hasExecuted should return false before execution");
        doOnce.execute();
        assertTrue(doOnce.hasExecuted(), "hasExecuted should return true after execution");
    }
}
