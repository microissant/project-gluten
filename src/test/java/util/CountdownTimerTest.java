package util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for the CountDownTimer.
 */
class CountdownTimerTest {

    private CountdownTimer timer;

    /**
     * Sets up a new CountDownTimer instance with a maxTime of 2 seconds before each test.
     */
    @BeforeEach
    void setUp() {
        timer = new CountdownTimer(2.0f, System.out::println);
    }

    /**
     * Tests the initial state of the timer. Should not be active and not complete
     */
    @Test
    void testInitialState() {
        assertFalse(timer.isActive());
        assertFalse(timer.isComplete());
    }

    /**
     * Tests starting the timer. Should set timer to active
     */
    @Test
    void testStart() {
        timer.start();
        assertTrue(timer.isActive());
    }

    /**
     * Tests stopping the timer. Should set timer to inactive
     */
    @Test
    void testStop() {
        timer.start();
        timer.stop();
        assertFalse(timer.isActive());
    }

    /**
     * Tests restarting the timer.
     */
    @Test
    void testRestart() {
        timer.start();
        timer.update(1.0);
        timer.restart();
        assertEquals(0.0, timer.getCurrentTime(), 0.001);
        assertTrue(timer.isActive());
    }

    /**
     * Tests resetting the timer. Should set current time to 0, and be active
     */
    @Test
    void testReset() {
        timer.start();
        timer.update(1.0);
        timer.reset();
        assertEquals(0.0, timer.getCurrentTime(), 0.001);
        assertFalse(timer.isActive());
    }

    /**
     * Tests updating the timer. Should update the current time to deltaTime
     */
    @Test
    void testUpdate() {
        timer.start();
        timer.update(1.0);
        assertEquals(1.0, timer.getCurrentTime(), 0.001);
    }

    /**
     * Tests checking if the timer is complete only when time has reached maxTime.
     */
    @Test
    void testIsComplete() {
        timer.start();
        timer.update(1.0);
        assertFalse(timer.isComplete());
        timer.update(1.0);
        assertTrue(timer.isComplete());
    }
}
