package media;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import entities.player.PlayerManager;
import gdx.GdxTest;
import model.event.EventBus;
import model.event.events.audio.PlaySoundEvent;
import model.event.events.audio.StopSoundEvent;
import model.event.events.entities.PlayerDeathEvent;
import model.event.events.game.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

/**
 * This class tests the functionality of the AudioPlayer class.
 */

public class AudioPlayerTest extends GdxTest {

    private AudioPlayer audioPlayer;
    private EventBus eventBus;


    /**
     * Sets up the necessary objects for testing.
     * @throws NoSuchFieldException if a field is not found
     * @throws IllegalAccessException if an illegal access is attempted
     */
    @BeforeEach
    public void setup() throws NoSuchFieldException, IllegalAccessException {
        super.setUp();
        eventBus = mock(EventBus.class);

        audioPlayer = new AudioPlayer(eventBus);

        setPrivateField(audioPlayer, "musicLevel", mock(Music.class));
        setPrivateField(audioPlayer, "musicPlayerDeath", mock(Music.class));
    }


    /* Tests if the new game event starts playing the level music. */
    @Test
    public void testNewGameEvent() {
        NewGameEvent event = new NewGameEvent();

        audioPlayer.newGameEvent(event);

        verify(audioPlayer.musicLevel).play();
    }


    /* Tests if the level end event stops the level music. */
    @Test
    public void testEndLevelEvent() {
        EndLevelEvent event = new EndLevelEvent();

        audioPlayer.levelEnd(event);

        verify(audioPlayer.musicLevel).stop();
    }

    /* Tests if the player death event stops the level music and starts the player death music. */
    @Test
    public void testPlayerDeathEvent() {
        PlayerDeathEvent event = new PlayerDeathEvent(mock(PlayerManager.class));

        audioPlayer.playerDeath(event);

        verify(audioPlayer.musicLevel).stop();
        verify(audioPlayer.musicPlayerDeath).play();
    }

    private void setPrivateField(Object target, String fieldName, Object value) throws NoSuchFieldException, IllegalAccessException {
        Field field = target.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        field.set(target, value);
    }


    /* Tests if the load next level event starts playing the level music. */
    @Test
    public void testLoadNextLevelEvent() {
        LoadNextLevelEvent event = new LoadNextLevelEvent();

        audioPlayer.nextLevelEvent(event);

        verify(audioPlayer.musicLevel).play();
    }


    /* Tests if the restart level event starts playing the level music. */
    @Test
    public void testRestartLevelEvent() {
        RestartLevelEvent event = new RestartLevelEvent();

        audioPlayer.restartLevel(event);

        verify(audioPlayer.musicLevel).play();
    }


    /**
     * Tests if the main menu event starts playing the main menu music.
     * @throws NoSuchFieldException if a field is not found
     * @throws IllegalAccessException if an illegal access is attempted
     */
    @Test
    public void testMainMenuEvent() throws NoSuchFieldException, IllegalAccessException {
        Music musicMainMock = mock(Music.class);
        setPrivateField(audioPlayer, "musicMain", musicMainMock);

        MainMenuActiveEvent mainMenuActiveEvent = new MainMenuActiveEvent();

        audioPlayer.mainMenu(mainMenuActiveEvent);

        verify(musicMainMock).play();
    }

    /**
     * Tests if the pause game event pauses all active sounds.
     * @throws NoSuchFieldException if a field is not found
     * @throws IllegalAccessException if an illegal access is attempted
     */
    public void testPauseGameEvent() throws NoSuchFieldException, IllegalAccessException {
        Sound soundMock = mock(Sound.class);
        Map<SoundID, Sound> soundMap = new HashMap<>();
        soundMap.put(SoundID.PLAYER_FOOTSTEP, soundMock);

        // Set the private field activeSounds using reflection
        setPrivateField(audioPlayer, "activeSounds", soundMap);

        PauseGameEvent pauseGameEvent = new PauseGameEvent();
        audioPlayer.pauseGame(pauseGameEvent);

        // Verify that the pause method was called on the sound mock
        verify(soundMock).pause();
    }

    @Test
    public void testPlaySoundWithLoopAddsToActiveSounds() throws NoSuchFieldException, IllegalAccessException {
        Map<SoundID, Sound> soundMap = new HashMap<>();
        setPrivateField(audioPlayer, "activeSounds", soundMap);

        PlaySoundEvent playSoundEvent = new PlaySoundEvent(SoundID.PLAYER_FOOTSTEP, true);
        audioPlayer.playSound(playSoundEvent);

        assertEquals(1, soundMap.size());
    }

    /**
     * Tests that when a sound is played with loop, it is added to the list of active sounds.
     *
     * @throws NoSuchFieldException if the named field is not found
     * @throws IllegalAccessException if the named field is not accessible
     */
    @Test
    public void testPlaySoundWithoutLoopDoesNotAddToActive() throws NoSuchFieldException, IllegalAccessException {
        Map<SoundID, Sound> soundMap = new HashMap<>();
        setPrivateField(audioPlayer, "activeSounds", soundMap);

        PlaySoundEvent playSoundEvent = new PlaySoundEvent(SoundID.PLAYER_FOOTSTEP);
        audioPlayer.playSound(playSoundEvent);

        assertTrue(soundMap.isEmpty());
    }


    /**
     * Tests that stopping a sound affects all active sounds.
     * @throws NoSuchFieldException if the named field is not found
     * @throws IllegalAccessException if the named field is not accessible
     */
    @Test
    public void testStopSoundAffectsAllActiveSounds() throws NoSuchFieldException, IllegalAccessException {
        Sound soundMock = mock(Sound.class);
        Map<SoundID, Sound> soundMap = new HashMap<>();
        soundMap.put(SoundID.PLAYER_FOOTSTEP, soundMock);

        // Set the private field activeSounds using reflection
        setPrivateField(audioPlayer, "activeSounds", soundMap);

        StopSoundEvent stopSoundEvent = new StopSoundEvent(SoundID.PLAYER_FOOTSTEP);
        audioPlayer.stopSound(stopSoundEvent);

        // Verify that the pause method was called on the sound mock
        verify(soundMock).stop();
    }



    /**
     * Tests that disposing the audio player affects all music.
     * @throws NoSuchFieldException if the named field is not found
     * @throws IllegalAccessException if the named field is not accessible
     */
    public void testDisposeAffectsAllMusic() throws NoSuchFieldException, IllegalAccessException {
        Music mainMusic = mock(Music.class);
        Music pauseMusic = mock(Music.class);
        Music levelMusic = mock(Music.class);
        Music playerDeathMusic = mock(Music.class);

        setPrivateField(audioPlayer, "musicMain", mainMusic);
        setPrivateField(audioPlayer, "musicPause", pauseMusic);
        setPrivateField(audioPlayer, "musicLevel", levelMusic);
        setPrivateField(audioPlayer, "musicPlayerDeath", playerDeathMusic);

        audioPlayer.dispose();

        verify(mainMusic, times(1)).dispose();
        verify(pauseMusic, times(1)).dispose();
        verify(levelMusic, times(1)).dispose();
        verify(playerDeathMusic, times(1)).dispose();
    }
}
