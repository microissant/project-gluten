# Rollefordeling

### Teamleder -Ståle Karlstad 
Overser at oppgaver er delegert og gjort.
### Arkitekt og Git-ansvarlig - Thomas Alme Matre 
Har overordnet ansvar for prosjektets arkitektur, godkjenner merge requests og har oversikt over GitLab-oppsettet
### Testingansvarlig - Ole Birketvedt 
Ser til at tilstrekkelige tester er skrevet og dekker behovet
### Kundekontakt - Shahnaz Shaheen al Mhethawy 
Ser til at kravene til kunden er opprettholdt
### Designansvarlig - Martin Gabriel Westerheim Gundersen 
Ser til at planlagte mekanismer og konsepter er implementert som forestilt

Med disse rollene som utgangspunkt ønsker vi å dekke behovene til prosjektet. Vi er åpne for at roller og beskrivelser kan endre seg.
# A2 - konsept

*Verden:*
* En todimensjonal gridbasert verden som ses ovenfra- og ned 
* Vertikal og horisontal scrolling med kamera som beveger seg med spilleren.
* Vegger som karakteren ikke kan gå gjennom og som i visse tilfeller kan knuses. 
* Spilleren beveger seg i alle retninger der det ikke finnes hindre. 
* Små isolerte nivåer med distinkte estetiske og mekaniske forskjeller. 
* Lineært historieforløp. Et område er stengt når du har forlatt det. 

*Fiender:* 
* Forskjellige atferdsmønstre og egenskaper. (Range, assault, flying)
* Noen elementer påfører skade ved kontakt med spilleren 
* Modulære specs (HP-, hastighets- og angreps-stats varierer)
* Hovedboss som må bekjempes for å vinne spillet

*Items:* 
* Spiller samler opp penger i løpet av spillet. Disse kan omsettes i forskjellige nyttige items/våpen som påvirker egenskaper og strategi

*Spiller:* 
* Har stats og mulighet til å bytte våpen 
* Diskrét HP (f. eks. tre hjerter til å begynne med)
* Kan gå, angripe, plukke opp items, blokke(?)

# A3.1
* Vi tenker å starte med et klassediagram for å tydeliggjøre hvilke klasser spillet burde bestå av. Dette vil gjøre arbeidsfordeling og TDD-prosessen enklere. 
* Vi tenker å bruke GitLab-wikien til å beskrive visse tekniske aspekter av prosjektet.
* Vi møtes fysisk hver onsdag og til hver gruppetime på fredag.
* Vi holder kontakt på Discord for lavterskel og rask kommunikasjon.
* Vi tar utgangspunkt i en Scrum-metodikk via GitLab Issue Board for å gi oss selv oversiktlige, overkommelige og forutsigbare arbeidsoppgaver.
* Vi jobber i feature branches og legger disse inn i merge requests.
* Ved møter har vi et slags “stand up meeting” hvor vi gir hverandre en oppsummering av hva hver enkelt har jobbet med, eventuelle utfordringer og hva vi gjør den neste tiden.
* Vi har en delt mappe i Google Drive for å dele filer som ikke skal direkte inn i Git-prosjektet
# A3.2

1. Vise et spillebrett
2. Vise spiller på spillebrett
3. Flytte spiller (vha taster e.l.)
4. Spiller interagerer med terreng
5. Vise fiender/monstre; de skal interagere med terreng og spiller
6. Spiller kan bruke våpen til å skade fiender
7. Spiller kan dø (ved kontakt med fiender, eller ved å falle utfor skjermen)
8. Mål for spillbrett (enten et sted, en mengde poeng, drepe alle fiender e.l.)
9. Nytt spillbrett når forrige er ferdig
10. Start-skjerm ved oppstart / game over
11. Spiller kan skyte i den retningen hen løper ved å trykke på en designert tast


# Brukerhistorier

### Som newbie gamer:
Ønsker jeg at konsepter skal introduseres på en forståelig og intuitiv måte
slik at jeg skjønner hva som kreves av meg for å oppnå progresjon.
Gitt at jeg spiller for første gang, vil jeg vite hva spillet går ut på og hvor jeg skal bevege meg

### Som hardcore gamer:
Ønsker jeg en utfordrende spillopplevelse i form av høy vanskelighetsgrad
slik at jeg ikke kjeder meg fordi det er for lett.
Gitt at jeg skal slåss mot en boss, vil jeg kunne velge bossens vanskelighetsgrad

### Som en person med fargeblindhet:
Ønsker jeg at spillet legger til rette for at jeg kan kunne forstå visuelt alt som skjer på skjermen
slik at jeg kan spille riktig uten å måtte stole utelukkende på fargeinformasjon.
Gitt at fienden og jeg skyter samtidig, vil jeg kunne skille disse fra hverandre

### Som en person med nedsatt motorikk:
Ønsker jeg assistanse fra spillet som likestiller meg med andre spillere 
slik at jeg ikke trenger perfekt presisjon med mus.
Gitt at jeg bruker kun én hånd, vil jeg kunne fullføre spillet

### Som person med nedsatt hørsel:
Ønsker jeg at spillet ikke baserer seg for tungt på lyd
slik at jeg kan fullføre det uten lyd.
Gitt at jeg vil skille mellom to fiender, vil jeg kunne gjøre dette basert på kriterier som ikke bare er audio

### Som barn eller forelder til spillende barn:
Ønsker jeg en aldersgrense og at spillet har motiver og mekanismer som egner seg for alderen
slik at barnet ikke blir eksponert for traumatiserende innhold.
Gitt at jeg leser om spillet før jeg spiller, vil jeg få informasjon om anbefalt aldersgrense

# A4: Kode
* Vi har bestemt oss for å bruke libGDX som rammeverk og tar utgangspunkt i skjelettet lagt ut av Anya. Vi har prøvd oss på å gjøre ulike småting, som å tegne og animere sirkler.

# A5: Oppsummering
Det er foreløpig ikke så mye å oppsummere, men vi opplever at vi har klaffet godt som team, brukt tiden effektivt ved møter og alle har vært engasjert i planlegging og idéskapning. Vi har begynt å bli kjent med verktøy som skal brukes fremover og akter å investere mye tid og innsats i læreprosessen.

Ved tekniske uklarheter i GitLab, har vi løst disse uten friksjon og med hjelp av god dialog.
