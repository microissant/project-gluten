# Prosjektrapport oblig 3

## Rollene
Vi har opplevd at de "offisielt" tildelte rollene ikke har vært så gjeldende i praksis. Utifra denne erfaringen har vi valgt å gjøre noen endringer i rollene slik at medlemmene får den rollen de føler de er best tilpasset.

### Teamleder - Thomas
Har overordnet ansvar for kodearkitektur. Overser at arbeidsoppgaver er delegert og gjort.

### Spilldesignansvarlig - Martin
Har ansvar for å definere og utforme spillopplevelsen.

### Sekretær - Ståle
Har ansvar for rapporter, rombooking og andre administrative oppgaver.

### Gitansvarlig - Ole
Har ansvar for det administrative ved GitLab.

### Kundekontakt - Shahnaz
Representerer kunden og ser til at kravene til kunden er opprettholdt.

## Metodikk
Siden forrige innlevering har vi delvis klart å ha tydeligere og mindre mål for hver uke (sprint) samt å jobbe i par. Dette har gjort det lettere for de med mindre erfaring å bidra. Vi burde ha begynt med parprogrammering tidligere, da det har gitt et bedre teamforhold og en bedre forståelse for hvordan kildekoden henger sammen. Vi burde også ha begynt med arbeidsoppgavene sammen som et team samme dag de ble tildelt for å sørge for at vi forstod hva og hvordan disse skulle gjøres. De totimers onsdagsmøtene burde vi derfor ha utvidet til det dobbelte samt utført i et egnet rom istedenfor fellesareal.

Vi har jobbet mer tidsmessig jevnt for å slippe skippertak før innlevering.

Til forrige innlevering bemerket vi en ubalanse i antall commits fra teammedlemmene. Dette har vi jobbet med å utligne ved å jobbe mer sammen og i par samt å tildele mer fornuftige arbeidsoppgaver, men her har vi ytterligere forbedringspotensiale.

## Dynamikk og kommunikasjon
Teamet har fortsatt å jobbe godt sammen. Etter forrige innlevering hadde vi et ønske om å forbedre kommunikasjonen mellom hvert møte, og dette har vi klart. Ved forslag til forbedring og annet har dette skjedd på en høflig og saklig måte.

# Krav og spesifikasjon
Samtlige punkter i den opprinnelige MVP-spesifikasjonen er på plass i kildekoden og i spillet. Med et solid og modulært fundament er det nå enklere å legge til flere funksjoner og mekanismer i spillet. Noen av hovedpunktene som er implementert er:

* Kollisjonsdeteksjon
* Tilfeldig genererte nivåer
* State machine til fiender
* Game state
* Pathfinding
* EventBus for å håndtere meldinger mellom komponenter
* Animasjoner
* Musikk og lyder
* Hovedskjerm og pausemeny
* HUD
* Nytt våpen: raketter
* Skyting med både mus og tastatur
* Kister med kjøpbare oppgraderinger
* Forbedret dokumentasjon
* Flere kodetester

## MVP

1. Vise et spillebrett
2. Vise spiller på spillebrett
3. Flytte spiller (ved hjelp av taster)
4. Spiller interagerer med terreng
5. Vise fiender; de skal interagere med terreng og spiller
6. Spiller kan bruke våpen til å skade fiender
7. Spiller kan dø (ved å bli skutt av fiender, eller ved å falle i hull)
8. Mål for spillbrett (drepe alle fiender og rømme via portal)
9. Nytt spillbrett når forrige er ferdig
10. Start-skjerm ved oppstart / game over
11. Spiller kan skyte i den retningen hen løper ved å trykke på en designert tast

# Brukerhistorier

Vi planlegger å prioritere brukerhistoriekravene i ca. følgende rekkefølge:

## Som newbie gamer:
Ønsker jeg at konsepter skal introduseres på en forståelig og intuitiv måte slik at jeg skjønner hva som kreves av meg for å oppnå progresjon.

| Akseptansekriterier | Arbeidsoppgaver |
| :------------------ | :-------------- |
| Gitt at jeg spiller for første gang, vil jeg vite hva spillet går ut på, hvor jeg skal bevege meg og at spillet ikke er for vanskelig med én gang | ✅ Skrive en kort introduksjon til spillet og dets mekanismer i README.md <br> |
| Gitt at jeg spiller for første gang, vil jeg vite hvordan jeg beveger meg | ✅ Skrive kontroller i README.md<br>⏳ Vise kontroller på hovedskjermen |

## Som spiller som liker variasjon:
Ønsker jeg at spillet har ulike våpen, pick-ups, oppgraderinger og fiender slik at jeg kan oppleve variasjon og unngå kjedsomhet.

| Akseptansekriterier | Arbeidsoppgaver |
| :------------------ | :-------------- |
| Gitt at jeg begynner et nivå, vil jeg ha variasjon i våpen, pick-ups, oppgraderinger og fiender | ✅ Plassere kister med oppgraderinger som kan oppdages<br>⏳ Utforme og plassere nye fiendetyper<br>⏳ Utforme flere våpentyper |

## Som spiller som liker å utforske:
Ønsker jeg at spillet inneholder hemmelige områder og skjulte gjenstander slik at jeg kan bruke tid på å oppdage alle spillverdenens kriker og kroker.

| Akseptansekriterier | Arbeidsoppgaver |
| :------------------ | :-------------- |
| Gitt at jeg utforsker et nivå, vil jeg kunne finne skjulte steder og belønninger | ✅ Plassere kister med oppgraderinger som kan oppdages<br>⏳ Plassere skjulte rom som kan oppdages og åpnes med sprengstoff |

## Som hardcore gamer:
Ønsker jeg en utfordrende spillopplevelse i form av høy vanskelighetsgrad slik at jeg ikke kjeder meg fordi det er for lett.

| Akseptansekriterier | Arbeidsoppgaver |
| :------------------ | :-------------- |
| Gitt at jeg kommer videre i spillet, ønsker jeg at vanskelighetsgraden øker | ⏳ Øke vanskelighetsgraden for hvert nivå, f. eks. med sterkere og flere fiender |

## Som en person med fargeblindhet:
Ønsker jeg at spillet legger til rette for at jeg kan kunne forstå visuelt alt som skjer på skjermen
slik at jeg kan spille riktig uten å måtte stole utelukkende på fargeinformasjon.

| Akseptansekriterier | Arbeidsoppgaver |
| :------------------ | :-------------- |
| Gitt at fienden og jeg skyter samtidig, vil jeg kunne skille disse fra hverandre | ⏳ Gjøre spillerens og fiendens prosjektiler visuelt atskillelige |

## Som en person med nedsatt motorikk:
Ønsker jeg assistanse fra spillet som likestiller meg med andre spillere slik at jeg ikke trenger perfekt presisjon med mus.

| Akseptansekriterier | Arbeidsoppgaver |
| :------------------ | :-------------- |
| Gitt at jeg ikke kan eller ønsker å bruke en mus, ønsker jeg å kunne skyte med tastaturet | ✅ Gjøre det mulig å skyte med tastaturet |

## Som person med nedsatt hørsel:
Ønsker jeg at spillet ikke baserer seg for tungt på lyd
slik at jeg kan fullføre det uten lyd.

| Akseptansekriterier | Arbeidsoppgaver |
| :------------------ | :-------------- |
| Gitt at jeg vil skille mellom to fiender, vil jeg kunne gjøre dette basert på kriterier som ikke bare er audio | ✅ Forsikre oss om at spillet kan nytes uten lyd |

## Som person som hører på musikk mens jeg spiller:
Ønsker jeg mulighet til å skru av spillets lyd.

| Akseptansekriterier | Arbeidsoppgaver |
| :------------------ | :-------------- |
| Gitt at jeg vil skru av spillets lyd og/eller, vil jeg kunne gjøre dette | ⏳ Legge til en mulighet for å justere spillets lyd i meny-/pauseskjermen |

## Som barn eller forelder til spillende barn:
Ønsker jeg en aldersgrense og at spillet har motiver og mekanismer som egner seg for alderen slik at barnet ikke blir eksponert for traumatiserende innhold.

| Akseptansekriterier | Arbeidsoppgaver |
| :------------------ | :-------------- |
| Gitt at jeg leser om spillet før jeg spiller, vil jeg få informasjon om anbefalt aldersgrense | ✅ Finne en passende aldersgrense for spillet <br> ✅ Dokumentere aldersgrensen i README.md |

# Produkt og kode
Etter tilbakemelding fra oblig 2, har vi nå fjernet ubrukt skjelettkode og lagt inn MVP-kravene, brukerhistoriene og akseptansekriteriene i dette dokumentet. Vi jobber videre med tester og dokumentasjon av kode.

Prosjektet kan bygges, testes og kjøres i Linux, Windows og OS X.