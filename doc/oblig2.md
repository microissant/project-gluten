# Prosjektrapport

Rollene
Vi synes rollene har fungert ok så langt. Vi merker at vi har jobbet med en litt løsere struktur enn hva rollene i utgangspunktet skulle tilsi, men det er ingen roller som har skilt seg ut som feil eller overflødige. ¨

Metodikk
Hva metodikk angår har vi nok delvis feilet i vårt mål om å følge scrum-fremgangsmåten. Etter det som var en ganske god start med oppretting av issue board og enighet om ukentlige frister har vi sklidd over i en litt mindre strukturert trend hvor folk har tatt oppgaver etter hvert som de dukker opp.
Dette skyldes nok i stor grad av tidvis mangel på kommunikasjon mellom møtene.

Dynamikk og kommuikasjon
Etter ukesvis med jobbing har vi erfart at teamet funker svært godt sosialt. Vi har to medlemmer med gode evner innen spillaging og tre stk som kan sies å være ny i faget. Alle vil bidra og det har ikke stått på arbeidslysten, men nykommerne burde kanskje være flinkere til å be om hjelp når vi blir forvirret.

Commits
Det har vært en overvekt av commiter fra Thomas og Martin. Dette skyldes eminent kompetanse hos dem og litt manglende kommunikasjon mellom møtene. Hvis Scrum hadde blitt praktisert optimalt ville vi kanskje vi ha delt oppgavene opp i mer overkommelige små-oppgaver, og dermed sett en jevnere fordeling av ferdiggjorte oppgaver

### Forbedringspunkter
- Følge scrum
- Prate mer sammen mellom møtene

# Krav og spesifikasjon

Vi har prioritert de aller fleste av mvp-kriteriene fra oblig1. Mye innsats har gått ned i å legge til rette for at grafikken som anvendes i spillet skal være på plass, slik at vi ikke nøyer oss med primitive representasjoner av brett og spiller. I skrivende øyeblikk er vi ikke noe særlig forbi mvp-statiet, noe som kan skyldes at det har vært mye tid brukt på spillets underliggende systemer og at disse skal samhandle på best mulig vis. Dette solide fundamentet vil forhåpentligvis lønne seg i tiden som kommer.






