# Prosjektrapport oblig 4

## Rollene

Ved forrige levering endret vi fordelingen og definisjonen av rollene. Vi har opplevd at denne rollefordelingen har fungert bra.

Det er verdt å merke seg at disse rollene er supplement utover de øvrige arbeidsoppgavene. Alle har også deltatt i programmering og prosjektarbeid.  

### Teamleder - Thomas
Har overordnet ansvar for kodearkitektur. Overser at arbeidsoppgaver er delegert og gjort.

### Spilldesignansvarlig - Martin
Har ansvar for å definere og utforme spillopplevelsen.

### Sekretær - Ståle
Har ansvar for rapporter, rombooking og fokuserer mye på testing.

### Gitansvarlig - Ole
Har ansvar for det administrative ved GitLab.

### Kundekontakt - Shahnaz
Representerer kunden og ser til at kravene til kunden er opprettholdt.

## Metodikk
Da vi startet på prosjektet var det ingen som hadde nevneverdig erfaring med samarbeidsprosjekter av dette kaliberet. Vi fant tidlig ut at scrum var noe som virket forlokkende og bestemte oss derfor for å bygge rutinene rundt dette. Målet var å møtes hver onsdag for kartlegging og planlegging, i tillegg til de oppsatte gruppetimene på fredager. De første ukene bar preg av tilvending og mangel på rutine. Vi brukte issueboardet på gitlab, men gikk i fellen å lage for omfattende og generelle issues. De medlemmene av teamet som ikke hadde tidligere spillmaker-erfaring slet med å komme i gang med sine designerte oppgaver og vi kommuniserte dårlig angående dette. De ukentlige fristene som vi satt for oss selv ble derfor alltid ikke overholdt. Vi fant etterhvert ut at parprogrammering var veien å gå for å forbedre den generelle arbeidsflyten. De siste ukene har vi funnet en bra rytme ved å møtes oftere og dele issuesene opp i mindre, overkommelige deler. Dette har båret frukter i form av flere committs, et kulere spill og en bedre lagfølelse. Alt i alt har vi ikke gjennomført scrum-metodikken slik vi hadde håpet på, men vi har lært mye om viktige prinsipper som må ligge i grunn for et inkluderende og effektivt gruppeprosjekt.


## Dynamikk og kommunikasjon
Kommunikasjonen har alltid vært respektfull, høflig og konstruktivt rettet. I begynnelsen havnet vi dog litt for ofte i sitasjoner hvor medlemmene av teamet jobbet med viktige implementasjoner for seg selv. Dette førte til at spillets retning var noe ukjent for dem som ikke jobbet med en viktig del av koden. Her burde vi ha tatt tak tidligere og aktivt organisert samtaler og diskusjoner rundt arkitektur og kodestil. Etterhvert som tiden gikk ble vi bedre kjent med hverandre og mer bevisst på viktigheten av god kommunikasjon. Vi har fortsatt i det gode sporet og hatt en god dialog de siste ukene. Gjengen har vært svært imøtekommende og fleksibel for å dra prosjektet i land. Det har vært behov for et par lange, felles programmeringsøkter utenom de vanlige tidene og vi har opplevd at organiseringen har gått knirkefritt.

Kompetanseforskjellene innad i teamet har dessverre resultert i en stor skjevfordeling i antall commits. Martin og Thomas har laget spill før og har derfor vært i stand til å programmere selvstendig, med en klar visjon om hva som burde implementeres og hvordan dette burde gjøres.
Koden har med tiden vokst seg større og mer komplisert enn hva som gjerne er vanlig for et INF112-prosjekt. Dette har gjort det vrient for dem uten spillerfaring å implementere løsninger uten veiledning. Som nevnt tidligere burde vi som team ha adressert dette fra starten av og organisert oss slik at viktige valg av designmønstre og generel arkitektur ble tydeliggjort i plenum.

# Krav og spesifikasjon
Samtlige punkter i den opprinnelige MVP-spesifikasjonen er på plass i kildekoden og i spillet. Med et solid og modulært fundament er det nå enklere å legge til flere funksjoner og mekanismer i spillet. Noen av hovedpunktene som er implementert er:

* Generell optimaliser av algoritmer/datastrukturer
* Flere våpentyper
* Flere fiendetyper
* Bosstyper
* Flere power-ups
* Meny instilligner for display, audio, controls
* Controls vises på hovedmeny
* Credits-skjerm
* Flere lydeffekter
* Optimalisering av kodestil
* Mer dokumentasjon
* Wiki
* Destructable walls
* Flere tester

## MVP

1. Vise et spillebrett
2. Vise spiller på spillebrett
3. Flytte spiller (ved hjelp av taster)
4. Spiller interagerer med terreng
5. Vise fiender; de skal interagere med terreng og spiller
6. Spiller kan bruke våpen til å skade fiender
7. Spiller kan dø (ved å bli skutt av fiender, eller ved å falle i hull)
8. Mål for spillbrett (drepe alle fiender og rømme via portal)
9. Nytt spillbrett når forrige er ferdig
10. Start-skjerm ved oppstart / game over
11. Spiller kan skyte i den retningen hen løper ved å trykke på en designert tast

# Brukerhistorier

Vi har prioritert brukerhistoriekravene i følgende rekkefølge:

## Som newbie gamer:
Ønsker jeg at konsepter skal introduseres på en forståelig og intuitiv måte slik at jeg skjønner hva som kreves av meg for å oppnå progresjon.

| Akseptansekriterier | Arbeidsoppgaver |
| :------------------ | :-------------- |
| Gitt at jeg spiller for første gang, vil jeg vite hva spillet går ut på, hvor jeg skal bevege meg og at spillet ikke er for vanskelig med én gang | ✅ Skrive en kort introduksjon til spillet og dets mekanismer i README.md <br> |
| Gitt at jeg spiller for første gang, vil jeg vite hvordan jeg beveger meg | ✅ Skrive kontroller i README.md<br>✅ Vise kontroller på hovedskjermen |

## Som spiller som liker variasjon:
Ønsker jeg at spillet har ulike våpen, pick-ups, oppgraderinger og fiender slik at jeg kan oppleve variasjon og unngå kjedsomhet.

| Akseptansekriterier | Arbeidsoppgaver                                                                                                             |
| :------------------ |:----------------------------------------------------------------------------------------------------------------------------|
| Gitt at jeg begynner et nivå, vil jeg ha variasjon i våpen, pick-ups, oppgraderinger og fiender | ✅ Plassere kister med oppgraderinger som kan oppdages<br>✅ Utforme og plassere nye fiendetyper<br>✅ Utforme flere våpentyper |

## Som spiller som liker å utforske:
Ønsker jeg at spillet inneholder hemmelige områder og skjulte gjenstander slik at jeg kan bruke tid på å oppdage alle spillverdenens kriker og kroker.

| Akseptansekriterier | Arbeidsoppgaver |
| :------------------ | :-------------- |
| Gitt at jeg utforsker et nivå, vil jeg kunne finne skjulte steder og belønninger | ✅ Plassere kister med oppgraderinger som kan oppdages<br>⏳ Plassere skjulte rom som kan oppdages og åpnes med sprengstoff |

## Som hardcore gamer:
Ønsker jeg en utfordrende spillopplevelse i form av høy vanskelighetsgrad slik at jeg ikke kjeder meg fordi det er for lett.

| Akseptansekriterier | Arbeidsoppgaver |
| :------------------ | :-------------- |
| Gitt at jeg kommer videre i spillet, ønsker jeg at vanskelighetsgraden øker | ⏳ Øke vanskelighetsgraden for hvert nivå, f. eks. med sterkere og flere fiender |

## Som en person med fargeblindhet:
Ønsker jeg at spillet legger til rette for at jeg kan kunne forstå visuelt alt som skjer på skjermen
slik at jeg kan spille riktig uten å måtte stole utelukkende på fargeinformasjon.

| Akseptansekriterier | Arbeidsoppgaver |
| :------------------ | :-------------- |
| Gitt at fienden og jeg skyter samtidig, vil jeg kunne skille disse fra hverandre | ⏳ Gjøre spillerens og fiendens prosjektiler visuelt atskillelige |

## Som en person med nedsatt motorikk:
Ønsker jeg assistanse fra spillet som likestiller meg med andre spillere slik at jeg ikke trenger perfekt presisjon med mus.

| Akseptansekriterier | Arbeidsoppgaver |
| :------------------ | :-------------- |
| Gitt at jeg ikke kan eller ønsker å bruke en mus, ønsker jeg å kunne skyte med tastaturet | ✅ Gjøre det mulig å skyte med tastaturet |

## Som person med nedsatt hørsel:
Ønsker jeg at spillet ikke baserer seg for tungt på lyd
slik at jeg kan fullføre det uten lyd.

| Akseptansekriterier | Arbeidsoppgaver |
| :------------------ | :-------------- |
| Gitt at jeg vil skille mellom to fiender, vil jeg kunne gjøre dette basert på kriterier som ikke bare er audio | ✅ Forsikre oss om at spillet kan nytes uten lyd |

## Som person som hører på musikk mens jeg spiller:
Ønsker jeg mulighet til å skru av spillets lyd.

| Akseptansekriterier | Arbeidsoppgaver                                                         |
| :------------------ |:------------------------------------------------------------------------|
| Gitt at jeg vil skru av spillets lyd og/eller, vil jeg kunne gjøre dette | ✅ Legge til en mulighet for å justere spillets lyd i meny-/pauseskjermen |

## Som barn eller forelder til spillende barn:
Ønsker jeg en aldersgrense og at spillet har motiver og mekanismer som egner seg for alderen slik at barnet ikke blir eksponert for traumatiserende innhold.

| Akseptansekriterier | Arbeidsoppgaver |
| :------------------ | :-------------- |
| Gitt at jeg leser om spillet før jeg spiller, vil jeg få informasjon om anbefalt aldersgrense | ✅ Finne en passende aldersgrense for spillet <br> ✅ Dokumentere aldersgrensen i README.md |

# Produkt og kode
Etter tilbakemelding fra oblig 3, har vi nå fjernet ubrukt skjelettkode og lagt inn MVP-kravene, brukerhistoriene og akseptansekriteriene i dette dokumentet. Vi jobber videre med tester og dokumentasjon av kode.

Siden oblig 3 har vi forbedret kodekvaliteten på følgende måte:

* Analysert koden gjennom Intellij sitt analyseverktøy. Mesteparten av de rapporterte feil/forslag har blitt rettet opp.
* Analysert koden gjennom Spotbugs. Deler av disse rapporterte bugs har blitt addresert.
* Analysert koden gjennom Intellij sin profiler for å finne ytelses-flaskehalser.

Prosjektet kan bygges, testes og kjøres i Linux, Windows og OS X.

# Notis

Endringer siden fredag 05.05:

* Møtereferater er lagt til.
* Test coverage har økt fra 65% til 75%.
* Mer dokumentasjon av klasser og tester har blitt lagt til.
* Bossen spawner ikke før døren til området er lukket.
* Bug ved muting av lyd er fikset.
